# SmartPlan App

## Development Process

- Run on Android with live reload

```console
ionic capacitor run android --livereload --address=IP
```

- Run on iOS with live reload

```console
ionic capacitor run ios --livereload --address=IP
```

## Build Process

- Run all tests

```console
npm run test
npm run test:watch
```

- Make sure the code for `capacitor-image-cache` is according to this [Pull Request](https://github.com/triniwiz/capacitor-image-cache/pull/4/commits/e57e7e8ef4f8d305a67f10b97a72596e538e77d0)

- Build the new version and sync with `Android` & `iOS`

```console
npm run update-mobile
```

- Open the Android Studio project and generate APK as usual

```console
npx cap open android
```

- Open the xCode project and generate IPA as usual

```console
npx cap open ios
```


## Build process (detailed)

### Steps

- bump version code and version number in Android bundle, XCode, `package.json` and `src/config/app.js`
- comment/uncomment `URLS` in file `src/config/api.js` to `QA` to first generate the staging builds
- `npm run update-mobile`
- generate staging APK (Android Studio) and IPA (XCode)
- comment/uncomment `URLS` in file `src/config/api.js` to `PROD` to generate the production builds
- `npm run update-mobile`
- generate production APK and IPA
- upload APKs and IPAs with FileZilla to Apps-cloud
- generate production App Bundle (Android Studio)
- upload iOS production build to iTunes (XCode)
- create new release in Playstore and upload production Android App Bundle
- create new release in iTunes Connect and choose the production build previously submited

### Generate APK in Android Studio

- Build -> Generate Signed Bundle / APK
- APK
  - keystore: `smartplan_key.jks`
  - keystore password: `solinca`
  - keystore alias: `smartplan`
  - keystore passphrase: `solinca`
  - build type: `release`
  - signature versions: `V1` & `V2`

### Generate App Bundle in Android Studio

- Build -> Generate Signed Bundle / APK
- Android App Bundle
  - keystore: `smartplan_key.jks`
  - keystore password: `solinca`
  - keystore alias: `smartplan`
  - keystore passphrase: `solinca`
  - build type: `release`

### Generate build in XCode

- Product -> Archive
- Window -> Organizer
- Distribute App
- Development
- Add Apps-cloud links for staging
  - `https://apps-cloud.net/solinca/teste-light/qa/Solinca.ipa`
  - `https://apps-cloud.net/solinca/teste-light/qa/57.png`
  - `https://apps-cloud.net/solinca/teste-light/qa/512.png`
- and production environments
  - `https://apps-cloud.net/solinca/teste-light/prod/Solinca.ipa`
  - `https://apps-cloud.net/solinca/teste-light/prod/57.png`
  - `https://apps-cloud.net/solinca/teste-light/prod/512.png`
- Automatically manage signing
- Export folder

### Upload build in XCode

- Window -> Organizer
- Distribute App
- App Store Connect
- Upload
- Automatically manage signing
- Upload

---

## Tech Stack

- [Ionic](https://ionicframework.com/)
- [Capacitor](https://capacitor.ionicframework.com/)
- [React Router](https://reacttraining.com/react-router/)
- [React](https://reactjs.org/)
- [Redux](https://redux.js.org/)
- [Redux Persist](https://github.com/rt2zz/redux-persist)
- [Redux Thunk](https://github.com/reduxjs/redux-thunk)
- [Reselect](https://github.com/reduxjs/reselect)
- [Axios](https://github.com/axios/axios)
- [Storybook](https://storybook.js.org/)
- [SASS](https://sass-lang.com/)
- [ESLint](https://eslint.org/)
- [Firebase Analytics](https://ionicframework.com/docs/native/firebase-analytics)
- [React-i18next](https://github.com/i18next/react-i18next)
- [React-placeholder](https://github.com/buildo/react-placeholder)
- [Jest](https://jestjs.io/)
- [Moment](https://momentjs.com/)
- [Lodash](https://lodash.com/)

---

## Routes

### Generic

- /
- /home
- /welcome
- /terms-of-service

### Register / Login / Onboarding

- /enter
- /login
- /register
- /lead
- /recover-password
- /set-password
- /token
- /on-boarding

### Query e Classes Apointements

- /classes
- /classes/next
- /classes/for-me
- /classes/modalities
- /classes/modalities/:id
- /classes/:id
- /classes/become-premium
- /locations

### User Profile

- /profile
- /profile/next
- /profile/favorites
- /profile/info
- /profile/info/edit/:type
- /profile/info/documents
- /profile/notifications
- /profile/data

### Training Plan

- /training-plan/
- /training-plan/welcome
- /training-plan/questionnaire
- /training-plan/select/:type
- /training-plan/goal
- /training-plan/classes
- /training-plan/information
- /training-plan/confirmation

### Support / Talk with us

- /support

### About Solinca

- /about/blog
- /about/blog/:id (?)
- /about/locations
- /about/locations/:id
- /about/support

---

## Redux

### App

- language
- global settings
  `what should we store as global settings?`

### Alerts

- visible
- alert
  - title
  - text
  - icon
  - cta
    `call to action - this will be the button(s) - needs better planning`

### Authentication

- status
  - logged in
  - logged out
  - active subscription (?)
  - registering
  - not registered
  - setting token
  - setting password
- isLoggedIn
- isLoginin
- isRegistering
- isValidatingToken
- isSettingPassword
- error
- email
- password
- token

### Classes

- isFetching
- isBooking
- isCanceling
- isAddingFavorites
- isRemovingFavorites
- error
- lastUpdate
- data (classes)
  - gym
  - date
  - time
  - available
  - vacancies
  - booked
  - alert
  - modality
  - favorite
  - scheduled
- favorites
  - modality
  - hour
- booked
  - classId

#### Locations

- isFetching
- error
- lastUpdate
- data (gyms)

### Modalities

- isFetching
- error
- lastUpdate
- data (modalities)
  - id
  - name

### Toasts

- visible
- toast
  - title
  - text
  - icon
  - type
    `success, warning, error`

### User

- email
- name
- photo
- phone
- address
- member number
- birthdate
- nif
- location
- premium
- biometric data  
  `should this be in the training plan module?`

#### Training Plan

- questionnaire
- level
  - new user |
  - new user ||
  - experienced user
- goal
  - generic
  - smart weight
  - smart fit
  - smart age
  - smart balance
- biometric data  
  `should this be in the user module?`
- classes
- exercises
- ?

#### Support

- reason
- name
- email
- member number
- message

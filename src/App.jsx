import React from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter } from 'react-router-dom';
import { IonApp } from '@ionic/react';
import AppContainer from './containers/general/AppContainer';
import './general.scss';

function App({ rehydrated }) {
  return (
    <IonApp>
      <BrowserRouter>
        <AppContainer rehydrated={rehydrated} />
      </BrowserRouter>
    </IonApp>
  );
}

App.propTypes = {
  rehydrated: PropTypes.bool.isRequired
};

export default App;

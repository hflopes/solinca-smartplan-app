import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { defineCustomElements } from '@ionic/pwa-elements/loader';
import smoothscroll from 'smoothscroll-polyfill';

import App from './App';
import store, { persistor } from './redux/store';
import './config/i18n';
import './index.css';

import '@material/react-tab-bar/dist/tab-bar.css';
import '@material/react-tab-scroller/dist/tab-scroller.css';
import '@material/react-tab/dist/tab.css';
import '@material/react-tab-indicator/dist/tab-indicator.css';
import '@ionic/core/css/core.css';
import '@ionic/core/css/ionic.bundle.css';

require('intersection-observer');

const rootElement = document.getElementById('root');

ReactDOM.render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      {rehydrated => <App rehydrated={rehydrated} />}
    </PersistGate>
  </Provider>,
  rootElement
);

defineCustomElements(window);
smoothscroll.polyfill();

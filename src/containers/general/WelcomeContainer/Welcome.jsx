import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { Link } from 'react-router-dom';
import { withTranslation } from 'react-i18next';
import PrimaryButton from '../../../components/buttons/PrimaryButton';
import withMenuNavigation from '../../../components/hoc/withMenuNavigation';
import AuthHeader from '../../../components/headers/AuthHeader';
import PageTitle from '../../../components/PageTitle';
import './Welcome.scss';

function Welcome({ t }) {
  return (
    <div className="welcome-page__container auth-page">
      <div id="notch" />
      <div className="welcome-page__header">
        <AuthHeader />
      </div>
      <div className="welcome-page__content">
        <div className="page-top">
          <PageTitle
            text={t('screens.Welcome.header')}
            textHighlight={t('screens.Welcome.headerBold')}
            alignment="center"
          />
          <p className="page-subtitle">{t('screens.Welcome.tag')}</p>
        </div>
        <div className="page-bottom">
          <PrimaryButton text={t('global.actions.enter')} link="/login" />
          <Link className="underline-btn" to="/register">
            {t('global.actions.register')}
          </Link>
        </div>
      </div>
    </div>
  );
}

Welcome.propTypes = {
  t: PropTypes.func.isRequired
};

export default compose(withTranslation(), withMenuNavigation())(Welcome);

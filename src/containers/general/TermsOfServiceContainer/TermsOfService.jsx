import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { Redirect, Link } from 'react-router-dom';
import _ from 'lodash';
import { withTranslation } from 'react-i18next';
import { IonGrid, IonRow } from '@ionic/react';
import PrimaryButton from '../../../components/buttons/PrimaryButton';
import Checkbox from '../../../components/Checkbox';
import { getTOS } from '../../../redux/selectors/terms';
import acceptTermsAction from '../../../redux/modules/terms/actions/accept';
import { removeAllLoading } from '../../../redux/modules/app/actions/toggleLoading';
import './TermsOfService.scss';

class TermsOfService extends Component {
  constructor(props) {
    super(props);

    let checked = [];
    const { authorizeText } = props;
    if (authorizeText && authorizeText.length) {
      checked = authorizeText.map(() => false);
    }
    this.state = {
      checked
    };
  }

  componentDidMount() {
    const { clearLoading } = this.props;
    // IonLoading has a bug where if it's dismissed too soon,
    // it doesnt get unrendered, so we have to delay its dismissing
    setTimeout(clearLoading, 1000);
  }

  componentWillReceiveProps(nextProps) {
    const { hasFetched, authorizeText } = nextProps;
    const { checked } = this.state;

    if (hasFetched && (!checked.length || checked.length !== authorizeText.length)) {
      this.setState({ checked: authorizeText.map(() => false) });
    }
  }

  onCheckToggle = index => {
    const { checked } = this.state;

    this.setState({
      checked: checked.map((item, i) => (i === index ? !item : item))
    });
  };

  submitTOS = () => {
    const { id, acceptTerms } = this.props;
    acceptTerms(id);
  };

  render() {
    const { checked } = this.state;
    const { authorizeText, title, isFetching, hasFetched, t, isAccepting, files, hasAccepted } = this.props;

    if (hasAccepted) {
      return <Redirect to="/login-redirect" />;
    }

    if (isFetching || !hasFetched) return null;
    const submitButtonDisabled = checked.length !== _.compact(checked).length;

    return (
      <div className="tos__container">
        <IonGrid>
          <IonRow>
            <h3 className="tos__title">{title}</h3>
          </IonRow>
          <IonRow>
            {files.length > 0 && (
              <ul className="tos__files">
                {files.map(file => (
                  <Link
                    key={file.url}
                    to={{
                      pathname: '/terms',
                      state: {
                        ...file
                      }
                    }}
                  >
                    <li key={file.url} className="tos__file-item">
                      {file.title}
                    </li>
                  </Link>
                ))}
              </ul>
            )}

            {authorizeText.map((text, index) => (
              <Checkbox
                key={text}
                onToggle={() => this.onCheckToggle(index)}
                checked={checked.length ? checked[index] : false}
                label={text}
              />
            ))}
          </IonRow>
          <IonRow>
            <PrimaryButton
              text={t('global.actions.confirm')}
              isDisabled={submitButtonDisabled}
              onClick={this.submitTOS}
              isLoading={isAccepting}
            />
          </IonRow>
        </IonGrid>
      </div>
    );
  }
}

TermsOfService.defaultProps = {
  files: [],
  authorizeText: [],
  id: -1,
  title: ''
};

TermsOfService.propTypes = {
  t: PropTypes.func.isRequired,
  clearLoading: PropTypes.func.isRequired,
  files: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string,
      url: PropTypes.string
    })
  ),
  acceptTerms: PropTypes.func.isRequired,
  hasAccepted: PropTypes.bool.isRequired,
  hasFetched: PropTypes.bool.isRequired,
  isFetching: PropTypes.bool.isRequired,
  isAccepting: PropTypes.bool.isRequired,
  authorizeText: PropTypes.arrayOf(PropTypes.string),
  id: PropTypes.number,
  title: PropTypes.string
};

const mapStateToProps = state => {
  const tosData = getTOS(state);
  const { isFetching, hasFetched, isAccepting, hasAccepted } = state.terms;
  return { ...tosData, isFetching, hasFetched, isAccepting, hasAccepted };
};

const mapDispatchToProps = {
  acceptTerms: acceptTermsAction,
  clearLoading: removeAllLoading
};

export default compose(withTranslation(), connect(mapStateToProps, mapDispatchToProps))(TermsOfService);

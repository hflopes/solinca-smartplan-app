import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';
import { IonGrid, IonRow, IonCol } from '@ionic/react';
import _ from 'lodash';
import SwipeableViews from 'react-swipeable-views';
import Image from '../../../components/Image';
import './OnBoarding.scss';
import { closeOnboarding } from '../../../redux/modules/auth/actions/login';

const SLIDES_COUNT = 3;
const DEGREES_PER_SLIDE = 360 / SLIDES_COUNT;

function OnBoarding() {
  const dispatch = useDispatch();
  const [swipeIndex, setSwipeIndex] = useState(0);
  const [skipPage, setSkipPage] = useState(false);
  const { t } = useTranslation();

  const slides = t('screens.OnBoarding.slides', { returnObjects: true });

  const rotationDegree = swipeIndex * DEGREES_PER_SLIDE;

  function skipSlide() {
    const newSwipeIndex = swipeIndex + 1;
    if (newSwipeIndex >= SLIDES_COUNT) {
      finishOnboarding();
      return;
    }

    setSwipeIndex(newSwipeIndex);
  }

  const rotationStyle = {
    transform: `rotate(${rotationDegree}deg)`
  };

  const invertedRotationStyle = {
    transform: `rotate(${-rotationDegree}deg)`
  };

  const finishOnboarding = () => {
    dispatch(closeOnboarding());
    setSkipPage(true);
  };

  if (skipPage) {
    return <Redirect to="/home" />;
  }

  return (
    <div className="on-boarding-container">
      <IonGrid>
        <IonRow className="logo-header">
          <IonCol>
            <IonRow>
              <div />
            </IonRow>
            <IonRow>
              <Image src="/img/logo_new_dark.svg" alt="" />
            </IonRow>
          </IonCol>
        </IonRow>
        <IonRow>
          <SwipeableViews index={swipeIndex} onChangeIndex={setSwipeIndex}>
            {slides.map((slide, i) => (
              <IonCol key={slide.text} className="swipe-page">
                <IonRow>
                  <Image src={`/img/backgrounds/on_boarding_${i + 1}.jpg`} alt="" />
                </IonRow>
                <IonRow>
                  <p>{slide.text}</p>
                </IonRow>
              </IonCol>
            ))}
          </SwipeableViews>
        </IonRow>
        <IonRow className="swiper-bottom-bar">
          <IonCol>
            <button onClick={finishOnboarding} type="button">
              Skip
            </button>
          </IonCol>
          <IonCol>
            <div className="swiper-bullets">
              {_.times(SLIDES_COUNT, i => (
                <div
                  key={`swipe-bullet-${i}`}
                  className={`bullet ${swipeIndex === i ? 'highlighted' : ''}`}
                />
              ))}
            </div>
          </IonCol>
          <IonCol>
            <button type="button" onClick={skipSlide}>
              <Image src="/img/icons/go_pink.svg" alt="->" />
            </button>
          </IonCol>
        </IonRow>
      </IonGrid>

      <div className="gray-circle" style={invertedRotationStyle} />
      <div className="pink-circle" style={rotationStyle} />

      <div className="title-overlay">
        <SwipeableViews index={swipeIndex}>
          {slides.map((slide, i) => (
            <div key={slide.title}>
              <h1>{slide.title}</h1>
            </div>
          ))}
        </SwipeableViews>
      </div>
    </div>
  );
}

export default OnBoarding;

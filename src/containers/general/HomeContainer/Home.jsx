import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';
import { compose } from 'redux';
import { connect } from 'react-redux';
import _ from 'lodash';
import { withTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
import { IonGrid, IonRow, IonCol, IonList } from '@ionic/react';
import Avatar from '../../../components/Avatar';
import LazyAvatar from '../../../components/loading/LazyAvatar';
import ClassCard from '../../../components/cards/ClassCard';
import PlanStartCard from '../../../components/cards/PlanStartCard';
import NewComplaintCard from '../../../components/cards/NewComplaintCard';
// import NewStatusCard from '../../../components/cards/NewStatusCard';
import NewRatingCard from '../../../components/cards/NewRatingCard';
import ProgressCard from '../../../components/cards/ProgressCard';
import BiometricCard from '../../../components/cards/BiometricCard';
import ObjectiveLabel from '../../../components/ObjectiveLabel';
import { getMonth, getDayOfWeek } from '../../../utils/dates';
import { getBaseGym, getGyms } from '../../../redux/selectors/gyms';
import { getUserFirstName, isFetchingUser, userUpgradedStatus } from '../../../redux/selectors/user';
import { getActivityCount } from '../../../redux/selectors/dashboard';
import {
  getBookedClasses,
  getTodayRecommendedClasses,
  getReviewClasses
} from '../../../redux/selectors/classes';
import {
  getBiometricData,
  getBiometricDateGap,
  getObjective,
  getGoalPlans,
  getGoalClasses
} from '../../../redux/selectors/plan';
import reloadAction from '../../../redux/reload/action';
import withRouteState from '../../../components/hoc/withRouteState';
import withMenuNavigation from '../../../components/hoc/withMenuNavigation';
import './Home.scss';
import { getHighlightedChallenges } from '../../../redux/selectors/challenges';
// import ChallengeHighlightedCard from '../../../components/cards/ChallengeHighlightedCard';
import { getUnreadComplaints } from '../../../redux/selectors/complaints';

// Format a list of classes into the following format:
// {
//    82: {gym: {..gymData}, classes: [...]}
//    17: {gym: {..gymData}, classes: [...]}
// }
function transform(classes, gyms) {
  const values = Object.values(classes);
  const obj = {};

  values.forEach(e => {
    if (obj[e.gymId]) {
      obj[e.gymId].classes.push(e);
    } else {
      obj[e.gymId] = {
        gym: gyms[e.gymId],
        classes: [e]
      };
    }
  });

  return obj;
}

class HomeContainer extends Component {
  componentWillMount() {
    const { reload } = this.props;
    reload('bookedClasses');
  }

  componentDidMount() {
    const { lastState } = this.props;
    if (!lastState) {
      return;
    }

    const { contentScrollY } = lastState;

    if (contentScrollY) {
      const element = document.getElementById('home-scroll');
      setTimeout(() => {
        element.scrollTo({ top: contentScrollY, left: 0, behaviour: 'smooth' });
      }, 500);
    }
  }

  componentWillUnmount() {
    const { saveState } = this.props;
    const element = document.getElementById('home-scroll');
    const contentScrollY = element.scrollTop;

    const currentState = { contentScrollY };
    saveState(currentState);
  }

  onScroll = () => {
    const { scrollTop } = document.getElementById('home-scroll');
    const scrollPercent = Math.min(1, scrollTop / 40);

    const header = document.getElementById('header-wrapper');
    const nav = document.getElementById('hamburger-nav');

    header.style.opacity = 1 - scrollPercent;
    nav.style.opacity = 1 - scrollPercent;
  };

  render() {
    const currentDate = new Date();
    const dayOfWeek = getDayOfWeek(currentDate.getDay());
    const day = currentDate.getDate();
    const month = getMonth(currentDate.getMonth());

    const {
      baseGymName,
      firstname,
      bookedClasses,
      recommendedClasses,
      t,
      isLoadingUser,
      biometricData,
      recommendedCount,
      lesMillsCount,
      otherCount,
      planCount,
      // biometricDateGap,
      // highlightedChallenges,
      reviewClasses,
      unreadComplaints,
      // upgradedStatus,
      objective,
      plansGoal,
      classesGoal
    } = this.props;

    const currentWeight = biometricData && biometricData.Peso && parseInt(biometricData.Peso, 10);
    const currentImc = biometricData && biometricData.IMC && parseFloat(biometricData.IMC, 10);
    const currentFat =
      biometricData && biometricData['% Massa Gorda'] && parseFloat(biometricData['% Massa Gorda'], 10);

    const imcInt = Math.floor(currentImc);
    const imcDecimal = Math.round((currentImc - imcInt) * 10);

    const fatInt = Math.floor(currentFat);
    const fatDecimal = Math.round((currentFat - fatInt) * 10);

    const gapString = t('screens.Home.activityGap', {
      day: moment()
        .startOf('month')
        .toDate()
        .getDate(),
      month: getMonth(
        moment()
          .startOf('month')
          .toDate()
          .getMonth()
      )
    });

    return (
      <div id="home-scroll" className="home-container full-height-flex" onScroll={this.onScroll}>
        <IonGrid>
          <IonRow className="header">
            <div id="header-wrapper">
              <IonCol>
                <Link to="/profile">{isLoadingUser ? <LazyAvatar size={42} /> : <Avatar />}</Link>
              </IonCol>
            </div>
          </IonRow>
          <IonRow className="row first-row">
            <h1 className="intro-header">
              {t('screens.Home.greeting')}
              <b>{`${firstname}`}</b>
            </h1>
          </IonRow>
          <IonRow className="row">
            <b className="gym-label">{baseGymName}</b>
            <span className="date-label">{t('screens.Classes.headerDate', { dayOfWeek, day, month })}</span>
          </IonRow>

          {objective && (
            <IonRow className="row">
              <span className="objective">{t('screens.Home.objective')}</span>
              <ObjectiveLabel />
            </IonRow>
          )}

          {reviewClasses.map(classData => {
            const {
              id,
              date,
              modality: { name }
            } = classData;
            const classDay = moment(date).format('DD/MM');
            const classHour = moment(date).format('HH:mm');
            return (
              <IonRow key={id} className="row">
                <NewRatingCard id={id} day={classDay} hour={classHour} name={name} classData={classData} />
              </IonRow>
            );
          })}

          {/* {upgradedStatus && (
            <IonRow className="row">
              <NewStatusCard />
            </IonRow>
          )} */}

          {unreadComplaints.map(complaint => (
            <IonRow key={complaint.id} className="row">
              <NewComplaintCard {...complaint} />
            </IonRow>
          ))}

          {_.map(bookedClasses, ({ gym, classes }) => (
            <IonRow key={`gym-${gym.id}-booked`} className="class-list">
              <b className="list-title">{t('screens.Home.classes.booked', { gym: gym.name })}</b>
              <IonList>
                {classes.map(c => (
                  <ClassCard key={`booked-${c.id}`} classData={c} showDate />
                ))}
              </IonList>
            </IonRow>
          ))}

          <IonRow className="row">
            <PlanStartCard />
          </IonRow>

          {_.map(recommendedClasses, ({ gym, classes }) => (
            <IonRow key={`gym-${gym.id}-recommended`} className="class-list">
              <b className="list-title">{t('screens.Home.classes.recommended', { gym: gym.name })}</b>
              <IonList>
                {classes.map(c => (
                  <ClassCard key={`recommended-${c.id}`} classData={c} />
                ))}
              </IonList>
            </IonRow>
          ))}

          {/* {highlightedChallenges.length ? (
            <IonRow className="row">
              <b className="list-title -challenges">{t('screens.Home.challenges')}</b>
              <div className="challenges__container">
                <ul className="challenges__list">
                  {highlightedChallenges.map(challenge => (
                    <li key={challenge.id} className="challenges__list-item">
                      <ChallengeHighlightedCard challenge={challenge} />
                    </li>
                  ))}
                </ul>
              </div>
            </IonRow>
          ) : null} */}

          {gapString && (
            <IonRow>
              <b className="list-title">{`${gapString}`}</b>
              <div className="dashboard-progress">
                <ProgressCard type="plans" value={planCount} goal={plansGoal} />
                <ProgressCard type="recommendedClasses" value={recommendedCount} goal={classesGoal} />
                <ProgressCard type="lesMillsClasses" value={lesMillsCount} />
                <ProgressCard type="otherClasses" value={otherCount} />
              </div>
              <div className="more-link">
                <Link to="/dashboard/calendar">{t('screens.Home.seeMore')}</Link>
              </div>
            </IonRow>
          )}

          {currentWeight && currentImc && currentFat && (
            <IonRow>
              <b className="list-title">{t('screens.Home.metrics')}</b>
              <div className="dashboard-metrics">
                <Link to="/dashboard/evolution/weight">
                  <BiometricCard
                    label={t('screens.Home.weight')}
                    valuePrimary={currentWeight}
                    valueSecondary={t('screens.Home.weightMetric')}
                  />
                </Link>
                <Link to="/dashboard/evolution/imc">
                  <BiometricCard
                    label={t('screens.Home.imc')}
                    valuePrimary={imcInt}
                    valueSecondary={`,${imcDecimal}`}
                  />
                </Link>
                <Link to="/dashboard/evolution/fat">
                  <BiometricCard
                    label={t('screens.Home.fat')}
                    valuePrimary={fatInt}
                    valueSecondary={`,${fatDecimal} %`}
                  />
                </Link>
              </div>
              <div className="more-link">
                <Link to="/dashboard/evolution/weight">{t('screens.Home.seeMore')}</Link>
              </div>
            </IonRow>
          )}
        </IonGrid>
      </div>
    );
  }
}

HomeContainer.defaultProps = {
  objective: ''
};

HomeContainer.propTypes = {
  upgradedStatus: PropTypes.bool.isRequired,
  saveState: PropTypes.func.isRequired,
  t: PropTypes.func.isRequired,
  firstname: PropTypes.string.isRequired,
  baseGymName: PropTypes.string.isRequired,
  lastState: PropTypes.shape({
    selectedSection: PropTypes.number,
    contentScrollY: PropTypes.number
  }).isRequired,
  unreadComplaints: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  reviewClasses: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  bookedClasses: PropTypes.shape({}).isRequired,
  recommendedClasses: PropTypes.shape({}).isRequired,
  isLoadingUser: PropTypes.bool.isRequired,
  biometricData: PropTypes.shape({
    '% Massa Gorda': PropTypes.string,
    IMC: PropTypes.string,
    Peso: PropTypes.string
  }).isRequired,
  recommendedCount: PropTypes.number.isRequired,
  lesMillsCount: PropTypes.number.isRequired,
  otherCount: PropTypes.number.isRequired,
  planCount: PropTypes.number.isRequired,
  biometricDateGap: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  highlightedChallenges: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  objective: PropTypes.string,
  plansGoal: PropTypes.number.isRequired,
  classesGoal: PropTypes.number.isRequired
};

const mapStateToProps = state => {
  const upgradedStatus = userUpgradedStatus(state);
  const baseGym = getBaseGym(state);
  const baseGymName = baseGym ? baseGym.name : '';
  const firstname = getUserFirstName(state);
  const gyms = getGyms(state);
  const unreadComplaints = getUnreadComplaints(state);
  const bookedClasses = transform(getBookedClasses(state), gyms);
  const reviewClasses = getReviewClasses(state);
  const recommendedClasses = transform(getTodayRecommendedClasses(state), gyms);
  const isLoadingUser = isFetchingUser(state);
  const biometricData = getBiometricData(state);
  const biometricDateGap = getBiometricDateGap(state);
  const highlightedChallenges = getHighlightedChallenges(state);
  const objective = getObjective(state);
  const start = moment()
    .startOf('month')
    .startOf('day')
    .toDate();
  const end = moment()
    .endOf('month')
    .endOf('day')
    .toDate();
  const recommendedCount = getActivityCount(state, start, end, 'recommendedClass');
  const lesMillsCount = getActivityCount(state, start, end, 'lesMillsClass');
  const otherCount = getActivityCount(state, start, end, 'otherClass');
  const planCount = getActivityCount(state, start, end, 'plan');
  const plansGoal = getGoalPlans(state);
  const classesGoal = getGoalClasses(state);

  return {
    upgradedStatus,
    baseGymName,
    firstname,
    reviewClasses,
    unreadComplaints,
    bookedClasses,
    recommendedClasses,
    isLoadingUser,
    biometricData,
    recommendedCount,
    lesMillsCount,
    otherCount,
    planCount,
    biometricDateGap,
    highlightedChallenges,
    objective,
    plansGoal,
    classesGoal
  };
};

const mapDispatchToProps = {
  reload: reloadAction
};

export default compose(
  withTranslation(),
  connect(mapStateToProps, mapDispatchToProps),
  withMenuNavigation(),
  withRouteState('Home')
)(HomeContainer);

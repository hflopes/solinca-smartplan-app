import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Image from '../../../components/Image';
import hideAction from '../../../redux/modules/toasts/actions/hide';
import './Toast.scss';
import { getToast, isToastVisible } from '../../../redux/selectors/toasts';

const DEFAULT_DURATION = 3000;

class Toast extends Component {
  componentWillReceiveProps(nextProps) {
    const { hide, visible } = this.props;

    if (nextProps.visible && !visible) {
      const { duration } = nextProps.toast;

      setTimeout(() => {
        hide();
      }, duration || DEFAULT_DURATION);
    }
  }

  getIcon = type => {
    if (!type) {
      return 'clear';
    }

    switch (type) {
      case 'success':
        return 'check_black';
      case 'error':
        return 'clear';
      default:
        return type;
    }
  };

  render() {
    const { visible, toast } = this.props;
    const { type, title, message } = toast;

    const toastClass = `toast ${type} ${visible ? 'showing' : ''}`;
    const toastIcon = `/img/icons/${this.getIcon(type)}.svg`;

    return (
      <>
        <div className={toastClass}>
          <Image src={toastIcon} alt="" />
          <div>
            <b>{title}</b>
            {message && <p>{message}</p>}
          </div>
        </div>
      </>
    );
  }
}

Toast.propTypes = {
  hide: PropTypes.func.isRequired,
  visible: PropTypes.bool.isRequired,
  toast: PropTypes.shape({
    type: PropTypes.string,
    title: PropTypes.string,
    message: PropTypes.string,
    duration: PropTypes.number
  }).isRequired
};

const mapStateToProps = state => {
  const visible = isToastVisible(state);
  const toast = getToast(state);
  return {
    visible,
    toast
  };
};

const mapDispatchToProps = {
  hide: hideAction
};

export default connect(mapStateToProps, mapDispatchToProps)(Toast);

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { Route, withRouter, Redirect } from 'react-router-dom';
import { hasFetchedInitialData, isAppOffline } from '../../redux/selectors/app';
import { userIsSyncing, userIsLoggedIn } from '../../redux/selectors/auth';

function PrivateRoute({ offline, fetchedInitialData, isSyncing, isLoggedIn, ...rest }) {
  if (isLoggedIn) {
    return <Route {...rest} />;
  }

  return <Redirect to="/logout" />;
}

PrivateRoute.propTypes = {
  offline: PropTypes.bool.isRequired,
  isSyncing: PropTypes.bool.isRequired,
  isLoggedIn: PropTypes.bool.isRequired,
  fetchedInitialData: PropTypes.bool.isRequired
};

const mapStateToProps = state => {
  const isSyncing = userIsSyncing(state);
  const isLoggedIn = userIsLoggedIn(state);
  const offline = isAppOffline(state);
  const fetchedInitialData = hasFetchedInitialData(state);
  return {
    isSyncing,
    isLoggedIn,
    offline,
    fetchedInitialData
  };
};

export default compose(withRouter, connect(mapStateToProps))(PrivateRoute);

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import './Snackbar.scss';
import { isSnackbarVisible, getSnackbar } from '../../../redux/selectors/snackbars';

const Snackbar = ({ visible, snackbar: { message } }) => {
  if (!visible) return null;

  return (
    <>
      <div className="snackbar">
        <p>{message}</p>
      </div>
    </>
  );
};

Snackbar.propTypes = {
  visible: PropTypes.bool.isRequired,
  snackbar: PropTypes.shape({
    message: PropTypes.string
  }).isRequired
};

const mapStateToProps = state => {
  const visible = isSnackbarVisible(state);
  const snackbar = getSnackbar(state);
  return {
    visible,
    snackbar
  };
};

export default connect(mapStateToProps)(Snackbar);

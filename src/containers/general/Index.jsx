import React from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { isAppOffline } from '../../redux/selectors/app';
import { userIsLoggedIn, userHasSynced } from '../../redux/selectors/auth';

const IndexContainer = ({ isLoggedIn, hasSynced, offline }) => {
  if ((offline && isLoggedIn) || (isLoggedIn && hasSynced)) {
    return <Redirect to="/login-redirect" />;
  }

  return <Redirect to="/logout" />;
};

IndexContainer.propTypes = {
  isLoggedIn: PropTypes.bool.isRequired,
  hasSynced: PropTypes.bool.isRequired,
  offline: PropTypes.bool.isRequired
};

const mapStateToProps = state => {
  const isLoggedIn = userIsLoggedIn(state);
  const hasSynced = userHasSynced(state);
  const offline = isAppOffline(state);
  return {
    isLoggedIn,
    hasSynced,
    offline
  };
};

export default connect(mapStateToProps)(IndexContainer);

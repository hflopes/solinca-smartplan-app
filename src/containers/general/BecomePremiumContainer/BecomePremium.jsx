import React from 'react';
import { withTranslation } from 'react-i18next';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import Image from '../../../components/Image';
import LayeredPage from '../../../components/LayeredPage';
import withBackNavigation from '../../../components/hoc/withBackNavigation';
import './BecomePremium.scss';

function BecomePremium({ t }) {
  const advantages = t(`screens.BecomePremium.advantages`, { returnObjects: true });

  const background = (
    <>
      <div className="premium__header">
        <div className="header__wrapper overlay">
          <Image src="/img/big_icons/premium.svg" alt="" />
          <h1>{t('global.terms.premium')}</h1>
        </div>
      </div>
    </>
  );

  const foreground = (
    <>
      <div className="premium__content">
        <h2>{t('screens.BecomePremium.advantagesTitle')}</h2>
        {advantages.map(({ title, description }) => (
          <div key={title} className="adv">
            <div className="badge">
              <Image src="/img/big_icons/premium_gold.svg" alt="" />
            </div>
            <h4>{title}</h4>
            <p>{description}</p>
          </div>
        ))}
      </div>
    </>
  );

  return (
    <LayeredPage
      className="premium__container"
      background={background}
      foreground={foreground}
      gap="45vh"
      extra="10vh"
      rounded
    />
  );
}

BecomePremium.propTypes = {
  t: PropTypes.func.isRequired
};

export default compose(withBackNavigation(), withTranslation())(BecomePremium);

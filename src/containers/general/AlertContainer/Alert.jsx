import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { IonGrid, IonRow } from '@ionic/react';
import ReactHtmlParser from 'react-html-parser';
import Image from '../../../components/Image';
import hideAction from '../../../redux/modules/alerts/actions/hide';
import PrimaryButton from '../../../components/buttons/PrimaryButton';
import './Alert.scss';
import { getAlert, isAlertVisible } from '../../../redux/selectors/alerts';

class Alert extends Component {
  onHide = () => {
    const { hide, alert } = this.props;
    const { onClick } = alert;

    if (onClick) {
      onClick();
    }

    hide();
  };

  renderCloseButton = persist => {
    if (persist) {
      return null;
    }

    return (
      <button type="button" className="close" onClick={this.onHide}>
        <Image src="/img/icons/clear.svg" alt="X" />
      </button>
    );
  };

  renderButton = () => {
    const { alert } = this.props;
    const { redirectUrl, cta, colorScheme } = alert;

    if (redirectUrl) {
      return (
        <PrimaryButton
          text={cta}
          onClick={this.onHide}
          link={redirectUrl}
          colorScheme={colorScheme || undefined}
        />
      );
    }

    return <PrimaryButton text={cta} onClick={this.onHide} colorScheme={colorScheme || undefined} />;
  };

  render() {
    const { visible, alert } = this.props;
    const { title, icon, persist, message, cta } = alert;

    const backgroundOnHide = () => {
      if (!persist) {
        this.onHide();
      }
    };

    return (
      <div
        tabIndex="0"
        role="button"
        className={`alert-bg full-height-flex ${visible ? 'showing' : ''}`}
        onClick={backgroundOnHide}
        onKeyPress={() => {}}
      >
        <div className={`alert page ${persist ? 'persist' : ''}`}>
          {this.renderCloseButton(persist)}
          <IonGrid className="full-height-flex ion-justify-content-around">
            {icon && (
              <IonRow>
                <div className="icon-container">
                  <Image src={`/img/${icon}`} alt="" />
                </div>
              </IonRow>
            )}
            <IonRow>
              <h1>{title}</h1>
              <p>{ReactHtmlParser(message)}</p>
            </IonRow>
            {cta && <IonRow>{this.renderButton()}</IonRow>}
          </IonGrid>
        </div>
      </div>
    );
  }
}

Alert.propTypes = {
  hide: PropTypes.func.isRequired,
  visible: PropTypes.bool.isRequired,
  alert: PropTypes.shape({
    title: PropTypes.string,
    persist: PropTypes.bool,
    message: PropTypes.string,
    icon: PropTypes.string,
    cta: PropTypes.string,
    redirectUrl: PropTypes.string,
    onClick: PropTypes.func,
    colorScheme: PropTypes.shape({})
  }).isRequired
};

const mapStateToProps = state => {
  const visible = isAlertVisible(state);
  const alert = getAlert(state);
  return {
    visible,
    alert
  };
};

const mapDispatchToProps = {
  hide: hideAction
};

export default connect(mapStateToProps, mapDispatchToProps)(Alert);

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { IonLoading } from '@ionic/react';
import { withTranslation } from 'react-i18next';
import { getLoadingModules } from '../../../redux/selectors/app';
import './Loading.scss';

class Loading extends Component {
  state = {
    isOpen: false
  };

  componentWillReceiveProps(nextProps) {
    const { loadingModules } = nextProps;

    this.triggerLoading(loadingModules.length !== 0);
  }

  triggerLoading = isOpen => {
    setTimeout(() => {
      this.setState({ isOpen });
    }, 300); // Values below 300 might not remove the loading component
  };

  render() {
    const { t, loadingModules } = this.props;
    const message = t(`loading.${loadingModules[0] || 'default'}`);

    const { isOpen } = this.state;
    return <IonLoading isOpen={isOpen} message={message} spinner="crescent" mode="ios" cssClass="loading" />;
  }
}

Loading.propTypes = {
  t: PropTypes.func.isRequired,
  loadingModules: PropTypes.arrayOf(PropTypes.any).isRequired
};

const mapStateToProps = state => {
  const loadingModules = getLoadingModules(state);
  return { loadingModules };
};

export default compose(withTranslation(), connect(mapStateToProps))(Loading);

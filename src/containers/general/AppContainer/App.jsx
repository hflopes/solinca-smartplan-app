import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Route, Switch, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { compose } from 'redux';
import _ from 'lodash';
import { Plugins } from '@capacitor/core';
import withNavigationTracker from '../../../components/hoc/withNavigationTracker';
import routes from '../../../config/routes';
import PrivateRoute from '../PrivateRoute';
import toggleKeyboardAction from '../../../redux/modules/device/actions/toggleKeyboard';
import fetchInitialDataAction from '../../../redux/modules/app/actions/fetchInitialData';
import startAppAction from '../../../redux/modules/app/actions/start';
import setPlatformAction from '../../../redux/modules/app/actions/setPlatform';
import toggleConnectedAction from '../../../redux/modules/app/actions/toggleConnected';
import { keyboardSources } from '../../../redux/modules/device/reducer';
import {
  hasFetchedInitialData,
  isAppOffline,
  hasErrorFetchingInitialData
} from '../../../redux/selectors/app';
import isDevelopmentMode from '../../../utils/development';
import LoadingContainer from '../LoadingContainer';
import AlertContainer from '../AlertContainer';
import ToastContainer from '../ToastContainer';
import SnackbarContainer from '../SnackbarContainer';
import AppMenu from '../../../components/navs/AppMenu';
import LocalPush from '../../../utils/push/local';
import OneSignal from '../../../utils/push/onesignal';
import { userIsSyncing } from '../../../redux/selectors/auth';
import { closeMenu } from '../../../utils/navigation';
import './App.scss';

const { Network, Keyboard, App, SplashScreen, Device } = Plugins;
const { handleBackNavigation } = require('../../../utils/navigation');

class AppContainer extends Component {
  state = {
    renderApp: false
  };

  async componentWillMount() {
    this.addNetworkListener();
    if (!process.env.NODE_ENV === 'development') {
      this.addAppStateListener();
    }

    App.addListener('backButton', () => {
      const { history, location } = this.props;
      handleBackNavigation(history, location);
    });

    try {
      const info = await Device.getInfo();
      const { setPlatform } = this.props;
      setPlatform(info.platform);
    } catch (e) {
      console.error(e);
    }

    if (isDevelopmentMode()) {
      return;
    }

    const { toggleKeyboard } = this.props;

    Keyboard.addListener('keyboardDidShow', () => {
      toggleKeyboard({ isKeyboardUp: true, source: keyboardSources.DEVICE_EVENT });
    });

    Keyboard.addListener('keyboardDidHide', () => {
      toggleKeyboard({ isKeyboardUp: false, source: keyboardSources.DEVICE_EVENT });
    });
  }

  componentDidMount() {
    // setup onesignal
    OneSignal();
    // setup listeners for local notifications
    LocalPush();
  }

  componentWillReceiveProps(nextProps) {
    const { rehydrated: wasRehydrating } = this.props;
    const { rehydrated, isSyncing, fetchedInitialData, offline, errorFetchingInitialData } = nextProps;
    if (!wasRehydrating && rehydrated) {
      const { startApp } = this.props;
      startApp();
    }

    const isOnlineAndSynced = fetchedInitialData && !isSyncing;
    const canRenderApp = rehydrated && (offline || isOnlineAndSynced || errorFetchingInitialData);
    if (canRenderApp) {
      this.setState({ renderApp: true }, () => {
        SplashScreen.hide();
      });
    }
  }

  componentDidUpdate(prevProps) {
    const { location: lastLocation } = prevProps;
    const { location } = this.props;
    if (location !== lastLocation) {
      const appContainer = document.getElementById('app');
      if (appContainer) {
        appContainer.scrollTo(0, 0);
      }
    }
  }

  componentWillUnmount() {
    this.removeNetworkListener();
    if (!process.env.NODE_ENV === 'development') {
      this.removeAppStateListener();
    }
  }

  addNetworkListener = () => {
    this.networkListener = Network.addListener('networkStatusChange', status => {
      const { connected } = status;
      const { toggleConnected } = this.props;
      toggleConnected();
      if (connected) {
        const { fetchInitialData, fetchedInitialData } = this.props;
        if (!fetchedInitialData) {
          fetchInitialData();
        }
      }
    });
  };

  removeNetworkListener = () => {
    if (this.networkListener) {
      this.networkListener.remove();
    }
  };

  addAppStateListener = () => {
    this.appStateListener = App.addListener('appStateChange', state => {
      const { isActive } = state;
      if (isActive) {
        const { fetchInitialData } = this.props;
        fetchInitialData();
      }
    });
  };

  removeAppStateListener = () => {
    if (this.appStateListener) {
      this.appStateListener.remove();
    }
  };

  render() {
    const { renderApp } = this.state;

    return (
      <>
        <div id="menu" className="nav-container">
          <AppMenu />
          <button type="button" className="dismiss-area" onClick={closeMenu} />
        </div>
        <div id="app" className="app-container">
          {renderApp ? (
            <Switch>
              {_.map(routes, ({ path, uniqueKey, component, auth }) =>
                auth ? (
                  <PrivateRoute key={path || uniqueKey} exact path={path} component={component} />
                ) : (
                  <Route key={path || uniqueKey} exact path={path} component={component} />
                )
              )}
            </Switch>
          ) : null}
          {/* It will remain with splashcreen until it's ready to load */}
          {/* Here we can add a custom loading animation */}
          <SnackbarContainer />
        </div>
        <LoadingContainer />
        <AlertContainer />
        <ToastContainer />
      </>
    );
  }
}

AppContainer.propTypes = {
  fetchedInitialData: PropTypes.bool.isRequired,
  isSyncing: PropTypes.bool.isRequired,
  offline: PropTypes.bool.isRequired,
  errorFetchingInitialData: PropTypes.bool.isRequired,
  rehydrated: PropTypes.bool.isRequired,
  history: PropTypes.shape({}).isRequired,
  location: PropTypes.shape({}).isRequired,
  toggleKeyboard: PropTypes.func.isRequired,
  fetchInitialData: PropTypes.func.isRequired,
  startApp: PropTypes.func.isRequired,
  toggleConnected: PropTypes.func.isRequired,
  setPlatform: PropTypes.func.isRequired
};

const mapStateToProps = state => {
  const fetchedInitialData = hasFetchedInitialData(state);
  const isSyncing = userIsSyncing(state);
  const offline = isAppOffline(state);
  const errorFetchingInitialData = hasErrorFetchingInitialData(state);
  return { offline, isSyncing, fetchedInitialData, errorFetchingInitialData };
};

const mapDispatchToProps = {
  toggleKeyboard: toggleKeyboardAction,
  fetchInitialData: fetchInitialDataAction,
  startApp: startAppAction,
  toggleConnected: toggleConnectedAction,
  setPlatform: setPlatformAction
};

export default compose(
  withRouter,
  withNavigationTracker,
  connect(mapStateToProps, mapDispatchToProps)
)(AppContainer);

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import _ from 'lodash';
import withRouteState from '../../../components/hoc/withRouteState';
import { getModalitiesGroupByCategory } from '../../../redux/selectors/modalities';
import ModalityCard from '../../../components/cards/ModalityCard';
import './Modalities.scss';

class Modalities extends Component {
  componentDidMount() {
    const { lastState } = this.props;
    if (!lastState) {
      return;
    }
    const { leftScrolls } = lastState;
    if (leftScrolls) {
      _.map(leftScrolls, (leftScroll, index) => {
        this.scrollToSavedPosition(index, leftScroll);
      });
    }
  }

  componentWillUnmount() {
    const { saveState } = this.props;
    const groups = Array.from(document.getElementsByClassName('group__list'));
    const leftScrolls = groups.map(group => group.scrollLeft);
    const currentState = { leftScrolls };
    saveState(currentState);
  }

  scrollToSavedPosition = (index, scroll) => {
    const element = document.getElementsByClassName('group__list')[index];
    if (element) {
      element.scrollTo({ top: 0, left: scroll, behaviour: 'smooth' });
      const { scrollLeft } = element;
      if (scrollLeft < scroll - 50) {
        setTimeout(() => {
          this.scrollToSavedPosition(index, scroll);
        }, 500);
      }
    } else {
      setTimeout(() => {
        this.scrollToSavedPosition(index, scroll);
      }, 500);
    }
  };

  render() {
    const { modalities } = this.props;
    return (
      <div className="modalities__container">
        {_.map(modalities, (list, key) => (
          <div key={key} id={key} className="group__container">
            <b className="group__title">{key}</b>
            <ul className="group__list">
              {list.map(m => (
                <li key={m.id} className="group__list-item">
                  <ModalityCard modality={m} />
                </li>
              ))}
            </ul>
          </div>
        ))}
      </div>
    );
  }
}

Modalities.propTypes = {
  modalities: PropTypes.shape({}).isRequired,
  saveState: PropTypes.func.isRequired,
  lastState: PropTypes.shape({
    leftScrolls: PropTypes.arrayOf(PropTypes.number)
  }).isRequired
};

const mapStateToProps = state => {
  const modalities = getModalitiesGroupByCategory(state);

  return { modalities };
};

export default compose(connect(mapStateToProps), withRouteState('Modalities'))(Modalities);

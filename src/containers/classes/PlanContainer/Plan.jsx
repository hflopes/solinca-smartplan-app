import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import _ from 'lodash';
import { IonList } from '@ionic/react';
import SwipeableViews from 'react-swipeable-views';
import { withTranslation } from 'react-i18next';
import BadgeTabs from '../../../components/tabs/BadgeTabs';
import ClassCard from '../../../components/cards/ClassCard';
import LoadingClassCard from '../../../components/loading/LazyClassCard';
import VisibleElement from '../../../components/VisibleElement';
import { getRecommendedClassesByDays, getClassesDays } from '../../../redux/selectors/classes';
import Empty from '../../../components/Empty';
import withRouteState from '../../../components/hoc/withRouteState';
import './Plan.scss';

const ITEM_LOAD_INCREMENT = 7;
const LOADING_ITEMS = 5;
const PIXELS = 100;

class Plan extends Component {
  constructor(props) {
    super(props);
    const { days, lastState } = props;
    this.state = {
      tabs: days.map(({ name }) => name),
      selectedDayIndex: lastState.selectedDayIndex || 0,
      dayRenderLimits: lastState.dayRenderLimits || Array(7).fill(ITEM_LOAD_INCREMENT)
    };
  }

  componentDidMount() {
    document.getElementById('app').classList.remove('scroll');

    const { lastState } = this.props;

    if (!lastState) {
      return;
    }

    const { contentScrollY } = lastState;

    if (contentScrollY) {
      this.scrollToSavedPosition();
    }
  }

  componentWillUnmount() {
    const { selectedDayIndex, dayRenderLimits } = this.state;
    const { saveState } = this.props;

    const { scrollTop: contentScrollY } = document.querySelector(
      '.react-swipeable-view-container'
    ).firstElementChild;

    const currentState = { selectedDayIndex, contentScrollY, dayRenderLimits };

    saveState(currentState);
  }

  scrollToSavedPosition = () => {
    const { lastState } = this.props;
    const { contentScrollY } = lastState;
    let hasElement = false;
    const parent = document.querySelector('.react-swipeable-view-container');
    if (parent) {
      const element = parent.firstElementChild;
      if (element) {
        element.scrollTo({
          top: contentScrollY,
          left: 0,
          behaviour: 'smooth'
        });
        hasElement = true;
        const { scrollTop } = element;
        if (scrollTop < contentScrollY - PIXELS) {
          setTimeout(() => {
            this.scrollToSavedPosition();
          }, 500);
        }
      }
    }

    if (!hasElement) {
      setTimeout(() => {
        this.scrollToSavedPosition();
      }, 500);
    }
  };

  /**
   * Converts the array index to the
   * day of week index (sunday == 0)
   */
  getDayOfWeekIndex = arrayIndex => {
    const { classes } = this.props;
    return classes[arrayIndex].day.index;
  };

  selectDay = index => {
    const { onDayChanged } = this.props;
    const parent = document.querySelector('.react-swipeable-view-container');
    if (parent) {
      const element = parent.firstElementChild;
      if (element) {
        element.scrollTop = 0;
      }
    }

    this.setState(
      {
        selectedDayIndex: index
      },
      () => {
        onDayChanged(index);
      }
    );
  };

  loadMore = () => {
    const { selectedDayIndex, dayRenderLimits } = this.state;

    const newDayRenderLimits = [...dayRenderLimits];
    newDayRenderLimits[this.getDayOfWeekIndex(selectedDayIndex)] += ITEM_LOAD_INCREMENT;

    this.setState({
      dayRenderLimits: newDayRenderLimits
    });
  };

  render() {
    const { selectedDayIndex, dayRenderLimits, tabs } = this.state;
    const { isLoading, t, classes } = this.props;

    if (isLoading) {
      return (
        <div className="plan-classes-container">
          <div className="tabs">
            <BadgeTabs tabs={tabs} onSelected={this.selectDay} selectedIndex={selectedDayIndex} />
          </div>
          <IonList className="classes-list">
            {_.times(LOADING_ITEMS, index => (
              <LoadingClassCard key={index} />
            ))}
          </IonList>
        </div>
      );
    }

    return (
      <div className="plan-classes-container">
        <div className="tabs">
          <BadgeTabs tabs={tabs} onSelected={this.selectDay} selectedIndex={selectedDayIndex} />
        </div>

        <SwipeableViews className="list" index={selectedDayIndex} disabled>
          {classes.map(({ day: { name }, classes: dailyClasses }) => {
            const hasClasses = dailyClasses.length > 0;
            if (!hasClasses) {
              return (
                <Empty
                  key={`empty-${name}`}
                  text={t('screens.Empty.classes_plan')}
                  imageSize="20vh"
                  image="/img/big_icons/class.svg"
                />
              );
            }

            const dayIndex = this.getDayOfWeekIndex(selectedDayIndex);
            return (
              <IonList key={`class-list-${name}`} className="classes-list">
                <>
                  {dailyClasses.map(
                    (c, i) => i < dayRenderLimits[dayIndex] && <ClassCard key={c.id} classData={c} />
                  )}
                  {dailyClasses.length > dayRenderLimits[dayIndex] && (
                    <VisibleElement onVisible={this.loadMore} />
                  )}
                </>
              </IonList>
            );
          })}
        </SwipeableViews>
      </div>
    );
  }
}

Plan.propTypes = {
  classes: PropTypes.arrayOf(
    PropTypes.shape({
      day: PropTypes.shape({
        index: PropTypes.number,
        name: PropTypes.string
      }),
      classes: PropTypes.arrayOf(PropTypes.shape({}))
    })
  ).isRequired,
  days: PropTypes.arrayOf(
    PropTypes.shape({
      index: PropTypes.number,
      name: PropTypes.string
    })
  ).isRequired,
  isLoading: PropTypes.bool.isRequired,
  onDayChanged: PropTypes.func.isRequired,
  t: PropTypes.func.isRequired,
  saveState: PropTypes.func.isRequired,
  lastState: PropTypes.shape({
    dayRenderLimits: PropTypes.arrayOf(PropTypes.number),
    selectedDayIndex: PropTypes.number,
    contentScrollY: PropTypes.number
  }).isRequired
};

const mapStateToProps = state => {
  const classes = getRecommendedClassesByDays(state);
  const days = getClassesDays(state);
  return { classes, days };
};

export default compose(withTranslation(), connect(mapStateToProps), withRouteState('Recommended'))(Plan);

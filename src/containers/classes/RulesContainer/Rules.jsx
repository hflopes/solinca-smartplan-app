import React, { Fragment } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';
import './Rules.scss';
import { cancelClassMinutes } from '../../../redux/selectors/auth';

function customRender(text) {
  function renderBold(bold) {
    return bold.split('**').map((t, i) => {
      if (i % 2 === 0) {
        return <Fragment key={t}>{t}</Fragment>;
      }

      return <b key={t}>{t}</b>;
    });
  }

  const blocks = text.split('\n').map(b => {
    const lines = b
      .split('))')
      .map(s => s.replace('((', ''))
      .filter(s => s.length);

    if (lines && lines.length > 1) {
      return (
        <ul key={b}>
          {lines.map(l => (
            <li key={l}>{renderBold(l)}</li>
          ))}
        </ul>
      );
    }

    return <p key={b}>{renderBold(b)}</p>;
  });

  return blocks;
}

const Rules = ({ t, maxMinutesAllowedToCancelClass }) => (
  <div className="rules-container">
    {customRender(t('screens.Classes.rules', { minutes: maxMinutesAllowedToCancelClass }))}
  </div>
);

Rules.propTypes = {
  maxMinutesAllowedToCancelClass: PropTypes.number.isRequired,
  t: PropTypes.func.isRequired
};

const mapStateToProps = state => {
  const maxMinutesAllowedToCancelClass = cancelClassMinutes(state);
  return { maxMinutesAllowedToCancelClass };
};

export default compose(withTranslation(), connect(mapStateToProps))(Rules);

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';
import classNames from 'classnames';

import SearchBar from '../../../components/SearchBar';
import SearchSuggestions from '../../../components/SearchSuggestions';
import SearchResults from '../../../components/SearchResults';
import PrimaryButton from '../../../components/buttons/PrimaryButton';
import { getTerms, getRecentTerms, getFormattedResults, isFetching } from '../../../redux/selectors/search';
import { isOffPeak } from '../../../redux/selectors/user';
import { escapeRegexCharacters } from '../../../utils/strings';
import submitQueryAction, {
  noResults,
  maxGymsFilters,
  maxModalitiesFilters
} from '../../../redux/modules/search/actions/submit';
import clearResultsAction from '../../../redux/modules/search/actions/clear';
import reloadAction from '../../../redux/reload/action';
import withRouteState from '../../../components/hoc/withRouteState';
import withBackNavigation from '../../../components/hoc/withBackNavigation';
import Image from '../../../components/Image';
import './Search.scss';

const MAX_FILTERS_GYM = 2;
const MAX_FILTERS_CLASSES = 3;

class SearchContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      searchText: '',
      suggestions: this.getSuggestions(''),
      hasChangedFilters: false,
      filters: {
        modalities: [],
        gyms: []
      }
    };
  }

  componentWillMount() {
    const { onNavigateBack, clearResults } = this.props;

    onNavigateBack(() => {
      clearResults();
      return false;
    });
  }

  componentDidMount() {
    const { lastState, reload } = this.props;
    const { contentScrollY } = lastState;

    if (!lastState) {
      return;
    }

    if (contentScrollY) {
      this.scrollToSavedPosition(contentScrollY);
    }

    reload('userPosition');
  }

  componentWillReceiveProps(nextProps) {
    const { isFetchingResults: wasFetchingResults, noResultsToast } = this.props;
    const { isFetchingResults, results } = nextProps;

    if (wasFetchingResults && !isFetchingResults && !Object.keys(results).length) {
      noResultsToast();
    }
  }

  componentWillUnmount() {
    const { saveState } = this.props;

    const currentState = {
      contentScrollY: document.querySelector('.search__container').scrollTop
    };

    saveState(currentState);
  }

  scrollToSavedPosition = scroll => {
    const element = document.querySelector('.search__container');
    element.scrollTop = scroll;
    const { scrollTop } = element;
    if (scrollTop < scroll - 100) {
      setTimeout(() => {
        this.scrollToSavedPosition(scroll);
      }, 500);
    }
  };

  /**
   * When the search input is cleared
   * restore the state text to empty
   * and the suggestions to the default (all terms).
   */
  onInputCleared = () => {
    this.setState({
      searchText: '',
      suggestions: this.getSuggestions('')
    });
  };

  /**
   * When the search input's text changes,
   * save the value on state and update
   * the suggestions based on it.
   */
  onInputChanged = ({ target: { value } }) => {
    this.setState({
      searchText: value,
      suggestions: this.getSuggestions(value)
    });
  };

  /**
   * Submit the filters as a search query.
   */
  onSubmit = () => {
    const { submitQuery } = this.props;
    const { filters } = this.state;

    const formattedGyms = filters.gyms.map(g => {
      return { type: 'gym', id: g.id };
    });

    const formattedModalities = filters.modalities.map(m => {
      return { type: 'modality', id: m.id };
    });

    const query = [...formattedGyms, ...formattedModalities];
    submitQuery(query);

    this.setState(
      {
        hasChangedFilters: false
      },
      this.onInputCleared
    );
  };

  /**
   * Toggle (add/remove) a modality to
   * the search filters.
   */
  toggleModalityFilter = modality => {
    const { filters } = this.state;
    const { modalities } = filters;

    // If already exists, remove it from the filters
    if (modalities.map(m => m.id).includes(modality.id)) {
      this.setState({
        filters: { ...filters, modalities: modalities.filter(m => m.id !== modality.id) },
        hasChangedFilters: true
      });
    } else {
      const reachedMaxModalitiesFilters = modalities.length >= MAX_FILTERS_CLASSES;

      if (!reachedMaxModalitiesFilters) {
        this.setState({
          filters: { ...filters, modalities: [...modalities, modality] },
          hasChangedFilters: true
        });
      } else {
        const { maxModalitiesFiltersToast } = this.props;
        maxModalitiesFiltersToast();
      }
    }
  };

  /**
   * Toggle (add/remove) a gym to
   * the search filters.
   */
  toggleGymFilter = gym => {
    const { filters } = this.state;
    const { gyms } = filters;

    // If already exists, remove it from the filters
    if (gyms.map(g => g.id).includes(gym.id)) {
      this.setState({
        filters: { ...filters, gyms: gyms.filter(g => g.id !== gym.id) },
        hasChangedFilters: true
      });
    } else {
      const reachedMaxGymFilters = gyms.length >= MAX_FILTERS_GYM;

      if (!reachedMaxGymFilters) {
        this.setState({
          filters: { ...filters, gyms: [...gyms, gym] },
          hasChangedFilters: true
        });
      } else {
        const { maxGymsFiltersToast } = this.props;
        maxGymsFiltersToast();
      }
    }
  };

  /**
   * Get all the suggested search terms
   * based on the search text (from text input).
   */
  getSuggestions = newText => {
    const { searchTerms } = this.props;
    const { modalities, gyms } = searchTerms;

    if (newText === '') {
      return searchTerms;
    }

    const escapedValue = escapeRegexCharacters(newText.trim());
    const regex = new RegExp(`^${escapedValue}`, 'i');

    const filteredGyms = gyms.filter(g => regex.test(g.name));
    const filteredModalities = modalities.filter(m => regex.test(m.name));

    const newSuggestions = {
      gyms: filteredGyms,
      modalities: filteredModalities
    };

    return newSuggestions;
  };

  shouldRenderResults = () => {
    const { searchText } = this.state;
    const { results } = this.props;

    return searchText.length === 0 && results && Object.keys(results).length > 0;
  };

  renderHeader = () => {
    const { t, isUserOffPeak } = this.props;
    const { filters, searchText } = this.state;
    const { modalities, gyms } = filters;
    const isDarkMode = this.shouldRenderResults();

    const placeholderKey = isUserOffPeak ? 'placeholderOffPeak' : 'placeholder';

    return (
      <>
        <SearchBar
          value={searchText}
          placeholder={t(`screens.Search.${placeholderKey}`)}
          onClear={this.onInputCleared}
          onChange={this.onInputChanged}
          darkMode={isDarkMode}
        />
        <ul className="filters-list">
          {gyms.map(filter => (
            <li key={`gym-${filter.id}`} className="filter">
              <button className="filter__button" type="button" onClick={() => this.toggleGymFilter(filter)}>
                <span className="filter__button-label">{filter.name}</span>
                <Image className="filter__button-icon" src="/img/icons/clear_white.svg" alt="" />
              </button>
            </li>
          ))}
          {modalities.map(filter => (
            <li key={`modality-${filter.id}`} className="filter">
              <button
                className="filter__button"
                type="button"
                onClick={() => this.toggleModalityFilter(filter)}
              >
                <span className="filter__button-label">{filter.name}</span>
                <Image className="filter__button-icon" src="/img/icons/clear_white.svg" alt="" />
              </button>
            </li>
          ))}
        </ul>
      </>
    );
  };

  renderSuggestions = () => {
    const { t, recentTerms } = this.props;
    const { suggestions, filters, searchText } = this.state;

    if (searchText === '') {
      return (
        <div className="search__recent">
          <h1 className="recent__label">{t('screens.Search.recentSearches')}</h1>
          <hr className="recent__divider" />
          <SearchSuggestions
            suggestions={recentTerms}
            filters={filters}
            onGymSelected={this.toggleGymFilter}
            onModalitySelected={this.toggleModalityFilter}
          />
        </div>
      );
    }

    return (
      <div className="search__suggestions">
        <SearchSuggestions
          suggestions={suggestions}
          filters={filters}
          onGymSelected={this.toggleGymFilter}
          onModalitySelected={this.toggleModalityFilter}
        />
      </div>
    );
  };

  renderResults = () => {
    const { t, results } = this.props;
    return (
      <>
        <h1 className="results__label">{t('screens.Search.searchResults')}</h1>
        <div className="results__container">
          <SearchResults results={results} />
        </div>
      </>
    );
  };

  renderBody = () => {
    if (this.shouldRenderResults()) {
      return <div className="search__results">{this.renderResults()}</div>;
    }

    return <div className="search__suggestions">{this.renderSuggestions()}</div>;
  };

  renderSubmitButton = () => {
    const { t } = this.props;
    const { filters, hasChangedFilters } = this.state;
    const { modalities, gyms } = filters;

    const hasFilters = modalities.length > 0 || gyms.length > 0;
    const showButton = hasFilters && (hasChangedFilters || !this.shouldRenderResults());

    return showButton && <PrimaryButton text={t('global.actions.search')} onClick={this.onSubmit} />;
  };

  render() {
    const containerClass = classNames('search__container', { '-dark-mode': this.shouldRenderResults() });

    return (
      <>
        <div className={containerClass}>
          <div id="notch" />
          <div className="search__header">{this.renderHeader()}</div>
          <div className="search__body">{this.renderBody()}</div>
          <div className="search__submit-button">{this.renderSubmitButton()}</div>
        </div>
      </>
    );
  }
}

SearchContainer.propTypes = {
  t: PropTypes.func.isRequired,
  searchTerms: PropTypes.shape({
    modalities: PropTypes.arrayOf(PropTypes.any),
    gyms: PropTypes.arrayOf(PropTypes.any)
  }).isRequired,
  recentTerms: PropTypes.shape({
    modalities: PropTypes.arrayOf(PropTypes.any),
    gyms: PropTypes.arrayOf(PropTypes.any)
  }).isRequired,
  results: PropTypes.shape({}).isRequired,
  isFetchingResults: PropTypes.bool.isRequired,
  submitQuery: PropTypes.func.isRequired,
  noResultsToast: PropTypes.func.isRequired,
  maxModalitiesFiltersToast: PropTypes.func.isRequired,
  maxGymsFiltersToast: PropTypes.func.isRequired,
  isUserOffPeak: PropTypes.bool.isRequired,
  saveState: PropTypes.func.isRequired,
  lastState: PropTypes.shape({
    contentScrollY: PropTypes.number
  }).isRequired,
  reload: PropTypes.func.isRequired,
  onNavigateBack: PropTypes.func.isRequired,
  clearResults: PropTypes.func.isRequired
};

const mapStateToProps = state => {
  const searchTerms = getTerms(state);
  const recentTerms = getRecentTerms(state);
  const results = getFormattedResults(state);
  const isFetchingResults = isFetching(state);
  const isUserOffPeak = isOffPeak(state);

  if (isUserOffPeak) {
    searchTerms.gyms = [];
    recentTerms.gyms = [];
  }

  return { searchTerms, recentTerms, results, isFetchingResults, isUserOffPeak };
};

const mapDispatchToProps = {
  submitQuery: submitQueryAction,
  noResultsToast: noResults,
  maxGymsFiltersToast: maxGymsFilters,
  maxModalitiesFiltersToast: maxModalitiesFilters,
  reload: reloadAction,
  clearResults: clearResultsAction
};

export default compose(
  withTranslation(),
  connect(mapStateToProps, mapDispatchToProps),
  withBackNavigation('light', false),
  withRouteState('Search')
)(SearchContainer);

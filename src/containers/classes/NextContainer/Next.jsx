import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { connect } from 'react-redux';
import { compose } from 'redux';
import SwipeableViews from 'react-swipeable-views';
import { withTranslation } from 'react-i18next';
import BadgeTabs from '../../../components/tabs/BadgeTabs';
import ClassCard from '../../../components/cards/ClassCard';
import LoadingClassCard from '../../../components/loading/LazyClassCard';
import VisibleElement from '../../../components/VisibleElement';
import Empty from '../../../components/Empty';
import withRouteState from '../../../components/hoc/withRouteState';
import { getClassesByDays, getClassesDays } from '../../../redux/selectors/classes';
import './Next.scss';
import { hasBlockedWarningMessage } from '../../../redux/selectors/auth';
// import { getSelectedGym } from '../../../redux/selectors/gyms';

const ITEM_LOAD_INCREMENT = 7;
const LOADING_ITEMS = 5;
const PIXELS = 100;

class Next extends Component {
  constructor(props) {
    super(props);
    const { days, lastState } = props;

    this.state = {
      tabs: days.map(({ name }) => name),
      selectedDayIndex: lastState.selectedDayIndex || 0,
      dayRenderLimits: lastState.dayRenderLimits || Array(8).fill(ITEM_LOAD_INCREMENT)
    };
  }

  componentDidMount() {
    const { lastState } = this.props;

    if (!lastState) {
      return;
    }

    const { contentScrollY } = lastState;

    if (contentScrollY) {
      this.scrollToSavedPosition();
    }
  }

  componentWillUnmount() {
    const { selectedDayIndex, dayRenderLimits } = this.state;
    const { saveState } = this.props;
    const container = document.querySelector('.react-swipeable-view-container').firstElementChild;
    const { scrollTop: contentScrollY } = container;

    const currentState = { selectedDayIndex, contentScrollY, dayRenderLimits };
    saveState(currentState);
  }

  scrollToSavedPosition = () => {
    const { lastState } = this.props;
    const { contentScrollY } = lastState;
    let hasElement = false;
    const parent = document.querySelector('.react-swipeable-view-container');
    if (parent) {
      const element = parent.firstElementChild;
      if (element) {
        element.scrollTo({
          top: contentScrollY,
          left: 0,
          behaviour: 'smooth'
        });
        hasElement = true;
        const { scrollTop } = element;
        if (scrollTop < contentScrollY - PIXELS) {
          setTimeout(() => {
            this.scrollToSavedPosition();
          }, 500);
        }
      }
    }

    if (!hasElement) {
      setTimeout(() => {
        this.scrollToSavedPosition();
      }, 500);
    }
  };

  /**
   * Converts the array index to the
   * day of week index (sunday == 0)
   */
  getDayOfWeekIndex = arrayIndex => {
    const { classes } = this.props;
    return classes[arrayIndex].day.index;
  };

  selectDay = index => {
    const { onDayChanged } = this.props;
    const parent = document.querySelector('.react-swipeable-view-container');
    if (parent) {
      const element = parent.firstElementChild;
      if (element) {
        element.scrollTop = 0;
      }
    }

    this.setState(
      {
        selectedDayIndex: index
      },
      () => {
        onDayChanged(index);
      }
    );
  };

  loadMore = () => {
    const { selectedDayIndex, dayRenderLimits } = this.state;

    const newDayRenderLimits = [...dayRenderLimits];
    newDayRenderLimits[this.getDayOfWeekIndex(selectedDayIndex)] += ITEM_LOAD_INCREMENT;

    this.setState({
      dayRenderLimits: newDayRenderLimits
    });
  };

  render() {
    const { selectedDayIndex, dayRenderLimits, tabs } = this.state;
    const { isLoading, t, classes, blockedWarningMessage } = this.props;

    if (isLoading || !classes) {
      return (
        <div className="next-classes-container">
          <div className="tabs">
            <BadgeTabs tabs={tabs} onSelected={this.selectDay} selectedIndex={selectedDayIndex} />
          </div>
          <div className="classes-list">
            {_.times(LOADING_ITEMS, index => (
              <LoadingClassCard key={index} />
            ))}
          </div>
        </div>
      );
    }

    return (
      <div className="next-classes-container">
        <div className="tabs">
          <BadgeTabs tabs={tabs} onSelected={this.selectDay} selectedIndex={selectedDayIndex} />
        </div>

        <SwipeableViews index={selectedDayIndex} disabled>
          {classes.map(({ day: { date }, classes: dailyClasses }, index) => {
            if (index !== selectedDayIndex) return null;
            const hasClasses = dailyClasses.length > 0;

            if (!hasClasses || blockedWarningMessage) {
              // if (gym && gym.closed) {
              //   return <Empty key={`empty-${name}`} text={t('screens.Empty.classes_closed')} />;
              // }

              return (
                <Empty
                  key={`empty-${date}`}
                  text={blockedWarningMessage || t('screens.Empty.classes')}
                  imageSize="20vh"
                  image="/img/big_icons/class.svg"
                  style={{ height: 'auto' }}
                />
              );
            }

            const dayIndex = this.getDayOfWeekIndex(selectedDayIndex);
            return (
              <div key={`class-list-${date}`} className="classes-list">
                <>
                  {dailyClasses.map(
                    (c, i) => i < dayRenderLimits[dayIndex] && <ClassCard key={c.id} classData={c} />
                  )}
                  {dailyClasses.length > dayRenderLimits[dayIndex] && (
                    <VisibleElement onVisible={this.loadMore} />
                  )}
                </>
              </div>
            );
          })}
        </SwipeableViews>
      </div>
    );
  }
}

Next.defaultProps = {
  blockedWarningMessage: ''
};

Next.propTypes = {
  classes: PropTypes.arrayOf(
    PropTypes.shape({
      day: PropTypes.shape({
        index: PropTypes.number,
        name: PropTypes.string
      }),
      classes: PropTypes.arrayOf(PropTypes.shape({}))
    })
  ).isRequired,
  days: PropTypes.arrayOf(
    PropTypes.shape({
      index: PropTypes.number,
      name: PropTypes.string
    })
  ).isRequired,
  isLoading: PropTypes.bool.isRequired,
  onDayChanged: PropTypes.func.isRequired,
  t: PropTypes.func.isRequired,
  blockedWarningMessage: PropTypes.string,
  saveState: PropTypes.func.isRequired,
  lastState: PropTypes.shape({
    dayRenderLimits: PropTypes.arrayOf(PropTypes.number),
    selectedDayIndex: PropTypes.number,
    contentScrollY: PropTypes.number
  }).isRequired
  // gym: PropTypes.shape({
  //   closed: PropTypes.bool
  // }).isRequired
};

const mapStateToProps = state => {
  const classes = getClassesByDays(state);
  const days = getClassesDays(state);
  const blockedWarningMessage = hasBlockedWarningMessage(state);
  // const gym = getSelectedGym(state);
  return { classes, days, blockedWarningMessage };
};

export default compose(withTranslation(), connect(mapStateToProps), withRouteState('Next'))(Next);

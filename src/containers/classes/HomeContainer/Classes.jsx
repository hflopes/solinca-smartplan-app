import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import { Swipeable } from 'react-swipeable';
import { withTranslation } from 'react-i18next';
import SwipeableViews from 'react-swipeable-views';
import { Link } from 'react-router-dom';
import Fab from '../../../components/buttons/FabButton';
import ClassesHeader from '../../../components/headers/ClassesHeader';
import RulesContainer from '../RulesContainer';
import PlanContainer from '../PlanContainer';
import NextContainer from '../NextContainer';
import ModalitiesContainer from '../ModalitiesContainer';
import reloadAction from '../../../redux/reload/action';
import selectGymAction from '../../../redux/modules/gyms/actions/select';
import SlidingList from '../../../components/SlidingList';
import { getGymsListOrderedByDistance, getSelectedGymId } from '../../../redux/selectors/gyms';
import { isFetchingUpcomingClasses, getRecommendedClasses } from '../../../redux/selectors/classes';
import withRouteState from '../../../components/hoc/withRouteState';
import withMenuNavigation from '../../../components/hoc/withMenuNavigation';
import withBackNavigation from '../../../components/hoc/withBackNavigation';
import './Classes.scss';
import { hasBlockedWarningMessage } from '../../../redux/selectors/auth';

class Classes extends Component {
  constructor(props) {
    super(props);

    const { lastState } = props;
    this.state = {
      selectedSection: lastState.selectedSection || 0,
      isShowingGymList: false,
      tabs: this.loadTabs()
    };
  }

  componentWillMount() {
    const { onNavigateBack } = this.props;

    onNavigateBack(() => {
      const { isShowingGymList } = this.state;
      if (isShowingGymList) {
        this.toggleGymList(true);
        return true;
      }

      return false;
    });
  }

  componentDidMount() {
    const { lastState, reload } = this.props;
    const { contentScrollY } = lastState;

    reload('modalities');
    reload('gyms');
    reload('upcomingClasses');

    if (!lastState) {
      return;
    }

    if (contentScrollY) {
      this.scrollTo(0, contentScrollY);
    }
  }

  componentWillUnmount() {
    const { selectedSection } = this.state;
    const { contentScrollY, selectedDay } = this;
    const { saveState } = this.props;

    const currentState = {
      selectedSection,
      contentScrollY,
      selectedDay
    };

    saveState(currentState);
  }

  loadTabs = () => {
    const { t } = this.props;
    return t('screens.Classes.tabs', { returnObjects: true });
  };

  select = index => {
    this.setState({ selectedSection: index });
    this.scrollTo(0);
  };

  scrollTo = y => {
    const container = document.getElementById('classes');
    container.scrollTo({
      top: y,
      left: 0,
      behaviour: 'smooth'
    });
  };

  saveScrollY = evt => {
    const { detail } = evt;
    const { scrollTop } = detail;
    this.contentScrollY = scrollTop;
  };

  saveDay = index => {
    this.selectedDay = index;
  };

  renderGymList = () => {
    const { isShowingGymList } = this.state;
    const { gyms, selectedGymId, selectGym, t } = this.props;

    const onSelect = index => {
      selectGym({ gym: gyms[index] });
      this.toggleGymList(true);
    };

    const selectedIndex = gyms.indexOf(gyms.filter(g => g.id === selectedGymId)[0]);

    const mappedGyms = gyms.map(g => {
      return {
        title: g.name,
        info: g.distance ? `${g.distance.toFixed(1)} km` : null
      };
    });

    const gymListCopy = {
      listTitle: t('screens.Classes.gymList.listTitle'),
      selectedTitle: t('screens.Classes.gymList.selectedTitle'),
      optionsTitle: t('screens.Classes.gymList.optionsTitle')
    };

    return (
      <Swipeable onSwipedDown={this.toggleGymList}>
        <SlidingList
          closeList={this.toggleGymList}
          id="classes-gym-list"
          list={mappedGyms}
          selectedIndex={selectedIndex}
          copy={gymListCopy}
          onSelect={onSelect}
          onDismiss={this.toggleGymList}
          isShowing={isShowingGymList}
        />
      </Swipeable>
    );
  };

  toggleGymList = force => {
    const list = document.getElementById('classes-gym-list');

    if ((!force || typeof force !== 'boolean') && list.scrollTop > 0) {
      return;
    }

    list.scrollTop = 0;
    const { isShowingGymList } = this.state;
    this.setState({ isShowingGymList: !isShowingGymList });
  };

  onScroll = () => {
    const content = document.getElementById('classes');
    const header = document.getElementById('classes-header');
    const headerWrapper = document.getElementById('classes-header-wrapper');

    const { scrollTop } = content;

    const yTranslate = Math.min(scrollTop, 120);
    const scrollPercent = Math.min(scrollTop / 120, 1);

    header.style.transform = `translate3d(0, -${yTranslate}px, 0)`;
    headerWrapper.style.opacity = 1 - scrollPercent * 1.9;
  };

  render() {
    const { tabs, isShowingGymList, selectedSection } = this.state;
    const { recommendedClasses, lastState, isFetchingUpcoming, blockedWarningMessage } = this.props;

    const { selectedDay } = lastState;

    return (
      <div id="classes" className="classes__container full-height-flex" onScroll={this.onScroll}>
        <ClassesHeader
          tabs={tabs}
          onSelect={this.select}
          onToggleGymList={() => this.toggleGymList(true)}
          isShowingGymList={isShowingGymList}
          defaultIndex={selectedSection}
        />

        <div className="classes__content">
          <SwipeableViews className="swipe-container" index={selectedSection} disabled>
            {selectedSection === 0 ? (
              <NextContainer
                isLoading={isFetchingUpcoming}
                onDayChanged={this.saveDay}
                defaultDay={selectedDay}
              />
            ) : null}
            {selectedSection === 1 ? <RulesContainer /> : null}
            {selectedSection === 2 ? (
              <PlanContainer
                isLoading={isFetchingUpcoming}
                classes={recommendedClasses}
                onDayChanged={this.saveDay}
                defaultDay={selectedDay}
              />
            ) : null}
            {selectedSection === 3 ? <ModalitiesContainer /> : null}
          </SwipeableViews>
        </div>

        {!blockedWarningMessage && (
          <Link to="/search">
            <Fab icon="search_white" />
          </Link>
        )}
        {this.renderGymList()}
      </div>
    );
  }
}

Classes.defaultProps = {
  blockedWarningMessage: ''
};

Classes.propTypes = {
  recommendedClasses: PropTypes.arrayOf(PropTypes.any).isRequired,
  gyms: PropTypes.arrayOf(PropTypes.any).isRequired,
  selectedGymId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  reload: PropTypes.func.isRequired,
  blockedWarningMessage: PropTypes.string,
  isFetchingUpcoming: PropTypes.bool.isRequired,
  saveState: PropTypes.func.isRequired,
  lastState: PropTypes.shape({
    selectedSection: PropTypes.number,
    contentScrollY: PropTypes.number,
    selectedDay: PropTypes.number
  }).isRequired,
  selectGym: PropTypes.func.isRequired,
  onNavigateBack: PropTypes.func.isRequired,
  t: PropTypes.func.isRequired
};

const mapStateToProps = state => {
  const recommendedClasses = getRecommendedClasses(state);
  const gyms = getGymsListOrderedByDistance(state);
  const selectedGymId = getSelectedGymId(state);
  const isFetchingUpcoming = isFetchingUpcomingClasses(state);
  const blockedWarningMessage = hasBlockedWarningMessage(state);

  return { recommendedClasses, gyms, selectedGymId, isFetchingUpcoming, blockedWarningMessage };
};

const mapDispatchToProps = {
  reload: reloadAction,
  selectGym: selectGymAction
};

export default compose(
  withTranslation(),
  connect(mapStateToProps, mapDispatchToProps),
  withMenuNavigation(),
  withBackNavigation('light', false),
  withRouteState('Classes')
)(Classes);

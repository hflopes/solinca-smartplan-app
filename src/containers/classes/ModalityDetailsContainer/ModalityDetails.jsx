import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';
import moment from 'moment';
import { getModality } from '../../../redux/selectors/modalities';
import { getSelectedGym } from '../../../redux/selectors/gyms';
import { getUpcomingClassesByModality, getBookedClasses } from '../../../redux/selectors/classes';
import LayeredPage from '../../../components/LayeredPage';
import ModalityHeader from '../../../components/headers/ModalityHeader';
import ClassMiniCard from '../../../components/cards/ClassMiniCard';
import ModalityDescription from '../../../components/ModalityDescription';
import PrimaryButton from '../../../components/buttons/PrimaryButton';
import bookAction from '../../../redux/modules/classes/actions/book';
import withBackNavigation from '../../../components/hoc/withBackNavigation';
import { DEFAULT_MODALITY } from '../../../config/visuals';
import './ModalityDetails.scss';
import { upcomingClassesLastUpdate } from '../../../redux/selectors/reload';

class ModalityDetails extends Component {
  state = {
    selectedClassIndex: -1
  };

  componentWillReceiveProps(nextProps) {
    const { classes, bookedClasses } = this.props;
    const { selectedClassIndex } = this.state;

    // If the number of booked classes increased
    // that means the user swipe-booked a class
    // so we should redirect to the details page
    if (bookedClasses < nextProps.bookedClasses) {
      const { history } = this.props;
      history.push(`/classes/${classes[selectedClassIndex].id}`);
    }
  }

  selectClass = index => {
    const { selectedClassIndex } = this.state;
    const newIndex = index === selectedClassIndex ? -1 : index;
    this.setState({ selectedClassIndex: newIndex });
  };

  onCTA = selectedClass => {
    if (selectedClass.isBooked) {
      const { history } = this.props;
      history.push(`/classes/${selectedClass.id}`);
    } else {
      this.bookClass(selectedClass);
    }
  };

  bookClass = selectedClass => {
    const { book } = this.props;
    book(selectedClass);
  };

  renderClasses = () => {
    const { classes, t, lastUpdate } = this.props;
    const { selectedClassIndex } = this.state;

    const minutesUpdated = moment().diff(moment(lastUpdate), 'minutes');
    const availableLabel = t(`screens.ModalityDetails.availableSchedules`);
    const noAvailableLabel = t(`screens.ModalityDetails.noAvailableSchedules`);

    return (
      <div className="classes">
        {classes.length > 0 ? (
          <>
            <b className="schedules-title">{availableLabel}</b>
            <div className="classes-list">
              {classes.map((c, index) => (
                <ClassMiniCard
                  key={c.id}
                  isDisabled={selectedClassIndex > -1 && index !== selectedClassIndex}
                  isSelected={index === selectedClassIndex}
                  classData={c}
                  onClick={this.selectClass}
                  index={index}
                />
              ))}
            </div>
            <div className="classes-timer">
              {minutesUpdated ? (
                <span>{t('screens.ModalityDetails.lastUpdateMinutes', { minutes: minutesUpdated })}</span>
              ) : (
                <span>{t('screens.ModalityDetails.lastUpdateRecent')}</span>
              )}
            </div>
          </>
        ) : (
          <b className="schedules-title solo">{noAvailableLabel}</b>
        )}
      </div>
    );
  };

  renderDescription = () => {
    const { modality } = this.props;
    return <ModalityDescription modality={modality} />;
  };

  renderContent = () => {
    return (
      <>
        {this.renderClasses()}
        {this.renderDescription()}
      </>
    );
  };

  renderHeader = () => {
    const { modality, selectedGym } = this.props;
    return <ModalityHeader modality={modality} gym={selectedGym} paddingBottom="83px" />;
  };

  renderCTA = () => {
    const { classes, t, modality } = this.props;
    const { selectedClassIndex } = this.state;

    // If no class is selected
    if (selectedClassIndex === -1) {
      return null;
    }

    const { color1, color2 } = modality;

    const hasValidColors = color1 && color2 && color1 !== '' && color2 !== '';
    const modalityColorScheme = { start: color1, end: color2 };

    const colorScheme = hasValidColors ? modalityColorScheme : DEFAULT_MODALITY.colorScheme;
    const selectedClass = classes[selectedClassIndex];
    const textKey = selectedClass.isBooked ? 'viewClassCTA' : 'bookClassCTA';

    return (
      <PrimaryButton
        text={t(`screens.ModalityDetails.${textKey}`)}
        colorScheme={colorScheme}
        onClick={() => this.onCTA(selectedClass)}
        smartContrast
      />
    );
  };

  render() {
    const header = <>{this.renderHeader()}</>;
    const content = <>{this.renderContent()}</>;
    const cta = <>{this.renderCTA()}</>;

    return (
      <LayeredPage
        className="modality-container"
        background={header}
        foreground={content}
        cta={cta}
        gap="370px"
        extra="10vh"
        rounded
      />
    );
  }
}

ModalityDetails.defaultProps = {
  modality: {},
  lastUpdate: null
};

ModalityDetails.propTypes = {
  modality: PropTypes.shape({
    name: PropTypes.string,
    intensity: PropTypes.number,
    description: PropTypes.string,
    fullName: PropTypes.string,
    color1: PropTypes.string,
    color2: PropTypes.string
  }),
  lastUpdate: PropTypes.instanceOf(Date),
  selectedGym: PropTypes.shape({}).isRequired,
  classes: PropTypes.arrayOf(PropTypes.any).isRequired,
  bookedClasses: PropTypes.arrayOf(PropTypes.any).isRequired,
  t: PropTypes.func.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func
  }).isRequired,
  book: PropTypes.func.isRequired
};

const mapStateToProps = (state, ownProps) => {
  const { modalityId } = ownProps;
  const modality = getModality(state, modalityId);
  const bookedClasses = getBookedClasses(state);
  const classes = getUpcomingClassesByModality(state, modalityId);
  const selectedGym = getSelectedGym(state);
  const lastUpdate = upcomingClassesLastUpdate(state);
  return { modality, classes, bookedClasses, selectedGym, lastUpdate };
};

const mapDispatchToProps = {
  book: bookAction
};

export default compose(
  withTranslation(),
  withRouter,
  withBackNavigation(),
  connect(mapStateToProps, mapDispatchToProps)
)(ModalityDetails);

export const BORDER_RADIUS = 28;
export const HEADER_FULL_HEIGHT = 355;
export const HEADER_REDUCED_HEIGHT = 140;
export const SCROLL_LIMIT = HEADER_FULL_HEIGHT - HEADER_REDUCED_HEIGHT - BORDER_RADIUS;

const headerDecorator = () => {
  return {
    height: `${HEADER_FULL_HEIGHT}px`
  };
};

const gymDecorator = scrollPercent => {
  return {
    transform: `translate3d(0, ${scrollPercent * -100}px, 0)`,
    opacity: 1 - scrollPercent
  };
};

const titleDecorator = scrollPercent => {
  return {
    transform: `translate3d(0, ${scrollPercent * -100}px, 0) scale(${1.35 - scrollPercent * 0.7})`
  };
};

const intensityDecorator = scrollPercent => {
  return {
    transform: `translate3d(0, ${scrollPercent * -100}px, 0)`,
    opacity: 1 - scrollPercent,
    paddingBottom: `${BORDER_RADIUS + 55}px`
  };
};

export const decorators = [
  { id: 'modality-header', decorator: headerDecorator },
  { id: 'modality-title', decorator: titleDecorator },
  { id: 'modality-intensity', decorator: intensityDecorator },
  { id: 'modality-gym', decorator: gymDecorator }
];

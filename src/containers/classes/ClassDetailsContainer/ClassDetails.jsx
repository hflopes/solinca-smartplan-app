import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';
import { IonGrid, IonCol } from '@ionic/react';
import moment from 'moment';
import { getClass, hasClassAlert } from '../../../redux/selectors/classes';
import ModalityHeader from '../../../components/headers/ModalityHeader';
import ModalityDescription from '../../../components/ModalityDescription';
import ClassMiniCard from '../../../components/cards/ClassMiniCard';
import PrimaryButton from '../../../components/buttons/PrimaryButton';
import RoundButton from '../../../components/buttons/RoundButton';
import { buildEvent, findEvent, addEvent, hasCalendarPermissions } from '../../../utils/calendar';
import toggleFavoriteAction from '../../../redux/modules/classes/actions/toggleFavorite';
import toggleAlertAction from '../../../redux/modules/classes/actions/toggleAlert';
import bookAction from '../../../redux/modules/classes/actions/book';
import cancelAction from '../../../redux/modules/classes/actions/cancel';
import withBackNavigation from '../../../components/hoc/withBackNavigation';
import LayeredPage from '../../../components/LayeredPage';
import { getPlatform } from '../../../redux/selectors/app';
import { DEFAULT_MODALITY } from '../../../config/visuals';
import './ClassDetails.scss';

class ClassDetails extends Component {
  state = {
    calendar: false,
    eventBuilt: false,
    event: {
      title: '',
      location: '',
      notes: '',
      startDate: null,
      endDate: null
    }
  };

  componentWillMount() {
    const { onNavigateBack } = this.props;

    onNavigateBack(null);
  }

  componentDidMount() {
    const { platform } = this.props;
    if (platform === 'android' || platform === 'ios') {
      hasCalendarPermissions(this.buildEvent);
    }
  }

  buildEvent = cb => {
    const { classData } = this.props;
    const event = buildEvent(classData);

    this.setState({ event, eventBuilt: true }, cb || this.checkEventCreated);
  };

  checkEventCreated = () => {
    const { event } = this.state;

    findEvent(event, () => {
      this.setState({ calendar: true });
    });
  };

  toggleFavorite = () => {
    const { toggleFavorite, classData } = this.props;
    toggleFavorite(classData);
  };

  toggleCalendar = () => {
    const { platform } = this.props;
    const { event, eventBuilt } = this.state;

    if (eventBuilt) {
      addEvent(
        event,
        () => {
          this.setState({ calendar: true });
        },
        this.checkEventCreated
      );
    } else if (platform === 'ios') {
      hasCalendarPermissions(this.toggleCalendar);
    } else {
      this.buildEvent(this.toggleCalendar);
    }
  };

  toggleAlert = () => {
    const { classData, toggleAlert } = this.props;

    toggleAlert(classData);
  };

  bookClass = () => {
    const { classData, book } = this.props;
    book(classData);
  };

  cancelClass = () => {
    const { classData, cancel } = this.props;
    cancel(classData);
  };

  renderActions = () => {
    const { t, classData, hasAlert } = this.props;
    const { calendar } = this.state;

    const { isFavorite, date } = classData;

    const favoriteStyles = {
      backgroundColor: isFavorite ? 'white' : '#A80766',
      icon: isFavorite ? 'heart_filled' : 'heart_outline',
      text: isFavorite ? 'removeFromFavoritesCTA' : 'addToFavoritesCTA'
    };

    const alertStyles = {
      backgroundColor: hasAlert ? 'white' : '#776e71',
      icon: hasAlert ? 'alert_filled' : 'alert_outline',
      text: hasAlert ? 'deactivateAlertCTA' : 'activateAlertCTA'
    };

    const calendarStyles = {
      backgroundColor: calendar ? 'white' : '#776e71',
      icon: calendar ? 'calendar_filled' : 'calendar_outline'
    };

    const alertDisabled = moment(date).isSameOrBefore(moment());

    return (
      <div className="actions">
        <IonGrid>
          <IonCol>
            <RoundButton
              color="#A80766"
              backgroundColor={favoriteStyles.backgroundColor}
              icon={favoriteStyles.icon}
              text={t(`screens.ClassDetails.${favoriteStyles.text}`)}
              onClick={this.toggleFavorite}
            />
          </IonCol>
          <IonCol>
            <RoundButton
              icon={calendarStyles.icon}
              backgroundColor={calendarStyles.backgroundColor}
              text={t('screens.ClassDetails.addToCalendarCTA')}
              textDisabled={t('screens.ClassDetails.addedToCalendarCTA')}
              onClick={this.toggleCalendar}
              disabled={calendar}
            />
          </IonCol>
          <IonCol>
            <RoundButton
              backgroundColor={alertStyles.backgroundColor}
              icon={alertStyles.icon}
              text={t(`screens.ClassDetails.${alertStyles.text}`)}
              onClick={this.toggleAlert}
              disabled={alertDisabled}
              textDisabled={t('screens.ClassDetails.disabledAlertCTA')}
            />
          </IonCol>
        </IonGrid>
      </div>
    );
  };

  renderDescription = () => {
    const { classData } = this.props;
    return <ModalityDescription modality={classData.modality} />;
  };

  renderContent = () => {
    return (
      <>
        {this.renderActions()}
        {this.renderDescription()}
      </>
    );
  };

  renderContentTop = () => {
    const { classData } = this.props;
    return <ClassMiniCard classData={classData} />;
  };

  renderHeader = () => {
    const { classData } = this.props;
    const { modality, gym, instructor } = classData;
    return <ModalityHeader instructor={instructor} modality={modality} gym={gym} paddingBottom="110px" />;
  };

  renderCTA = () => {
    const { classData, t } = this.props;
    const { isBooked, modality } = classData;
    const { color1, color2 } = modality;

    const hasValidColors = color1 && color2 && color1 !== '' && color2 !== '';
    const modalityColorScheme = { start: color1, end: color2 };

    const textKey = isBooked ? 'cancelClassCTA' : 'bookClassCTA';
    const onClickCallback = isBooked ? this.cancelClass : this.bookClass;
    const colorScheme = hasValidColors ? modalityColorScheme : DEFAULT_MODALITY.colorScheme;

    return (
      <PrimaryButton
        text={t(`screens.ModalityDetails.${textKey}`)}
        colorScheme={colorScheme}
        onClick={onClickCallback}
        smartContrast
      />
    );
  };

  render() {
    const header = <>{this.renderHeader()}</>;
    const content = <>{this.renderContent()}</>;
    const cta = <>{this.renderCTA()}</>;
    const edge = <>{this.renderContentTop()}</>;

    return (
      <LayeredPage
        className="class-container"
        background={header}
        foreground={content}
        cta={cta}
        edge={edge}
        gap="370px"
        extra="10vh"
        edgeGap="55px"
        rounded
      />
    );
  }
}

ClassDetails.propTypes = {
  classData: PropTypes.shape({
    id: PropTypes.string,
    date: PropTypes.instanceOf(Date),
    duration: PropTypes.number,
    studio: PropTypes.string,
    instructor: PropTypes.string,
    modality: PropTypes.shape({
      name: PropTypes.string,
      color1: PropTypes.string,
      color2: PropTypes.string,
      fullName: PropTypes.string
    }),
    gym: PropTypes.shape({}),
    isFavorite: PropTypes.bool,
    isBooked: PropTypes.bool
  }).isRequired,
  hasAlert: PropTypes.bool.isRequired,
  t: PropTypes.func.isRequired,
  toggleFavorite: PropTypes.func.isRequired,
  toggleAlert: PropTypes.func.isRequired,
  onNavigateBack: PropTypes.func.isRequired,
  platform: PropTypes.string.isRequired,
  book: PropTypes.func.isRequired,
  cancel: PropTypes.func.isRequired
};

const mapStateToProps = (state, ownProps) => {
  const { classId } = ownProps;
  const classData = getClass(state, classId);
  const hasAlert = hasClassAlert(state, classId);
  const platform = getPlatform(state);
  return { classData, hasAlert, platform };
};

const mapDispatchToProps = {
  toggleFavorite: toggleFavoriteAction,
  toggleAlert: toggleAlertAction,
  book: bookAction,
  cancel: cancelAction
};

export default compose(
  withTranslation(),
  withBackNavigation(),
  connect(mapStateToProps, mapDispatchToProps)
)(ClassDetails);

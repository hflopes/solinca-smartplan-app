import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Link } from 'react-router-dom';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';
import InfoDetail from '../../../components/InfoDetail';
import reloadAction from '../../../redux/reload/action';
import { getUser3dBody } from '../../../redux/selectors/user';
import './Info.scss';

const infoDetailLabel = (t, label) => {
  return t(`screens.Profile.info.labels.${label}`);
};

function Info({ t, name, address, email, nif, phone, memberNumber, birthDate, url3dBody }) {
  return (
    <div className="profile-info">
      <InfoDetail label={infoDetailLabel(t, 'name')} value={name} />
      <InfoDetail label={infoDetailLabel(t, 'email')} value={email.toLowerCase()} isEditable field="email" />
      <InfoDetail label={infoDetailLabel(t, 'phone')} value={phone} isEditable field="phone" />
      <InfoDetail label={infoDetailLabel(t, 'address')} value={address} isEditable field="address" />
      <InfoDetail
        label={infoDetailLabel(t, 'password')}
        value="1234567890"
        isPassword
        isEditable
        field="password"
      />
      <InfoDetail label={infoDetailLabel(t, 'memberNumber')} value={String(memberNumber)} />
      <InfoDetail label={infoDetailLabel(t, 'birthdate')} value={moment(birthDate).format('DD/MM/YYYY')} />
      <InfoDetail label={infoDetailLabel(t, 'nif')} value={String(nif)} />
      <InfoDetail
        label={infoDetailLabel(t, 'receipts')}
        value={t('screens.Profile.info.labels.receiptsList')}
        isList
        field="receipts"
      />

      {url3dBody && (
        <>
          <InfoDetail label={infoDetailLabel(t, '3dbody')} value="" />
          <Link to={{ pathname: '/3dbody', state: { url: url3dBody } }}>
            <button className="file-btn" type="button">
              <span className="file-btn__label">Visualizar PDF 3D Body</span>
              <div className="file-btn__img">
                <img src="/img/icons/pdf.svg" alt="" />
              </div>
            </button>
          </Link>
        </>
      )}

      {/* lastReceipt && (
        <Link to={{ pathname: '/terms', state: { url: lastReceipt, title: infoDetailLabel(t, 'receipt') } }}>
          <button className="download-btn" type="button">
            <span className="download-btn__label">{infoDetailLabel(t, 'receipt')}</span>
            <div className="download-btn__img">
              <img src="/img/icons/download.svg" alt={infoDetailLabel(t, 'receipt')} />
            </div>
          </button>
        </Link>
      ) */}
    </div>
  );
}

Info.defaultProps = {
  url3dBody: undefined
};

Info.propTypes = {
  t: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  address: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  nif: PropTypes.string.isRequired,
  phone: PropTypes.string.isRequired,
  memberNumber: PropTypes.string.isRequired,
  birthDate: PropTypes.instanceOf(Date).isRequired,
  url3dBody: PropTypes.string
  // lastReceipt: PropTypes.string.isRequired
};

const mapStateToProps = state => {
  const { data, receiptsList } = state.user;
  const url3dBody = getUser3dBody(state);
  return { ...data, receiptsList, url3dBody };
};

const mapDispatchToProps = {
  reload: reloadAction
};

export default compose(withTranslation(), connect(mapStateToProps, mapDispatchToProps))(Info);

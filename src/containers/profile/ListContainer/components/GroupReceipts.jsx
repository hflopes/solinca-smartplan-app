import React, { useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { withTranslation } from 'react-i18next';
import { motion, AnimatePresence } from 'framer-motion';
import classNames from 'classnames';
import Image from '../../../../components/Image';
import './GroupReceipts.scss';

function GroupReceipts({ receiptsElement, t }) {
  const [expanded, setExpanded] = useState(false);

  const onToggle = useCallback(() => setExpanded(e => !e), []);

  return (
    <div className={classNames('receipts__group', { '-expanded': expanded })}>
      <motion.header initial={false} onClick={onToggle}>
        <div className="group-header">
          <b className="group-title">
            {t('global.dates.months.' + (parseInt(receiptsElement.month, 10) + 1).toString()) +
              ' - ' +
              receiptsElement.year}
          </b>
          <Image className="group-toggle" src="/img/icons/dropdown_pink.svg" alt="" />
        </div>
      </motion.header>

      <AnimatePresence initial={false}>
        {expanded && (
          <motion.section
            key="content"
            initial="collapsed"
            animate="open"
            exit="collapsed"
            variants={{
              open: { opacity: 1, height: 'auto' },
              collapsed: { opacity: 0, height: 0 }
            }}
            transition={{ duration: 0.1 }}
          >
            {receiptsElement.receiptsValues.map(receipt => (
              <Link
                key={receipt.pathFile}
                to={{ pathname: '/terms', state: { url: receipt.pathFile, title: receipt.title } }}
              >
                <button className="download-btn" type="button">
                  <span className="download-btn__label">
                    {receipt.title || t('screens.Profile.receipts.title')}
                  </span>
                  <div className="download-btn__img">
                    <img
                      src="/img/icons/download.svg"
                      alt={receipt.title || t('screens.Profile.receipts.title')}
                    />
                  </div>
                </button>
              </Link>
            ))}
          </motion.section>
        )}
      </AnimatePresence>
    </div>
  );
}

GroupReceipts.propTypes = {
  t: PropTypes.func.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  receiptsElement: PropTypes.object.isRequired
};

export default withTranslation()(GroupReceipts);

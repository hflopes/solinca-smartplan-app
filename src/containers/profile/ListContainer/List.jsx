import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { useSelector } from 'react-redux';
import { IonLoading } from '@ionic/react';
import _ from 'lodash';
import { withTranslation } from 'react-i18next';
import { getUserReceipts, isFetchingUserReceipts } from '../../../redux/selectors/receipts';
import withBackNavigation from '../../../components/hoc/withBackNavigation';
import GroupReceipts from './components/GroupReceipts';
import './List.scss';

function onScroll() {
  const { scrollTop } = document.getElementById('list-scroll');
  const scrollPercent = Math.min(1, scrollTop / 20);

  const nav = document.getElementById('clear-nav');
  nav.style.opacity = 1 - scrollPercent;
}

function List({ t }) {
  const loadingReceipts = useSelector(state => isFetchingUserReceipts(state));
  const receipts = useSelector(state => getUserReceipts(state));

  // Variables to use in the creation of the groupReceipts object
  let groupReceipts = {};

  // Group all the receipts in an Object by month
  // We need to show them by month on the screen,m so this will really help out
  groupReceipts = _.groupBy(receipts, element => {
    return element.date.getMonth();
  });

  // Reverse the order of the object (this one is tricky)
  // Chain the Lodash methods and use map for traverse the object.
  // For each of the elements, create a new object with:
  //    month - The value to be used and shown on the accordion title
  //    receiptsValue - The object with all the receipt values
  //    year - Get the year from the first element (they are all from the same year)
  // After that, order the object by month in descending order.
  // Finish it off by returning an array with the specefied order and data
  const reversedGroupReceipts = _.chain(groupReceipts)
    .map((currentValue, index) => ({
      month: index,
      receiptsValues: currentValue,
      year: currentValue[0].date.getFullYear()
    }))
    .orderBy(group => Number(group.month), ['desc'])
    .value();

  return (
    <>
      <IonLoading
        isOpen={loadingReceipts}
        message={t('loading.receipts')}
        spinner="crescent"
        mode="ios"
        cssClass="loading"
      />
      {loadingReceipts ? null : (
        <div id="list-scroll" className="list__container" onScroll={onScroll}>
          {receipts.length ? (
            <div className="list-list">
              {reversedGroupReceipts.map(receiptsElement => (
                <GroupReceipts receiptsElement={receiptsElement} />
              ))}
            </div>
          ) : (
            <h2>{t('screens.Profile.receipts.empty')}</h2>
          )}
        </div>
      )}
    </>
  );
}

List.propTypes = {
  t: PropTypes.func.isRequired
};

export default compose(withTranslation(), withBackNavigation('dark'))(List);

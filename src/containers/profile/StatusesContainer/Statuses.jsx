import React, { useState, useCallback } from 'react';
import className from 'classnames';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { motion, AnimatePresence } from 'framer-motion';
import STATUSES from '../../../config/statuses';
import withBackNavigation from '../../../components/hoc/withBackNavigation';
import './Statuses.scss';

function getStatuses(t) {
  return STATUSES.map(s => {
    return { ...t(`screens.Statuses.${s}`, { returnObjects: true }), icon: `/img/big_icons/status_${s}.svg` };
  });
}

function onScroll() {
  const { scrollTop } = document.getElementById('statuses-scroll');
  const scrollPercent = Math.min(1, scrollTop / 40);

  const nav = document.getElementById('clear-nav');
  nav.style.opacity = 1 - scrollPercent;
}

function Status({ status, isFirst, isLast }) {
  const [isExpanded, setIsExpanded] = useState(false);

  const onCardToggled = useCallback(() => setIsExpanded(!isExpanded), [isExpanded]);

  return (
    <>
      <button
        type="button"
        className={className('status-card', { '-simple': !status.description })}
        onClick={onCardToggled}
      >
        <img className="status-card__badge" src={status.icon} alt="" />
        <div className="status-card__content">
          <b className="status-title">{status.title}</b>

          {!isFirst && (
            <AnimatePresence initial={false}>
              <motion.p
                className={className('status-text', { '-collapsed': !isExpanded })}
                animate={isExpanded ? 'expanded' : 'collapsed'}
                variants={{
                  expanded: { height: 'auto' },
                  collapsed: { height: 100 }
                }}
                transition={{ duration: 0.3 }}
              >
                {status.description}
              </motion.p>
            </AnimatePresence>
          )}
        </div>
      </button>
      {!isLast && <div className="dashed-line" />}
    </>
  );
}

function Statuses() {
  const { t } = useTranslation();
  const statuses = getStatuses(t);

  return (
    <div id="statuses-scroll" className="statuses__container" onScroll={onScroll}>
      <b className="statuses__title">{t('screens.Statuses.title')}</b>
      <p className="statuses__text">{t('screens.Statuses.description')}</p>
      <div className="statuses__list">
        {statuses.map((s, i) => (
          <Status
            key={s.title}
            status={s}
            isFirst={i === 0}
            isLast={i >= statuses.length - 1}
            isExpanded={Math.random() < 0.5}
          />
        ))}
      </div>
    </div>
  );
}

Status.propTypes = {
  isFirst: PropTypes.bool.isRequired,
  isLast: PropTypes.bool.isRequired,
  status: PropTypes.shape({
    icon: PropTypes.string,
    title: PropTypes.string,
    description: PropTypes.string
  }).isRequired
};

export default withBackNavigation('light')(Statuses);

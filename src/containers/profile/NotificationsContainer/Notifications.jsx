import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';
import { IonList, IonLoading } from '@ionic/react';
import NotificationToggle from '../../../components/toggles/Notification';
import changeNotificationAlert from '../../../redux/modules/user/actions/changeNotificationAlerts';
import './Notifications.scss';

class NotificationsContainer extends Component {
  onChange = (type, checked) => {
    const { notifications, changeAlert } = this.props;
    const matches = notifications.filter(n => n.notificationType === type);

    if (!matches || !matches.length || matches[0].value === checked) {
      return;
    }

    changeAlert({ notificationType: type, value: checked });
  };

  render() {
    const { notifications, isUpdatingNotification, t } = this.props;

    return (
      <div className="profile-notifications">
        <IonLoading
          isOpen={isUpdatingNotification}
          onDidDismiss={this.toggleLoading}
          message={t('global.terms.loading')}
        />
        <h5>{t('screens.Profile.notifications.title')}</h5>
        <p>{t('screens.Profile.notifications.description')}</p>
        <IonList>
          {notifications
            .filter(n => n.notificationType !== 'period')
            .map(({ notificationType, value, title, description }) => (
              <NotificationToggle
                key={notificationType}
                label={title}
                description={description}
                type={notificationType}
                checked={value}
                onChange={({ detail }) => this.onChange(detail.value, detail.checked)}
              />
            ))}
        </IonList>
      </div>
    );
  }
}

NotificationsContainer.propTypes = {
  notifications: PropTypes.arrayOf(
    PropTypes.shape({
      type: PropTypes.string,
      value: PropTypes.bool
    })
  ).isRequired,
  isUpdatingNotification: PropTypes.bool.isRequired,
  changeAlert: PropTypes.func.isRequired,
  t: PropTypes.func.isRequired
};

const mapStateToProps = state => {
  const { notifications, isUpdatingNotification } = state.user;
  return { notifications, isUpdatingNotification };
};

const mapDispatchToProps = {
  changeAlert: changeNotificationAlert
};

export default compose(
  withTranslation(),
  connect(mapStateToProps, mapDispatchToProps)
)(NotificationsContainer);

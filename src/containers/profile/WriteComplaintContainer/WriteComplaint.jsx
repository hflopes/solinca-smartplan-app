import React, { useState, useCallback, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
import TextArea from '../../../components/inputs/TextArea';
import PrimaryButton from '../../../components/buttons/PrimaryButton';
import { getUserContactData } from '../../../redux/selectors/user';
import sendTicketAction from '../../../redux/modules/support/actions/sendTicket';
import './WriteComplaint.scss';

function WriteComplaint({ onClosed }) {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const [message, setMessage] = useState('');

  const contactData = useSelector(state => getUserContactData(state));

  function handleSubmit(e) {
    if (!e) {
      return;
    }

    e.preventDefault();

    const { name, email, phone, defaultGym } = contactData;
    const motive = 'Reclamações';
    const ticketBody = { name, email, phone, message, gymId: defaultGym, motive };

    dispatch(sendTicketAction(ticketBody))
      .then(() => handleClose())
      .catch(() => {});
  }

  function handleMessageChanged(evt) {
    const { value } = evt.target;
    setMessage(value);
  }

  function handleClose() {
    setMessage('');
    onClosed();
  }

  const onSubmit = useCallback(handleSubmit, [message]);
  const onMessageChanged = useCallback(handleMessageChanged, [message]);
  const onCloseClicked = useCallback(handleClose, [onClosed]);

  const isDisabled = useMemo(() => message.length === 0, [message]);

  return (
    <form className="write-complaint__container" onSubmit={onSubmit}>
      <button className="close-button" type="button" onClick={onCloseClicked}>
        <img src="/img/icons/clear.svg" alt="" />
      </button>

      <div className="input-wrapper">
        <div className="subject">
          <span className="subject__label">{t('screens.Contact.motiveSelection')}</span>
          <b className="subject__value">{t('screens.Contact.motives.complaint')}</b>
        </div>
        <TextArea placeholder="Mensagem" onChange={onMessageChanged} initialValue="" darkMode />
      </div>

      <PrimaryButton
        text={t('global.actions.confirm')}
        icon="go_white"
        type="submit"
        isDisabled={isDisabled}
      />
    </form>
  );
}

WriteComplaint.propTypes = {
  onClosed: PropTypes.func.isRequired
};

export default WriteComplaint;

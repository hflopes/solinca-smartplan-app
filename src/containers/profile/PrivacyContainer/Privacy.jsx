import React from 'react';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { getTOS } from '../../../redux/selectors/terms';
import './Privacy.scss';

function Privacy() {
  const tos = useSelector(state => getTOS(state));

  if (!tos || !tos.files) {
    return null;
  }

  return (
    <div className="privacy__container">
      {tos.files.map(file => (
        <Link key={file.url} to={{ pathname: '/terms', state: { ...file } }}>
          <button className="privacy-btn" type="button">
            <span className="privacy-btn__label">{file.title}</span>
            <div className="privacy-btn__img">
              <img src="/img/icons/pdf.svg" alt={file.title} />
            </div>
          </button>
        </Link>
      ))}
    </div>
  );
}

export default Privacy;

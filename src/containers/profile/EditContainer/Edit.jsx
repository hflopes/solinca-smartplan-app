import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { withTranslation } from 'react-i18next';
import { withRouter } from 'react-router';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { IonContent, IonGrid, IonRow } from '@ionic/react';
import Image from '../../../components/Image';
import TextInput from '../../../components/inputs/Text';
import TextArea from '../../../components/inputs/TextArea';
import PrimaryButton from '../../../components/buttons/PrimaryButton';
import updateUserInfo from '../../../redux/modules/user/actions/updateInfo';
import updateUserPassword from '../../../redux/modules/user/actions/updatePassword';
import toggleKeyboardAction from '../../../redux/modules/device/actions/toggleKeyboard';
import { keyboardIsUp } from '../../../redux/selectors/device';
import './Edit.scss';

const getInputType = fieldType => {
  switch (fieldType) {
    case 'email':
      return 'email';

    case 'phone':
      return 'tel';

    default:
      return 'text';
  }
};

class EditContainer extends Component {
  state = {
    value: '',
    currentPassword: '',
    newPassword: '',
    newPasswordConfirmation: '',
    updated: false,
    redirect: false
  };

  componentDidMount() {
    const { location } = this.props;
    const { state } = location;
    const { value } = state;

    this.setState({ value });
  }

  componentDidUpdate(prevProps) {
    const { isUpdatingInfo: wasUpdating } = prevProps;
    const { isUpdatingInfo } = this.props;
    if (wasUpdating && !isUpdatingInfo) {
      this.triggerRedirect();
    }

    const { isUpdatingPassword: wasUpdatingPassword } = prevProps;
    const { isUpdatingPassword, updatedPassword } = this.props;
    if (wasUpdatingPassword && !isUpdatingPassword && updatedPassword) {
      this.triggerRedirect();
    }
  }

  triggerRedirect = () => {
    this.setState({ redirect: true });
  };

  onValueChanged = evt => {
    const { value } = evt.target;
    this.setState({ value, updated: true });
  };

  onCurrentPasswordValueChanged = evt => {
    const { value } = evt.target;
    this.setState({ currentPassword: value }, this.checkPasswordButtonValid);
  };

  onNewPasswordValueChanged = evt => {
    const { value } = evt.target;
    this.setState({ newPassword: value }, this.checkPasswordButtonValid);
  };

  onNewPasswordConfirmationValueChanged = evt => {
    const { value } = evt.target;
    this.setState({ newPasswordConfirmation: value }, this.checkPasswordButtonValid);
  };

  checkPasswordButtonValid = () => {
    const { currentPassword, newPassword, newPasswordConfirmation } = this.state;

    this.setState({
      updated: Boolean(
        currentPassword && newPassword && newPasswordConfirmation && newPassword === newPasswordConfirmation
      )
    });
  };

  onUpdate = () => {
    const { value } = this.state;
    const { updateInfo, match } = this.props;
    const { params } = match;
    const { type } = params;
    updateInfo({ field: type, value });
  };

  onUpdatePassword = () => {
    const { updatePassword } = this.props;
    const { currentPassword, newPassword, newPasswordConfirmation } = this.state;
    updatePassword({ currentPassword, newPassword, newPasswordConfirmation });
  };

  onSubmit = () => {
    const { match } = this.props;
    const { params } = match;
    const { type } = params;

    if (type === 'password') {
      this.onUpdatePassword();
    } else {
      this.onUpdate();
    }
  };

  renderInputPassword = () => {
    const { t, toggleKeyboard } = this.props;
    return (
      <div className="passwordEdit">
        <TextInput
          type="password"
          placeholder={t('screens.Profile.info.labels.currentPassword')}
          onChange={this.onCurrentPasswordValueChanged}
          onFocus={() => {
            toggleKeyboard({ isKeyboardUp: true });
          }}
          onBlur={() => {
            toggleKeyboard({ isKeyboardUp: false });
          }}
        />
        <TextInput
          type="password"
          placeholder={t('screens.Profile.info.labels.newPassword')}
          onChange={this.onNewPasswordValueChanged}
          onFocus={() => {
            toggleKeyboard({ isKeyboardUp: true });
          }}
          onBlur={() => {
            toggleKeyboard({ isKeyboardUp: false });
          }}
        />
        <TextInput
          type="password"
          placeholder={t('screens.Profile.info.labels.newPasswordConfirmation')}
          onChange={this.onNewPasswordConfirmationValueChanged}
          onFocus={() => {
            toggleKeyboard({ isKeyboardUp: true });
          }}
          onBlur={() => {
            toggleKeyboard({ isKeyboardUp: false });
          }}
        />
      </div>
    );
  };

  renderInput = () => {
    const { t, toggleKeyboard, match, location } = this.props;
    const { params } = match;
    const { state } = location;
    const { type } = params;
    const { value } = state;

    if (type === 'address') {
      return (
        <TextArea
          initialValue={type !== 'password' ? value : null}
          placeholder={t(`screens.Profile.info.labels.${type}`)}
          onChange={this.onValueChanged}
          onFocus={() => toggleKeyboard({ isKeyboardUp: true })}
          onBlur={() => toggleKeyboard({ isKeyboardUp: false })}
          autoFocus
        />
      );
    }

    return (
      <TextInput
        initialValue={type !== 'password' ? value : null}
        type={getInputType(type)}
        placeholder={t(`screens.Profile.info.labels.${type}`)}
        onChange={this.onValueChanged}
        onFocus={() => toggleKeyboard({ isKeyboardUp: true })}
        onBlur={() => toggleKeyboard({ isKeyboardUp: false })}
        autoFocus
      />
    );
  };

  renderButton = () => {
    const { updated } = this.state;
    const { t, isUpdatingInfo, isUpdatingPassword, match } = this.props;
    const { params } = match;
    const { type } = params;

    return (
      <PrimaryButton
        text={t(`screens.Profile.edit.buttons.${type}`)}
        isDisabled={!updated}
        isLoading={isUpdatingInfo || isUpdatingPassword}
        onClick={this.onSubmit}
      />
    );
  };

  render() {
    const { redirect } = this.state;
    const { isKeyboardUp, match, updatedEmail } = this.props;
    const { params } = match;
    const { type } = params;
    const keyboardClass = isKeyboardUp ? 'keyboard' : '';

    if (updatedEmail) {
      return <Redirect to="/logout" />;
    }

    if (redirect) {
      return <Redirect to="/profile" />;
    }

    return (
      <IonContent forceOverscroll={false}>
        <div className={`edit-profile-page full-height-flex ${keyboardClass}`}>
          <IonGrid className="full-height-flex ion-justify-content-between full-width">
            <form onSubmit={this.onSubmit}>
              <IonRow>
                <button className="close" type="button" onClick={this.triggerRedirect}>
                  <Image alt="X" src="/img/icons/clear.svg" />
                </button>
              </IonRow>
              {type === 'password' ? (
                <IonRow>{this.renderInputPassword()}</IonRow>
              ) : (
                <IonRow>{this.renderInput()}</IonRow>
              )}
              <IonRow>{this.renderButton()}</IonRow>
            </form>
          </IonGrid>
        </div>
      </IonContent>
    );
  }
}

EditContainer.propTypes = {
  t: PropTypes.func.isRequired,
  isKeyboardUp: PropTypes.bool.isRequired,
  isUpdatingInfo: PropTypes.bool.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      type: PropTypes.string
    })
  }).isRequired,
  location: PropTypes.shape({
    state: PropTypes.shape({
      value: PropTypes.string
    })
  }).isRequired,
  toggleKeyboard: PropTypes.func.isRequired,
  updatePassword: PropTypes.func.isRequired,
  updatedPassword: PropTypes.bool.isRequired,
  updatedEmail: PropTypes.bool.isRequired,
  isUpdatingPassword: PropTypes.bool.isRequired,
  updateInfo: PropTypes.func.isRequired
};

const mapStateToProps = state => {
  const isKeyboardUp = keyboardIsUp(state);
  const { isUpdatingInfo, isUpdatingPassword, updatedPassword, updatedEmail } = state.user;
  return { isKeyboardUp, isUpdatingInfo, isUpdatingPassword, updatedPassword, updatedEmail };
};

const mapDispatchToProps = {
  toggleKeyboard: toggleKeyboardAction,
  updateInfo: updateUserInfo,
  updatePassword: updateUserPassword
};

export default compose(
  withRouter,
  withTranslation(),
  connect(mapStateToProps, mapDispatchToProps)
)(EditContainer);

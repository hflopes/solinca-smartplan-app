import React from 'react';
import _ from 'lodash';
import { useSelector } from 'react-redux';
import InfoDetail from '../../../components/InfoDetail';
import { getBiometricData } from '../../../redux/selectors/plan';
import './Biometric.scss';

function Biometric() {
  const biometricData = useSelector(state => getBiometricData(state));

  return (
    <div className="profile-biometric">
      {_.map(biometricData, (value, key) => (
        <InfoDetail key={key} label={key} value={value} />
      ))}
    </div>
  );
}

export default Biometric;

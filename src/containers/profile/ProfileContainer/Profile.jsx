import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';
import classNames from 'classnames';
import SwipeableViews from 'react-swipeable-views';
import Fab from '../../../components/buttons/FabButton';
import ProfileHeader from '../../../components/headers/ProfileHeader';
import InfoContainer from '../InfoContainer';
// import NotificationsContainer from '../NotificationsContainer';
import BiometricContainer from '../BiometricContainer';
import PrivacyContainer from '../PrivacyContainer';
import ComplaintsContainer from '../ComplaintsContainer';
import WriteComplaintContainer from '../WriteComplaintContainer';
import { getFavoriteClasses, getBookedClasses } from '../../../redux/selectors/classes';
import { getGyms } from '../../../redux/selectors/gyms';
import withMenuNavigation from '../../../components/hoc/withMenuNavigation';
import withRouteState from '../../../components/hoc/withRouteState';
import './Profile.scss';

class Profile extends Component {
  constructor(props) {
    super(props);

    const { lastState } = props;

    this.state = {
      selectedSection: lastState.selectedSection || 0,
      isModalOpen: false,
      tabs: this.loadTabs()
    };
  }

  componentDidMount() {
    const { lastState } = this.props;
    const { contentScrollY } = lastState;

    if (!lastState) {
      return;
    }

    if (contentScrollY) {
      const element = document.getElementById('profile');
      element.scrollTo({ top: contentScrollY, left: 0, behaviour: 'smooth' });
    }
  }

  componentWillUnmount() {
    const { selectedSection } = this.state;
    const { saveState } = this.props;
    const element = document.getElementById('profile');
    const contentScrollY = element.scrollTop;

    const currentState = { selectedSection, contentScrollY };

    saveState(currentState);
  }

  loadTabs = () => {
    const { t } = this.props;

    return t('screens.Profile.tabs', { returnObjects: true });
  };

  selectSection = index => {
    this.setState({ selectedSection: index });
  };

  closeModal = () => {
    this.setState({ isModalOpen: false });
  };

  openModal = () => {
    this.setState({ isModalOpen: true });
  };

  onScroll = () => {
    const content = document.getElementById('profile');
    const header = document.getElementById('profile-header');
    const headerWrapper = document.getElementById('profile-header-wrapper');

    const { scrollTop } = content;

    const yTranslate = Math.min(scrollTop, 340);
    const scrollPercent = scrollTop / 340;

    header.style.transform = `translate3d(0, -${yTranslate}px, 0)`;
    headerWrapper.style.opacity = 1 - scrollPercent;
  };

  render() {
    const { tabs, selectedSection, isModalOpen } = this.state;

    return (
      <div id="profile" className="profile__container full-height-flex" onScroll={this.onScroll}>
        <ProfileHeader defaultIndex={selectedSection} onSelect={this.selectSection} tabs={tabs} />

        <div className="profile__content">
          <SwipeableViews index={selectedSection} disabled>
            <InfoContainer />
            <BiometricContainer />
            {/* <NotificationsContainer /> */}
            <PrivacyContainer />
            <ComplaintsContainer />
          </SwipeableViews>
        </div>

        <div className={classNames('fab-wrapper', { '-visible': selectedSection === 3 })}>
          <Fab icon="message" onClick={this.openModal} isDark />
        </div>

        <div className={classNames('write-complaint-modal', { '-visible': isModalOpen })}>
          <WriteComplaintContainer onClosed={this.closeModal} />
        </div>
      </div>
    );
  }
}

Profile.propTypes = {
  bookedClasses: PropTypes.shape({}).isRequired,
  favoriteClasses: PropTypes.shape({}).isRequired,
  t: PropTypes.func.isRequired,
  saveState: PropTypes.func.isRequired,
  lastState: PropTypes.shape({
    selectedSection: PropTypes.number,
    contentScrollY: PropTypes.number
  }).isRequired
};

// Format a list of classes into the following format:
// {
//    82: {gym: {..gymData}, classes: [...]}
//    17: {gym: {..gymData}, classes: [...]}
// }
function transform(classes, gyms) {
  const values = Object.values(classes);
  const obj = {};

  values.forEach(e => {
    if (obj[e.gymId]) {
      obj[e.gymId].classes.push(e);
    } else {
      obj[e.gymId] = {
        gym: gyms[e.gymId],
        classes: [e]
      };
    }
  });

  return obj;
}

const mapStateToProps = state => {
  const gyms = getGyms(state);
  const bookedClasses = getBookedClasses(state);
  const favoriteClasses = getFavoriteClasses(state);

  if (!gyms || Object.keys(gyms).length === 0) {
    return { bookedClasses: {}, favoriteClasses: {} };
  }

  return {
    bookedClasses: transform(bookedClasses, gyms),
    favoriteClasses: transform(favoriteClasses, gyms)
  };
};

export default compose(
  withTranslation(),
  connect(mapStateToProps),
  withMenuNavigation(),
  withRouteState('Profile')
)(Profile);

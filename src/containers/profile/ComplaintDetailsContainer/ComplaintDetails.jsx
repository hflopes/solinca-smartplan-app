import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import withBackNavigation from '../../../components/hoc/withBackNavigation';
import { getComplaints } from '../../../redux/selectors/complaints';
import { getUserFirstName } from '../../../redux/selectors/user';
import readComplaint from '../../../redux/modules/complaints/actions/read';
import './ComplaintDetails.scss';

function onScroll() {
  const { scrollTop } = document.getElementById('complaint-scroll');
  const scrollPercent = Math.min(1, scrollTop / 20);

  const nav = document.getElementById('clear-nav');
  nav.style.opacity = 1 - scrollPercent;
}

function ComplaintDetails({ id }) {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  useEffect(() => {
    return () => {
      dispatch(readComplaint(id));
    };
  }, [dispatch, id]);
  const userFirstName = useSelector(state => getUserFirstName(state));
  const complaints = useSelector(state => getComplaints(state));
  const complaint = complaints[id];

  if (!complaint) {
    return null;
  }

  const { replies } = complaint;
  return (
    <div id="complaint-scroll" className="complaint-thread__container" onScroll={onScroll}>
      <div className="white-cover" />

      {replies.map(reply => {
        const { date, message } = reply;
        const dateFormated = date ? moment(date).format('DD/MM/YY') : moment().format('DD/MM/YY');
        return (
          <div className="complaint__container">
            <div className="complaint__header">
              <div className="top">
                <span className="date">{dateFormated}</span>
              </div>
              <div className="bottom">
                <span className="destination-label">{t('screens.Complaint.to')}</span>
                <span className="destination-name">{userFirstName}</span>
              </div>
            </div>
            <div className="complaint__body">
              <p className="complaint__message">{message}</p>
            </div>
          </div>
        );
      })}
      {!replies.length ? <p className="complaint__empty">{t('screens.Complaint.empty')}</p> : null}
    </div>
  );
}

ComplaintDetails.propTypes = {
  id: PropTypes.string.isRequired
};

export default withBackNavigation('dark')(ComplaintDetails);

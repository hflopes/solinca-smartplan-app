import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';
import _ from 'lodash';
import './Complaints.scss';
import { getLatestComplaints } from '../../../redux/selectors/complaints';
import fetchComplaints from '../../../redux/modules/complaints/actions/fetch';

function Complaint({ isUnread, isReplied, date, title, message }) {
  function status() {
    if (isUnread) {
      return (
        <div className="complaint__status">
          <div className="dot-status" />
        </div>
      );
    }

    if (!isReplied) {
      return (
        <div className="complaint__status">
          <img className="img-status" src="/img/icons/arrow_down_gray.svg" alt="" />
        </div>
      );
    }

    return <div className="complaint__status" />;
  }

  return (
    <div className="complaint">
      <div className="complaint__header">
        {status()}
        <b className="complaint__title">{title}</b>
        <span className="complaint__date">{moment(date).format('DD/MM/YY')}</span>
        <img className="complaint__more" src="/img/icons/dropdown_gray.svg" alt="" />
      </div>
      <div className="complaint__body">{message}</div>
    </div>
  );
}

Complaint.defaultProps = {
  isUnread: true,
  isReplied: false,
  date: new Date(),
  message: '',
  title: ''
};

Complaint.propTypes = {
  isUnread: PropTypes.bool,
  isReplied: PropTypes.bool,
  date: PropTypes.instanceOf(Date),
  message: PropTypes.string,
  title: PropTypes.string
};

function Complaints() {
  const complaints = useSelector(state => getLatestComplaints(state));
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchComplaints());
  }, [dispatch]);
  return (
    <div className="complaints__container">
      <div className="complaints-list">
        {_.orderBy(complaints, ['date'], ['desc']).map(c => (
          <Link key={c.id} to={`/profile/complaints/${c.id}`}>
            <Complaint {...c} />
          </Link>
        ))}
      </div>
    </div>
  );
}

export default Complaints;

import React, { useState } from 'react';
import { useDispatch, connect } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { useTranslation } from 'react-i18next';
import Input from '../../../components/inputs/Text';
import Area from '../../../components/inputs/TextArea';
import Avatar from '../../../components/Avatar';
import PrimaryButton from '../../../components/buttons/PrimaryButton';
import withMenuNavigation from '../../../components/hoc/withMenuNavigation';
import SlidingList from '../../../components/SlidingList';
import renewPlanAction from '../../../redux/modules/support/actions/renewPlan';
import { getUserName, getUserEmail } from '../../../redux/selectors/user';
import './Renew.scss';

function TextInput({ label, type, onChange, initialValue }) {
  return (
    <div className="row">
      <span className="label">{label}</span>
      <Input
        onChange={e => onChange(e.target.value)}
        type={type}
        initialValue={initialValue}
        disabled={Boolean(initialValue)}
      />
    </div>
  );
}

function TextArea({ label, type, onChange }) {
  return (
    <div className="row">
      <span className="label">{label}</span>
      <Area onChange={e => onChange(e.target.value)} type={type} />
    </div>
  );
}

function Selector({ label, options, selectedIndex, onSelect }) {
  const [showing, setShowing] = useState(false);
  const { t } = useTranslation();

  const copy = { listTitle: label, selectedTitle: '', optionsTitle: '' };

  return (
    <>
      <div className="row">
        <span className="label">{label}</span>
        <button className="list-toggle" type="button" onClick={() => setShowing(true)} tabIndex="-1">
          <b className="list-toggle__label">
            {selectedIndex > -1 ? options[selectedIndex].title : t('screens.PlanRenew.selectOption')}
          </b>
          <img className="list-toggle__icon" src="/img/icons/dropdown_pink.svg" alt="" />
        </button>
      </div>
      <SlidingList
        list={options}
        selectedIndex={selectedIndex}
        copy={copy}
        isShowing={showing}
        onSelect={index => {
          onSelect(index);
          setShowing(false);
        }}
        onDismiss={() => setShowing(false)}
      />
    </>
  );
}

function SelectorSpecific({ label, options, triggerIndex, selectedIndex, onSelect, onChange }) {
  const [showing, setShowing] = useState(false);
  const { t } = useTranslation();

  const copy = { listTitle: label, selectedTitle: '', optionsTitle: '' };
  const triggerWord = options.length === 2 ? t('screens.PlanRenew.yes') : t('screens.PlanRenew.other');

  return (
    <>
      <div className="row">
        <span className="label">{label}</span>
        <button className="list-toggle" type="button" onClick={() => setShowing(true)} tabIndex="-1">
          <b className="list-toggle__label">
            {selectedIndex > -1 ? options[selectedIndex].title : t('screens.PlanRenew.selectOption')}
          </b>
          <img className="list-toggle__icon" src="/img/icons/dropdown_pink.svg" alt="" />
        </button>
        <br />
        <span className="label -specific">{t('screens.PlanRenew.selectOptionWhy', { triggerWord })}</span>
        <Area onChange={e => onChange(e.target.value)} disabled={triggerIndex !== selectedIndex} />
      </div>
      <SlidingList
        list={options}
        selectedIndex={selectedIndex}
        copy={copy}
        isShowing={showing}
        onSelect={index => {
          onSelect(index);
          setShowing(false);
        }}
        onDismiss={() => setShowing(false)}
      />
    </>
  );
}

function Renew({ userName, userEmail }) {
  const dispatch = useDispatch();
  const router = useHistory();
  const { t } = useTranslation();

  const [submitting, setSubmitting] = useState(false);
  const [name] = useState(userName);
  const [email] = useState(userEmail);
  const [age, setAge] = useState('');
  const [genderIndex, serGenderIndex] = useState(-1);
  const [weight, setWeight] = useState('');
  const [height, setHeight] = useState('');
  const [objectiveIndex, setObjectiveIndex] = useState(-1);
  const [objective, setObjective] = useState('');
  const [activityIndex, setActivityIndex] = useState(-1);
  const [frequency, setFrequency] = useState('');
  const [duration, setDuration] = useState('');
  const [locationIndex, setLocationIndex] = useState(-1);
  const [preferred, setPreferred] = useState('');
  const [leastPreferred, setLeastPreferred] = useState('');
  const [equipment, setEquipment] = useState('');
  const [limitationsIndex, setLimitationsIndex] = useState(-1);
  const [limitations, setLimitations] = useState('');
  const [healthIndex, setHealthIndex] = useState(-1);
  const [health, setHealth] = useState('');
  const [medicationIndex, setMedicationIndex] = useState(-1);
  const [medication, setMedication] = useState('');
  const [motivatedIndex, setMotivatedIndex] = useState(-1);

  const OBJECTIVE_OPTIONS = t('screens.PlanRenew.options.objectives', { returnObjects: true });
  const YES_NO_OPTIONS = t('screens.PlanRenew.options.yesNo', { returnObjects: true });
  const GENDER_OPTIONS = t('screens.PlanRenew.options.gender', { returnObjects: true });
  const LOCATION_OPTIONS = t('screens.PlanRenew.options.location', { returnObjects: true });
  const ACTIVITY_OPTIONS = t('screens.PlanRenew.options.activity', { returnObjects: true });

  const isDisabled =
    name.length === 0 ||
    email.length === 0 ||
    age.length === 0 ||
    genderIndex === -1 ||
    weight.length === 0 ||
    height.length === 0 ||
    objectiveIndex === -1 ||
    activityIndex === -1 ||
    frequency.length === 0 ||
    duration.length === 0 ||
    locationIndex === -1 ||
    preferred.length === 0 ||
    leastPreferred.length === 0 ||
    equipment.length === 0 ||
    limitationsIndex === -1 ||
    healthIndex === -1 ||
    medicationIndex === -1 ||
    motivatedIndex === -1;

  async function handleSubmit(e) {
    if (!e) return;
    e.preventDefault();

    const payload = {
      name,
      email,
      age,
      gender: genderIndex === -1 ? null : GENDER_OPTIONS[genderIndex].title,
      weight,
      height,
      objective:
        objectiveIndex === -1 || objectiveIndex === 4 ? objective : OBJECTIVE_OPTIONS[objectiveIndex].title,
      activity: activityIndex === -1 ? null : ACTIVITY_OPTIONS[activityIndex].title,
      frequency,
      duration,
      location: locationIndex === -1 ? null : LOCATION_OPTIONS[locationIndex].title,
      preferred,
      leastPreferred,
      equipment,
      limitations: limitationsIndex < 1 ? limitations : t('screens.PlanRenew.no'),
      health: healthIndex < 1 ? health : t('screens.PlanRenew.no'),
      medication: medicationIndex < 1 ? medication : t('screens.PlanRenew.no'),
      motivated: motivatedIndex === -1 ? null : YES_NO_OPTIONS[motivatedIndex].title
    };

    try {
      setSubmitting(true);
      await dispatch(renewPlanAction(payload));
      setSubmitting(false);
      router.push('/home');
    } catch (error) {
      setSubmitting(false);
    }
  }

  return (
    <div className="plan-renew__container">
      <div className="renew-header">
        <div id="notch" />
        <Link to="/profile">
          <Avatar size={36} />
        </Link>
        <h1 className="renew-title">{t('screens.PlanRenew.title')}</h1>
      </div>
      <form onSubmit={handleSubmit}>
        <TextInput label={t('screens.PlanRenew.labels.age')} onChange={setAge} type="number" />
        <Selector
          label={t('screens.PlanRenew.labels.gender')}
          selectedIndex={genderIndex}
          onSelect={serGenderIndex}
          options={GENDER_OPTIONS}
        />
        <TextInput label={t('screens.PlanRenew.labels.weight')} onChange={setWeight} type="number" />
        <TextInput label={t('screens.PlanRenew.labels.height')} onChange={setHeight} type="number" />
        <SelectorSpecific
          label={t('screens.PlanRenew.labels.objective')}
          triggerIndex={4}
          selectedIndex={objectiveIndex}
          onSelect={setObjectiveIndex}
          onChange={setObjective}
          options={OBJECTIVE_OPTIONS}
        />
        <Selector
          label={t('screens.PlanRenew.labels.level')}
          selectedIndex={activityIndex}
          onSelect={setActivityIndex}
          options={ACTIVITY_OPTIONS}
        />
        <TextInput
          label={t('screens.PlanRenew.labels.weeklyWorkouts')}
          onChange={setFrequency}
          type="number"
        />
        <TextInput
          label={t('screens.PlanRenew.labels.workoutDuration')}
          onChange={setDuration}
          type="number"
        />
        <Selector
          label={t('screens.PlanRenew.labels.workoutPlace')}
          selectedIndex={locationIndex}
          onSelect={setLocationIndex}
          options={LOCATION_OPTIONS}
        />
        <TextArea label={t('screens.PlanRenew.labels.activitiesLike')} onChange={setPreferred} />
        <TextArea label={t('screens.PlanRenew.labels.activitiesDislike')} onChange={setLeastPreferred} />
        <SelectorSpecific
          label={t('screens.PlanRenew.labels.limitations')}
          triggerIndex={0}
          selectedIndex={limitationsIndex}
          onSelect={setLimitationsIndex}
          onChange={setLimitations}
          options={YES_NO_OPTIONS}
        />
        <SelectorSpecific
          label={t('screens.PlanRenew.labels.healthConditions')}
          triggerIndex={0}
          selectedIndex={healthIndex}
          onSelect={setHealthIndex}
          onChange={setHealth}
          options={YES_NO_OPTIONS}
        />
        <SelectorSpecific
          label={t('screens.PlanRenew.labels.medications')}
          triggerIndex={0}
          selectedIndex={medicationIndex}
          onSelect={setMedicationIndex}
          onChange={setMedication}
          options={YES_NO_OPTIONS}
        />
        <TextArea label={t('screens.PlanRenew.labels.equipment')} onChange={setEquipment} />
        <Selector
          label={t('screens.PlanRenew.labels.home')}
          selectedIndex={motivatedIndex}
          onSelect={setMotivatedIndex}
          options={YES_NO_OPTIONS}
        />
        <PrimaryButton
          text={t('global.actions.confirm')}
          type="submit"
          onClick={handleSubmit}
          isDisabled={isDisabled}
          isLoading={submitting}
        />
      </form>
    </div>
  );
}

SelectorSpecific.propTypes = {
  label: PropTypes.string.isRequired,
  options: PropTypes.arrayOf().isRequired,
  triggerIndex: PropTypes.number.isRequired,
  selectedIndex: PropTypes.number.isRequired,
  onSelect: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired
};

Selector.propTypes = {
  label: PropTypes.string.isRequired,
  options: PropTypes.arrayOf().isRequired,
  selectedIndex: PropTypes.number.isRequired,
  onSelect: PropTypes.func.isRequired
};

TextInput.defaultProps = {
  initialValue: ''
};

TextInput.propTypes = {
  label: PropTypes.string.isRequired,
  initialValue: PropTypes.string,
  type: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired
};

TextArea.propTypes = {
  label: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired
};

Renew.propTypes = {
  userName: PropTypes.string.isRequired,
  userEmail: PropTypes.string.isRequired
};

const mapStateToProps = state => {
  const userName = getUserName(state);
  const userEmail = getUserEmail(state);

  return { userEmail, userName };
};

export default compose(connect(mapStateToProps), withMenuNavigation())(Renew);

import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { IonContent } from '@ionic/react';
import { withTranslation } from 'react-i18next';
import withMenuNavigation from '../../../components/hoc/withMenuNavigation';
import './Error.scss';

function Error({ t, message }) {
  return (
    <>
      <IonContent forceOverscroll={false}>
        <div className="full-height-flex plan-error-container">
          <div className="wrapper">
            <h1>
              {t('screens.PlanError.title')}
              <b>{t('screens.PlanError.titleBold')}</b>
            </h1>
            <p>{message || t('screens.PlanError.introText')}</p>
          </div>
        </div>
      </IonContent>
    </>
  );
}

Error.propTypes = {
  message: PropTypes.string.isRequired,
  t: PropTypes.func.isRequired
};

export default compose(withTranslation(), withMenuNavigation())(Error);

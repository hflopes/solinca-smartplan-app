import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { IonContent } from '@ionic/react';
import { withTranslation } from 'react-i18next';
import { withRouter } from 'react-router-dom';
import classNames from 'classnames';
import _ from 'lodash';
import QuestionPanel from '../../../components/QuestionPanel';
import { setupQuestionnaire } from '../../../redux/modules/plan/actions/setup';
import PrimaryButton from '../../../components/buttons/PrimaryButton';
import withBackNavigation from '../../../components/hoc/withBackNavigation';
import { USER_TYPES, USER_FREQUENCIES, USER_OBJECTIVES } from '../../../redux/modules/plan/constants';
import withRouteState from '../../../components/hoc/withRouteState';
import './Questionnaire.scss';

const ANSWERS_POOL = [USER_TYPES, USER_FREQUENCIES, USER_OBJECTIVES];

class Questionnaire extends Component {
  constructor(props) {
    super(props);

    const { lastState } = props;

    const step = lastState.step || 0;
    const chosenIndexes = lastState.chosenIndexes || [];
    const selectedAnswerIndex = chosenIndexes.length === 0 ? -1 : chosenIndexes[step];

    this.state = {
      step,
      chosenIndexes,
      selectedAnswerIndex
    };
  }

  componentWillMount() {
    const { onNavigateBack } = this.props;
    onNavigateBack(() => {
      const { step, chosenIndexes } = this.state;
      if (step === 0) {
        return false;
      }

      this.setState({
        step: step - 1,
        selectedAnswerIndex: chosenIndexes[step - 1]
      });

      return true;
    });
  }

  componentWillUnmount() {
    const { step, chosenIndexes } = this.state;
    const { saveState, onNavigateBack } = this.props;

    const currentState = {
      step,
      chosenIndexes
    };

    saveState(currentState);
    onNavigateBack(null);
  }

  submitAnswer = () => {
    const { step, chosenIndexes, selectedAnswerIndex } = this.state;
    const newIndexes = [...chosenIndexes];
    newIndexes[step] = selectedAnswerIndex;

    const nextIndex = chosenIndexes.length > step + 1 ? chosenIndexes[step + 1] : -1;

    this.setState(
      {
        step: Math.min(step + 1, ANSWERS_POOL.length - 1),
        selectedAnswerIndex: nextIndex,
        chosenIndexes: newIndexes
      },
      () => {
        if (step + 1 === ANSWERS_POOL.length) {
          const { setupPlanQuestionnaire, history } = this.props;
          const literalAnsers = [];

          newIndexes.forEach((e, i) => {
            literalAnsers.push(Object.values(ANSWERS_POOL[i])[e].key);
          });

          const formattedAnswers = {
            userType: literalAnsers[0],
            userFrequency: literalAnsers[1],
            userObjective: literalAnsers[2]
          };

          setupPlanQuestionnaire(formattedAnswers);

          if (formattedAnswers.userObjective === USER_OBJECTIVES.BALANCE.key) {
            history.push('/plan/select/classes');
          } else {
            history.push('/plan/select/exercise-type');
          }
        }
      }
    );
  };

  selectAnswer = index => {
    this.setState({
      selectedAnswerIndex: index
    });
  };

  render() {
    const { t } = this.props;
    const { step, selectedAnswerIndex } = this.state;

    const containerClass = classNames({
      'questionnaire-container': true,
      'full-height-flex': true,
      'has-selected': selectedAnswerIndex > -1
    });

    return (
      <div id="questionnaire" className={containerClass}>
        <IonContent forceOverscroll={false}>
          <div className="question-container">
            <div id="notch" />
            <h1>{t(`screens.PlanSelect.questionnaire.${step}.question`)}</h1>
          </div>
          <div className="answers-container">
            <div className="answers-wrapper">
              <QuestionPanel
                title={t('screens.PlanSelect.selectAnswer')}
                answers={_.map(ANSWERS_POOL[step])}
                selectedAnswerIndex={selectedAnswerIndex}
                onSelect={this.selectAnswer}
              />
            </div>
          </div>
          <div className="submit-container">
            {selectedAnswerIndex > -1 && (
              <PrimaryButton
                text={t('global.actions.confirm')}
                icon="check_white"
                onClick={this.submitAnswer}
              />
            )}
          </div>
        </IonContent>
      </div>
    );
  }
}

Questionnaire.propTypes = {
  setupPlanQuestionnaire: PropTypes.func.isRequired,
  t: PropTypes.func.isRequired,
  onNavigateBack: PropTypes.func.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func
  }).isRequired,
  saveState: PropTypes.func.isRequired,
  lastState: PropTypes.shape({
    step: PropTypes.number,
    chosenIndexes: PropTypes.arrayOf(PropTypes.any)
  }).isRequired
};

const mapDispatchToProps = {
  setupPlanQuestionnaire: setupQuestionnaire
};

export default compose(
  withTranslation(),
  withRouter,
  withBackNavigation(),
  connect(null, mapDispatchToProps),
  withRouteState('Questionnaire')
)(Questionnaire);

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { Link, Redirect, withRouter } from 'react-router-dom';
import { IonGrid, IonRow, IonAlert, IonContent } from '@ionic/react';
import { withTranslation } from 'react-i18next';
import scrollIntoView from 'smooth-scroll-into-view-if-needed';
import moment from 'moment';
import Avatar from '../../../components/Avatar';
import BadgeSwiper from '../../../components/BadgeSwiper';
import ExerciseCard from '../../../components/cards/ExerciseCard';
import StretchesCard from '../../../components/cards/StretchesCard';
import withMenuNavigation from '../../../components/hoc/withMenuNavigation';
import ExerciseTimer from '../../../components/ExerciseTimer';
import PrimaryButton from '../../../components/buttons/PrimaryButton';
import {
  completeExercise,
  cancelExercise,
  startRest,
  completeRest,
  forceCompletePlan,
  cancelRest
} from '../../../redux/modules/plan/actions/execute';
import selectPlanAction, {selectPlanOld as selectPlanOldAction} from '../../../redux/modules/plan/actions/select';
import { getPlatform } from '../../../redux/selectors/app';
import {
  hasFailedToFetchPlan,
  hasPlan,
  hasErrorCoach,
  getCurrentExercises,
  isExecutingExercise,
  isExecutingPlan,
  isResting,
  getProgress,
  getNextExercise,
  getCurrentSessionExercise,
  getCurrentSession,
  getCompletedExercises,
  getCurrentPlan,
  getWeekPlans,
  isLoadingPlans,
  getPlans
} from '../../../redux/selectors/plan';
import Image from '../../../components/Image';
import Fab from '../../../components/buttons/FabButton';
import ScrollAnchor from '../../../components/ScrollAnchor';
import ObjectiveLabel from '../../../components/ObjectiveLabel';
import './PlanHome.scss';

const REST_TIME = 1; // in minutes

const SWIPES = t => [
  {
    title: t('screens.PlanHome.swipes.waterTitle'),
    description: t('screens.PlanHome.swipes.waterText'),
    icon: '/img/big_icons/water.svg'
  },
  {
    title: t('screens.PlanHome.swipes.towelTitle'),
    description: t('screens.PlanHome.swipes.towelText'),
    icon: '/img/big_icons/towel.svg'
  }
];

function RestDivider({ id, onStart }) {
  return (
    <div id={id} className="rest-divider">
      <ScrollAnchor />
      <div className="divider-badge">
        <Image src="/img/big_icons/exercise.svg" alt="" />
        <Fab icon="stopwatch_start_white" onClick={onStart} />
      </div>
      <div className="divider-info">
        <Image src="/img/icons/stopwatch_gray.svg" alt="" />
        <span>{`até ${REST_TIME} min. de descanso`}</span>
      </div>
    </div>
  );
}

class PlanHome extends Component {
  state = {
    confirmCompletion: false
  };

  componentDidMount() {
    const { hasAnyPlan, isExecuting } = this.props;

    if (!hasAnyPlan) {
      return;
    }

    if (isExecuting) {
      // If was in the middle of completing the plan
      // scroll to the next exercise
      this.scrollToIncompleteExercise();
    }
  }

  componentWillReceiveProps(nextProps) {
    const { isExecutingPlanExercise, isRestingNow, nextExercise, currentExercise } = this.props;

    // Finished exercise, expand rest divider
    if (currentExercise && isExecutingPlanExercise && !nextProps.isExecutingPlanExercise) {
      const restId = `${currentExercise.id}-rest`;
      const restElement = document.getElementById(restId);

      if (!restElement) {
        return;
      }

      // Collapse all other rest dividers
      const restDividers = document.querySelectorAll('.rest-divider');
      restDividers.forEach(e => {
        e.classList.remove('expanded');
      });

      // Expand the next rest divider
      restElement.classList.add('expanded');

      setTimeout(() => this.scrollTo(restId), 400);
    }

    // Finished rest, move to next exercise
    if (isRestingNow && !nextProps.isRestingNow && nextExercise) {
      setTimeout(() => {
        this.scrollTo(nextExercise.id);
      }, 400);
    }

    // If has no next exercise, scroll to the first incomplete
    if (isRestingNow && !nextProps.isRestingNow && !nextExercise) {
      this.scrollToIncompleteExercise();
    }

    const { weekPlans, currentPlan, selectPlan } = this.props;
    if (weekPlans && weekPlans.length === 1 && (!currentPlan || !currentPlan.name)) {
      const { name, date } = weekPlans[0];
      selectPlan(name, date);
    }
  }

  scrollTo = id => {
    const card = document.getElementById(id);

    if (!card) {
      return;
    }

    const scrollAnchor = card.querySelector('.scroll-anchor');

    if (!scrollAnchor) {
      return;
    }

    const { platform } = this.props;
    if (platform === 'ios') {
      scrollIntoView(scrollAnchor, {
        behavior: 'smooth',
        block: 'center',
        inline: 'nearest'
      });
    } else {
      scrollAnchor.scrollIntoView({
        behavior: 'smooth',
        block: 'center',
        inline: 'nearest'
      });
    }
  };

  scrollToEnd = () => {
    const container = document.getElementById('plan-home-container');
    container.scrollTo(0, 5000);
  };

  scrollToIncompleteExercise = () => {
    const { currentSession } = this.props;

    if (currentSession) {
      const { exercises } = currentSession;
      const nextExercise = currentSession && exercises.find(e => !e.duration);
      if (nextExercise) {
        setTimeout(() => {
          this.scrollTo(nextExercise.id);
        }, 400);
        return;
      }
    }

    this.scrollToEnd();
  };

  selectTab = (name, date) => {
    const { selectPlan } = this.props;
    selectPlan(name, date);
  };

  selectTabOld = i => {
    const { selectPlanOld } = this.props;
    selectPlanOld(i);
  };


  forceCompletePlan = () => {
    const { completePlan, history } = this.props;
    completePlan();

    history.push('/plan/finish');
  };

  requestCompletePlan = () => {
    const { currentExercises, completedExercises } = this.props;

    if (completedExercises.length < currentExercises.length) {
      this.setState({
        confirmCompletion: true
      });
    } else {
      this.forceCompletePlan();
    }
  };

  startRest = () => {
    const { startPlanRest } = this.props;
    startPlanRest();
  };

  onTimerCancel = () => {
    const { isRestingNow, cancelPlanRest, cancelPlanExercise } = this.props;

    if (isRestingNow) {
      cancelPlanRest();
    } else {
      cancelPlanExercise();
    }
  };

  onTimerComplete = duration => {
    const { isRestingNow, completePlanExercise, completePlanRest, currentExercise } = this.props;

    if (isRestingNow) {
      completePlanRest();
    } else {
      completePlanExercise({ exercise: currentExercise, duration });
    }
  };

  onScroll = () => {
    const container = document.getElementById('plan-home-container');
    const progress = document.getElementById('progress');

    if (!container || !progress) {
      return;
    }

    const scrollY = container.scrollTop;

    // Weird calculation to ensure the fading doesn't start too soon
    // (only about halfway through the page header)
    const scrollPercent = Math.min(1, Math.max(0, (scrollY - 100) / 200));

    progress.style.opacity = scrollPercent;
  };

  render() {
    const {
      failedToFetchPlan,
      hasAnyPlan,
      coachError,
      currentPlan,
      currentExercises,
      isExecutingPlanExercise,
      progress,
      weekPlans,
      isRestingNow,
      isExecuting,
      t,
      isLoading,
      plans
    } = this.props;
    const { confirmCompletion } = this.state;

    if (isLoading) {
      return (
        <>
          <IonContent forceOverscroll={false}>
            <div className="full-height-flex plan-welcome-container">
              <div className="wrapper">
                <h1>
                  {t('screens.PlanLoading.title')}
                  <b>{t('screens.PlanLoading.titleBold')}</b>
                </h1>
                <Image src="/img/big_icons/plan.svg" alt="" />
                <p>{t('screens.PlanLoading.introText')}</p>
              </div>
            </div>
          </IonContent>
        </>
      );
    }

    if (failedToFetchPlan) {
      return <Redirect to="/plan/error" />;
    }

    if (!hasAnyPlan) {
      return <Redirect to="/plan/welcome" />;
    }

    if (coachError || !weekPlans.length) {
      return (
        <div id="plan-home-container" onScroll={this.onScroll}>
          <div id="notch" />
          <IonGrid id="plan-header">
            <div className="plan-header__avatar">
              <Link to="/profile">
                <Avatar size={42} />
              </Link>
            </div>
  
            <IonRow>
              <h1>
                {t('screens.PlanHome.header.start')}
                <b>{` ${t('screens.PlanHome.header.trainingPlan')}`}</b>
              </h1>
            </IonRow>
  
            <IonRow>
            <span>{t('screens.PlanHome.objective')}</span>
            <ObjectiveLabel />
            </IonRow>
          </IonGrid>
  
          <div className="plan-content">
            <IonGrid>
              <IonRow className="plan-warnings">
                <span className="warning-title">{t('screens.PlanHome.warnings')}</span>
                <BadgeSwiper swipes={SWIPES(t)} />
              </IonRow>
            
  
              <IonRow className="plan-exercises">
                {plans && plans.length >= 1 && (
                  <div className="tab-bar">
                    {plans.map(({ name }, i) => (
                      <button
                        key={name}
                        type="button"
                        className={`tab ${currentPlan && name === currentPlan.name ? 'selected' : ''}`}
                        onClick={() => this.selectTabOld(i)}
                      >
                        {`Plano ${name}`}
                      </button>
                    ))}
                  </div>
                )}
                <IonGrid className="plan-exercises-list">
                  {currentExercises.map((ex, i) => {
                    const key = `${currentPlan.name.replace(/\s/g, '')}-${i}-${ex.id}`;
                    return (
                      <IonRow id={ex.id} key={key}>
                        {ex.name.toLowerCase() === 'alongamentos' ? (
                          <StretchesCard exercise={ex} />
                        ) : (
                          <>
                            <ExerciseCard exercise={ex} />
                            <RestDivider id={`${ex.id}-rest`} onStart={this.startRest} />
                          </>
                        )}
                      </IonRow>
                    );
                  })}
                </IonGrid>
              </IonRow>
  
              {isExecuting && (
                <IonRow className="plan-cta">
                  <PrimaryButton text={t('screens.PlanHome.cta')} onClick={this.requestCompletePlan} />
                </IonRow>
              )}
  
              {progress && (
                <div id="progress" className="progress-chip__container">
                  <div className="progress-chip">
                    {progress && `${progress} ${t('screens.PlanHome.exercises')}`}
                  </div>
                </div>
              )}
            </IonGrid>
          </div>
  
          <ExerciseTimer
            show={isExecutingPlanExercise || isRestingNow}
            duration={isRestingNow ? REST_TIME * 60 : 0}
            onComplete={this.onTimerComplete}
            onDismiss={this.onTimerCancel}
          />
  
          <IonAlert
            isOpen={confirmCompletion}
            header={t('screens.PlanHome.alert.title')}
            message={t('screens.PlanHome.alert.message')}
            buttons={[
              {
                text: 'Cancelar',
                role: 'cancel',
                cssClass: 'secondary',
                handler: () => {
                  this.setState({
                    confirmCompletion: false
                  });
                }
              },
              {
                text: 'Confirmar',
                handler: () => {
                  this.forceCompletePlan();
                }
              }
            ]}
          />
        </div>
      );
  
  
      // return (
      //   <Redirect
      //     to={{
      //       pathname: '/plan/error',
      //       state: { message: t('screens.PlanError.coach') }
      //     }}
      //   />
      // );
    }
    console.log(weekPlans);
    return (
      <div id="plan-home-container" onScroll={this.onScroll}>
        <div id="notch" />
        <IonGrid id="plan-header">
          <div className="plan-header__avatar">
            <Link to="/profile">
              <Avatar />
            </Link>
          </div>

          <IonRow>
            <h1>
              {t('screens.PlanHome.header.start')}
              <b>{` ${t('screens.PlanHome.header.trainingPlan')}`}</b>
            </h1>
          </IonRow>

          <IonRow>
            <span>{t('screens.PlanHome.objective')}</span>
            <ObjectiveLabel />
          </IonRow>
        </IonGrid>

        <div className="plan-content">
          <IonGrid>
            <IonRow className="plan-warnings">
              <span className="warning-title">{t('screens.PlanHome.warnings')}</span>
              <BadgeSwiper swipes={SWIPES(t)} />
            </IonRow>

            <IonRow className="plan-exercises">
              {weekPlans && weekPlans.length > 1 ? (
                <div className="tab-bar">
                  {weekPlans.map(weekPlan => {
                    const { name, date, day } = weekPlan;
                    const currentDate = currentPlan && currentPlan.date;
                    const isSelected = moment(date).isSame(moment(currentDate), 'day');

                    return (
                      <button
                        key={day}
                        type="button"
                        className={`tab ${isSelected ? 'selected' : ''}`}
                        onClick={() => this.selectTab(name, date)}
                      >
                        {day}
                      </button>
                    );
                  })}
                </div>
              ) : null}
              <IonGrid className="plan-exercises-list">
                {currentExercises.map((ex, i) => {
                  const key = `${currentPlan.name.replace(/\s/g, '')}-${i}-${ex.id}`;
                  return (
                    <IonRow id={ex.id} key={key}>
                      {ex.name.toLowerCase() === 'alongamentos' ? (
                        <StretchesCard exercise={ex} />
                      ) : (
                        <>
                          <ExerciseCard exercise={ex} />
                          <RestDivider id={`${ex.id}-rest`} onStart={this.startRest} />
                        </>
                      )}
                    </IonRow>
                  );
                })}
              </IonGrid>
            </IonRow>

            {isExecuting && (
              <IonRow className="plan-cta">
                <PrimaryButton text={t('screens.PlanHome.cta')} onClick={this.requestCompletePlan} />
              </IonRow>
            )}

            {progress && (
              <div id="progress" className="progress-chip__container">
                <div className="progress-chip">
                  {progress && `${progress} ${t('screens.PlanHome.exercises')}`}
                </div>
              </div>
            )}
          </IonGrid>
        </div>

        <ExerciseTimer
          show={isExecutingPlanExercise || isRestingNow}
          duration={isRestingNow ? REST_TIME * 60 : 0}
          onComplete={this.onTimerComplete}
          onDismiss={this.onTimerCancel}
        />

        <IonAlert
          isOpen={confirmCompletion}
          header={t('screens.PlanHome.alert.title')}
          message={t('screens.PlanHome.alert.message')}
          buttons={[
            {
              text: 'Cancelar',
              role: 'cancel',
              cssClass: 'secondary',
              handler: () => {
                this.setState({
                  confirmCompletion: false
                });
              }
            },
            {
              text: 'Confirmar',
              handler: () => {
                this.forceCompletePlan();
              }
            }
          ]}
        />
      </div>
    );
  }
}

RestDivider.propTypes = {
  id: PropTypes.string.isRequired,
  onStart: PropTypes.func.isRequired
};

PlanHome.defaultProps = {
  nextExercise: null,
  currentExercise: null,
  currentSession: null
};

PlanHome.propTypes = {
  t: PropTypes.func.isRequired,
  failedToFetchPlan: PropTypes.bool.isRequired,
  hasAnyPlan: PropTypes.bool.isRequired,
  coachError: PropTypes.bool.isRequired,
  currentExercises: PropTypes.arrayOf(PropTypes.any).isRequired,
  completedExercises: PropTypes.arrayOf(PropTypes.any).isRequired,
  isExecutingPlanExercise: PropTypes.bool.isRequired,
  isExecuting: PropTypes.bool.isRequired,
  isLoading: PropTypes.bool.isRequired,
  completePlan: PropTypes.func.isRequired,
  selectPlan: PropTypes.func.isRequired,
  completePlanExercise: PropTypes.func.isRequired,
  cancelPlanExercise: PropTypes.func.isRequired,
  startPlanRest: PropTypes.func.isRequired,
  completePlanRest: PropTypes.func.isRequired,
  cancelPlanRest: PropTypes.func.isRequired,
  progress: PropTypes.string.isRequired,
  weekPlans: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  currentSession: PropTypes.shape({
    exercises: PropTypes.arrayOf(PropTypes.any)
  }),
  isRestingNow: PropTypes.bool.isRequired,
  nextExercise: PropTypes.shape({
    id: PropTypes.string
  }),
  currentExercise: PropTypes.shape({
    id: PropTypes.string
  }),
  currentPlan: PropTypes.shape({
    name: PropTypes.string,
    date: PropTypes.instanceOf(Date)
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func
  }).isRequired,
  platform: PropTypes.string.isRequired
};

const mapStateToProps = state => {
  const failedToFetchPlan = hasFailedToFetchPlan(state);
  const hasAnyPlan = hasPlan(state);
  const coachError = hasErrorCoach(state);
  const currentExercises = getCurrentExercises(state);
  const currentPlan = getCurrentPlan(state);
  const completedExercises = getCompletedExercises(state);
  const currentExercise = getCurrentSessionExercise(state);
  const currentSession = getCurrentSession(state);
  const isExecutingPlanExercise = isExecutingExercise(state);
  const isExecuting = isExecutingPlan(state);
  const progress = getProgress(state);
  const nextExercise = getNextExercise(state);
  const isRestingNow = isResting(state);
  const platform = getPlatform(state);
  const weekPlans = getWeekPlans(state);
  const isLoading = isLoadingPlans(state);
  const plans = getPlans(state);

  return {
    failedToFetchPlan,
    hasAnyPlan,
    coachError,
    currentExercise,
    currentExercises,
    completedExercises,
    isExecutingPlanExercise,
    isExecuting,
    progress,
    nextExercise,
    isRestingNow,
    currentSession,
    currentPlan,
    platform,
    weekPlans,
    isLoading,
    plans
  };
};

const mapDispatchToProps = {
  completePlanExercise: completeExercise,
  completePlan: forceCompletePlan,
  cancelPlanExercise: cancelExercise,
  startPlanRest: startRest,
  completePlanRest: completeRest,
  cancelPlanRest: cancelRest,
  selectPlan: selectPlanAction,
  selectPlanOld: selectPlanOldAction
};

export default compose(
  withTranslation(),
  withRouter,
  withMenuNavigation(),
  connect(mapStateToProps, mapDispatchToProps)
)(PlanHome);

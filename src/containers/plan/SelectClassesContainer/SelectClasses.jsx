import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { IonContent, IonGrid, IonRow } from '@ionic/react';
import _ from 'lodash';
import { withRouter } from 'react-router-dom';
import { withTranslation } from 'react-i18next';
import SwipeableViews from 'react-swipeable-views';
import ModalitySelectCard from '../../../components/cards/ModalitySelectCard';
import PrimaryButton from '../../../components/buttons/PrimaryButton';
import withBackNavigation from '../../../components/hoc/withBackNavigation';
import { setupModalitiesPlan } from '../../../redux/modules/plan/actions/setup';
import withRouteState from '../../../components/hoc/withRouteState';
import './SelectClasses.scss';

const styles = {
  root: {
    // gap between start and end of screen
    padding: '0 calc(50vw - 115px)'
  },
  slideContainer: {
    // gap between cards
    padding: '0 5px'
  }
};

const MINIMUM_CLASSES_SELECTED = 2;

class SelectClasses extends Component {
  constructor(props) {
    super(props);

    const { lastState } = props;
    const hasHighlighted = lastState && lastState.highlightedIndex !== undefined;

    this.state = {
      highlightedIndex: hasHighlighted ? lastState.highlightedIndex : 0,
      selectedIndexes: lastState.selectedIndexes || []
    };
  }

  componentWillUnmount() {
    const { highlightedIndex, selectedIndexes } = this.state;
    const { saveState } = this.props;

    const currentState = {
      highlightedIndex,
      selectedIndexes
    };

    saveState(currentState);
  }

  highlight = index => {
    this.setState({
      highlightedIndex: index
    });
  };

  select = index => {
    const { selectedIndexes, highlightedIndex } = this.state;

    if (index !== highlightedIndex) {
      this.setState({
        highlightedIndex: index
      });
      return;
    }

    if (selectedIndexes.includes(index)) {
      this.setState({ selectedIndexes: selectedIndexes.filter(i => i !== index) });
    } else {
      this.setState({ selectedIndexes: [...selectedIndexes, index] });
    }
  };

  submit = () => {
    const { selectedIndexes } = this.state;
    const { modalities, setupModalities, history } = this.props;
    const selectedModalities = modalities.filter((m, i) => selectedIndexes.includes(i));

    setupModalities(selectedModalities);
    history.push('/plan/confirmation');
  };

  renderBottomRow = () => {
    const { t, modalities } = this.props;
    const { highlightedIndex, selectedIndexes } = this.state;
    const canSkip = selectedIndexes && selectedIndexes.length >= MINIMUM_CLASSES_SELECTED;

    return (
      <>
        {modalities.length < 10 && (
          <div className="swiper-bullets">
            {_.times(modalities.length, i => (
              <div
                key={`swipe-bullet-${i}`}
                className={`bullet ${highlightedIndex === i ? 'highlighted' : ''}`}
              />
            ))}
          </div>
        )}
        {canSkip && (
          <PrimaryButton text={t('global.actions.confirm')} icon="check_white" onClick={this.submit} />
        )}
      </>
    );
  };

  render() {
    const { t, modalities } = this.props;
    const { highlightedIndex, selectedIndexes } = this.state;

    return (
      <div className="select-classes-container full-height-flex">
        <IonContent forceOverscroll={false}>
          <IonGrid className="full-height-flex ion-justify-content-between">
            <IonRow>
              <div id="notch" />
              <h1>
                {`${t('screens.PlanSelect.recommendedPlan')} `}
                <b>{t('screens.PlanSelect.planTypes.smartAge')}</b>
              </h1>
            </IonRow>
            <IonRow>
              <div className="swiper-title">{t('screens.PlanSelect.chooseYourClasses')}</div>
              {modalities.length === 0 ? (
                <span className="no-modality-label">{t('screens.PlanSelect.modalitiesEmpty')}</span>
              ) : (
                <SwipeableViews
                  index={highlightedIndex}
                  style={styles.root}
                  slideStyle={styles.slideContainer}
                  onChangeIndex={this.highlight}
                >
                  {modalities.map((m, i) =>
                    Math.abs(highlightedIndex - i) < 3 ? (
                      <ModalitySelectCard
                        key={m.id}
                        modality={m}
                        isFocused={highlightedIndex === i}
                        isSelected={selectedIndexes.includes(i)}
                        onClick={() => this.select(i)}
                      />
                    ) : (
                      <Fragment key={`fragment-${m.id}`} />
                    )
                  )}
                </SwipeableViews>
              )}
            </IonRow>
            <IonRow>{this.renderBottomRow()}</IonRow>
          </IonGrid>
        </IonContent>
      </div>
    );
  }
}

SelectClasses.propTypes = {
  t: PropTypes.func.isRequired,
  modalities: PropTypes.arrayOf(PropTypes.any).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func
  }).isRequired,
  setupModalities: PropTypes.func.isRequired,
  saveState: PropTypes.func.isRequired,
  lastState: PropTypes.shape({
    highlightedIndex: PropTypes.number,
    selectedIndexes: PropTypes.arrayOf(PropTypes.any)
  }).isRequired
};

const mapStateToProps = state => {
  const { setup } = state.plan;
  const { recommendedModalities } = setup;
  return { modalities: recommendedModalities };
};

const mapDispatchToProps = {
  setupModalities: setupModalitiesPlan
};

export default compose(
  withTranslation(),
  withRouter,
  withBackNavigation(),
  connect(mapStateToProps, mapDispatchToProps),
  withRouteState('SelectClasses')
)(SelectClasses);

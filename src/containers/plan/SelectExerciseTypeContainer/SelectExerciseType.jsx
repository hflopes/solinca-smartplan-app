import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { IonContent, IonGrid, IonRow } from '@ionic/react';
import _ from 'lodash';
import { withRouter } from 'react-router-dom';
import { withTranslation } from 'react-i18next';
import SwipeableViews from 'react-swipeable-views';
import SelectCard from '../../../components/cards/SelectCard';
import PrimaryButton from '../../../components/buttons/PrimaryButton';
import { setupExerciseType } from '../../../redux/modules/plan/actions/setup';
import withBackNavigation from '../../../components/hoc/withBackNavigation';
import withRouteState from '../../../components/hoc/withRouteState';
import { EXERCISE_TYPES, USER_TYPES, USER_OBJECTIVES } from '../../../redux/modules/plan/constants';
import { getSetup } from '../../../redux/selectors/plan';
import './SelectExerciseType.scss';

const styles = {
  root: {
    // gap between start and end of screen
    padding: '0 calc(50vw - 115px)'
  },
  slideContainer: {
    // gap between cards
    padding: '0 5px'
  }
};

class SelectExerciseType extends Component {
  constructor(props) {
    super(props);

    const { lastState } = props;
    const isValid = lastState && lastState.selectedIndex !== undefined;
    const index = Math.max(0, lastState.selectedIndex);

    this.state = {
      highlightedIndex: isValid ? index : 0,
      selectedIndex: isValid ? index : -1
    };
  }

  componentWillUnmount() {
    const { selectedIndex } = this.state;
    const { saveState } = this.props;

    const currentState = {
      selectedIndex
    };

    saveState(currentState);
  }

  highlight = index => {
    this.setState({
      highlightedIndex: index
    });
  };

  select = index => {
    const { selectedIndex, highlightedIndex } = this.state;

    if (index !== highlightedIndex) {
      this.setState({
        highlightedIndex: index
      });
      return;
    }

    if (selectedIndex === index) {
      this.setState({ selectedIndex: -1 });
    } else {
      this.setState({ selectedIndex: index });
    }
  };

  submit = () => {
    const { selectedIndex } = this.state;
    const { setupPlanExerciseType, history, userType, userObjective } = this.props;
    setupPlanExerciseType(Object.keys(EXERCISE_TYPES)[selectedIndex]);

    if (userType === USER_TYPES.EXPERIENCED_USER.key && userObjective === USER_OBJECTIVES.MUSCLE.key) {
      history.push('/plan/confirmation');
    } else {
      history.push('/plan/select/classes');
    }
  };

  renderBottomRow = () => {
    const { t } = this.props;
    const { highlightedIndex, selectedIndex } = this.state;

    return (
      <>
        <div className="swiper-bullets">
          {_.times(2, i => (
            <div
              key={`swipe-bullet-${i}`}
              className={`bullet ${highlightedIndex === i ? 'highlighted' : ''}`}
            />
          ))}
        </div>
        {selectedIndex !== -1 && (
          <PrimaryButton text={t('global.actions.confirm')} icon="check_white" onClick={this.submit} />
        )}
      </>
    );
  };

  render() {
    const { t } = this.props;
    const { highlightedIndex, selectedIndex } = this.state;

    return (
      <div className="select-exercise-type-container full-height-flex">
        <IonContent forceOverscroll={false}>
          <IonGrid className="full-height-flex ion-justify-content-between">
            <IonRow>
              <div id="notch" />
              <h1>
                {`${t('screens.PlanSelect.trainingOf')} `}
                <b>{t('screens.PlanSelect.trainingTypes.resistance')}</b>
              </h1>
            </IonRow>
            <IonRow>
              <div className="swiper-title">{t('screens.PlanSelect.chooseYourExerciseType')}</div>
              <SwipeableViews
                index={highlightedIndex}
                style={styles.root}
                slideStyle={styles.slideContainer}
                onChangeIndex={this.highlight}
              >
                {_.map(EXERCISE_TYPES, (exerciseType, index) => {
                  const numIndex = Object.keys(EXERCISE_TYPES).indexOf(index);
                  const selected = selectedIndex === numIndex;
                  const { title, description, key } = exerciseType;
                  const isBodyWeight = key === 'BODY_WEIGHT';

                  const imgUrl = `/img/backgrounds/exercise_${isBodyWeight ? 'body_weight' : 'machines'}.png`;

                  return (
                    <SelectCard
                      key={`exercise-type-select-card-${numIndex}`}
                      title={title}
                      description={description}
                      imageUrl={imgUrl}
                      highlighted={highlightedIndex === numIndex}
                      selected={selected}
                      onClick={() => this.select(numIndex)}
                      disabled={false}
                    />
                  );
                })}
              </SwipeableViews>
            </IonRow>
            <IonRow>{this.renderBottomRow()}</IonRow>
          </IonGrid>
        </IonContent>
      </div>
    );
  }
}

SelectExerciseType.propTypes = {
  t: PropTypes.func.isRequired,
  userObjective: PropTypes.string.isRequired,
  userType: PropTypes.string.isRequired,
  setupPlanExerciseType: PropTypes.func.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func
  }).isRequired,
  saveState: PropTypes.func.isRequired,
  lastState: PropTypes.shape({
    selectedIndex: PropTypes.number
  }).isRequired
};

const mapStateToProps = state => {
  const { userObjective, userType } = getSetup(state);
  return { userObjective, userType };
};

const mapDispatchToProps = {
  setupPlanExerciseType: setupExerciseType
};

export default compose(
  withTranslation(),
  withRouter,
  withBackNavigation(),
  connect(mapStateToProps, mapDispatchToProps),
  withRouteState('SelectExerciseType')
)(SelectExerciseType);

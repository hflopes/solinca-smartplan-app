import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { IonContent } from '@ionic/react';
import { withTranslation } from 'react-i18next';
import Image from '../../../components/Image';
import PrimaryButton from '../../../components/buttons/PrimaryButton';
import './Confirmation.scss';

function Confirmation({ t }) {
  return (
    <>
      <IonContent forceOverscroll={false}>
        <div className="full-height-flex plan-confirmation-container">
          <div className="wrapper">
            <h1>
              {t('screens.PlanConfirmation.title')}
              <b>{t('screens.PlanConfirmation.titleBold')}</b>
            </h1>
            <Image src="/img/big_icons/foot_red.svg" alt="" />
            <p>{t('screens.PlanConfirmation.infoText')}</p>
          </div>
          <div className="sticky-button">
            <PrimaryButton text={t('screens.PlanConfirmation.cta')} link="/plan" />
          </div>
        </div>
      </IonContent>
    </>
  );
}

Confirmation.propTypes = {
  t: PropTypes.func.isRequired
};

export default compose(withTranslation())(Confirmation);

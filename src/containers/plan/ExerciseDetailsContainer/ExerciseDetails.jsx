import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { withTranslation } from 'react-i18next';
import withBackNavigation from '../../../components/hoc/withBackNavigation';
import { getExercise, isExerciseCompleted } from '../../../redux/selectors/plan';
import LayeredPage from '../../../components/LayeredPage';
import Image from '../../../components/Image';
import { formatSeconds } from '../../../utils/dates';
import './ExerciseDetails.scss';

const DARK_GRADIENT = 'linear-gradient(0deg, rgba(0,0,0,0.4) 0%, rgba(0,0,0,0.4) 100%)';

function MediaPlayer({ gif }) {
  return (
    <div className="wrapper -gif">
      <Image className="media__gif" src={gif} />
      <div className="gif-bottom-hack" />
    </div>
  );
}

function ImageHeader({ name, image }) {
  const bgImageStyle = {
    background: `${DARK_GRADIENT}, url("${image}")`,
    backgroundSize: 'cover',
    backgroundPosition: 'center'
  };

  return (
    <div className="wrapper -image" style={bgImageStyle}>
      <div className="overlay">
        <h1>{name}</h1>
      </div>
    </div>
  );
}

function DefaultHeader({ name }) {
  return (
    <div className="wrapper -default">
      <div className="overlay">
        <h1>{name}</h1>
      </div>
    </div>
  );
}

function ExerciseDuration({ isCompleted, duration }) {
  const timer = formatSeconds(duration);

  const durationClass = classNames({
    timer: true,
    hidden: isCompleted
  });

  return (
    <div className={durationClass}>
      <Image src="/img/icons/stopwatch_gray.svg" alt="" />
      <b>{timer}</b>
    </div>
  );
}

function ExerciseComplete({ isCompleted }) {
  const completeClass = classNames({
    complete__check: true,
    hidden: !isCompleted
  });

  return (
    <div className={completeClass}>
      <Image src="/img/icons/check_pink.svg" alt="" />
    </div>
  );
}

class ExerciseDetails extends Component {
  renderHeader = ({ name, image, gif }) => {
    if (gif) {
      return <MediaPlayer gif={gif} />;
    }

    if (image) {
      return <ImageHeader name={name} image={image} />;
    }

    return <DefaultHeader name={name} />;
  };

  render() {
    const { t, exercise, isCompleted } = this.props;
    const {
      name,
      image,
      equipment,
      instructions,
      duration,
      pse,
      params,
      gif,
      observations,
      tex_descricao
    } = exercise;

    const headerClass = classNames({
      'exercise-details__header': true,
      '-expanded': gif
    });

    const background = (
      <>
        <div className={headerClass}>{this.renderHeader({ name, image, gif })}</div>
      </>
    );

    const foreground = (
      <>
        <div className="exercise-details__content">
          {gif && <h1 className="exercise-details__title">{name}</h1>}
          <div className="exercise-stats__container">
            {equipment && (
              <div className="exercise-stat">
                <span className="exercise-stat__label">{t('global.trainingPlan.equipment.short')}</span>
                <b className="exercise-stat__value">{equipment || '---'}</b>
              </div>
            )}
            <div className="exercise-stat">
              <span className="exercise-stat__label">{t('global.trainingPlan.pse.short')}</span>
              <b className="exercise-stat__value">{pse || '---'}</b>
            </div>
            {params.map(({ label, value }) => (
              <div key={label} className="exercise-stat">
                <span className="exercise-stat__label">{t(`global.trainingPlan.${label}.short`)}</span>
                <b className="exercise-stat__value">{value}</b>
              </div>
            ))}
          </div>
          <div className="exercise-info__container">
            {instructions || tex_descricao ? (
              <>
                <b className="instructions__title">{t(`global.trainingPlan.instructions.full`)}</b>
                <p className="instructions__text">{instructions || tex_descricao}</p>
              </>
            ) : null}
            {observations && (
              <>
                <br />
                <b className="instructions__title">{t(`global.trainingPlan.observations.full`)}</b>
                <p className="instructions__text">{observations}</p>
              </>
            )}
          </div>
        </div>
      </>
    );

    return (
      <>
        <LayeredPage
          className="exercise-details__container"
          background={background}
          foreground={foreground}
          gap={gif ? '430px' : '320px'}
          extra="10vh"
          edgeGap="328px"
          rounded
        />
        <div className="exercise__top-element">
          <ExerciseDuration isCompleted={isCompleted} duration={duration} />
          <ExerciseComplete isCompleted={isCompleted} />
        </div>
      </>
    );
  }
}

MediaPlayer.defaultProps = {
  gif: undefined
};

MediaPlayer.propTypes = {
  gif: PropTypes.string
};

ImageHeader.propTypes = {
  name: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired
};

DefaultHeader.propTypes = {
  name: PropTypes.string.isRequired
};

ExerciseComplete.propTypes = {
  isCompleted: PropTypes.bool.isRequired
};

ExerciseDuration.propTypes = {
  isCompleted: PropTypes.bool.isRequired,
  duration: PropTypes.number.isRequired
};

ExerciseDetails.propTypes = {
  t: PropTypes.func.isRequired,
  exercise: PropTypes.shape({
    name: PropTypes.string,
    equipment: PropTypes.string,
    instructions: PropTypes.string,
    duration: PropTypes.number,
    pse: PropTypes.number,
    params: PropTypes.arrayOf(PropTypes.any),
    gif: PropTypes.string,
    image: PropTypes.string,
    video: PropTypes.string,
    observations: PropTypes.string
  }).isRequired,
  isCompleted: PropTypes.bool.isRequired
};

const mapStateToProps = (state, ownProps) => {
  const exercise = getExercise(state, ownProps.id);
  const isCompleted = isExerciseCompleted(state, ownProps.id);
  return { exercise, isCompleted };
};

export default compose(withTranslation(), withBackNavigation(), connect(mapStateToProps))(ExerciseDetails);

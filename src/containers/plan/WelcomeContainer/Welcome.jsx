import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { IonContent } from '@ionic/react';
import { withTranslation } from 'react-i18next';
import Image from '../../../components/Image';
import PrimaryButton from '../../../components/buttons/PrimaryButton';
import withMenuNavigation from '../../../components/hoc/withMenuNavigation';
import './Welcome.scss';

function Welcome({ t }) {
  return (
    <>
      <IonContent forceOverscroll={false}>
        <div className="full-height-flex plan-welcome-container">
          <div className="wrapper">
            <h1>
              {t('screens.PlanWelcome.title')}
              <b>{t('screens.PlanWelcome.titleBold')}</b>
            </h1>
            <Image src="/img/big_icons/plan.svg" alt="" />
            <p>{t('screens.PlanWelcome.introText')}</p>
          </div>
          <div className="sticky-button">
            <PrimaryButton text={t('global.actions.go')} icon="go_white" link="/plan/questionnaire" />
          </div>
        </div>
      </IonContent>
    </>
  );
}

Welcome.propTypes = {
  t: PropTypes.func.isRequired
};

export default compose(withTranslation(), withMenuNavigation())(Welcome);

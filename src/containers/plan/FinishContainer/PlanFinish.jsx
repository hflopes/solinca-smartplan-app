import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';
import { getRecommendedModalities } from '../../../redux/selectors/plan';
import ModalityCard from '../../../components/cards/ModalityCard';
import PrimaryButton from '../../../components/buttons/PrimaryButton';
import './PlanFinish.scss';

function PlanFinish({ t, recommendedModalities }) {
  return (
    <div className="plan-finish__container">
      <div className="header">
        <h1 className="header__title">{t('screens.PlanFinish.title')}</h1>
        <p className="header__text">{t('screens.PlanFinish.text')}</p>
      </div>

      <div className="modalities">
        <b className="modalities__title">{t('screens.PlanFinish.modalitiesTitle')}</b>
        <ul className="modalities__list">
          {recommendedModalities.map(c => (
            <li key={c.id}>
              <ModalityCard modality={c} />
            </li>
          ))}
        </ul>
      </div>

      <div className="cta">
        <PrimaryButton text={t('screens.PlanFinish.cta')} link="/home" />
      </div>
    </div>
  );
}

PlanFinish.propTypes = {
  t: PropTypes.func.isRequired,
  recommendedModalities: PropTypes.arrayOf(PropTypes.any).isRequired
};

const mapStateToProps = state => {
  const recommendedModalities = getRecommendedModalities(state);
  return { recommendedModalities };
};

export default compose(withTranslation(), connect(mapStateToProps))(PlanFinish);

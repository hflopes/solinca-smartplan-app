import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';
import { Swipeable } from 'react-swipeable';
import { Link } from 'react-router-dom';
import withMenuNavigation from '../../components/hoc/withMenuNavigation';
import LayeredPage from '../../components/LayeredPage';
import Avatar from '../../components/Avatar';
import SlidingList from '../../components/SlidingList';
import Image from '../../components/Image';
import Input from '../../components/inputs/Text';
import TextArea from '../../components/inputs/TextArea';
import PrimaryButton from '../../components/buttons/PrimaryButton';
import sendTicketAction from '../../redux/modules/support/actions/sendTicket';
import { getGymsListOrderedByDistance } from '../../redux/selectors/gyms';
import { getUserContactData } from '../../redux/selectors/user';
import './Nutrition.scss';

function Nutrition() {
  const { t } = useTranslation();

  const gyms = useSelector(state => getGymsListOrderedByDistance(state));
  const contactData = useSelector(state => getUserContactData(state));

  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [phone, setPhone] = useState('');
  const [message, setMessage] = useState('');
  const [selectedGym, setSelectedGym] = useState(null);
  const [isShowingGyms, setShowingGyms] = useState(false);

  const dispatch = useDispatch();

  const onEmailChanged = e => {
    setEmail(e.target.value);
  };

  const onPhoneChanged = e => {
    setPhone(e.target.value);
  };

  const onMessageChanged = e => {
    setMessage(e.target.value);
  };

  const onGymChanged = index => {
    hideGymList();
    setSelectedGym(gyms[index]);
  };

  const onSubmit = e => {
    if (!e) {
      return;
    }

    e.preventDefault();

    const motive = t('screens.Nutrition.motive');
    const gymId = selectedGym.id;
    const withAlert = true;

    dispatch(sendTicketAction({ name, email, phone, message, selectedGym, motive, gymId, withAlert }));
  };

  const showGymList = () => {
    setShowingGyms(true);
  };

  const hideGymList = () => {
    setShowingGyms(false);
  };

  const gymListCopy = {
    listTitle: t('components.gymSlidingList.listTitle'),
    selectedTitle: t('components.gymSlidingList.selectedTitle'),
    optionsTitle: t('components.gymSlidingList.optionsTitle')
  };

  const mappedGyms = gyms.map(({ name: gymName, distance }) => ({
    title: gymName,
    info: distance ? `${distance.toFixed(1)} km` : null
  }));

  const selectedGymIndex = selectedGym ? gyms.indexOf(selectedGym) : -1;
  const canSubmit = email.length > 0 && phone.length > 0 && selectedGym && message;

  useEffect(() => {
    setName(contactData.name);
    setEmail(contactData.email);
    setPhone(contactData.phone);
    setSelectedGym(gyms.find(g => g.id === contactData.defaultGym));
  }, [contactData, gyms]);

  const renderEmailInput = (
    <Input
      type="email"
      initialValue={contactData.email}
      placeholder={t('global.terms.email')}
      darkMode
      onChange={onEmailChanged}
    />
  );

  const renderPhoneInput = (
    <Input
      type="tel"
      initialValue={contactData.phone}
      placeholder={t('global.terms.phone')}
      darkMode
      onChange={onPhoneChanged}
    />
  );

  const renderGymList = (
    <button tabIndex="-1" className="open-list-button" type="button" onClick={showGymList}>
      <b>{selectedGym ? selectedGym.name : t('components.gymSlidingList.listTitle')}</b>
      <Image src="/img/icons/dropdown_pink.svg" alt="" />
    </button>
  );

  const renderUniqueMotive = (
    <button tabIndex="-1" className="open-list-button disabled-btn" disabled type="button">
      <b>{t('screens.Nutrition.motive')}</b>
      <Image src="/img/icons/dropdown_light_gray.svg" alt="" />
    </button>
  );

  const renderMessageInput = (
    <TextArea
      placeholder={t('global.terms.message')}
      onChange={onMessageChanged}
      initialValue={message}
      darkMode
    />
  );

  const renderSubmit = (
    <PrimaryButton
      text={t('components.booking.nutrition.confirm')}
      onClick={onSubmit}
      isDisabled={!canSubmit}
      type="submit"
    />
  );

  const background = (
    <div className="nutrition__background">
      <div id="notch" />
      <h3 className="page-title">{t('screens.Nutrition.title')}</h3>
    </div>
  );

  const foreground = (
    <div className="nutrition__foreground">
      <b className="form__title">{t('screens.Nutrition.form_title')}</b>
      <form className="nutrition__form" onSubmit={onSubmit}>
        <div className="form__row">{renderEmailInput}</div>
        <div className="form__row">{renderPhoneInput}</div>
        <div className="form__row">{renderGymList}</div>
        <div className="form__row">{renderUniqueMotive}</div>
        <div className="form__row">{renderMessageInput}</div>
        <div className="form__row">{renderSubmit}</div>
      </form>
    </div>
  );

  const gymList = (
    <Swipeable onSwipedDown={hideGymList}>
      <SlidingList
        list={mappedGyms}
        selectedIndex={selectedGymIndex}
        copy={gymListCopy}
        isShowing={isShowingGyms}
        onSelect={onGymChanged}
        onDismiss={hideGymList}
      />
    </Swipeable>
  );

  return (
    <>
      <LayeredPage
        className="nutrition__container"
        background={background}
        foreground={foreground}
        gap="calc(4vh + 250px)"
        rounded
      />
      {gymList}
      <div className="nutrition-header__avatar">
        <Link to="/profile">
          <Avatar />
        </Link>
      </div>
    </>
  );
}

export default withMenuNavigation('light')(Nutrition);

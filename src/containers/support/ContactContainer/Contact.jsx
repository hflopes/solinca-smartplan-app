import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { withTranslation } from 'react-i18next';
import { Swipeable } from 'react-swipeable';
import { connect } from 'react-redux';
import Image from '../../../components/Image';
import ContactHeader from '../../../components/headers/ContactHeader';
import SlidingList from '../../../components/SlidingList';
import TextInput from '../../../components/inputs/Text';
import TextArea from '../../../components/inputs/TextArea';
import PrimaryButton from '../../../components/buttons/PrimaryButton';
import InfoDetail from '../../../components/InfoDetail';
import PrivacyForm from '../../../components/PrivacyForm';
import sendTicketAction from '../../../redux/modules/support/actions/sendTicket';
import saveFormAction from '../../../redux/modules/support/actions/saveForm';
import resetSentAction from '../../../redux/modules/support/actions/resetSent';
import { getGymsListOrderedByDistance } from '../../../redux/selectors/gyms';
import { getUserContactData } from '../../../redux/selectors/user';
import { hasSentTicket, getContactForm } from '../../../redux/selectors/support';
import withBackNavigation from '../../../components/hoc/withBackNavigation';
import withMenuNavigation from '../../../components/hoc/withMenuNavigation';
import { userIsLoggedIn } from '../../../redux/selectors/auth';
import { validateEmail } from '../../../utils/validations';
import './Contact.scss';

const formatGyms = gyms => {
  return gyms.map(({ name, distance }) => {
    return {
      title: name,
      info: distance ? `${distance.toFixed(1)} km` : null
    };
  });
};

const formatMotives = motives => {
  return motives.map(m => {
    return { title: m, info: '' };
  });
};

class Contact extends Component {
  constructor(props) {
    super(props);

    const { form, isLoggedIn } = props;

    this.state = {
      inputs: {
        name: form ? form.name : '',
        email: form ? form.email : '',
        phone: form ? form.phone : '',
        gym: form ? form.gym : null,
        motive: form ? form.motive : null,
        message: form ? form.message : ''
      },
      isShowingGyms: false,
      isShowingMotives: false,
      hasAcceptedTerms: isLoggedIn
    };
  }

  componentWillMount() {
    const { onNavigateBack } = this.props;

    onNavigateBack(() => {
      const { isShowingGyms, isShowingMotives } = this.state;
      if (isShowingGyms || isShowingMotives) {
        this.hideLists();
        return true;
      }

      return false;
    });
  }

  componentDidMount() {
    this.populateReduxData();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.hasSent) {
      const { history, isLoggedIn, resetSent } = this.props;

      // For some reason the IonLoading
      // doesnt not get un-rendered if we change routes
      // using <Redirect>, so this is a work-around
      setTimeout(() => {
        history.push(isLoggedIn ? '/home' : '/welcome');
        resetSent();
      }, 500);
    }
  }

  populateReduxData = () => {
    const { isLoggedIn, userContactData, form, gyms } = this.props;

    if (!isLoggedIn) {
      return;
    }

    const { inputs } = this.state;
    const { name, email, phone, defaultGym } = userContactData;

    let selectedGymId = -1;

    if (form && form.gym) {
      selectedGymId = form.gym.id;
    } else {
      selectedGymId = defaultGym;
    }

    const selectedGym = gyms.find(gym => gym.id === selectedGymId);

    this.setState({ inputs: { ...inputs, name, phone, email, gym: selectedGym } });
  };

  showGymList = () => {
    this.setState({ isShowingGyms: true });
  };

  showMotivesList = () => {
    this.setState({ isShowingMotives: true });
  };

  hideLists = () => {
    this.setState({
      isShowingMotives: false,
      isShowingGyms: false
    });
  };

  onNameChanged = evt => {
    const { value } = evt.target;
    const { inputs } = this.state;

    this.setState({
      inputs: { ...inputs, name: value }
    });
  };

  onEmailChanged = evt => {
    const { value } = evt.target;
    const { inputs } = this.state;

    this.setState({
      inputs: { ...inputs, email: value }
    });
  };

  onPhoneChanged = evt => {
    const { value } = evt.target;
    const { inputs } = this.state;

    this.setState({
      inputs: { ...inputs, phone: value }
    });
  };

  onMessageChanged = evt => {
    const { value } = evt.target;
    const { inputs } = this.state;

    this.setState({
      inputs: { ...inputs, message: value }
    });
  };

  onGymChanged = index => {
    const { gyms } = this.props;
    const { inputs } = this.state;

    this.setState(
      {
        inputs: { ...inputs, gym: gyms[index] }
      },
      this.hideLists
    );
  };

  onMotiveChanged = index => {
    const { motives } = this.props;
    const { inputs } = this.state;

    this.setState(
      {
        inputs: { ...inputs, motive: motives[index] }
      },
      this.hideLists
    );
  };

  onPrivacyChanged = isAccepted => {
    this.setState({
      hasAcceptedTerms: isAccepted
    });
  };

  saveFormStatus = () => {
    const { saveForm } = this.props;
    const { inputs } = this.state;
    saveForm(inputs);
  };

  onSubmit = e => {
    if (!e) {
      return;
    }

    e.preventDefault();

    const { inputs } = this.state;
    const { name, email, phone, gym, motive, message } = inputs;
    const { sendTicket } = this.props;

    sendTicket({ name, email, phone, message, gym, motive });
  };

  onScroll = () => {
    const content = document.getElementById('contact');
    const header = document.getElementById('contact-header');
    const avatar = document.getElementById('avatar');
    const headerInfoWrapper = document.getElementById('contact-header-info');
    const headerTitle = document.getElementById('contact-header-title');

    const { scrollTop } = content;

    const yTranslate = Math.min(scrollTop, 160);
    const scrollPercent = Math.min(scrollTop / 160, 1);

    header.style.transform = `translate3d(0, -${yTranslate}px, 0)`;
    headerTitle.style.transform = `translate3d(0, ${Math.max(0, scrollPercent * 100)}px, 0)`;
    headerInfoWrapper.style.opacity = Math.max(1 - scrollPercent * 2, 0);

    if (avatar) {
      avatar.style.transform = `translate3d(0, ${Math.max(0, yTranslate)}px, 0)`;
    }
  };

  renderNameInput = () => {
    const { inputs } = this.state;
    const { t, isLoggedIn, userContactData } = this.props;
    const { name } = inputs;

    if (isLoggedIn) {
      return <InfoDetail label={t('screens.Profile.info.labels.name')} value={userContactData.name} />;
    }

    return (
      <TextInput
        placeholder={t('global.terms.name')}
        initialValue={name}
        onChange={this.onNameChanged}
        darkMode
      />
    );
  };

  renderEmailInput = () => {
    const { inputs } = this.state;
    const { t, isLoggedIn, userContactData } = this.props;
    const { email } = inputs;

    if (isLoggedIn) {
      return (
        <InfoDetail
          label={t('screens.Profile.info.labels.email')}
          value={userContactData.email.toLowerCase()}
        />
      );
    }

    return (
      <TextInput
        placeholder={t('global.terms.email')}
        type="email"
        onChange={this.onEmailChanged}
        darkMode
        initialValue={email}
      />
    );
  };

  renderPhoneInput = () => {
    const { inputs } = this.state;
    const { t, isLoggedIn, userContactData } = this.props;
    const { phone } = inputs;

    if (isLoggedIn) {
      return <InfoDetail label={t('screens.Profile.info.labels.phone')} value={userContactData.phone} />;
    }

    return (
      <TextInput
        placeholder={t('global.terms.phone')}
        type="tel"
        onChange={this.onPhoneChanged}
        darkMode
        initialValue={phone}
      />
    );
  };

  renderListToggle = (label, callback) => {
    return (
      <button className="list-toggle" type="button" onClick={callback} tabIndex="-1">
        <b className="list-toggle__label">{label}</b>
        <Image className="list-toggle__icon" src="/img/icons/dropdown_pink.svg" alt="" />
      </button>
    );
  };

  renderMessageInput = () => {
    const { t } = this.props;
    const { inputs } = this.state;
    const { message } = inputs;

    return (
      <TextArea
        placeholder={t('global.terms.message')}
        onChange={this.onMessageChanged}
        initialValue={message}
        darkMode
      />
    );
  };

  renderPrivacy = () => {
    const { isLoggedIn } = this.props;

    if (isLoggedIn) {
      return null;
    }

    return <PrivacyForm onChanged={this.onPrivacyChanged} onVisitLink={this.saveFormStatus} />;
  };

  renderSubmit = () => {
    const { t } = this.props;
    const { inputs, hasAcceptedTerms } = this.state;
    const { motive, gym, name, email, phone, message } = inputs;

    const hasFields = validateEmail(email) && !!phone && !!gym && !!name && !!message && !!motive;
    const canSubmit = hasAcceptedTerms && hasFields;

    return (
      <PrimaryButton
        text={t('global.actions.send')}
        onClick={this.onSubmit}
        isDisabled={!canSubmit}
        type="submit"
      />
    );
  };

  renderGymList = () => {
    const { t, gyms } = this.props;
    const { isShowingGyms } = this.state;

    const gymListCopy = {
      listTitle: t('components.gymSlidingList.listTitle'),
      selectedTitle: t('components.gymSlidingList.selectedTitle'),
      optionsTitle: t('components.gymSlidingList.optionsTitle')
    };

    const mappedGyms = formatGyms(gyms);

    return (
      <SlidingList
        closeList={this.hideLists}
        list={mappedGyms}
        selectedIndex={-1}
        copy={gymListCopy}
        isShowing={isShowingGyms}
        onSelect={this.onGymChanged}
        onDismiss={this.hideLists}
      />
    );
  };

  renderMotivesList = () => {
    const { t, motives } = this.props;
    const { isShowingMotives } = this.state;

    const motiveListCopy = {
      listTitle: t('components.motiveSlidingList.listTitle'),
      selectedTitle: t('components.motiveSlidingList.selectedTitle'),
      optionsTitle: t('components.motiveSlidingList.optionsTitle')
    };

    const mappedMotives = formatMotives(motives);

    return (
      <SlidingList
        closeList={this.hideLists}
        type="motives"
        list={mappedMotives}
        selectedIndex={-1}
        copy={motiveListCopy}
        isShowing={isShowingMotives}
        onSelect={this.onMotiveChanged}
        onDismiss={this.hideLists}
      />
    );
  };

  render() {
    const { t, isLoggedIn } = this.props;
    const { inputs } = this.state;
    const { motive, gym } = inputs;

    const gymToggleLabel = gym ? gym.name : t('screens.Contact.gymSelection');
    const motiveToggleLabel = motive || t('screens.Contact.motiveSelection');

    return (
      <div id="contact" className="contact__container" onScroll={this.onScroll}>
        <ContactHeader isLoggedIn={isLoggedIn} />
        <form className="contact__form" onSubmit={this.onSubmit}>
          <div className="form__row">{this.renderNameInput()}</div>
          <div className="form__row">{this.renderEmailInput()}</div>
          <div className="form__row">{this.renderPhoneInput()}</div>
          <div className="form__row">{this.renderListToggle(gymToggleLabel, this.showGymList)}</div>
          <div className="form__row">{this.renderListToggle(motiveToggleLabel, this.showMotivesList)}</div>
          <div className="form__row">{this.renderMessageInput()}</div>
          <div className="form__row">{this.renderPrivacy()}</div>
          <div className="form__row">{this.renderSubmit()}</div>
        </form>
        <Swipeable onSwipedDown={this.hideLists}>
          {this.renderGymList()}
          {this.renderMotivesList()}
        </Swipeable>
      </div>
    );
  }
}

Contact.propTypes = {
  t: PropTypes.func.isRequired,
  isLoggedIn: PropTypes.bool.isRequired,
  userContactData: PropTypes.shape({
    name: PropTypes.string,
    phone: PropTypes.string,
    email: PropTypes.string,
    defaultGym: PropTypes.string
  }).isRequired,
  hasSent: PropTypes.bool.isRequired,
  sendTicket: PropTypes.func.isRequired,
  gyms: PropTypes.arrayOf(PropTypes.any).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func
  }).isRequired,
  motives: PropTypes.arrayOf(PropTypes.string).isRequired,
  resetSent: PropTypes.func.isRequired,
  saveForm: PropTypes.func.isRequired,
  form: PropTypes.shape({
    gym: PropTypes.shape({
      id: PropTypes.string
    }),
    name: PropTypes.string,
    motive: PropTypes.string,
    message: PropTypes.string,
    phone: PropTypes.string,
    email: PropTypes.string
  }).isRequired,
  onNavigateBack: PropTypes.func.isRequired
};

const mapStateToProps = (state, ownProps) => {
  const { t } = ownProps;
  const gyms = getGymsListOrderedByDistance(state);
  const hasSent = hasSentTicket(state);
  const isLoggedIn = userIsLoggedIn(state);
  const userContactData = isLoggedIn ? getUserContactData(state) : {};
  const form = getContactForm(state);

  const motivesKey = isLoggedIn ? 'loggedIn' : 'loggedOut';
  const motives = t(`screens.Contact.motives.${motivesKey}`, { returnObjects: true });

  return { hasSent, userContactData, isLoggedIn, form, gyms, motives };
};

const mapDispatchToProps = {
  sendTicket: sendTicketAction,
  saveForm: saveFormAction,
  resetSent: resetSentAction
};

export default compose(
  withTranslation(),
  withRouter,
  withMenuNavigation(),
  withBackNavigation('light', false),
  connect(mapStateToProps, mapDispatchToProps)
)(Contact);

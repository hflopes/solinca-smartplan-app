import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import { withTranslation } from 'react-i18next';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { IonRow } from '@ionic/react';
import { Swipeable } from 'react-swipeable';
import Image from '../../../components/Image';
import withBackNavigation from '../../../components/hoc/withBackNavigation';
import Input from '../../../components/inputs/Text';
import PrimaryButton from '../../../components/buttons/PrimaryButton';
import { getGymsListOrderedByDistance } from '../../../redux/selectors/gyms';
import { hasSentLead } from '../../../redux/selectors/support';
import { isFetchingCampaign, getCampaignData } from '../../../redux/selectors/campaigns';
import sendLeadAction from '../../../redux/modules/support/actions/sendLead';
import saveFormAction from '../../../redux/modules/support/actions/saveForm';
import resetSentAction from '../../../redux/modules/support/actions/resetSent';
import reloadAction from '../../../redux/reload/action';
import LayeredPage from '../../../components/LayeredPage';
import SlidingList from '../../../components/SlidingList';
import Checkbox from '../../../components/Checkbox';
import { getContactTOS } from '../../../redux/selectors/terms';
import { validateEmail, validatePhone } from '../../../utils/validations';
import './Lead.scss';

const DEFAULT_BACKGROUND_IMAGE = '/img/backgrounds/leads.jpg';

class Lead extends Component {
  constructor(props) {
    super(props);
    const { gymId, email, gyms, form } = props;
    const { tosData } = props;
    const { authorizeText } = tosData;

    const isValidAuthorizeText = authorizeText && authorizeText.length;

    const gymMatches =
      (gymId || form.gymId) && gyms && gyms.length
        ? gyms.filter(g => g.id === gymId || g.id === form.gymId)
        : null;
    const selectedGym = gymMatches && gymMatches.length ? gymMatches[0] : null;

    this.state = {
      name: form ? form.name : '',
      email: email || form ? form.email : '',
      phone: form ? form.phone : '',
      gymId: gymId || form ? form.gymId : null,
      selectedGym,
      isShowingGymList: false,
      checked: isValidAuthorizeText ? authorizeText.map(() => false) : []
    };
  }

  componentWillMount() {
    const { onNavigateBack } = this.props;

    onNavigateBack(() => {
      const { isShowingGymList } = this.state;
      if (isShowingGymList) {
        this.hideGymList();
        return true;
      }

      return false;
    });
  }

  componentDidMount() {
    const { reload } = this.props;
    reload('campaigns');
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.hasSent) {
      const { history, resetSent } = this.props;

      // does not get un-rendered if we change routes
      // using <Redirect>, so this is a work-around
      setTimeout(() => {
        history.push('/welcome');
        resetSent();
      }, 500);
    }

    const { hasFetchedTOS, tosData } = nextProps;
    const { authorizeText } = tosData;
    const { checked } = this.state;

    if (hasFetchedTOS && (!checked || !checked.length)) {
      this.setState({ checked: authorizeText.map(() => false) });
    }
  }

  onNameChanged = evt => {
    const { value } = evt.target;
    this.setState({
      name: value
    });
  };

  onEmailChanged = evt => {
    const { value } = evt.target;
    this.setState({
      email: value
    });
  };

  onPhoneChanged = evt => {
    const { value } = evt.target;
    this.setState({
      phone: value
    });
  };

  onGymChanged = index => {
    const { gyms } = this.props;

    this.hideGymList();

    this.setState({
      gymId: gyms[index].id,
      selectedGym: gyms[index]
    });
  };

  onCheckToggle = index => {
    const { checked } = this.state;

    this.setState({
      checked: checked.map((item, i) => (i === index ? !item : item))
    });
  };

  onSubmit = e => {
    if (!e) {
      return;
    }

    e.preventDefault();

    const { sendLead } = this.props;
    const { name, email, phone, gymId } = this.state;
    sendLead({ name, email, phone, gymId });
  };

  showGymList = () => {
    this.setState({
      isShowingGymList: true
    });
  };

  hideGymList = () => {
    this.setState({
      isShowingGymList: false
    });
  };

  saveFormStatus = () => {
    const { saveForm } = this.props;
    const { name, email, phone, gymId } = this.state;
    saveForm({ name, email, phone, gymId });
  };

  renderBackground = () => {
    const { campaignData } = this.props;
    const { title, description, highlightText, fileUrl } = campaignData;

    const isValidImage = fileUrl && fileUrl !== '';
    const imageUrl = isValidImage ? fileUrl : DEFAULT_BACKGROUND_IMAGE;
    const gradient = 'linear-gradient(0deg, rgba(0, 0, 0, 0) 30%, rgba(0, 0, 0, 0.6) 100%)';

    const headerStyle = {
      backgroundImage: `${gradient}, url(${imageUrl})`,
      backgroundSize: 'cover',
      backgroundPosition: 'center'
    };

    return (
      <div className="leads__header" style={headerStyle}>
        <div className="header__gym">
          <div id="notch" />
          <div className="separator" />
          <Image src="/img/logo_new.svg" alt="" />
        </div>
        <h1 className="header__title">
          {title}
          <b className="header__title-bold">{` ${highlightText}`}</b>
        </h1>
        <h3 className="header__text">{description}</h3>
      </div>
    );
  };

  renderForeground = () => {
    const { t, tosData, hasFetchedTOS } = this.props;
    const { checked, email, phone, name, selectedGym } = this.state;

    const { authorizeText, title, consentText, files } = tosData;
    const acceptedTerms = hasFetchedTOS && checked.length === _.compact(checked).length;
    const hasFields = validateEmail(email) && validatePhone(phone) && selectedGym && name;
    const canSubmit = acceptedTerms && hasFields;

    return (
      <div className="leads__content">
        <b className="form__title">{t('screens.Leads.fillTheForm')}</b>
        <form onSubmit={this.onSubmit}>
          <Input
            type="text"
            initialValue={name}
            placeholder={t('global.terms.name')}
            darkMode
            onChange={this.onNameChanged}
          />

          <Input
            type="email"
            initialValue={email}
            placeholder={t('global.terms.email')}
            darkMode
            onChange={this.onEmailChanged}
          />

          <Input
            type="tel"
            initialValue={phone}
            placeholder={t('global.terms.phone')}
            darkMode
            onChange={this.onPhoneChanged}
          />

          <button tabIndex="-1" className="open-list-button" type="button" onClick={this.showGymList}>
            <b>{selectedGym ? selectedGym.name : t('global.terms.gym')}</b>
            <Image src="/img/icons/dropdown_pink.svg" alt="" />
          </button>

          {hasFetchedTOS && (
            <div className="leads-tos__container">
              <IonRow>
                <h3 className="tos__title">{title}</h3>
              </IonRow>
              {consentText && (
                <IonRow>
                  <p className="tos__text">{consentText}</p>
                </IonRow>
              )}
              <IonRow>
                <ul className="tos__files">
                  {files.map(file => (
                    <Link
                      onClick={this.saveFormStatus}
                      key={file.url}
                      to={{
                        pathname: '/terms',
                        state: {
                          ...file
                        }
                      }}
                    >
                      <li className="tos__file-item">{file.title}</li>
                    </Link>
                  ))}
                </ul>

                {authorizeText.map((text, index) => (
                  <Checkbox
                    key={text}
                    onToggle={() => this.onCheckToggle(index)}
                    checked={checked.length ? checked[index] : false}
                    label={text}
                  />
                ))}
              </IonRow>
            </div>
          )}
          <PrimaryButton
            text={t('global.actions.confirm')}
            icon="go_white"
            onClick={this.onSubmit}
            isDisabled={!canSubmit}
            type="submit"
          />
        </form>
      </div>
    );
  };

  renderGymList = () => {
    const { t, gyms } = this.props;
    const { isShowingGymList, selectedGym } = this.state;

    const gymListCopy = {
      listTitle: t('components.gymSlidingList.listTitle'),
      selectedTitle: t('components.gymSlidingList.selectedTitle'),
      optionsTitle: t('components.gymSlidingList.optionsTitle')
    };

    const mappedGyms = gyms.map(g => {
      return {
        title: g.name,
        info: g.distance ? `${g.distance.toFixed(1)} km` : null
      };
    });

    const selectedGymIndex = selectedGym ? gyms.indexOf(selectedGym) : -1;

    return (
      <Swipeable onSwipedDown={this.hideGymList}>
        <SlidingList
          list={mappedGyms}
          selectedIndex={selectedGymIndex}
          copy={gymListCopy}
          isShowing={isShowingGymList}
          onSelect={this.onGymChanged}
          onDismiss={this.hideGymList}
        />
      </Swipeable>
    );
  };

  render() {
    const background = <>{this.renderBackground()}</>;
    const foreground = <>{this.renderForeground()}</>;

    return (
      <>
        <LayeredPage
          className="lead-container"
          background={background}
          foreground={foreground}
          gap="400px"
          extra="10vh"
          rounded
        />
        {this.renderGymList()}
      </>
    );
  }
}

Lead.defaultProps = {
  email: '',
  gymId: null,
  tosData: {
    consentText: '',
    title: '',
    authorizeText: [],
    files: []
  }
};

Lead.propTypes = {
  gyms: PropTypes.arrayOf(PropTypes.any).isRequired,
  gymId: PropTypes.string,
  email: PropTypes.string,
  sendLead: PropTypes.func.isRequired,
  t: PropTypes.func.isRequired,
  campaignData: PropTypes.shape({
    title: PropTypes.string,
    description: PropTypes.string,
    highlightText: PropTypes.string,
    fileUrl: PropTypes.string
  }).isRequired,
  hasSent: PropTypes.bool.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func
  }).isRequired,
  hasFetchedTOS: PropTypes.bool.isRequired,
  onNavigateBack: PropTypes.func.isRequired,
  reload: PropTypes.func.isRequired,
  resetSent: PropTypes.func.isRequired,
  saveForm: PropTypes.func.isRequired,
  form: PropTypes.shape({
    gymId: PropTypes.string,
    name: PropTypes.string,
    phone: PropTypes.string,
    email: PropTypes.string
  }).isRequired,
  tosData: PropTypes.shape({
    title: PropTypes.string,
    consentText: PropTypes.string,
    authorizeText: PropTypes.arrayOf(PropTypes.string),
    files: PropTypes.arrayOf(
      PropTypes.shape({
        title: PropTypes.string,
        url: PropTypes.string
      })
    )
  })
};

const mapStateToProps = state => {
  const gyms = getGymsListOrderedByDistance(state);
  const isFetching = isFetchingCampaign(state);
  const campaignData = getCampaignData(state);
  const hasSent = hasSentLead(state);
  const tosData = getContactTOS(state);
  const { email } = state.register;
  const { hasFetched: hasFetchedTOS } = state.terms;
  const { form } = state.support;

  return { gyms, hasSent, isFetching, campaignData, tosData, hasFetchedTOS, email, form };
};

const mapDispatchToProps = {
  sendLead: sendLeadAction,
  saveForm: saveFormAction,
  resetSent: resetSentAction,
  reload: reloadAction
};

export default compose(
  withBackNavigation(),
  withRouter,
  withTranslation(),
  connect(mapStateToProps, mapDispatchToProps)
)(Lead);

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withTranslation } from 'react-i18next';
import SwipeableViews from 'react-swipeable-views';
import GamificationHeader from '../../../components/headers/GamificationHeader';
import withRouteState from '../../../components/hoc/withRouteState';
import withMenuNavigation from '../../../components/hoc/withMenuNavigation';
import withBackNavigation from '../../../components/hoc/withBackNavigation';
import { getPlatform } from '../../../redux/selectors/app';
import Challenges from '../ChallengesContainer';
import MyChallenges from '../MyChallengesContainer';
import RedeemPopup from '../RedeemPopupContainer';
import RemovePopup from '../RemovePopupContainer';
import Catalog from '../CatalogContainer';
import Cart from '../CartContainer';
import './Gamification.scss';

class Gamification extends Component {
  constructor(props) {
    super(props);
    const { lastState } = props;
    let selectedSection = 0;
    if (props.section) {
      selectedSection = getInitialTabIndex(props.section);
    } else if (lastState && lastState.selectedSection) {
      ({ selectedSection } = lastState);
    }
    this.state = {
      selectedSection,
      redeemProductId: null,
      removeProductId: null
    };
  }

  componentDidMount() {
    const { lastState } = this.props;
    if (!lastState) {
      return;
    }

    const { leftScrolls } = lastState;
    if (leftScrolls) {
      leftScrolls.map((leftScroll, index) => {
        return this.scrollToSavedPosition(index, leftScroll);
      });
    }
    const { topScroll } = lastState;
    if (topScroll) {
      this.scrollTopSavedPosition(topScroll);
    }
  }

  componentWillUnmount() {
    const { saveState } = this.props;
    const groups = Array.from(document.querySelectorAll('[class$="__list"]'));
    // const groups = Array.from(document.getElementsByClassName('group__list'));
    const leftScrolls = groups.map(group => group.scrollLeft);
    const container = document.getElementById('gamification');
    const topScroll = container ? container.scrollTop : 0;
    const { selectedSection } = this.state;
    const currentState = { leftScrolls, topScroll, selectedSection };
    saveState(currentState);
  }

  select = index => {
    this.setState({ selectedSection: index });
    this.scrollTo(0);
  };

  scrollTo = y => {
    const container = document.getElementById('gamification');
    container.scrollTo({
      top: y,
      left: 0,
      behaviour: 'smooth'
    });
  };

  scrollToSavedPosition = (index, scroll) => {
    const element = document.querySelectorAll('[class$="__list"]')[index];
    if (element) {
      element.scrollTo({ top: 0, left: scroll, behaviour: 'smooth' });
      const { scrollLeft } = element;
      if (scrollLeft < scroll - 50) {
        setTimeout(() => {
          this.scrollToSavedPosition(index, scroll);
        }, 500);
      }
    } else {
      setTimeout(() => {
        this.scrollToSavedPosition(index, scroll);
      }, 500);
    }
  };

  scrollTopSavedPosition = scroll => {
    const element = document.getElementById('gamification');
    if (element) {
      element.scrollTo({ top: scroll, behaviour: 'smooth' });
    } else {
      setTimeout(() => {
        this.scrollTopSavedPosition(scroll);
      }, 500);
    }
  };

  onScroll = () => {
    const { platform } = this.props;
    const content = document.getElementById('gamification');
    const header = document.getElementById('gamification-header');
    const headerWrapper = document.getElementById('gamification-header-wrapper');

    const { scrollTop } = content;

    if (platform === 'ios' && scrollTop < 0) {
      content.scrollTo(0, 0);
      return;
    }

    let headerInfoHeight = 180;
    const headerHide = document.getElementById('header-hide');
    if (headerHide) {
      headerInfoHeight = headerHide.offsetHeight + 20; // 20 is the margin value
    }

    const yTranslate = Math.min(scrollTop, headerInfoHeight);
    const scrollPercent = scrollTop / headerInfoHeight;

    header.style.transform = `translate3d(0, -${yTranslate}px, 0)`;
    headerWrapper.style.opacity = 1 - scrollPercent;
  };

  render() {
    const { selectedSection, redeemProductId, removeProductId } = this.state;
    const { t } = this.props;

    return (
      <div id="gamification" className="gamification__container full-height-flex" onScroll={this.onScroll}>
        {selectedSection === 2 && (
          <div className="filters-chip__container">
            <Link className="remove-link" to="/gamification/catalog/filters">
              <button type="button" className="filters-chip">
                <img src="/img/icons/filter.svg" alt="" />
                {t('screens.Catalog.button')}
              </button>
            </Link>
          </div>
        )}
        <GamificationHeader
          tabs={t('screens.Gamification.tabs', { returnObjects: true })}
          onSelect={this.select}
          defaultIndex={selectedSection}
        />
        <div className="gamification__content">
          <SwipeableViews className="swipe-container" index={selectedSection} disabled>
            {selectedSection === 0 ? <Challenges /> : null}
            {selectedSection === 1 ? <MyChallenges /> : null}
            {selectedSection === 2 ? <Catalog /> : null}
            {selectedSection === 3 ? (
              <Cart
                onSelectRedeem={id => this.setState({ redeemProductId: id })}
                onSelectRemove={id => this.setState({ removeProductId: id })}
              />
            ) : null}
          </SwipeableViews>
        </div>
        {redeemProductId && (
          <RedeemPopup
            productId={redeemProductId}
            onDismiss={() => {
              this.setState({ redeemProductId: null });
            }}
          />
        )}
        {removeProductId && (
          <RemovePopup
            productId={removeProductId}
            onDismiss={() => {
              this.setState({ removeProductId: null });
            }}
          />
        )}
      </div>
    );
  }
}

Gamification.defaultProps = {
  section: ''
};

Gamification.propTypes = {
  platform: PropTypes.string.isRequired,
  section: PropTypes.string,
  saveState: PropTypes.func.isRequired,
  lastState: PropTypes.shape({
    leftScrolls: PropTypes.arrayOf(PropTypes.number),
    topScroll: PropTypes.number,
    selectedSection: PropTypes.number
  }).isRequired,
  t: PropTypes.func.isRequired
};

function getInitialTabIndex(section) {
  switch (section) {
    case 'catalog':
      return 2;
    case 'cart':
      return 3;
    default:
      return 0;
  }
}

const mapStateToProps = state => {
  const platform = getPlatform(state);
  return { platform };
};

const mapDispatchToProps = {};

export default compose(
  withTranslation(),
  connect(mapStateToProps, mapDispatchToProps),
  withMenuNavigation(),
  withBackNavigation('light', false),
  withRouteState('Gamification')
)(Gamification);

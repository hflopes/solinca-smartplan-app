import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { getCartRewards, getPurchased } from '../../../redux/selectors/rewards';
import { getMonth } from '../../../utils/dates';
import CartProductCard from '../../../components/cards/CartProductCard';
import './Cart.scss';

function Cart({ onSelectRedeem, onSelectRemove }) {
  const { t } = useTranslation();

  const rewards = useSelector(getCartRewards);
  const purchased = useSelector(getPurchased);

  return (
    <div className="cart__container">
      {rewards && rewards.length > 0 && (
        <>
          <p className="section-title">{t('screens.Cart.to_redeem')}</p>
          <div className="cart__list">
            {rewards.map(r => (
              <CartProductCard
                key={`cart-${r.id}`}
                product={r}
                onRemoveClicked={() => onSelectRemove(r.id)}
                onSelectRedeem={onSelectRedeem}
              />
            ))}
          </div>
        </>
      )}
      {purchased && purchased.length > 0 && (
        <>
          <p className="section-title">{t('screens.Cart.purchased')}</p>
          <div className="purchased__list">
            {purchased.map(r => (
              <PurchasedProduct key={`purchased-${r.id}`} product={r} />
            ))}
          </div>
        </>
      )}
    </div>
  );
}

Cart.propTypes = {
  onSelectRedeem: PropTypes.func.isRequired,
  onSelectRemove: PropTypes.func.isRequired
};

function PurchasedProduct({ product }) {
  const { imageURL, name, points, datePurchased } = product;

  const day = datePurchased.getDate();
  const month = datePurchased.getMonth();
  const year = datePurchased.getFullYear();

  const date = `${day} ${getMonth(month)}, ${year}`;

  return (
    <div className="purchased-product-card">
      <img src={imageURL} alt={name} />
      <div className="purchase-details">
        <span className="date">{date}</span>
        <b className="title">{name}</b>
        <span className="points">{`${points} pts`}</span>
      </div>
    </div>
  );
}

PurchasedProduct.propTypes = {
  product: PropTypes.shape({
    name: PropTypes.string,
    imageURL: PropTypes.string,
    points: PropTypes.number,
    datePurchased: PropTypes.instanceOf(Date)
  }).isRequired
};

export default Cart;

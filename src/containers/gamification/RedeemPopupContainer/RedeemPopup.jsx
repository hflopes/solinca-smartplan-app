import React, { useState, Fragment } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { IonAlert } from '@ionic/react';
import { getReward } from '../../../redux/selectors/rewards';
import SwipeButton from '../../../components/buttons/SwipeButton';
import redeemAction from '../../../redux/modules/gamification/rewards/actions/redeem';
import './RedeemPopup.scss';

function customRender(text) {
  function renderBold(bold) {
    return bold.split('**').map((t, i) => {
      if (i % 2 === 0) {
        return <Fragment key={t}>{t}</Fragment>;
      }

      return <b key={t}>{t}</b>;
    });
  }

  const blocks = text.split('\n').map(b => {
    const lines = b
      .split('))')
      .map(s => s.replace('((', ''))
      .filter(s => s.length);

    if (lines && lines.length > 1) {
      return (
        <ul key={b}>
          {lines.map(l => (
            <li key={l}>{renderBold(l)}</li>
          ))}
        </ul>
      );
    }

    return <p key={b}>{renderBold(b)}</p>;
  });

  return blocks;
}

function RedeemPopup({ productId, onDismiss }) {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const reward = useSelector(state => getReward(state, productId));
  const [hasSwiped, setSwiped] = useState(false);

  async function handleRedeem() {
    try {
      await dispatch(redeemAction(productId));
      onDismiss();
    } catch (error) {
      onDismiss();
    }
  }

  if (!reward) {
    return null;
  }

  return (
    <div className="redeem-popup__container">
      <div className="redeem-popup">
        <div className="top">
          <button type="button" onClick={onDismiss}>
            <img src="/img/icons/clear.svg" alt="" />
          </button>
        </div>
        <div className="mid">
          <img src={reward.imageURL} alt={reward.name} />
          <b>{reward.name}</b>
          <span>{`${reward.points} pts`}</span>
        </div>
        <div className="bottom">{customRender(t('screens.RedeemPopup.helper'))}</div>
        <SwipeButton text={t('screens.RedeemPopup.button')} onSwipe={() => setSwiped(true)} />
      </div>
      <IonAlert
        isOpen={hasSwiped}
        header={t('screens.RedeemPopup.title')}
        message={t('screens.RedeemPopup.text')}
        buttons={[
          {
            text: t('global.actions.cancel'),
            role: 'cancel',
            cssClass: 'secondary',
            handler: onDismiss
          },
          {
            text: t('global.actions.confirm'),
            handler: handleRedeem
          }
        ]}
      />
    </div>
  );
}

RedeemPopup.propTypes = {
  productId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  onDismiss: PropTypes.func.isRequired
};

export default RedeemPopup;

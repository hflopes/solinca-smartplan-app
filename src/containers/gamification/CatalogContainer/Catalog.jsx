import React from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { getFilteredRewards, getFilters } from '../../../redux/selectors/rewards';
import FeaturedProductCard from '../../../components/cards/FeaturedProductCard';
import ProductCard from '../../../components/cards/ProductCard';
import './Catalog.scss';

function Catalog() {
  const { t } = useTranslation();
  const rewards = useSelector(getFilteredRewards);
  const filters = useSelector(getFilters);

  const featuredRewards = rewards.filter(r => r.isFeatured);

  return (
    <div className="catalog__container">
      {filters && filters.length > 0 && (
        <div className="filters-container">
          {filters.map(f => (
            <div key={`${f.key}-${f.value}`} className="filter-badge">
              {f.value}
            </div>
          ))}
        </div>
      )}
      {featuredRewards.length ? (
        <>
          <p className="section-title">{t('screens.Challenges.highlighted')}</p>
          <div className="featured__list">
            {featuredRewards.map(r => (
              <FeaturedProductCard key={`featured-${r.id}`} product={r} />
            ))}
          </div>
        </>
      ) : null}
      <p className="section-title">{t('screens.Catalog.all_products')}</p>
      <div className="all__list">
        {rewards.map(r => (
          <ProductCard key={`product-${r.id}`} product={r} />
        ))}
      </div>
    </div>
  );
}

export default Catalog;

import React, { useState, useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Link, useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { motion, AnimatePresence } from 'framer-motion';
import classNames from 'classnames';
import { withTranslation } from 'react-i18next';
import PrimaryButton from '../../../components/buttons/PrimaryButton';
import CheckBox from '../../../components/Checkbox';
import { getAllFilters } from '../../../redux/selectors/rewards';
import { applyFilters } from '../../../redux/modules/gamification/rewards/actions/filter';
import './CatalogFilters.scss';

function CollapsableMenu({ label, options, onOptionSelected }) {
  const [expanded, setExpanded] = useState(true);
  const onToggle = useCallback(() => setExpanded(!expanded), [expanded]);

  return (
    <>
      <motion.header initial={false} onClick={onToggle}>
        <button
          className={classNames('list-toggle', { '-expanded': expanded })}
          type="button"
          onClick={onToggle}
        >
          <b className="list-toggle__label">{label}</b>
          <img className="list-toggle__icon" src="/img/icons/dropdown_pink.svg" alt="" />
        </button>
      </motion.header>
      <AnimatePresence initial={false}>
        {expanded && (
          <motion.section
            key="content"
            initial="collapsed"
            animate="open"
            exit="collapsed"
            variants={{
              open: { opacity: 1, height: 'auto' },
              collapsed: { opacity: 0, height: 0 }
            }}
            transition={{ duration: 0.1 }}
          >
            {options &&
              options.map(o => (
                <div key={o.title} className="filter-option">
                  <b className="filter-option__label">{o.title}</b>
                  <CheckBox checked={o.value} onToggle={() => onOptionSelected(o)} />
                </div>
              ))}
          </motion.section>
        )}
      </AnimatePresence>
    </>
  );
}

function CatalogFilters({ t }) {
  const router = useHistory();
  const dispatch = useDispatch();

  const savedFilters = useSelector(getAllFilters);
  const [filters, setFilters] = useState({ brands: [], categories: [] });

  function toggleBrandFilter(o) {
    setFilters({
      brands: filters.brands.map(f => (f.title === o.title ? { ...f, value: !f.value } : f)),
      categories: filters.categories
    });
  }

  function toggleCategoryFilter(o) {
    setFilters({
      brands: filters.brands,
      categories: filters.categories.map(f => (f.title === o.title ? { ...f, value: !f.value } : f))
    });
  }

  function handleSubmit() {
    const formattedFilters = [
      ...filters.brands.filter(b => b.value).map(b => ({ key: 'brand', value: b.title })),
      ...filters.categories.filter(b => b.value).map(b => ({ key: 'category', value: b.title }))
    ];

    dispatch(applyFilters(formattedFilters));
    router.push('/gamification/catalog');
  }

  useEffect(() => {
    setFilters(savedFilters);
  }, [savedFilters]);

  return (
    <div className="catalog-filters__container">
      <button className="exit-btn" type="button">
        <Link to="/gamification/catalog">
          <img src="/img/icons/clear.svg" alt="" />
        </Link>
      </button>
      <div className="content">
        <p className="section-title">{t('screens.Catalog.filter.title')}</p>
        <CollapsableMenu
          label={t('screens.Catalog.filter.brands')}
          options={filters.brands}
          onOptionSelected={toggleBrandFilter}
        />
        <CollapsableMenu
          label={t('screens.Catalog.filter.categories')}
          options={filters.categories}
          onOptionSelected={toggleCategoryFilter}
        />
      </div>
      <PrimaryButton text={t('screens.Catalog.filter.button')} onClick={handleSubmit} />
    </div>
  );
}

CollapsableMenu.defaultProps = {
  options: []
};

CollapsableMenu.propTypes = {
  label: PropTypes.string.isRequired,
  onOptionSelected: PropTypes.func.isRequired,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string,
      value: PropTypes.bool
    })
  )
};

CatalogFilters.propTypes = {
  t: PropTypes.func.isRequired
};

export default withTranslation()(CatalogFilters);

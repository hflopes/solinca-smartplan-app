import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withTranslation } from 'react-i18next';
import _ from 'lodash';
import withRouteState from '../../../components/hoc/withRouteState';
import ChallengeCard from '../../../components/cards/ChallengeCard';
import ChallengeHighlightedCard from '../../../components/cards/ChallengeHighlightedCard';
import InviteChallengeCard from '../../../components/cards/InviteChallengeCard';
import {
  getChallengesGrouped,
  getHighlightedChallenges,
  getInviteFriendsChallenge
} from '../../../redux/selectors/challenges';
import './Challenges.scss';

const ChallengeGroup = ({ title, challenges }) => (
  <div key={title} id={title} className="challenge-group__container">
    <b className="group__title">{title}</b>
    <ul className="group__list">
      {challenges.map(challenge => (
        <li key={challenge.id} className="group__list-item">
          <ChallengeCard challenge={challenge} />
        </li>
      ))}
    </ul>
  </div>
);

const HighlightGroup = ({ highlighted }) => (
  <div className="highlight__container">
    <ul className="highlight__list">
      {highlighted.map(challenge => (
        <li key={challenge.id} className="highlight__list-item">
          <ChallengeHighlightedCard challenge={challenge} />
        </li>
      ))}
    </ul>
  </div>
);

HighlightGroup.propTypes = {
  highlighted: PropTypes.arrayOf(PropTypes.shape({})).isRequired
};

class Challenges extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { groupedChallenges, highlightedChallenges, inviteFriendsChallenge, t } = this.props;

    return (
      <>
        {highlightedChallenges.length ? (
          <>
            <p className="challenges_title">{t('screens.Challenges.highlighted')}</p>
            <HighlightGroup highlighted={highlightedChallenges} />
          </>
        ) : null}
        <InviteChallengeCard challenge={inviteFriendsChallenge} />
        <p className="challenges_title">{t('screens.Challenges.other')}</p>
        <div className="gamification__content-other">
          {_.map(groupedChallenges, ({ groupDescription, challenges }) => (
            <ChallengeGroup key={groupDescription} title={groupDescription} challenges={challenges} />
          ))}
        </div>
      </>
    );
  }
}

Challenges.propTypes = {
  groupedChallenges: PropTypes.shape({}).isRequired,
  highlightedChallenges: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  inviteFriendsChallenge: PropTypes.shape({}).isRequired,
  t: PropTypes.func.isRequired
};

ChallengeGroup.propTypes = {
  challenges: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  title: PropTypes.string.isRequired
};

const mapStateToProps = state => {
  const groupedChallenges = getChallengesGrouped(state);
  const highlightedChallenges = getHighlightedChallenges(state);
  const inviteFriendsChallenge = getInviteFriendsChallenge(state);
  return { groupedChallenges, highlightedChallenges, inviteFriendsChallenge };
};

const mapDispatchToProps = {};

export default compose(
  withTranslation(),
  connect(mapStateToProps, mapDispatchToProps),
  withRouteState('GamificationChallenges')
)(Challenges);

import React from 'react';
// import React, { useState } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { connect, useSelector } from 'react-redux';
import { compose } from 'redux';
import { Plugins } from '@capacitor/core';
import { withTranslation } from 'react-i18next';
// import { IonAlert } from '@ionic/react';
import LayeredPage from '../../../components/LayeredPage';
import ChallengeDetailsCard from '../../../components/cards/ChallengeDetailsCard';
import PrimaryButton from '../../../components/buttons/PrimaryButton';
import Badge from '../../../components/Badge';
import Rule from './components/Rule';
import Stamps from './components/Stamps';
import Ranking from './components/Ranking';
import withRouteState from '../../../components/hoc/withRouteState';
import withBackNavigation from '../../../components/hoc/withBackNavigation';
import shareAction from '../../../redux/modules/gamification/challenges/actions/share';
import joinAction from '../../../redux/modules/gamification/challenges/actions/join';
import cancelAction from '../../../redux/modules/gamification/challenges/actions/cancel';
import { getChallenge, isJoiningOrCancelling } from '../../../redux/selectors/challenges';
import './ChallengeDetails.scss';

const { Share } = Plugins;

function ChallengeDetails({ id, join, cancel, share, t }) {
  // const [alert, setAlert] = useState(false);
  const challenge = useSelector(state => getChallenge(state, id));
  const disableButton = useSelector(state => isJoiningOrCancelling(state, id));

  if (!challenge) return null;

  const {
    rulesGamification,
    description,
    group,
    hasJoined,
    hasFinished,
    totalStamps,
    filledStamps,
    inviteCode,
    progress,
    dateBegin,
    dateEnd,
    dateLastUpdate,
    userRanking,
    hasRanking,
    usersInChallenge,
    inviteURL,
    inviteText
  } = challenge;

  let updatedCopy = '';
  if (dateLastUpdate) {
    const diffMinutes = moment().diff(moment(dateLastUpdate), 'minutes');
    if (diffMinutes < 60) {
      if (diffMinutes < 2) {
        updatedCopy = t('screens.ChallengeDetails.updated_now');
      } else {
        updatedCopy = t('screens.ChallengeDetails.updated_minutes', { minutes: diffMinutes });
      }
    } else {
      const diffHours = moment().diff(moment(dateLastUpdate), 'hours');
      if (diffHours < 12) {
        if (diffHours < 2) {
          updatedCopy = t('screens.ChallengeDetails.updated_hour');
        } else {
          updatedCopy = t('screens.ChallengeDetails.updated_hours', { hours: diffHours });
        }
      } else {
        const dateUpdated = moment(dateLastUpdate).format('HH:mm [de] DD/MM');
        updatedCopy = t('screens.ChallengeDetails.updated_date', { date: dateUpdated });
      }
    }
  }

  const groupCode = group && group.code;

  const action = () => {
    // if (!alert) {
    //   setAlert(true);
    //   return;
    // }
    const data = {
      DateBegin: dateBegin,
      DateEnd: dateEnd,
      ChallengeId: id
    };
    if (hasJoined) {
      data.Active = 0;
      cancel(data);
    } else {
      data.Active = 1;
      join(data);
    }
    // setAlert(false);
  };

  // const onDismiss = () => {
  //   setAlert(false);
  // };

  const shareDialog = async () => {
    try {
      const title = t('screens.ChallengeDetails.invite_code_title');
      const copy = inviteText;
      const url = inviteURL;
      await Share.share({
        title,
        text: copy,
        url,
        dialogTitle: title
      });
      share(id, challenge.name, groupCode);
    } catch (e) {
      console.log(e);
    }
  };

  const cardHeight = hasJoined ? 120 : 170;
  const headerHeight = hasJoined ? 420 : 380;
  const badgeImage = challenge.group && challenge.group.url_image_enabled;
  const buttonText = hasJoined ? t('screens.ChallengeDetails.cancel') : t('screens.ChallengeDetails.accept');
  const isInviteChallenge = groupCode === 'convidar_amigo';
  const showButton = !isInviteChallenge && !hasFinished;

  const background = (
    <div className="challenge__background">
      {badgeImage ? <Badge size="medium" image={badgeImage} /> : <div style={{ height: '26vw' }} />}
      <h1 className="challenge__title">{challenge.name || challenge.description}</h1>
      {hasJoined && !isInviteChallenge && (
        <>
          <div className="progress-bar">
            <div className="progress-bar__filled" style={{ width: `${progress}%` }} />
          </div>
          <span className="progress">{t('screens.ChallengeDetails.progress', { progress })}</span>
          {updatedCopy ? <span className="updated">{updatedCopy}</span> : null}
        </>
      )}
    </div>
  );

  const foreground = (
    <div className="challenge__foreground" style={{ paddingTop: `calc(7vw + ${cardHeight / 2}px)` }}>
      {hasRanking ? <Ranking ranking={userRanking} total={usersInChallenge} /> : null}
      <div className="challenge__description">
        <span className="section-label">{t('screens.ChallengeDetails.about')}</span>
        <p className="challenge__description-text">{description}</p>
      </div>

      {rulesGamification && rulesGamification.length ? (
        <div className="challenge__rulesGamification">
          {rulesGamification.map(({ title, description: ruleDescription }) => (
            <Rule key={title} title={title} description={ruleDescription} />
          ))}
        </div>
      ) : null}

      {totalStamps ? (
        <div className="challenge__stamps">
          <Stamps total={totalStamps} filled={filledStamps} />
        </div>
      ) : null}

      {isInviteChallenge && (
        <>
          <div className="invite-code">
            <span className="invite-code__label">{t('screens.ChallengeDetails.invite_code')}</span>
            <b className="invite-code__value">{inviteCode}</b>
          </div>
          <PrimaryButton text={t('screens.ChallengeDetails.invite_code_share')} onClick={shareDialog} />
        </>
      )}
      {showButton && (
        <PrimaryButton
          text={buttonText}
          onClick={action}
          isDisabled={disableButton}
          isLoading={disableButton}
        />
      )}

      {/* <IonAlert
        isOpen={alert}
        header={t('screens.RedeemPopup.title')}
        message={t('screens.RedeemPopup.text')}
        buttons={[
          {
            text: t('global.actions.cancel'),
            role: 'cancel',
            cssClass: 'secondary',
            handler: onDismiss
          },
          {
            text: t('global.actions.confirm'),
            handler: action
          }
        ]}
      /> */}
    </div>
  );

  const edge = (
    <div className="challenge__edge">
      <ChallengeDetailsCard challenge={challenge} hasJoined={hasJoined} hasFinished={hasFinished} />
    </div>
  );

  return (
    <LayeredPage
      className="challenge__container"
      background={background}
      foreground={foreground}
      edge={edge}
      gap={`calc(4vh + ${headerHeight}px)`}
      edgeGap={`${cardHeight / 2}px`}
      rounded
    />
  );
}

const mapDispatchToProps = {
  join: joinAction,
  cancel: cancelAction,
  share: shareAction
};

ChallengeDetails.propTypes = {
  id: PropTypes.string.isRequired,
  cancel: PropTypes.func.isRequired,
  join: PropTypes.func.isRequired,
  share: PropTypes.func.isRequired,
  t: PropTypes.func.isRequired
};

export default compose(
  withBackNavigation('light'),
  withTranslation(),
  connect(null, mapDispatchToProps),
  withRouteState('ChallengeDetails')
)(ChallengeDetails);

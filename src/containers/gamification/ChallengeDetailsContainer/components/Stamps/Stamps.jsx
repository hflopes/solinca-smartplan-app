import React from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withTranslation } from 'react-i18next';
import Image from '../../../../../components/Image';
import './Stamps.scss';

function Stamps({ total, filled, t }) {
  const width = window.innerWidth * 0.86;
  const spacing = 10;

  const stampsPerRow = width < 280 ? 4 : 5;
  const stampSize = Math.floor((width - (stampsPerRow - 1) * spacing) / stampsPerRow);

  const stampStyle = { width: stampSize, height: stampSize, margin: spacing / 2 };
  const listStyle = { marginLeft: -(spacing / 2), marginRight: -(spacing / 2) };

  const cards = Math.round(filled / total);
  const filledNew = filled % total;

  return (
    <div className="stamps__container">
      <div className="stamps__header">
        <span className="section-label">{t('screens.ChallengeDetails.stamps_label')}</span>
        <b className="stamps-value">{`${filled} / ${total}`}</b>
      </div>
      {_.times(cards, c => {
        const allFilled = c > 0;
        return (
          <div className="stamps__list" style={listStyle}>
            {_.times(total, i => {
              const isFilled = allFilled || i < filledNew;
              return (
                <div
                  key={`challenge-stamp-${i}`}
                  className={classNames('stamp', { '-filled': isFilled })}
                  style={stampStyle}
                >
                  <Image src="/img/logo_lesmills.svg" alt="" />
                </div>
              );
            })}
          </div>
        );
      })}
    </div>
  );
}

Stamps.propTypes = {
  t: PropTypes.func.isRequired,
  total: PropTypes.number.isRequired,
  filled: PropTypes.number.isRequired
};

export default withTranslation()(Stamps);

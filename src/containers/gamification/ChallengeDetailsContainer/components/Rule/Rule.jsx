import React, { useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { motion, AnimatePresence } from 'framer-motion';
import Image from '../../../../../components/Image';
import './Rule.scss';

function Rule({ title, description }) {
  const [expanded, setExpanded] = useState(false);

  const onToggle = useCallback(() => setExpanded(e => !e), []);

  return (
    <div className={classNames('challenge__rule', { '-expanded': expanded })}>
      <motion.header initial={false} onClick={onToggle}>
        <div className="rule-header">
          <b className="rule-title">{title}</b>
          <Image className="rule-toggle" src="/img/icons/dropdown_pink.svg" alt="" />
        </div>
      </motion.header>

      <AnimatePresence initial={false}>
        {expanded && (
          <motion.section
            key="content"
            initial="collapsed"
            animate="open"
            exit="collapsed"
            variants={{
              open: { opacity: 1, height: 'auto' },
              collapsed: { opacity: 0, height: 0 }
            }}
            transition={{ duration: 0.1 }}
          >
            <p className="rule-description">{description}</p>
          </motion.section>
        )}
      </AnimatePresence>
    </div>
  );
}

Rule.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired
};

export default Rule;

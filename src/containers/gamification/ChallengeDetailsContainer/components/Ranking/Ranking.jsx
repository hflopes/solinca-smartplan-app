import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { withTranslation } from 'react-i18next';
import { getBaseGym } from '../../../../../redux/selectors/gyms';
import Image from '../../../../../components/Image';
import Avatar from '../../../../../components/Avatar';
import './Ranking.scss';

function Ranking({ total, ranking, t }) {
  const baseGym = useSelector(state => getBaseGym(state));
  const gymName = baseGym ? baseGym.name : '';
  return (
    <div className="challenge__ranking">
      <div className="ranking-orb">
        <Avatar />
        <h2 className="ranking-place">{t('screens.ChallengeDetails.ranking_position', { ranking })}</h2>
        <span className="ranking-label">{t('screens.ChallengeDetails.ranking_label')}</span>
      </div>

      <div className="ranking-participants">
        <div className="participants-img">
          <Image className="participant-front" src="/img/icons/menu_profile_gray.svg" alt="" />
          <Image className="participant-back" src="/img/icons/menu_profile_gray.svg" alt="" />
        </div>
        <span className="participant-count">
          {t('screens.ChallengeDetails.ranking_total', { total, gym: gymName })}
        </span>
      </div>

      <b className="ranking-motivation">{t('screens.ChallengeDetails.ranking_motivation')}</b>
    </div>
  );
}

Ranking.defaultProps = {
  total: 0,
  ranking: 0
};

Ranking.propTypes = {
  t: PropTypes.func.isRequired,
  total: PropTypes.number,
  ranking: PropTypes.number
};

export default withTranslation()(Ranking);

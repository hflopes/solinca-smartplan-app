import React from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import LayeredPage from '../../../components/LayeredPage';
import PrimaryButton from '../../../components/buttons/PrimaryButton';
import { getReward, getCartIds } from '../../../redux/selectors/rewards';
import { getUserPoints } from '../../../redux/selectors/user';
import withBackNavigation from '../../../components/hoc/withBackNavigation';
import { getMonth } from '../../../utils/dates';
import { addToCart, removeFromCart } from '../../../redux/modules/gamification/rewards/actions/cart';
import './RewardDetails.scss';

const DARK_GRADIENT = 'linear-gradient(0deg, rgba(0,0,0,0.4) 0%, rgba(0,0,0,0.4) 100%)';

function RewardDateCard({ duration, points }) {
  return (
    <div className="reward-date__card">
      <div className="card-duration">{duration}</div>
      <div className="card-points">{`${points} pts`}</div>
    </div>
  );
}

RewardDateCard.propTypes = {
  duration: PropTypes.string.isRequired,
  points: PropTypes.number.isRequired
};

function RewardDetails({ id }) {
  const router = useHistory();
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const reward = useSelector(state => getReward(state, id));
  const cartIds = useSelector(getCartIds);
  const userPoints = useSelector(getUserPoints);

  if (!reward) {
    return null;
  }

  const { name, imageURL, properties, description, characteristics, points } = reward;
  const brands = properties.filter(p => p.type === 'brand');
  const brand = brands.length ? brands[0] : {};

  const canRedeem = userPoints >= points;
  const hasInCart = cartIds && cartIds.length > 0 && cartIds.includes(id);

  const cardHeight = 85;
  const headerHeight = 320;

  function handleAddToCart() {
    dispatch(addToCart(id));
    router.push('/gamification/cart');
  }

  function handleRemoveFromCart() {
    dispatch(removeFromCart(id));
  }

  const background = (
    <div
      className="reward__background"
      style={{
        background: `${DARK_GRADIENT}, url("${imageURL}")`,
        backgroundSize: 'cover',
        backgroundPosition: 'center'
      }}
    >
      <h1>{name}</h1>
    </div>
  );

  const foreground = (
    <div className="reward__foreground" style={{ paddingTop: `calc(7vw + ${cardHeight / 2}px)` }}>
      {brand ? (
        <div className="reward-brand">
          <img src={brand.image} alt="" />
        </div>
      ) : null}
      <span className="section-title">{t('screens.Product.title')}</span>
      {description ? (
        <>
          <b>{t('screens.Product.description')}</b>
          <p>{description}</p>
        </>
      ) : null}
      {characteristics ? (
        <>
          <b>{t('screens.Product.characteristics')}</b>
          <p>{characteristics}</p>
        </>
      ) : null}
    </div>
  );

  const edge = (
    <div className="reward__edge">
      <RewardDateCard
        duration={getDuration(reward.dateHourStart, reward.dateHourEnd)}
        points={reward.points}
      />
    </div>
  );

  const cta = (
    <div className="reward__cta">
      {getCTAButton(hasInCart, canRedeem, handleRemoveFromCart, handleAddToCart, t)}
    </div>
  );

  return (
    <LayeredPage
      className="reward__container"
      background={background}
      foreground={foreground}
      edge={edge}
      cta={cta}
      gap={`calc(4vh + ${headerHeight}px)`}
      edgeGap={`${cardHeight / 2}px`}
      rounded
    />
  );
}

RewardDetails.propTypes = {
  id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired
};

function getCTAButton(hasInCart, canRedeem, onRemove, onAdd, t) {
  if (hasInCart) {
    return <PrimaryButton text={t('screens.Product.remove')} onClick={onRemove} />;
  }

  return (
    <PrimaryButton
      text={canRedeem ? t('screens.Product.add') : t('screens.Product.noPoints')}
      onClick={onAdd}
      isDisabled={!canRedeem}
    />
  );
}

function getDuration(dateBegin, dateEnd) {
  const pad = num => (num < 10 ? `0${num}` : num);

  const startDate = dateBegin.getDate();
  const startMonth = dateBegin.getMonth();
  const endDate = dateEnd.getDate();
  const endMonth = dateEnd.getMonth();

  const start = `${pad(startDate)} ${getMonth(startMonth).slice(0, 3)}.`;
  const end = `${pad(endDate)} ${getMonth(endMonth).slice(0, 3)}.`;

  return `${start} - ${end}`;
}

export default withBackNavigation('light')(RewardDetails);

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withTranslation } from 'react-i18next';
import moment from 'moment';
import withRouteState from '../../../components/hoc/withRouteState';
import { getOngoingChallenges, getFinishedChallenges } from '../../../redux/selectors/challenges';
import ChallengeCard from '../../../components/cards/ChallengeCard';
import Badge from '../../../components/Badge';
import { getUnlockedBadges } from '../../../redux/selectors/badges';
import './MyChallenges.scss';

const OngoingGroup = ({ challenges }) => (
  <div className="ongoing__container">
    <ul className="ongoing__list">
      {challenges.map(challenge => (
        <li key={challenge.id} className="ongoing__list-item">
          <ChallengeCard challenge={challenge} />
        </li>
      ))}
    </ul>
  </div>
);

const FinishedGroup = ({ challenges }) => (
  <div className="ongoing__container">
    <ul className="ongoing__list">
      {challenges.map(challenge => (
        <li key={challenge.id} className="ongoing__list-item">
          <ChallengeCard challenge={challenge} />
        </li>
      ))}
    </ul>
  </div>
);

const Badges = ({ badges, t }) => (
  <div className="badges__container">
    <ul className="badges__list">
      {badges.map(badge => (
        <li key={badge.id} className="badges__list-item">
          <Badge size="medium" image={badge.image} />
          {badge.unlocked && (
            <p className="badges__list-date">{moment(badge.dateConclusion).format('DD MMM. YYYY')}</p>
          )}
          <p className="badges__list-name">{badge.name}</p>
          {badge.unlocked && badge.points ? (
            <p className="badges__list-points">
              {t('screens.MyChallenges.points', { points: badge.points })}
            </p>
          ) : null}
        </li>
      ))}
    </ul>
  </div>
);

Badges.propTypes = {
  t: PropTypes.func.isRequired,
  badges: PropTypes.arrayOf(PropTypes.shape({})).isRequired
};

class MyChallenges extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { t, ongoingChallenges, finishedChallenges, badges } = this.props;
    return (
      <>
        <p className="mychallenges__title">{t('screens.MyChallenges.ongoing')}</p>
        <OngoingGroup challenges={ongoingChallenges} />
        <p className="mychallenges__title">{t('screens.MyChallenges.finished')}</p>
        <FinishedGroup challenges={finishedChallenges} />
        <h1 className="mychallenges__badges">{badges.length}</h1>
        <p className="mychallenges__text">
          {badges.length > 1 ? t('screens.MyChallenges.badges') : t('screens.MyChallenges.badge')}
        </p>
        <Badges badges={badges} t={t} />
      </>
    );
  }
}

MyChallenges.propTypes = {
  ongoingChallenges: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  finishedChallenges: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  badges: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  t: PropTypes.func.isRequired
};

OngoingGroup.propTypes = {
  challenges: PropTypes.arrayOf(PropTypes.shape({})).isRequired
};

FinishedGroup.propTypes = {
  challenges: PropTypes.arrayOf(PropTypes.shape({})).isRequired
};

const mapStateToProps = state => {
  const ongoingChallenges = getOngoingChallenges(state);
  const finishedChallenges = getFinishedChallenges(state);
  const badges = getUnlockedBadges(state);

  return { ongoingChallenges, finishedChallenges, badges };
};

const mapDispatchToProps = {};

export default compose(
  withTranslation(),
  connect(mapStateToProps, mapDispatchToProps),
  withRouteState('GamificationChallenges')
)(MyChallenges);

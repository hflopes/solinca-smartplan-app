import React from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { getReward } from '../../../redux/selectors/rewards';
import PrimaryButton from '../../../components/buttons/PrimaryButton';
import { removeFromCart } from '../../../redux/modules/gamification/rewards/actions/cart';
import './RemovePopup.scss';

function RemovePopup({ productId, onDismiss }) {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const reward = useSelector(state => getReward(state, productId));

  function handleRemove() {
    dispatch(removeFromCart(productId));
    onDismiss();
  }

  if (!reward) {
    return null;
  }

  return (
    <div className="remove-popup__container">
      <div className="remove-popup">
        <div className="top">
          <button type="button" onClick={onDismiss}>
            <img src="/img/icons/clear.svg" alt="" />
          </button>
        </div>
        <div className="mid">
          <img src={reward.image} alt="" />
          <b>{reward.name}</b>
          <span>{`${reward.points} pts`}</span>
        </div>
        <div className="bottom">
          <b>{t('screens.Cart.remove')}</b>
        </div>
        <PrimaryButton text={t('global.actions.confirm')} onClick={handleRemove} />
      </div>
    </div>
  );
}

RemovePopup.propTypes = {
  productId: PropTypes.string.isRequired,
  onDismiss: PropTypes.func.isRequired
};

export default RemovePopup;

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import logoutAction from '../../../redux/modules/auth/actions/logout';
import { userIsLoggedIn } from '../../../redux/selectors/auth';

class Logout extends Component {
  componentDidMount() {
    const { logout } = this.props;
    logout();
  }

  render() {
    const { isLoggedIn } = this.props;

    if (!isLoggedIn) {
      return <Redirect to="/welcome" />;
    }

    return null;
  }
}

Logout.propTypes = {
  isLoggedIn: PropTypes.bool.isRequired,
  logout: PropTypes.func.isRequired
};

const mapStateToProps = state => {
  const isLoggedIn = userIsLoggedIn(state);
  return { isLoggedIn };
};

const mapDispatchToProps = {
  logout: logoutAction
};

export default connect(mapStateToProps, mapDispatchToProps)(Logout);

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import { compose } from 'redux';
import { withTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { IonContent, IonGrid, IonRow } from '@ionic/react';
import AuthHeader from '../../../components/headers/AuthHeader';
import TextInput from '../../../components/inputs/Text';
import PrimaryButton from '../../../components/buttons/PrimaryButton';
import { loginInvalidPassword, loginInvalidEmail } from '../../../redux/modules/auth/actions/login';
import registerAction from '../../../redux/modules/auth/actions/register';
import toggleKeyboardAction from '../../../redux/modules/device/actions/toggleKeyboard';
import withBackNavigation from '../../../components/hoc/withBackNavigation';
import { validatePassword, validateEmail } from '../../../utils/validations';
import {
  userIsLoggedIn,
  userIsActive,
  userHasRequestedRegister,
  userIsRegistered
} from '../../../redux/selectors/auth';
import { keyboardIsUp } from '../../../redux/selectors/device';
import { appApiURL } from '../../../redux/selectors/app';
import './Register.scss';

const PAGE_STATES = ['INSERT_EMAIL', 'INSERT_PASSWORD', 'CONFIRM_PASSWORD'];

class Register extends Component {
  state = {
    apiURL: '',
    currentState: PAGE_STATES[0],
    email: '',
    password: '',
    passwordConfirmation: ''
  };

  componentWillMount() {
    const { onNavigateBack } = this.props;

    onNavigateBack(() => {
      const { currentState } = this.state;
      const currentIndex = PAGE_STATES.indexOf(currentState);

      if (currentIndex > 0) {
        this.setState({
          currentState: PAGE_STATES[currentIndex - 1]
        });
        return true;
      }

      return false;
    });
  }

  componentDidMount() {
    const { apiURL } = this.props;
    this.setState({ apiURL });
  }

  componentWillReceiveProps(nextProps) {
    const { apiURL: nextURL } = nextProps;
    const { apiURL } = this.state;
    if (apiURL && apiURL !== nextURL) {
      this.setState({ apiURL: nextURL });
      const { email, password, passwordConfirmation } = this.state;
      const { register } = this.props;
      register({ email, password, passwordConfirmation });
    }
  }

  onEmailChanged = evt => {
    const { value } = evt.target;
    this.setState({
      email: value
    });
  };

  onPasswordChanged = evt => {
    const { value } = evt.target;
    this.setState({
      password: value
    });
  };

  onPasswordConfirmationChanged = evt => {
    const { value } = evt.target;
    this.setState({
      passwordConfirmation: value
    });
  };

  nextState = e => {
    if (!e) {
      return;
    }

    e.preventDefault();

    const { currentState, email, password, passwordConfirmation } = this.state;
    const currentIndex = PAGE_STATES.indexOf(currentState);

    if (currentState === 'INSERT_EMAIL' && !validateEmail(email)) {
      const { invalidEmail } = this.props;
      invalidEmail();
      return;
    }

    if (currentState === 'INSERT_PASSWORD' && !validatePassword(password)) {
      const { invalidPassword } = this.props;
      invalidPassword();
      return;
    }

    if (currentIndex < PAGE_STATES.length - 1) {
      const nextState = PAGE_STATES[currentIndex + 1];
      this.setState({ currentState: nextState });
      return;
    }

    // If all fields have been filled,
    // submit them to redux
    const { register } = this.props;
    register({ email, password, passwordConfirmation });
  };

  canSkip = () => {
    const { currentState, email, password, passwordConfirmation } = this.state;

    switch (currentState) {
      case 'INSERT_EMAIL':
        return email.length > 0;
      case 'INSERT_PASSWORD':
        return password.length > 0;
      case 'CONFIRM_PASSWORD':
        return passwordConfirmation.length > 0;
      default:
        return true;
    }
  };

  renderInput({ t, toggleKeyboard }) {
    const { currentState } = this.state;

    switch (currentState) {
      case 'INSERT_EMAIL':
        return (
          <TextInput
            key="register-input-email"
            type="email"
            placeholder={t('global.terms.email')}
            onChange={this.onEmailChanged}
            onFocus={() => {
              toggleKeyboard({ isKeyboardUp: true });
            }}
            onBlur={() => {
              toggleKeyboard({ isKeyboardUp: false });
            }}
          />
        );
      case 'INSERT_PASSWORD':
        return (
          <TextInput
            key="register-input-password"
            type="password"
            placeholder={t('global.terms.password')}
            onChange={this.onPasswordChanged}
            onFocus={() => {
              toggleKeyboard({ isKeyboardUp: true });
            }}
            onBlur={() => {
              toggleKeyboard({ isKeyboardUp: false });
            }}
            autoFocus
          />
        );
      case 'CONFIRM_PASSWORD':
        return (
          <TextInput
            key="register-input-password-confirm"
            type="password"
            placeholder={t('components.inputs.passwordConfirmation')}
            onChange={this.onPasswordConfirmationChanged}
            onFocus={() => {
              toggleKeyboard({ isKeyboardUp: true });
            }}
            onBlur={() => {
              toggleKeyboard({ isKeyboardUp: false });
            }}
            autoFocus
          />
        );
      default:
        return null;
    }
  }

  renderTitle({ t }) {
    const { currentState } = this.state;

    switch (currentState) {
      case 'INSERT_EMAIL':
        return (
          <h1>
            <>{t('screens.Register.headerInsertEmail')}</>
            <b>{t('global.terms.email')}</b>
          </h1>
        );
      case 'INSERT_PASSWORD':
        return (
          <h1>
            <>{t('screens.Register.headerInsertPassword')}</>
            <b>{t('global.terms.password')}</b>
          </h1>
        );
      case 'CONFIRM_PASSWORD':
        return (
          <h1>
            <>{t('screens.Register.headerConfirmPassword')}</>
            <b>{t('global.terms.password')}</b>
          </h1>
        );
      default:
        return null;
    }
  }

  renderHeader({ t, isKeyboardUp }) {
    if (isKeyboardUp) {
      return null;
    }

    return (
      <>
        <AuthHeader />
        {this.renderTitle({ t })}
      </>
    );
  }

  renderButton({ t }) {
    return (
      <PrimaryButton
        text={t('global.actions.go')}
        icon="go_white"
        isDisabled={!this.canSkip()}
        onClick={this.nextState}
        type="submit"
      />
    );
  }

  render() {
    const { isLoggedIn, hasRequested, isRegistered, isActive } = this.props;

    if (isLoggedIn) {
      return <Redirect to="/login-redirect" />;
    }

    if (hasRequested) {
      if (!isActive && !isRegistered) {
        return <Redirect to="/leads" />;
      }

      if (!isActive && isRegistered) {
        return <Redirect to="/token" />;
      }
    }

    return (
      <IonContent forceOverscroll={false}>
        <div className="register__container auth-page full-height-flex">
          <div id="notch" />
          <IonGrid className="full-height-flex ion-justify-content-between">
            <form onSubmit={this.nextState}>
              <IonRow>{this.renderHeader(this.props)}</IonRow>
              <IonRow>{this.renderInput(this.props)}</IonRow>
              <IonRow>{this.renderButton(this.props)}</IonRow>
            </form>
          </IonGrid>
        </div>
      </IonContent>
    );
  }
}

Register.propTypes = {
  register: PropTypes.func.isRequired,
  isRegistered: PropTypes.bool.isRequired,
  isActive: PropTypes.bool.isRequired,
  isLoggedIn: PropTypes.bool.isRequired,
  hasRequested: PropTypes.bool.isRequired,
  onNavigateBack: PropTypes.func.isRequired,
  invalidPassword: PropTypes.func.isRequired,
  invalidEmail: PropTypes.func.isRequired,
  apiURL: PropTypes.string.isRequired
};

const mapStateToProps = state => {
  const isActive = userIsActive(state);
  const isRegistered = userIsRegistered(state);
  const hasRequested = userHasRequestedRegister(state);
  const isLoggedIn = userIsLoggedIn(state);
  const isKeyboardUp = keyboardIsUp(state);
  const apiURL = appApiURL(state);
  return {
    isLoggedIn,
    isActive,
    isRegistered,
    hasRequested,
    isKeyboardUp,
    apiURL
  };
};

const mapDispatchToProps = {
  register: registerAction,
  toggleKeyboard: toggleKeyboardAction,
  invalidPassword: loginInvalidPassword,
  invalidEmail: loginInvalidEmail
};

export default compose(
  withTranslation(),
  withBackNavigation(),
  connect(mapStateToProps, mapDispatchToProps)
)(Register);

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import {
  userIsLoggedIn,
  userIsNewUser,
  showOnboarding,
  shouldReadTos,
  userIsCanceledOrSuspended
} from '../../../redux/selectors/auth';

function LoginRedirect({
  isLoggedIn,
  userShouldReadTos,
  isCanceledOrSuspended,
  // isNewUser,
  userShouldSeeOnboarding
}) {
  if (!isLoggedIn) {
    return <Redirect to="/" />;
  }

  if (isCanceledOrSuspended) {
    return <Redirect to="/about/gyms" />;
  }

  if (userShouldReadTos) {
    return <Redirect to="/terms-of-service" />;
  }

  // if (isNewUser) {
  //   return <Redirect to="/on-boarding" />;
  // }
  if (userShouldSeeOnboarding) {
    return <Redirect to="/on-boarding" />;
  }

  return <Redirect to="/home" />;
}

LoginRedirect.propTypes = {
  isLoggedIn: PropTypes.bool.isRequired,
  userShouldReadTos: PropTypes.bool.isRequired,
  isCanceledOrSuspended: PropTypes.bool.isRequired,
  // isNewUser: PropTypes.bool.isRequired,
  userShouldSeeOnboarding: PropTypes.bool.isRequired
};

const mapStateToProps = state => {
  const userShouldReadTos = shouldReadTos(state);
  const isLoggedIn = userIsLoggedIn(state);
  const isCanceledOrSuspended = userIsCanceledOrSuspended(state);
  const isNewUser = userIsNewUser(state);
  const userShouldSeeOnboarding = showOnboarding(state);
  return { isLoggedIn, isCanceledOrSuspended, isNewUser, userShouldReadTos, userShouldSeeOnboarding };
};

export default connect(mapStateToProps, null)(LoginRedirect);

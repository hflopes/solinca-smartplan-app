import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { withTranslation } from 'react-i18next';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { IonContent, IonGrid, IonRow } from '@ionic/react';
import TokenInput from '../../../components/inputs/Token';
import PrimaryButton from '../../../components/buttons/PrimaryButton';
import AuthHeader from '../../../components/headers/AuthHeader';
import verifyTokenAction from '../../../redux/modules/auth/actions/verifyToken';
import toggleKeyboardAction from '../../../redux/modules/device/actions/toggleKeyboard';
import withBackNavigation from '../../../components/hoc/withBackNavigation';
import { userIsVerifyingToken, userHasRequestedToken, tokenIsVerified } from '../../../redux/selectors/auth';
import { keyboardIsUp } from '../../../redux/selectors/device';

const TOKEN_LENGTH = 4;

class Token extends Component {
  state = {
    token: ''
  };

  onChangeToken = value => {
    this.setState({
      token: value
    });
  };

  onSubmit = () => {
    const { token } = this.state;
    const { verifyToken } = this.props;
    verifyToken({ token });
  };

  renderHeader = ({ t, isKeyboardUp }) => {
    if (isKeyboardUp) {
      return null;
    }

    return (
      <>
        <AuthHeader />
        <h1>
          <>{t('screens.Token.header')}</>
          <b>{t('global.terms.email')}</b>
        </h1>
      </>
    );
  };

  renderInput = ({ hasRequested, isTokenVerified, toggleKeyboard }) => {
    const isWrong = hasRequested && !isTokenVerified;
    return (
      <TokenInput
        length={TOKEN_LENGTH}
        onChange={this.onChangeToken}
        isWrong={isWrong}
        onFocus={() => {
          toggleKeyboard({ isKeyboardUp: true });
        }}
        onBlur={() => {
          toggleKeyboard({ isKeyboardUp: false });
        }}
      />
    );
  };

  renderButton = ({ t, isVerifyingToken }) => {
    const { token } = this.state;
    const isButtonDisabled = token.length < TOKEN_LENGTH;
    return (
      <PrimaryButton
        text={t('global.actions.go')}
        icon="go_white"
        isLoading={isVerifyingToken}
        isDisabled={isButtonDisabled}
        onClick={this.onSubmit}
      />
    );
  };

  render() {
    const { isKeyboardUp, hasRequested, isTokenVerified } = this.props;
    const keyboardClass = isKeyboardUp ? 'keyboard' : '';

    if (hasRequested && isTokenVerified) {
      return <Redirect to="/login" />;
    }

    return (
      <>
        <IonContent forceOverscroll={false}>
          <div className={`auth-page full-height-flex ${keyboardClass}`}>
            <IonGrid className="full-height-flex ion-justify-content-between">
              <IonRow>{this.renderHeader(this.props)}</IonRow>
              <IonRow>{this.renderInput(this.props)}</IonRow>
              <IonRow>{this.renderButton(this.props)}</IonRow>
            </IonGrid>
          </div>
        </IonContent>
      </>
    );
  }
}

Token.propTypes = {
  verifyToken: PropTypes.func.isRequired,
  hasRequested: PropTypes.bool.isRequired,
  isTokenVerified: PropTypes.bool.isRequired,
  isKeyboardUp: PropTypes.bool.isRequired
};

const mapStateToProps = state => {
  const isVerifyingToken = userIsVerifyingToken(state);
  const hasRequested = userHasRequestedToken(state);
  const isTokenVerified = tokenIsVerified(state);
  const isKeyboardUp = keyboardIsUp(state);
  return {
    isVerifyingToken,
    hasRequested,
    isTokenVerified,
    isKeyboardUp
  };
};

const mapDispatchToProps = {
  verifyToken: verifyTokenAction,
  toggleKeyboard: toggleKeyboardAction
};

export default compose(
  withTranslation(),
  withBackNavigation(),
  connect(mapStateToProps, mapDispatchToProps)
)(Token);

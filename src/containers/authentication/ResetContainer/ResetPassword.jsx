import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import { compose } from 'redux';
import { withTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import AuthHeader from '../../../components/headers/AuthHeader';
import PageTitle from '../../../components/PageTitle';
import TextInput from '../../../components/inputs/Text';
import PrimaryButton from '../../../components/buttons/PrimaryButton';
import resetPasswordAction from '../../../redux/modules/auth/actions/resetPassword';
import toggleKeyboardAction from '../../../redux/modules/device/actions/toggleKeyboard';
import withBackNavigation from '../../../components/hoc/withBackNavigation';
import { keyboardIsUp } from '../../../redux/selectors/device';
import { userIsResetting, userHasRequestedReset } from '../../../redux/selectors/auth';
import { appApiURL } from '../../../redux/selectors/app';
import './ResetPassword.scss';

const PAGE_STATES = [
  {
    key: 'INSERT_EMAIL',
    pageTitleI18n: 'screens.Register.headerInsertEmail',
    pageSubtitleI18n: 'global.terms.email',
    inputType: 'text'
  },
  {
    key: 'INSERT_PASSWORD',
    pageTitleI18n: 'screens.Register.headerInsertPassword',
    pageSubtitleI18n: 'global.terms.password',
    inputType: 'password'
  },
  {
    key: 'CONFIRM_PASSWORD',
    pageTitleI18n: 'screens.Register.headerConfirmPassword',
    pageSubtitleI18n: 'global.terms.password',
    inputType: 'password'
  }
];

class ResetPassword extends Component {
  state = {
    apiURL: '',
    currentState: PAGE_STATES[0],
    email: '',
    password: '',
    passwordConfirmation: ''
  };

  componentWillMount() {
    const { onNavigateBack } = this.props;

    onNavigateBack(() => {
      const { currentState } = this.state;
      const currentIndex = PAGE_STATES.indexOf(currentState);

      if (currentIndex > 0) {
        this.setState({
          currentState: PAGE_STATES[currentIndex - 1]
        });
        return true;
      }

      return false;
    });
  }

  componentDidMount() {
    const { apiURL } = this.props;
    this.setState({ apiURL });
  }

  componentWillReceiveProps(nextProps) {
    const { apiURL: nextURL } = nextProps;
    const { apiURL } = this.state;
    if (apiURL && apiURL !== nextURL) {
      this.setState({ apiURL: nextURL });
      const { email, password, passwordConfirmation } = this.state;
      const { resetPassword } = this.props;
      resetPassword({ email, password, passwordConfirmation });
    }
  }

  onEmailChanged = evt => {
    const { value } = evt.target;
    this.setState({
      email: value
    });
  };

  onPasswordChanged = evt => {
    const { value } = evt.target;
    this.setState({
      password: value
    });
  };

  onPasswordConfirmationChanged = evt => {
    const { value } = evt.target;
    this.setState({
      passwordConfirmation: value
    });
  };

  hideKeyboard = () => {
    const { toggleKeyboard } = this.props;
    toggleKeyboard({ isKeyboardUp: false });
  };

  showKeyboard = () => {
    const { toggleKeyboard } = this.props;
    toggleKeyboard({ isKeyboardUp: true });
  };

  canSkip = () => {
    const { currentState, email, password, passwordConfirmation } = this.state;

    switch (currentState.key) {
      case 'INSERT_EMAIL':
        return email.length > 0;
      case 'INSERT_PASSWORD':
        return password.length > 0;
      case 'CONFIRM_PASSWORD':
        return passwordConfirmation.length > 0;
      default:
        return true;
    }
  };

  onChange = e => {
    const { currentState } = this.state;

    switch (currentState.key) {
      case 'INSERT_EMAIL':
        return this.onEmailChanged(e);
      case 'INSERT_PASSWORD':
        return this.onPasswordChanged(e);
      case 'CONFIRM_PASSWORD':
        return this.onPasswordConfirmationChanged(e);
      default:
        return null;
    }
  };

  onSubmit = () => {
    const { currentState, email, password, passwordConfirmation } = this.state;
    const currentIndex = PAGE_STATES.indexOf(currentState);

    if (currentIndex < PAGE_STATES.length - 1) {
      const nextState = PAGE_STATES[currentIndex + 1];
      this.setState({ currentState: nextState });
      return;
    }

    // If all fields have been filled,
    // submit them to redux
    const { resetPassword } = this.props;
    resetPassword({ email, password, passwordConfirmation });
  };

  render() {
    const { t, isKeyboardUp, hasRequestedReset } = this.props;
    const { currentState } = this.state;
    const { key, inputType, pageTitleI18n, pageSubtitleI18n } = currentState;

    if (hasRequestedReset) {
      return <Redirect to="/token" />;
    }

    return (
      <div className="reset-password__container auth-page">
        <form className="reset-password__form" onSubmit={this.onSubmit}>
          <div className="form__header">
            {!isKeyboardUp && (
              <>
                <AuthHeader />
                <PageTitle text={t(pageTitleI18n)} textHighlight={t(pageSubtitleI18n)} />
              </>
            )}
          </div>
          <div className="form__inputs">
            <TextInput
              key={key}
              type={inputType}
              placeholder={t(pageSubtitleI18n)}
              onFocus={this.showKeyboard}
              onBlur={this.hideKeyboard}
              onChange={this.onChange}
            />
          </div>
          <div className="form__cta">
            <PrimaryButton
              text={t('global.actions.go')}
              icon="go_white"
              isDisabled={!this.canSkip()}
              onClick={this.onSubmit}
            />
          </div>
        </form>
      </div>
    );
  }
}

ResetPassword.propTypes = {
  t: PropTypes.func.isRequired,
  resetPassword: PropTypes.func.isRequired,
  toggleKeyboard: PropTypes.func.isRequired,
  hasRequestedReset: PropTypes.bool.isRequired,
  isKeyboardUp: PropTypes.bool.isRequired,
  onNavigateBack: PropTypes.func.isRequired,
  apiURL: PropTypes.string.isRequired
};

const mapStateToProps = state => {
  const isKeyboardUp = keyboardIsUp(state);
  const isResetting = userIsResetting(state);
  const hasRequestedReset = userHasRequestedReset(state);
  const apiURL = appApiURL(state);
  return { isKeyboardUp, isResetting, hasRequestedReset, apiURL };
};

const mapDispatchToProps = {
  resetPassword: resetPasswordAction,
  toggleKeyboard: toggleKeyboardAction
};

export default compose(
  withTranslation(),
  withBackNavigation(),
  connect(mapStateToProps, mapDispatchToProps)
)(ResetPassword);

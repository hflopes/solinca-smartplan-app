import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { withTranslation } from 'react-i18next';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { IonContent, IonGrid, IonRow } from '@ionic/react';
import loginAction, { loginInvalidEmail } from '../../../redux/modules/auth/actions/login';
import TextInput from '../../../components/inputs/Text';
import PrimaryButton from '../../../components/buttons/PrimaryButton';
import withBackNavigation from '../../../components/hoc/withBackNavigation';
import AuthHeader from '../../../components/headers/AuthHeader';
import toggleKeyboardAction from '../../../redux/modules/device/actions/toggleKeyboard';
import { keyboardIsUp } from '../../../redux/selectors/device';
import { validateEmail } from '../../../utils/validations';
import './Login.scss';
import { userIsLoggedIn, userIsWaitingReset, userIsLoggingIn } from '../../../redux/selectors/auth';
import { appApiURL } from '../../../redux/selectors/app';

class Login extends Component {
  state = {
    apiURL: '',
    email: '',
    password: ''
  };

  componentDidMount() {
    const { apiURL } = this.props;
    this.setState({ apiURL });
  }

  componentWillReceiveProps(nextProps) {
    const { apiURL: nextURL } = nextProps;
    const { apiURL } = this.state;
    if (apiURL !== nextURL) {
      this.setState({ apiURL: nextURL });
      const { email, password } = this.state;
      const { login } = this.props;
      login({ email, password });
    }
  }

  onEmailChanged = evt => {
    const { value } = evt.target;
    this.setState({
      email: value
    });
  };

  onPasswordChanged = evt => {
    const { value } = evt.target;
    this.setState({
      password: value
    });
  };

  onSubmit = e => {
    if (!e) {
      return;
    }

    e.preventDefault();

    const { email, password } = this.state;
    const { login, invalidEmail } = this.props;

    if (!validateEmail(email)) {
      invalidEmail();
    } else {
      login({ email, password });
    }
  };

  renderHeader = () => {
    const { t, isKeyboardUp } = this.props;
    if (isKeyboardUp) {
      return null;
    }

    return (
      <>
        <AuthHeader />
        <h1>
          <>{t('screens.Login.header')}</>
          <b>{t('screens.Login.headerBold')}</b>
        </h1>
      </>
    );
  };

  renderInputs = () => {
    const { t, toggleKeyboard } = this.props;
    return (
      <>
        <TextInput
          type="email"
          placeholder={t('global.terms.email')}
          onChange={this.onEmailChanged}
          onFocus={() => {
            toggleKeyboard({ isKeyboardUp: true });
          }}
          onBlur={() => {
            toggleKeyboard({ isKeyboardUp: false });
          }}
        />
        <TextInput
          type="password"
          placeholder={t('global.terms.password')}
          onChange={this.onPasswordChanged}
          onFocus={() => {
            toggleKeyboard({ isKeyboardUp: true });
          }}
          onBlur={() => {
            toggleKeyboard({ isKeyboardUp: false });
          }}
        />

        <Link to="/reset-password" className="link">
          {t('screens.Login.forgotPassword')}
        </Link>
      </>
    );
  };

  renderButton = () => {
    const { t, waitingReset, isLoggingIn } = this.props;
    const { email, password } = this.state;
    const isButtonDisabled = email.length === 0 || password.length === 0 || waitingReset;

    return (
      <PrimaryButton
        text={t('global.actions.go')}
        icon="go_white"
        isDisabled={isButtonDisabled}
        isLoading={isLoggingIn}
        onClick={this.onSubmit}
        type="submit"
      />
    );
  };

  render() {
    const { isLoggedIn, isKeyboardUp } = this.props;
    const keyboardClass = isKeyboardUp ? 'keyboard' : '';

    if (isLoggedIn) {
      return <Redirect to="/login-redirect" />;
    }

    return (
      <>
        <IonContent forceOverscroll={false}>
          <div className={`login-page auth-page full-height-flex ${keyboardClass}`}>
            <div id="notch" />
            <IonGrid className="full-height-flex ion-justify-content-between">
              <form onSubmit={this.onSubmit} noValidate>
                <IonRow>{this.renderHeader()}</IonRow>
                <IonRow className="inputs-row">{this.renderInputs()}</IonRow>
                <IonRow>{this.renderButton()}</IonRow>
              </form>
            </IonGrid>
          </div>
        </IonContent>
      </>
    );
  }
}

Login.propTypes = {
  isKeyboardUp: PropTypes.bool.isRequired,
  isLoggedIn: PropTypes.bool.isRequired,
  isLoggingIn: PropTypes.bool.isRequired,
  waitingReset: PropTypes.bool.isRequired,
  apiURL: PropTypes.string.isRequired,
  login: PropTypes.func.isRequired,
  invalidEmail: PropTypes.func.isRequired,
  toggleKeyboard: PropTypes.func.isRequired,
  t: PropTypes.func.isRequired
};

const mapStateToProps = state => {
  const isLoggedIn = userIsLoggedIn(state);
  const isLoggingIn = userIsLoggingIn(state);
  const waitingReset = userIsWaitingReset(state);
  const isKeyboardUp = keyboardIsUp(state);
  const apiURL = appApiURL(state);
  return { isLoggedIn, isLoggingIn, isKeyboardUp, waitingReset, apiURL };
};

const mapDispatchToProps = {
  login: loginAction,
  invalidEmail: loginInvalidEmail,
  toggleKeyboard: toggleKeyboardAction
};

export default compose(
  withTranslation(),
  withBackNavigation(),
  connect(mapStateToProps, mapDispatchToProps)
)(Login);

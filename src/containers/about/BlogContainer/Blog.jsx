import React, { Component } from 'react';
import { compose } from 'redux';
import { withTranslation } from 'react-i18next';
import { IonSpinner } from '@ionic/react';
import withBackNavigation from '../../../components/hoc/withBackNavigation';
import './Blog.scss';

class Blog extends Component {
  state = {
    loading: true
  };

  hideLoading = () => {
    this.setState({ loading: false });
  };

  render() {
    const { loading } = this.state;
    return (
      <div className="blog-container full-height-flex">
        <div id="notch" />
        <h1>Blog</h1>
        {loading && <IonSpinner name="crescent" />}
        <iframe onLoad={this.hideLoading} title="Blog" src="https://www.solinca.pt/solinca/blog/" />
      </div>
    );
  }
}

export default compose(withTranslation(), withBackNavigation('dark'))(Blog);

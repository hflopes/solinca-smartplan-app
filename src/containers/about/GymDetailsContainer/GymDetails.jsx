import React from 'react';
import { compose } from 'redux';
import { withTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Image from '../../../components/Image';
import LayeredPage from '../../../components/LayeredPage';
import { getGym } from '../../../redux/selectors/gyms';
import withBackNavigation from '../../../components/hoc/withBackNavigation';
import { format } from './schedule';
import RoundButton from '../../../components/buttons/RoundButton';
import './GymDetails.scss';

const formatPhoneNumber = phone => {
  const formatNumber = num => {
    return num.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ');
  };

  if (phone.includes(')')) {
    const split = phone.split(')');
    const extention = `${split[0]})`;
    const number = split[1];

    const formatted = `${extention} ${formatNumber(number)}`;

    return formatted;
  }

  return formatNumber(phone);
};

function openMaps({ latitude, longitude }) {
  if (
    navigator.platform.indexOf('iPhone') !== -1 ||
    navigator.platform.indexOf('iPad') !== -1 ||
    navigator.platform.indexOf('iPod') !== -1
  ) {
    window.open(`maps://maps.google.com/maps?daddr=${latitude},${longitude}&amp;ll=`);
  } else {
    window.open(`https://maps.google.com/maps?daddr=${latitude},${longitude}&amp;ll=`);
  }
}

function dialPhone(phone) {
  window.location.href = `tel:${phone}`;
}

function sendEmail(email) {
  window.location.href = `mailto:${email}`;
}

function GymDetails({ t, gym }) {
  const { name, timeSchedule, address, email, phone, coordinates } = gym;
  const schedules = format(timeSchedule);

  const background = (
    <>
      <div className="gym-details__header">
        <div id="notch" />
        <div className="gym__logo">
          <div className="line" />
          <Image src="/img/logo_new.svg" alt="" />
        </div>
        <h1 className="gym__name">{name}</h1>
      </div>
    </>
  );

  const foreground = (
    <>
      <div className="gym-details__content">
        <div className="section actions">
          <RoundButton
            icon="directions"
            text={t('global.terms.directions')}
            onClick={() => openMaps(coordinates)}
            disabled={coordinates === undefined}
          />
          <RoundButton icon="phone" text={t('global.actions.contact')} onClick={() => dialPhone(phone)} />
          <RoundButton icon="email" text={t('global.actions.sendEmail')} onClick={() => sendEmail(email)} />
        </div>
        <div className="section schedules">
          <h3 className="section__title">{t('global.terms.schedules')}</h3>
          {schedules.map(({ label, schedule }) => (
            <div className="section__item" key={label}>
              <b className="section__item-label">{label}</b>
              <span className="section__item-text">{schedule}</span>
            </div>
          ))}
          <hr />
        </div>
        <div className="section contacts">
          <h3 className="section__title">{t('global.terms.contacts')}</h3>
          <div className="section__item">
            <b className="section__item-label">{`${t('global.terms.email')}:`}</b>
            <span className="section__item-text">{email}</span>
          </div>
          <div className="section__item">
            <b className="section__item-label">{`${t('global.terms.phone')}:`}</b>
            <span className="section__item-text">{formatPhoneNumber(phone)}</span>
          </div>
          <hr />
        </div>
        <div className="section address">
          <h3 className="section__title">{t('global.terms.address')}</h3>
          <p className="address__text">{address}</p>
        </div>
      </div>
    </>
  );

  return (
    <LayeredPage
      className="gym-details__container"
      background={background}
      foreground={foreground}
      gap="45vh"
      extra="10vh"
      rounded
    />
  );
}

GymDetails.propTypes = {
  gym: PropTypes.shape({
    name: PropTypes.string,
    timeSchedule: PropTypes.arrayOf(PropTypes.any),
    address: PropTypes.string,
    email: PropTypes.string,
    phone: PropTypes.string,
    coordinates: PropTypes.shape({})
  }).isRequired,
  t: PropTypes.func.isRequired
};

const mapStateToProps = (state, ownProps) => {
  const gym = getGym(state, ownProps.gymId);
  return { gym };
};

export default compose(withBackNavigation(), withTranslation(), connect(mapStateToProps))(GymDetails);

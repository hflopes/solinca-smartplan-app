import { getDayOfWeek } from '../../../utils/dates';

export function format(schedules) {
  if (!schedules || !schedules.length) {
    return [{ label: 'Nenhum horário encontrado.', schedule: null }];
  }

  const changeIndexes = [];
  const gaps = [];
  let lastSchedule = null;

  schedules.forEach((s, i) => {
    const curSchedule = concat(s);

    if (!lastSchedule || lastSchedule !== curSchedule) {
      changeIndexes.push(i);
      lastSchedule = curSchedule;
    }
  });

  if (changeIndexes.length === 1) {
    return [{ label: 'Todos os dias', schedule: concat(schedules[0]) }];
  }

  for (let i = 0; i < changeIndexes.length; i += 1) {
    const index = changeIndexes[i];
    if (i === changeIndexes.length - 1) {
      /**
       * Day is the last of the list, so it should be displayed seperately.
       *
       * Ex:
       * Dom - Sex: 09h - 23h
       * Sáb: 11h - 20h <-- this
       */
      gaps.push({ index, label: getDayOfWeek(index), schedule: concat(schedules[index]) });
    } else {
      const nextIndex = changeIndexes[i + 1];

      if (nextIndex - index === 1) {
        /**
         * The next day is different than the current one,
         * so add the current day seperately
         */
        gaps.push({ index, label: getDayOfWeek(index), schedule: concat(schedules[index]) });
      } else {
        gaps.push({
          index,
          label: `${getDayOfWeek(index)} - ${getDayOfWeek(nextIndex - 1)}`,
          schedule: concat(schedules[index])
        });
      }
    }
  }

  // Sunday has a different schedule than Monday,
  // so shift the array around to make sure sunday is last
  if (changeIndexes.includes(1)) {
    // Sunday is the same as saturday, so label it as weekend
    if (concat(schedules[0]) === concat(schedules[6])) {
      return [
        ...gaps.filter(g => g.index !== 0 && g.index !== 6),
        {
          index: 0,
          label: `Fins de semana`,
          schedule: concat(schedules[0])
        }
      ];
    }

    return [
      ...gaps.filter(g => g.index !== 0),
      {
        index: 0,
        label: `${getDayOfWeek(0)}`,
        schedule: concat(schedules[0])
      }
    ];
  }

  return gaps;
}

export function concat({ openingHour, closingHour }) {
  return `${openingHour} - ${closingHour}`;
}

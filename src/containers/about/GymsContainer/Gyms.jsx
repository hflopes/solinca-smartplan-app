import React, { Component, Fragment } from 'react';
import { compose } from 'redux';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';
import { withRouter } from 'react-router';
import SwipeableViews from 'react-swipeable-views';
import withBackNavigation from '../../../components/hoc/withBackNavigation';
import PrimaryButton from '../../../components/buttons/PrimaryButton';
import { getGymsListOrderedByDistance } from '../../../redux/selectors/gyms';
import { userIsCanceledOrSuspended, userIsLoggedIn } from '../../../redux/selectors/auth';
import GymCard from '../../../components/cards/GymCard';
import withRouteState from '../../../components/hoc/withRouteState';
import reloadAction from '../../../redux/reload/action';
import './Gyms.scss';

const styles = {
  root: {
    // gap between start and end of screen
    padding: '0 calc(50vw - 135px)'
  },
  slideContainer: {
    // gap between cards
    padding: '0 5px'
  }
};

class Gyms extends Component {
  state = {
    highlightedIndex: 0
  };

  constructor(props) {
    super(props);

    const { lastState } = props;

    this.state = {
      highlightedIndex: lastState.highlightedIndex || 0
    };
  }

  componentWillMount() {
    const { onNavigateBack, isCanceledOrSuspended, reload } = this.props;

    if (isCanceledOrSuspended) {
      onNavigateBack(() => {
        const { history } = this.props;
        history.push('/logout');
        return true;
      });
    } else {
      reload('userPosition');
    }
  }

  componentWillUnmount() {
    const { highlightedIndex } = this.state;
    const { saveState } = this.props;

    const currentState = {
      highlightedIndex
    };

    saveState(currentState);
  }

  highlight = i => {
    this.setState({
      highlightedIndex: i
    });
  };

  join = () => {
    const { highlightedIndex } = this.state;
    const { gyms, history } = this.props;
    const selectedGym = gyms[highlightedIndex];

    history.push(`/leads/${selectedGym.id}`);
  };

  onScroll = () => {
    const { scrollTop } = document.getElementById('gyms-scroll');
    const scrollPercent = Math.min(1, scrollTop / 40);
    const header = document.getElementById('clear-nav');

    header.style.opacity = 1 - scrollPercent;
  };

  render() {
    const { highlightedIndex } = this.state;
    const { t, gyms, isLoggedIn, isCanceledOrSuspended } = this.props;

    return (
      <div id="gyms-scroll" className="gyms-container full-height-flex" onScroll={this.onScroll}>
        <div id="notch" />
        <div className="gyms__header">
          <h1>{t('screens.About.about')}</h1>
          <h1>{t('screens.About.us')}</h1>
        </div>
        <div className="gyms__body">
          <div className="gyms__list">
            <b className="list-title">{t('screens.Gyms.closestGyms')}</b>
            <SwipeableViews
              index={highlightedIndex}
              style={styles.root}
              slideStyle={styles.slideContainer}
              onChangeIndex={this.highlight}
            >
              {gyms &&
                gyms.map((g, i) =>
                  // When a card is highlighted, only render
                  // the neighbouring 4 cards (2 on each side)
                  g && g.id && Math.abs(highlightedIndex - i) < 3 ? (
                    <Link key={g.id} to={`/about/gyms/${g.id}`}>
                      <GymCard gym={g} highlighted={i === highlightedIndex} />
                    </Link>
                  ) : (
                    <Fragment key={`fragment-${g.id}`} />
                  )
                )}
            </SwipeableViews>
          </div>
        </div>
        <div className="gyms__cta">
          {!isLoggedIn && <PrimaryButton text={t('global.actions.join')} onClick={this.join} />}
          {isCanceledOrSuspended && (
            <PrimaryButton text={t('global.actions.contact')} link="/support/contact" />
          )}
        </div>
      </div>
    );
  }
}

Gyms.propTypes = {
  t: PropTypes.func.isRequired,
  isLoggedIn: PropTypes.bool.isRequired,
  gyms: PropTypes.arrayOf(PropTypes.any).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func
  }).isRequired,
  saveState: PropTypes.func.isRequired,
  lastState: PropTypes.shape({
    highlightedIndex: PropTypes.number
  }).isRequired,
  onNavigateBack: PropTypes.func.isRequired,
  isCanceledOrSuspended: PropTypes.bool.isRequired,
  reload: PropTypes.func.isRequired
};

const mapStateToProps = state => {
  const gyms = getGymsListOrderedByDistance(state);
  const isLoggedIn = userIsLoggedIn(state);
  const isCanceledOrSuspended = userIsCanceledOrSuspended(state);
  return { gyms, isLoggedIn, isCanceledOrSuspended };
};

const mapDispatchToProps = {
  reload: reloadAction
};

export default compose(
  withTranslation(),
  withBackNavigation('dark'),
  withRouter,
  connect(mapStateToProps, mapDispatchToProps),
  withRouteState('Gyms')
)(Gyms);

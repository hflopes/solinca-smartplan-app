import React, { Fragment, Component } from 'react';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';
import { IonGrid, IonRow } from '@ionic/react';
import SwipeableViews from 'react-swipeable-views';
import withMenuNavigation from '../../../components/hoc/withMenuNavigation';
import PrimaryButton from '../../../components/buttons/PrimaryButton';
import BadgeTabs from '../../../components/tabs/BadgeTabs';
import withRouteState from '../../../components/hoc/withRouteState';
import './About.scss';

function customRender(text) {
  function renderBold(bold) {
    return bold.split('**').map((t, i) => {
      if (i % 2 === 0) {
        return <Fragment key={t}>{t}</Fragment>;
      }

      return <b key={t}>{t}</b>;
    });
  }

  const blocks = text.split('\n').map(b => {
    const lines = b
      .split('))')
      .map(s => s.replace('((', ''))
      .filter(s => s.length);

    if (lines && lines.length > 1) {
      return (
        <ul key={b}>
          {lines.map(l => (
            <li key={l}>{renderBold(l)}</li>
          ))}
        </ul>
      );
    }

    return <p key={b}>{renderBold(b)}</p>;
  });

  return blocks;
}

class About extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedTabIndex: 0
    };
  }

  componentDidMount() {
    const {
      lastState: { selectedTabIndex }
    } = this.props;
    if (selectedTabIndex >= 0) {
      setTimeout(this.initialScroll, 500);
    }
  }

  componentWillUnmount() {
    const { selectedTabIndex } = this.state;
    const { saveState } = this.props;

    const currentState = {
      selectedTabIndex
    };

    saveState(currentState);
  }

  initialScroll = (tries = 0) => {
    if (tries >= 5) return;
    try {
      const {
        lastState: { selectedTabIndex }
      } = this.props;
      const tabContent = document.getElementsByClassName('mdc-tab-scroller__scroll-content')[0];
      tabContent.children[selectedTabIndex].click();
    } catch (error) {
      setTimeout(() => {
        this.initialScroll(tries + 1);
      }, 500);
    }
  };

  selectTab = i => {
    this.setState({
      selectedTabIndex: i
    });
  };

  onScroll = () => {
    const { scrollTop } = document.getElementById('about-scroll');
    const scrollPercent = Math.min(1, scrollTop / 40);
    const header = document.getElementById('hamburger-nav');

    header.style.opacity = 1 - scrollPercent;
  };

  render() {
    const { t } = this.props;
    const { selectedTabIndex } = this.state;

    const tabs = t(`screens.About.tabs`, { returnObjects: true });
    const interaction = t(`screens.About.interaction`);
    const evaluation = t(`screens.About.evaluation`);
    const classes = t(`screens.About.classes`);
    const training = t(`screens.About.training`);
    const water = t(`screens.About.water`);

    const interactionElements = customRender(interaction);
    const evaluationElements = customRender(evaluation);
    const classesElements = customRender(classes);
    const trainingElements = customRender(training);
    const waterElements = customRender(water);

    return (
      <div id="about-scroll" className="about-container full-height-flex" onScroll={this.onScroll}>
        <div id="notch" />
        <IonGrid>
          <IonRow>
            <div className="page-header">
              <h1>{t('screens.About.about')}</h1>
              <h1>{t('screens.About.us')}</h1>
            </div>
          </IonRow>
          <IonRow>
            <div className="tab-bar">
              <BadgeTabs tabs={tabs} selectedIndex={selectedTabIndex} onSelected={this.selectTab} />
            </div>
          </IonRow>
          <IonRow>
            <SwipeableViews index={selectedTabIndex} onChangeIndex={this.selectTab}>
              <div className="slide">{interactionElements}</div>
              <div className="slide">{evaluationElements}</div>
              <div className="slide">{classesElements}</div>
              <div className="slide">{trainingElements}</div>
              <div className="slide">{waterElements}</div>
            </SwipeableViews>
          </IonRow>
        </IonGrid>
        <div className="btn-wrapper">
          <PrimaryButton text={t('screens.About.findGyms')} link="/about/gyms" />
        </div>
      </div>
    );
  }
}

About.propTypes = {
  t: PropTypes.func.isRequired,
  saveState: PropTypes.func.isRequired,
  lastState: PropTypes.shape({
    selectedTabIndex: PropTypes.number
  }).isRequired
};

export default compose(withTranslation(), withMenuNavigation('dark'), withRouteState('About'))(About);

import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { useSelector } from 'react-redux';
import moment from 'moment';
import { withTranslation } from 'react-i18next';
import LayeredPage from '../../../components/LayeredPage';
import Image from '../../../components/Image';
import ProgressMiniCard from '../../../components/cards/ProgressMiniCard';
import withBackNavigation from '../../../components/hoc/withBackNavigation';
import WeekCalendar from './components/WeekCalendar';
import MonthCalendar from './components/MonthCalendar';
import { getTime, getMonth } from '../../../utils/dates';
import { getPaddedActivities, getActivityCount } from '../../../redux/selectors/dashboard';
import { getGoalPlans, getGoalClasses } from '../../../redux/selectors/plan';
import './Calendar.scss';

const icons = {
  plan: '/img/big_icons/foot_red.svg',
  recommendedClass: '/img/big_icons/recommended_classes_orange.svg',
  lesMillsClass: '/img/big_icons/lesmills_classes_pink.svg',
  otherClass: '/img/big_icons/other_classes_purple.svg'
};

function DayDetail({ activities, date, onClose, t }) {
  const selectedActivities = activities.flat().filter(a => moment(a.date).isSame(date, 'day'));
  const dayLabel = `${date.getDate()} ${getMonth(date.getMonth())}, ${date.getFullYear()}`;

  return (
    <div className="day-detail__container" role="button" tabIndex={0} onClick={onClose} onKeyDown={() => {}}>
      <div className="day-detail__popup">
        <div className="popup-header">
          <div className="popup-labels">
            <b className="popup-title">{t('screens.Calendar.popup.title')}</b>
            <span className="popup-subtitle">{dayLabel}</span>
          </div>
          <button className="popup-close" type="button" onClick={onClose}>
            <Image src="/img/icons/clear.svg" alt="" />
          </button>
        </div>
        <div className="popup-body">
          {selectedActivities.map(a => (
            <div key={`${a.date.getTime()}/${a.type.split('-')[0]}`} className="detail-item">
              <div className="detail-item__time">{getTime(a.date)}</div>
              <div className="detail-item__description">
                <Image className="detail-item__icon" src={icons[a.type.split('-')[0]]} alt="" />
                <b className="detail-item__title">{a.name}</b>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

function Calendar({ t }) {
  const [zoomMode, setZoomMode] = useState('week');
  const [selectedDetailDay, setSelectedDetailDay] = useState(null);
  const [monthIndex, setMonthIndex] = useState(3);

  const start = moment()
    .subtract(3, 'month')
    .toDate();
  const end = moment()
    .add(3, 'month')
    .toDate();

  const startMonth = moment()
    .startOf('month')
    .startOf('day')
    .toDate();
  const endMonth = moment()
    .endOf('month')
    .endOf('day')
    .toDate();

  const activities = useSelector(state => getPaddedActivities(state, start, end));
  const plansGoal = useSelector(state => getGoalPlans(state));
  const classesGoal = useSelector(state => getGoalClasses(state));

  const gapString = t('screens.Calendar.gap', {
    day: moment()
      .startOf('month')
      .toDate()
      .getDate(),
    month: getMonth(
      moment()
        .startOf('month')
        .toDate()
        .getMonth()
    )
  });

  const recomnendedCount = useSelector(state =>
    getActivityCount(state, startMonth, endMonth, 'recommendedClass')
  );
  const lesMillsCount = useSelector(state => getActivityCount(state, startMonth, endMonth, 'lesMillsClass'));
  const otherCount = useSelector(state => getActivityCount(state, startMonth, endMonth, 'otherClass'));
  const planCount = useSelector(state => getActivityCount(state, startMonth, endMonth, 'plan'));

  function onZoomOut() {
    if (zoomMode === 'week') {
      setZoomMode('month');
    }
  }

  function onZoomIn() {
    if (zoomMode === 'month') {
      setZoomMode('week');
    }
  }

  function onDayDetailSelected(date) {
    setSelectedDetailDay(date);
  }

  function onDayPopupClosed() {
    setSelectedDetailDay(null);
  }

  function onMonthDaySelect({ date, type }) {
    if (type && type !== 'none') {
      setSelectedDetailDay(date);
    }
  }

  const background = (
    <div className="calendar__background">
      <div id="notch" />
      <h3 className="page-title">{t('screens.Calendar.title')}</h3>
      <span className="timeago-label">{gapString}</span>
      <div className="card-container">
        <ProgressMiniCard type="plans" value={planCount} goal={plansGoal} />
        <ProgressMiniCard type="recommendedClasses" value={recomnendedCount} goal={classesGoal} />
        <ProgressMiniCard type="lesMillsClasses" value={lesMillsCount} />
        <ProgressMiniCard type="otherClasses" value={otherCount} />
      </div>
    </div>
  );

  const foreground = (
    <div className="calendar__foreground">
      {zoomMode === 'week' ? (
        <WeekCalendar
          startDate={start}
          endDate={end}
          activities={activities}
          onDaySelected={onDayDetailSelected}
          monthIndex={monthIndex}
        />
      ) : (
        <MonthCalendar
          startDate={start}
          endDate={end}
          activities={activities}
          onMonthDaySelect={onMonthDaySelect}
          setMonthIndex={setMonthIndex}
          monthIndex={monthIndex}
        />
      )}
    </div>
  );

  const cta = (
    <div className="zoom-control">
      <button type="button" className="zoom-btn" onClick={onZoomOut}>
        <Image src={`/img/icons/view_month_${zoomMode === 'month' ? 'pink' : 'gray'}.svg`} alt="" />
        <span>{t('screens.Calendar.month')}</span>
      </button>
      <div className="control-separator" />
      <button type="button" className="zoom-btn" onClick={onZoomIn}>
        <Image src={`/img/icons/view_week_${zoomMode === 'week' ? 'pink' : 'gray'}.svg`} alt="" />
        <span>{t('screens.Calendar.week')}</span>
      </button>
    </div>
  );

  return (
    <>
      <LayeredPage
        className="calendar__container"
        background={background}
        foreground={foreground}
        cta={cta}
        gap="calc(4vh + 300px)"
        rounded
      />
      {selectedDetailDay && (
        <DayDetail t={t} activities={activities} date={selectedDetailDay} onClose={onDayPopupClosed} />
      )}
    </>
  );
}

DayDetail.propTypes = {
  activities: PropTypes.arrayOf(PropTypes.arrayOf).isRequired,
  date: PropTypes.instanceOf(Date).isRequired,
  onClose: PropTypes.func.isRequired,
  t: PropTypes.func.isRequired
};

Calendar.propTypes = {
  t: PropTypes.func.isRequired
};

export default compose(withTranslation(), withBackNavigation('light'))(Calendar);

import React, { useState } from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import SwipeableViews from 'react-swipeable-views';
import BadgeTabs from '../../../../../components/tabs/BadgeTabs';
import { getMonth } from '../../../../../utils/dates';
import './MonthCalendar.scss';

const bars = {
  plan: {
    className: 'red'
  },
  recommendedClass: {
    className: 'orange'
  },
  lesMillsClass: {
    className: 'pink'
  },
  otherClass: {
    className: 'purple'
  },
  none: {
    className: 'gray'
  }
};

function getMonthDates(startDate, endDate) {
  const dates = [];

  const pastMonths = Math.round(moment().diff(moment(startDate), 'months', true));
  const futureMonths = Math.round(moment(endDate).diff(moment(), 'months', true));

  for (let i = 1; i <= pastMonths + 1; i += 1) {
    dates.push(
      moment()
        .subtract(pastMonths - i + 1, 'month')
        .startOf('day')
        .toDate()
    );
  }
  for (let i = 1; i <= futureMonths; i += 1) {
    dates.push(
      moment()
        .add(i, 'month')
        .startOf('day')
        .toDate()
    );
  }

  return [dates];
}

function MonthCalendar({ startDate, endDate, activities, onMonthDaySelect, setMonthIndex, monthIndex }) {
  const [monthDates] = getMonthDates(startDate, endDate, monthIndex);
  const [selectedMonthIndex, setSelectedMonthIndex] = useState(monthIndex);

  const selectedMonth = monthDates[selectedMonthIndex];

  const label = selectedMonth.getFullYear();

  function onMonthChanged(index) {
    if (index < monthDates.length) {
      setSelectedMonthIndex(index);
      setMonthIndex(index);
    }
  }

  const itemClass = type => classNames({ 'activity-bar': true, [`-${bars[type].className}`]: true });

  return (
    <div className="month-calendar__container">
      <span className="top-label">{label}</span>

      <BadgeTabs
        tabs={monthDates.map(d => getMonth(d.getMonth()).substr(0, 3))}
        selectedIndex={selectedMonthIndex}
        defaultIndex={monthIndex}
        onSelected={onMonthChanged}
      />

      <SwipeableViews index={selectedMonthIndex} onChangeIndex={onMonthChanged}>
        {monthDates.map((d, i) => {
          const timestamp = d.getTime();

          return (
            <div className="month-activities" key={`month-${timestamp}`}>
              {activities
                .map(a => a[0])
                .filter(a => a.date.getMonth() === monthDates[i].getMonth())
                .map(item => {
                  const type = item.type.split('-')[0];
                  return (
                    <div
                      onClick={() => onMonthDaySelect(item)}
                      key={`${item.date.getTime()}-${type}`}
                      className={itemClass(type)}
                    />
                  );
                })}
            </div>
          );
        })}
      </SwipeableViews>
    </div>
  );
}

MonthCalendar.propTypes = {
  startDate: PropTypes.instanceOf(Date).isRequired,
  endDate: PropTypes.instanceOf(Date).isRequired,
  setMonthIndex: PropTypes.func.isRequired,
  onMonthDaySelect: PropTypes.func.isRequired,
  monthIndex: PropTypes.number.isRequired,
  activities: PropTypes.arrayOf(PropTypes.arrayOf).isRequired
};

export default MonthCalendar;

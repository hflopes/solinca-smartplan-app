import React, { Fragment, useState } from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';
import classNames from 'classnames';
import SwipeableViews from 'react-swipeable-views';
import Image from '../../../../../components/Image';
import { getDayOfWeek, getMonth } from '../../../../../utils/dates';
import './WeekCalendar.scss';

const styles = {
  root: {
    // gap between start and end of screen
    padding: '0 calc(50vw - 75px)'
  },
  slideContainer: {
    // gap between cards
    padding: '10px 0'
  }
};

function getCalendarItems(activities) {
  const items = {};

  activities.forEach(a => {
    a.forEach(b => {
      const date = new Date(`${b.date.getMonth() + 1}/${b.date.getDate()}/${b.date.getFullYear()}`);
      date.setHours(0);
      date.setMinutes(0);
      date.setSeconds(0);
      date.setMilliseconds(0);

      const timestamp = date.getTime();

      if (items[timestamp]) {
        items[timestamp].activities.push(b);
      } else {
        items[timestamp] = { activities: [b] };
      }
    });
  });

  return items;
}

function getSurroundingDates(startDate, endDate, monthIndex) {
  let todayIndex = -1;
  const dates = [];

  const pastDays = moment().diff(moment(startDate), 'days');
  const futureDays = moment(endDate).diff(moment(), 'days');

  for (let i = 1; i <= pastDays; i += 1) {
    dates.push(
      moment()
        .subtract(pastDays - i, 'day')
        .startOf('day')
        .toDate()
    );
  }

  todayIndex = dates.length - 1;

  for (let i = 1; i <= futureDays; i += 1) {
    dates.push(
      moment()
        .add(i, 'day')
        .startOf('day')
        .toDate()
    );
  }

  if (monthIndex === 6) {
    todayIndex = dates.length - 1;
  } else if (monthIndex > 3) {
    todayIndex += (monthIndex - (6 - 3)) * 30;
  } else if (monthIndex < 3) {
    todayIndex -= (3 - monthIndex) * 30;
  }

  return [dates, todayIndex];
}

function Activity({ date, type, stacked, checked, highlighted, onClick, t }) {
  const cards = {
    plan: {
      icon: '/img/big_icons/foot.svg',
      label: t('screens.Calendar.weekly.cards.plan'),
      className: 'red'
    },
    recommendedClass: {
      icon: '/img/big_icons/recommended_classes.svg',
      label: t('screens.Calendar.weekly.cards.recommendedClass'),
      className: 'orange'
    },
    lesMillsClass: {
      icon: '/img/big_icons/lesmills_classes.svg',
      label: t('screens.Calendar.weekly.cards.lesMillsClass'),
      className: 'pink'
    },
    otherClass: {
      icon: '/img/big_icons/other_classes.svg',
      label: t('screens.Calendar.weekly.cards.otherClass'),
      className: 'purple'
    },
    none: {
      label: t('screens.Calendar.weekly.cards.none'),
      className: 'gray'
    }
  };
  const config = cards[type];

  return (
    <div className={classNames({ 'activity-container': true, '-dimmed': !highlighted })}>
      <button className="activity-tab" type="button" onClick={onClick}>
        {`${date.getDate()}, ${getDayOfWeek(date.getDay()).substr(0, 3)}`}
      </button>
      <button className={classNames({ 'activity-card': true })} type="button" onClick={onClick}>
        <div className={classNames({ 'card-body': true, [`-${config.className}`]: true })}>
          {checked && <Image className="activity-card__check" src="/img/icons/check_white.svg" alt="" />}
          {config.icon && <Image className="activity-card__icon" src={config.icon} alt="" />}
          <span className="activity-card__label">{config.label}</span>
        </div>
        {stacked && <div className="card-footer">{t('screens.Calendar.weekly.additional')}</div>}
      </button>
    </div>
  );
}

function WeekCalendar({ startDate, endDate, activities, onDaySelected, t, monthIndex }) {
  const [dayDates, todayIndex] = getSurroundingDates(startDate, endDate, monthIndex);
  const [selectedDayIndex, setSelectedDayIndex] = useState(todayIndex);

  const selectedDay = dayDates[selectedDayIndex];
  const label = `${getMonth(selectedDay.getMonth())}, ${selectedDay.getFullYear()}`;

  const calendarItems = getCalendarItems(activities);

  function onDayChanged(index) {
    if (index < dayDates.length) {
      setSelectedDayIndex(index);
    }
  }

  function onDayClicked(index, date, type) {
    if (index !== selectedDayIndex) {
      setSelectedDayIndex(index);
    } else if (type !== 'none') {
      onDaySelected(date);
    }
  }

  return (
    <div className="week-calendar__container">
      <span className="top-label">{label}</span>

      <div className="week-calendar">
        <SwipeableViews
          index={selectedDayIndex}
          style={styles.root}
          slideStyle={styles.slideContainer}
          onChangeIndex={onDayChanged}
        >
          {dayDates.map((d, i) => {
            const timestamp = d.getTime();
            const item = calendarItems[timestamp];
            const activity = item.activities[0];
            const isStacked = item.activities.length >= 2;
            const type = activity.type.split('-')[0];
            const isChecked = timestamp < Date.now() && type !== 'none';
            const isHighlighted = selectedDayIndex === i;

            // Only show the neighbouring 8 cards (4 on each side)
            // To prevent rendering dozens of invisible cards
            return Math.abs(selectedDayIndex - i) < 5 ? (
              <Activity
                t={t}
                key={timestamp}
                date={d}
                type={type}
                stacked={isStacked}
                checked={isChecked}
                highlighted={isHighlighted}
                onClick={() => onDayClicked(i, d, type)}
              />
            ) : (
              <Fragment key={`frag-${timestamp}`} />
            );
          })}
        </SwipeableViews>
      </div>
    </div>
  );
}

Activity.propTypes = {
  date: PropTypes.instanceOf(Date).isRequired,
  type: PropTypes.string.isRequired,
  stacked: PropTypes.bool.isRequired,
  checked: PropTypes.bool.isRequired,
  highlighted: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
  t: PropTypes.func.isRequired
};

WeekCalendar.propTypes = {
  startDate: PropTypes.instanceOf(Date).isRequired,
  endDate: PropTypes.instanceOf(Date).isRequired,
  activities: PropTypes.arrayOf(PropTypes.arrayOf).isRequired,
  monthIndex: PropTypes.number.isRequired,
  onDaySelected: PropTypes.func.isRequired,
  t: PropTypes.func.isRequired
};

export default withTranslation()(WeekCalendar);

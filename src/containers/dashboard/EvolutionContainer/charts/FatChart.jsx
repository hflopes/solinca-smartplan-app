import React, { useMemo, useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';
import Chart from '../../../../components/Chart';
import { getFatHistory, getContextMessage, hasSeenFatGraph } from '../../../../redux/selectors/dashboard';
import { seenFatGraph } from '../../../../redux/modules/dashboard/actions/seenGraphs';
import { getMonth } from '../../../../utils/dates';

function buildIntervals(points, step) {
  const minY = Math.min(...points.map(p => p.y));
  const maxY = Math.max(...points.map(p => p.y));

  const intervalMin = minY - (minY % step);
  const intervalMax = maxY + step - (maxY % step);

  const intervals = [];

  for (let i = intervalMin; i <= intervalMax; i += step) {
    intervals.push({
      label: i,
      minBound: i - step / 2,
      maxBound: i + step / 2
    });
  }

  return intervals;
}

function formatTitle(point, isLatest, t) {
  const date = new Date(point.x);
  const prefix = isLatest ? t('screens.Evolution.charts.titleLatest') : t('screens.Evolution.charts.title');
  const day = date.getDate();
  const month = getMonth(date.getMonth());
  const year = date.getFullYear();

  return `${prefix} ${day} ${month} ${year}`;
}

function FatChart({ zoomScale, setCanZoom, t }) {
  const dispatch = useDispatch();
  const [selectedPointIndex, setSelectedPointIndex] = useState(0);
  const history = useSelector(state => getFatHistory(state));
  const contextMsg = useSelector(state => getContextMessage(state));
  const seenGraph = useSelector(state => hasSeenFatGraph(state));

  const points = useMemo(() => history.map(h => ({ x: h.date, y: h.fat })), [history]);
  const intervals = useMemo(() => buildIntervals(points, 2), [points]);

  useEffect(() => setSelectedPointIndex(points.length - 1), [points]);
  useEffect(() => {
    if (points.length > 1) {
      setCanZoom(true);
    } else {
      setCanZoom(false);
    }
  }, [points, setCanZoom]);

  if (!points || points.length === 0) {
    return (
      <div className="empty">
        <p>{t('screens.Evolution.charts.empty')}</p>
      </div>
    );
  }

  const width = points.length * 100 * zoomScale;
  const height = 250;

  function onSelected(index) {
    setSelectedPointIndex(index);
  }

  const hideModal = () => {
    dispatch(seenFatGraph());
  };

  const renderConfig = {
    title: formatTitle(points[selectedPointIndex], selectedPointIndex === points.length - 1, t),
    inverted: true,
    width,
    height,
    showIntervalDescription: false,
    showLegend: false,
    showLabel: true,
    showShadow: true,
    showModal: !seenGraph,
    modalHide: hideModal
  };

  return (
    <>
      <div className="selected__container">
        <b className="value">{points[selectedPointIndex].y}</b>
        <span className="metric">%</span>
        <span className="description">{contextMsg}</span>
      </div>
      <Chart
        dataPoints={points}
        intervals={intervals}
        onDataPointSelected={onSelected}
        renderConfig={renderConfig}
      />
    </>
  );
}

FatChart.propTypes = {
  zoomScale: PropTypes.number.isRequired,
  setCanZoom: PropTypes.func.isRequired,
  t: PropTypes.func.isRequired
};

export default withTranslation()(FatChart);

import React, { useMemo, useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';
import Chart from '../../../../components/Chart';
import { getIMCHistory, getContextMessage, hasSeenIMCGraph } from '../../../../redux/selectors/dashboard';
import { seenIMCGraph } from '../../../../redux/modules/dashboard/actions/seenGraphs';
import { getMonth } from '../../../../utils/dates';

function formatTitle(point, isLatest, t) {
  const date = new Date(point.x);
  const prefix = isLatest ? t('screens.Evolution.charts.titleLatest') : t('screens.Evolution.charts.title');
  const day = date.getDate();
  const month = getMonth(date.getMonth());
  const year = date.getFullYear();

  return `${prefix} ${day} ${month} ${year}`;
}

function ImcChart({ zoomScale, setCanZoom, t }) {
  const dispatch = useDispatch();
  const [selectedPointIndex, setSelectedPointIndex] = useState(0);
  const history = useSelector(state => getIMCHistory(state));
  const contextMsg = useSelector(state => getContextMessage(state));
  const seenGraph = useSelector(state => hasSeenIMCGraph(state));

  const points = useMemo(() => history.map(h => ({ x: h.date, y: h.imc })), [history]);

  useEffect(() => setSelectedPointIndex(points.length - 1), [points]);
  useEffect(() => {
    if (points.length > 1) {
      setCanZoom(true);
    } else {
      setCanZoom(false);
    }
  }, [points, setCanZoom]);

  if (!points || points.length === 0) {
    return (
      <div className="empty">
        <p>{t('screens.Evolution.charts.empty')}</p>
      </div>
    );
  }

  const width = points.length * 100 * zoomScale;
  const height = 200;

  function onSelected(index) {
    setSelectedPointIndex(index);
  }

  const hideModal = () => {
    dispatch(seenIMCGraph());
  };

  const renderConfig = {
    title: formatTitle(points[selectedPointIndex], selectedPointIndex === points.length - 1, t),
    inverted: false,
    width,
    height,
    showIntervalDescription: true,
    showLegend: true,
    showLabel: true,
    showShadow: false,
    showModal: !seenGraph,
    modalHide: hideModal
  };

  const currentValue = points[selectedPointIndex].y;
  const currentValueInt = Math.floor(points[selectedPointIndex].y);
  const currentValueDouble = Math.round((currentValue - currentValueInt) * 10);
  const INTERVALS = [
    {
      label: t('screens.Evolution.charts.imc.intervals.first.label'),
      description: t('screens.Evolution.charts.imc.intervals.first.label'),
      legend: t('screens.Evolution.charts.imc.intervals.first.legend'),
      minBound: 10,
      maxBound: 18.5,
      fill: '#fffb00'
    },
    {
      label: t('screens.Evolution.charts.imc.intervals.second.label'),
      description: t('screens.Evolution.charts.imc.intervals.second.label'),
      legend: t('screens.Evolution.charts.imc.intervals.second.legend'),
      minBound: 18.5,
      maxBound: 24.9,
      fill: '#00d455'
    },
    {
      label: t('screens.Evolution.charts.imc.intervals.third.label'),
      description: t('screens.Evolution.charts.imc.intervals.third.label'),
      legend: t('screens.Evolution.charts.imc.intervals.third.legend'),
      minBound: 25,
      maxBound: 29.9,
      fill: '#fffb00'
    },
    {
      label: t('screens.Evolution.charts.imc.intervals.fourth.label'),
      description: t('screens.Evolution.charts.imc.intervals.fourth.label'),
      legend: t('screens.Evolution.charts.imc.intervals.fourth.legend'),
      minBound: 30,
      maxBound: 34.9,
      fill: '#ff0000'
    },
    {
      label: t('screens.Evolution.charts.imc.intervals.fifth.label'),
      description: t('screens.Evolution.charts.imc.intervals.fifth.label'),
      legend: t('screens.Evolution.charts.imc.intervals.fifth.legend'),
      minBound: 35,
      maxBound: 39.9,
      fill: '#ff0000'
    },
    {
      label: t('screens.Evolution.charts.imc.intervals.sixth.label'),
      description: t('screens.Evolution.charts.imc.intervals.sixth.label'),
      legend: t('screens.Evolution.charts.imc.intervals.sixth.legend'),
      minBound: 40,
      maxBound: 50,
      fill: '#ff0000'
    }
  ];

  return (
    <>
      <div className="selected__container">
        <b className="value">{currentValueInt}</b>
        <b className="value-decimal">{`,${currentValueDouble}`}</b>
        <span className="description">{contextMsg}</span>
      </div>
      <Chart
        dataPoints={points}
        intervals={INTERVALS}
        onDataPointSelected={onSelected}
        renderConfig={renderConfig}
      />
    </>
  );
}

ImcChart.propTypes = {
  zoomScale: PropTypes.number.isRequired,
  setCanZoom: PropTypes.func.isRequired,
  t: PropTypes.func.isRequired
};

export default withTranslation()(ImcChart);

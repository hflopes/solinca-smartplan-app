import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useParams } from 'react-router';
import { compose } from 'redux';
import { withTranslation } from 'react-i18next';
import LayeredPage from '../../../components/LayeredPage';
import BadgeTabs from '../../../components/tabs/BadgeTabs';
import Image from '../../../components/Image';
import withBackNavigation from '../../../components/hoc/withBackNavigation';
import WeightChart from './charts/WeightChart';
import ImcChart from './charts/ImcChart';
import FatChart from './charts/FatChart';
import './Evolution.scss';

const ZOOM_MIN = 0.25;
const ZOOM_MAX = 1;
const ZOOM_INTERVAL = 0.25;

function getMetricIndex(metric) {
  switch (metric) {
    case 'weight':
      return 0;
    case 'imc':
      return 1;
    default:
      return 2;
  }
}

function Evolution({ t }) {
  const { metric } = useParams();

  const [canZoom, setCanZoom] = useState(false);
  const [zoomScale, setZoomScale] = useState(1);
  const [selectedTabIndex, setSelectedTabIndex] = useState(0);

  useEffect(() => setSelectedTabIndex(getMetricIndex(metric)), [metric]);

  function onZoomOut() {
    if (zoomScale <= ZOOM_MIN) return;
    setZoomScale(zoomScale - ZOOM_INTERVAL);
  }

  function onZoomIn() {
    if (zoomScale >= ZOOM_MAX) return;
    setZoomScale(zoomScale + ZOOM_INTERVAL);
  }

  const background = (
    <div className="evolution__background">
      <div id="notch" />
      <h3 className="page-title">{t('screens.Evolution.title')}</h3>
    </div>
  );

  const foreground = (
    <div className="evolution__foreground">
      <div className="evolution__tabs">
        <BadgeTabs
          tabs={t('screens.Evolution.tabs', { returnObjects: true })}
          selectedIndex={selectedTabIndex}
          onSelected={setSelectedTabIndex}
        />
      </div>
      <div className="evolution__chart">
        {selectedTabIndex === 0 && (
          <WeightChart key="weight-chart" setCanZoom={setCanZoom} zoomScale={zoomScale} />
        )}
        {selectedTabIndex === 1 && <ImcChart key="imc-chart" setCanZoom={setCanZoom} zoomScale={zoomScale} />}
        {selectedTabIndex === 2 && <FatChart key="fat-chart" setCanZoom={setCanZoom} zoomScale={zoomScale} />}
      </div>
    </div>
  );

  const cta = (
    <>
      {canZoom ? (
        <div className="zoom-control">
          <button type="button" className="zoom-btn" onClick={onZoomOut}>
            {zoomScale === ZOOM_MIN ? (
              <Image src="/img/icons/zoom_out_gray.svg" alt="" />
            ) : (
              <Image src="/img/icons/zoom_out_pink.svg" alt="" />
            )}
            <span>{t('screens.Evolution.zoomOut')}</span>
          </button>
          <div className="control-separator" />
          <button type="button" className="zoom-btn" onClick={onZoomIn}>
            {zoomScale === ZOOM_MAX ? (
              <Image src="/img/icons/zoom_in_gray.svg" alt="" />
            ) : (
              <Image src="/img/icons/zoom_in_pink.svg" alt="" />
            )}
            <span>{t('screens.Evolution.zoomIn')}</span>
          </button>
        </div>
      ) : null}
    </>
  );

  return (
    <LayeredPage
      className="evolution__container"
      background={background}
      foreground={foreground}
      cta={cta}
      gap="calc(4vh + 130px)"
      rounded
    />
  );
}

Evolution.propTypes = {
  t: PropTypes.func.isRequired
};

export default compose(withTranslation(), withBackNavigation('light'))(Evolution);

import React from 'react';
import EvolutionContainer from '../../containers/dashboard/EvolutionContainer';

function EvolutionScreen() {
  return <EvolutionContainer />;
}

export default EvolutionScreen;

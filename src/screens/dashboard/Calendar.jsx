import React from 'react';
import CalendarContainer from '../../containers/dashboard/CalendarContainer';

function CalendarScreen() {
  return <CalendarContainer />;
}

export default CalendarScreen;

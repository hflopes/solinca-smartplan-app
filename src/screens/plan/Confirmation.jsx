import React from 'react';
import ConfirmationContainer from '../../containers/plan/ConfirmationContainer';

function Confirmation() {
  return <ConfirmationContainer />;
}

export default Confirmation;

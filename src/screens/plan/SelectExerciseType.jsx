import React from 'react';
import SelectExerciseTypeContainer from '../../containers/plan/SelectExerciseTypeContainer';

function SelectExerciseType() {
  return <SelectExerciseTypeContainer />;
}

export default SelectExerciseType;

import React from 'react';
import QuestionnaireContainer from '../../containers/plan/QuestionnaireContainer';

function Questionnaire() {
  return <QuestionnaireContainer />;
}

export default Questionnaire;

import React from 'react';
import WelcomeContainer from '../../containers/plan/WelcomeContainer';

function Welcome() {
  return <WelcomeContainer />;
}

export default Welcome;

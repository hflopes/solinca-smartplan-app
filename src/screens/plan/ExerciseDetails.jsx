import React from 'react';
import PropTypes from 'prop-types';
import ExerciseDetailsContainer from '../../containers/plan/ExerciseDetailsContainer';

function ExerciseDetails({ match }) {
  const { id } = match.params;
  return <ExerciseDetailsContainer id={id} />;
}

ExerciseDetails.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string
    })
  }).isRequired
};

export default ExerciseDetails;

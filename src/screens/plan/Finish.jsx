import React from 'react';
import FinishContainer from '../../containers/plan/FinishContainer';

function Finish() {
  return <FinishContainer />;
}

export default Finish;

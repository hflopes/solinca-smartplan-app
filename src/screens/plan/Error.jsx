import React from 'react';
import PropTypes from 'prop-types';
import ErrorContainer from '../../containers/plan/ErrorContainer';

function Error({ location }) {
  const { state } = location;
  return <ErrorContainer message={state && state.message} />;
}

Error.propTypes = {
  location: PropTypes.shape({
    state: PropTypes.shape({
      message: PropTypes.string
    })
  }).isRequired
};

export default Error;

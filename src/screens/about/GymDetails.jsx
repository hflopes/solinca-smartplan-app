import React from 'react';
import PropTypes from 'prop-types';
import GymDetailsContainer from '../../containers/about/GymDetailsContainer';

function GymDetails({ match }) {
  const { id } = match.params;
  return <GymDetailsContainer gymId={id} />;
}

GymDetails.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string
    })
  }).isRequired
};

export default GymDetails;

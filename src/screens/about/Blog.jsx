import React from 'react';
import BlogContainer from '../../containers/about/BlogContainer';

function BlogScreen() {
  return <BlogContainer />;
}

export default BlogScreen;

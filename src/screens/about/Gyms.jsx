import React from 'react';
import GymsContainer from '../../containers/about/GymsContainer';

function Gyms() {
  return <GymsContainer />;
}

export default Gyms;

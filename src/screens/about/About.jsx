import React from 'react';
import AboutContainer from '../../containers/about/AboutContainer';

function About() {
  return <AboutContainer />;
}

export default About;

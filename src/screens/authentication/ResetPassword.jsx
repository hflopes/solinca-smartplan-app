import React from 'react';
import ResetPassword from '../../containers/authentication/ResetContainer';

function ResetScreen() {
  return <ResetPassword />;
}

export default ResetScreen;

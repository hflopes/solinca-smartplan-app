import React from 'react';
import LogoutContainer from '../../containers/authentication/LogoutContainer';

function LogoutScreen() {
  return <LogoutContainer />;
}

export default LogoutScreen;

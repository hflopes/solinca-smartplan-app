import React from 'react';
import LoginContainer from '../../containers/authentication/LoginContainer';

function LoginScreen() {
  return <LoginContainer />;
}

export default LoginScreen;

import React from 'react';
import RegisterContainer from '../../containers/authentication/RegisterContainer';

function RegisterScreen() {
  return <RegisterContainer />;
}

export default RegisterScreen;

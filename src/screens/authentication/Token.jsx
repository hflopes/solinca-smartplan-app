import React from 'react';
import TokenContainer from '../../containers/authentication/TokenContainer';

function TokenScreen() {
  return <TokenContainer />;
}

export default TokenScreen;

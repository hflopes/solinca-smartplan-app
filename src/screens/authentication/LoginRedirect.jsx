import React from 'react';
import LoginRedirectContainer from '../../containers/authentication/LoginRedirectContainer';

function LoginRedirectScreen() {
  return <LoginRedirectContainer />;
}

export default LoginRedirectScreen;

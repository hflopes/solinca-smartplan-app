import React from 'react';
import BecomePremiumContainer from '../../containers/general/BecomePremiumContainer';

function BecomePremium() {
  return <BecomePremiumContainer />;
}

export default BecomePremium;

import React from 'react';
import WelcomeContainer from '../../containers/general/WelcomeContainer';

function Welcome() {
  return <WelcomeContainer />;
}

export default Welcome;

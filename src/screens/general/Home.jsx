import React from 'react';
import HomeContainer from '../../containers/general/HomeContainer';

function HomeScreen() {
  return <HomeContainer />;
}

export default HomeScreen;

import React from 'react';
import OnBoardingContainer from '../../containers/general/OnBoardingContainer';

function OnBoardingScreen() {
  return <OnBoardingContainer />;
}

export default OnBoardingScreen;

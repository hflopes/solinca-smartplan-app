import React from 'react';
import TermsOfServiceContainer from '../../containers/general/TermsOfServiceContainer';

function TermsOfService() {
  return <TermsOfServiceContainer />;
}

export default TermsOfService;

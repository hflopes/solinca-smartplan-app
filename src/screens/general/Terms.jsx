import React from 'react';
import PropTypes from 'prop-types';
import TermsContainer from '../../components/PDF';

function Terms({ location }) {
  const { url, title } = location.state;
  console.log('url: ', url);
  console.log('title: ', title);
  return <TermsContainer title={title} url={url} />;
}

Terms.propTypes = {
  location: PropTypes.shape({
    state: PropTypes.shape({
      url: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired
    })
  }).isRequired
};

export default Terms;

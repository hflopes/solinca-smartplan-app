import React from 'react';
import IndexContainer from '../../containers/general/Index';

function IndexScreen() {
  return <IndexContainer />;
}

export default IndexScreen;

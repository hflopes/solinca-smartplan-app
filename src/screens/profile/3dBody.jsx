import React from 'react';
import PropTypes from 'prop-types';
import PDF3dBodyContainer from '../../components/PDF';

function PDF3dBody({ location }) {
  const { url } = location.state;
  console.log(url);
  return <PDF3dBodyContainer title="3D Body" url={url} />;
}

PDF3dBody.propTypes = {
  location: PropTypes.shape({
    state: PropTypes.shape({
      url: PropTypes.string.isRequired
    })
  }).isRequired
};

export default PDF3dBody;

import React from 'react';
import ProfileContainer from '../../containers/profile/ProfileContainer';

function Profile() {
  return <ProfileContainer />;
}

export default Profile;

import React from 'react';
import StatusesContainer from '../../containers/profile/StatusesContainer';

function Statuses() {
  return <StatusesContainer />;
}

export default Statuses;

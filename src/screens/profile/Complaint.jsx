import React from 'react';
import { useParams } from 'react-router-dom';
import ComplaintDetailsContainer from '../../containers/profile/ComplaintDetailsContainer';

function ComplaintDetailsScreen() {
  const { id } = useParams();
  return <ComplaintDetailsContainer id={id} />;
}

export default ComplaintDetailsScreen;

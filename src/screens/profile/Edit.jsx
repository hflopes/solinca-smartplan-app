import React from 'react';
import EditContainer from '../../containers/profile/EditContainer';

function EditProfile() {
  return <EditContainer />;
}

export default EditProfile;

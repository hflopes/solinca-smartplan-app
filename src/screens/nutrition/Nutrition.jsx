import React from 'react';
import Nutrition from '../../containers/nutrition/Nutrition';

function NutritionScreen() {
  return <Nutrition />;
}

export default NutritionScreen;

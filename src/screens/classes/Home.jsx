import React from 'react';
import HomeContainer from '../../containers/classes/HomeContainer';

function HomeScreen() {
  return <HomeContainer />;
}

export default HomeScreen;

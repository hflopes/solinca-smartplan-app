import React from 'react';
import PropTypes from 'prop-types';
import ModalityDetailsContainer from '../../containers/classes/ModalityDetailsContainer';

function ModalityDetails({ match }) {
  const { id } = match.params;
  return <ModalityDetailsContainer modalityId={id} />;
}

ModalityDetails.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string
    })
  }).isRequired
};

export default ModalityDetails;

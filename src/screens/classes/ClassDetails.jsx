import React from 'react';
import PropTypes from 'prop-types';
import ClassDetailsContainer from '../../containers/classes/ClassDetailsContainer';

function ClassDetails({ match }) {
  const { id } = match.params;
  return <ClassDetailsContainer classId={id} />;
}

ClassDetails.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string
    })
  }).isRequired
};

export default ClassDetails;

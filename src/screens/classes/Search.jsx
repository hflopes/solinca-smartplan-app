import React from 'react';
import SearchContainer from '../../containers/classes/SearchContainer';

function SearchScreen() {
  return <SearchContainer />;
}

export default SearchScreen;

import React from 'react';
import PropTypes from 'prop-types';
import RewardDetailsContainer from '../../containers/gamification/RewardDetailsContainer';

function RewardDetailsScreen({ match }) {
  const { id } = match.params;
  return <RewardDetailsContainer id={id} />;
}

RewardDetailsScreen.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string
    })
  }).isRequired
};

export default RewardDetailsScreen;

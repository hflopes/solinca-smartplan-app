import React from 'react';
import CatalogFiltersContainer from '../../containers/gamification/CatalogFilters';

function CatalogFilters() {
  return <CatalogFiltersContainer />;
}

export default CatalogFilters;

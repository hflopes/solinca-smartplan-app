import React from 'react';
import PropTypes from 'prop-types';
import ChallengeDetailsContainer from '../../containers/gamification/ChallengeDetailsContainer';

function ChallengeDetailsScreen({ match }) {
  const { id } = match.params;
  return <ChallengeDetailsContainer id={id} />;
}

ChallengeDetailsScreen.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string
    })
  }).isRequired
};

export default ChallengeDetailsScreen;

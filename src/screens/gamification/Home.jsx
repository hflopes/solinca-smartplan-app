import React from 'react';
import PropTypes from 'prop-types';
import GamificationContainer from '../../containers/gamification/HomeContainer';

function GamificationScreen({ match }) {
  const { section } = match.params;
  return <GamificationContainer section={section} />;
}

GamificationScreen.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      section: PropTypes.string
    })
  }).isRequired
};

export default GamificationScreen;

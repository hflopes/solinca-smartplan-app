import React from 'react';
import PropTypes from 'prop-types';
import LeadContainer from '../../containers/support/LeadContainer';

function Lead({ match }) {
  const { gymId } = match.params;
  return <LeadContainer gymId={gymId} />;
}

Lead.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      gymId: PropTypes.string
    })
  }).isRequired
};

export default Lead;

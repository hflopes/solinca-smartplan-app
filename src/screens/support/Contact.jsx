import React from 'react';
import ContactContainer from '../../containers/support/ContactContainer';

function Contact() {
  return <ContactContainer />;
}

export default Contact;

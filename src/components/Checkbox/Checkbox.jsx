import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Image from '../Image';
import './Checkbox.scss';

function Checkbox({ onToggle, checked, label }) {
  const btnClass = classNames({
    checkbox__button: true,
    checked
  });

  return (
    <div className="checkbox__container">
      <button className={btnClass} type="button" onClick={onToggle}>
        <Image className="checkbox__icon" src="/img/icons/check_white.svg" alt="" />
      </button>
      {label && (
        <button className="checkbox__label" type="button" onClick={onToggle}>
          {label}
        </button>
      )}
    </div>
  );
}

Checkbox.defaultProps = {
  label: undefined
};

Checkbox.propTypes = {
  onToggle: PropTypes.func.isRequired,
  checked: PropTypes.bool.isRequired,
  label: PropTypes.string
};

export default Checkbox;

import React from 'react';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';
import './ModalityDescription.scss';

function ModalityDescription({ t, modality }) {
  const { benefits, description } = modality;
  return (
    <div className="modality-description">
      <h3>{t('components.modalityDescription.about')}</h3>
      <p>{description}</p>
      <h3>{t('components.modalityDescription.benefits')}</h3>
      <p>{benefits}</p>
    </div>
  );
}

ModalityDescription.propTypes = {
  t: PropTypes.func.isRequired,
  modality: PropTypes.shape({
    description: PropTypes.string,
    benefits: PropTypes.string
  }).isRequired
};

export default withTranslation()(ModalityDescription);

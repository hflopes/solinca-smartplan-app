import React from 'react';
import './ScrollAnchor.scss';

const ScrollAnchor = () => <div className="scroll-anchor" />;

export default ScrollAnchor;

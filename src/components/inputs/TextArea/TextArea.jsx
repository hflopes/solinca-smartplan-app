import React, { Component } from 'react';
import TextareaAutosize from 'react-autosize-textarea';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import './TextArea.scss';

class TextArea extends Component {
  state = {
    value: '',
    isFocused: false
  };

  componentDidMount() {
    const { initialValue } = this.props;

    if (initialValue) {
      this.setState({ value: initialValue });
    }
  }

  onChange = evt => {
    this.setState({
      value: evt.target.value
    });

    const { onChange } = this.props;

    if (onChange) {
      onChange(evt);
    }
  };

  onFocus = evt => {
    const { onFocus } = this.props;

    if (onFocus) {
      onFocus(evt);
    }

    this.setState({
      isFocused: true
    });
  };

  onBlur = evt => {
    const { onBlur } = this.props;

    if (onBlur) {
      onBlur(evt);
    }

    this.setState({
      isFocused: false
    });
  };

  onKeyUp = evt => {
    const { onEnter } = this.props;

    if (onEnter && evt.key === 'Enter') {
      onEnter(evt);
    }
  };

  render() {
    const { type, placeholder, darkMode, disabled } = this.props;
    const { value, isFocused } = this.state;

    const isEmpty = !value || value.length === 0;

    const containerClass = classNames({
      'text-area': true,
      full: true,
      'input-faded': isEmpty,
      dark: darkMode
    });

    const placeholderClass = classNames({
      placeholder: true,
      focused: isFocused || (value && value.length > 0)
    });

    return (
      <div className={containerClass}>
        <TextareaAutosize
          type={type}
          onChange={this.onChange}
          onFocus={this.onFocus}
          onBlur={this.onBlur}
          onKeyUp={this.onKeyUp}
          value={value}
          disabled={disabled}
        />
        <span className={placeholderClass}>{placeholder}</span>
      </div>
    );
  }
}

TextArea.defaultProps = {
  type: 'text',
  placeholder: '',
  initialValue: '',
  onFocus: undefined,
  onBlur: undefined,
  onEnter: undefined,
  darkMode: false,
  disabled: false
};

TextArea.propTypes = {
  type: PropTypes.string,
  placeholder: PropTypes.string,
  initialValue: PropTypes.string,
  disabled: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  onEnter: PropTypes.func,
  darkMode: PropTypes.bool
};

export default TextArea;

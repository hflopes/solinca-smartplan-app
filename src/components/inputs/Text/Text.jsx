import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Image from '../../Image';
import './Text.scss';

const IMG_SHOW_PASSWORD = '/img/icons/eye_on.svg';
const IMG_HIDE_PASSWORD = '/img/icons/eye_off.svg';

class TextInput extends Component {
  state = {
    showPassword: false,
    value: '',
    isFocused: false
  };

  componentDidMount() {
    const { type, initialValue } = this.props;
    const isPassword = type === 'password';

    if (isPassword) {
      this.setState({
        showPassword: false
      });
    }

    if (initialValue) {
      this.setState({ value: initialValue });
    }
  }

  onChange = evt => {
    this.setState({
      value: evt.target.value
    });

    const { onChange } = this.props;

    if (onChange) {
      onChange(evt);
    }
  };

  onFocus = evt => {
    const { onFocus } = this.props;

    if (onFocus) {
      onFocus(evt);
    }

    this.setState({
      isFocused: true
    });
  };

  onBlur = evt => {
    const { onBlur } = this.props;

    if (onBlur) {
      onBlur(evt);
    }

    this.setState({
      isFocused: false
    });
  };

  onKeyUp = evt => {
    const { onEnter } = this.props;

    if (onEnter && evt.key === 'Enter') {
      onEnter(evt);
    }
  };

  togglePassword = () => {
    const { showPassword } = this.state;

    this.setState({
      showPassword: !showPassword
    });

    this.input.focus();
  };

  buildPasswordToggle(isPassword, showPassword, isEmpty) {
    if (!isPassword || isEmpty) {
      return null;
    }

    const imgUrl = showPassword ? IMG_HIDE_PASSWORD : IMG_SHOW_PASSWORD;

    return (
      <button type="button" onClick={this.togglePassword}>
        <Image alt="" src={imgUrl} />
      </button>
    );
  }

  render() {
    const { type, placeholder, darkMode, autoFocus, disabled } = this.props;
    const { showPassword, value, isFocused } = this.state;
    const isPassword = type === 'password';

    const currentType = isPassword && showPassword ? 'text' : type;
    const isEmpty = !value || value.length === 0;

    const containerClass = classNames({
      'line-input': true,
      full: true,
      'input-faded': isEmpty,
      dark: darkMode
    });

    const inputClass = classNames({
      'right-margin': isPassword
    });

    const placeholderClass = classNames({
      placeholder: true,
      focused: isFocused || (value && value.length > 0)
    });

    return (
      <div className={containerClass}>
        <input
          ref={i => {
            this.input = i;
          }}
          className={inputClass}
          type={currentType}
          onChange={this.onChange}
          onFocus={this.onFocus}
          onBlur={this.onBlur}
          onKeyUp={this.onKeyUp}
          value={value}
          autoFocus={autoFocus}
          disabled={disabled}
        />
        {this.buildPasswordToggle(isPassword, showPassword, isEmpty)}
        <span className={placeholderClass}>{placeholder}</span>
      </div>
    );
  }
}

TextInput.defaultProps = {
  type: 'text',
  placeholder: '',
  initialValue: '',
  onFocus: undefined,
  onBlur: undefined,
  onEnter: undefined,
  darkMode: false,
  disabled: false,
  autoFocus: false
};

TextInput.propTypes = {
  type: PropTypes.string,
  placeholder: PropTypes.string,
  initialValue: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  onEnter: PropTypes.func,
  darkMode: PropTypes.bool,
  autoFocus: PropTypes.bool,
  disabled: PropTypes.bool
};

export default TextInput;

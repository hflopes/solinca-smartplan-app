import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import './Input.scss';

const BACKSPACE_KEYCODE = 8;
const BLOCK_KEYCODES = [69, 188, 189, 190]; // block chars: , . - e

const getId = index => `token-input-${index}`;

class TokenInput extends Component {
  state = {
    values: []
  };

  onChange = (event, index) => {
    const { value } = event.target;

    if (!value || !value.match(/^[0-9]*$/gm)) return;

    const { values } = this.state;
    const isMultiple = value.toString(10).split('').length > 1;
    if (!isMultiple) {
      // Update the cached value
      values[index] = value;
      this.setState({ values });

      // If the user typed a number
      if (isFinite(value)) {
        this.skipTo(index + 1);
      }
    } else {
      const valuesSplit = value.toString(10).split('');
      let indexToInc = 0;
      valuesSplit.map(valueSingle => { // eslint-disable-line
        // If the user typed a number
        if (isFinite(valueSingle)) {
          values[index + indexToInc] = valueSingle;
          this.setState({ values });
          indexToInc += 1;
        }
      });
      this.skipTo(indexToInc);
    }
  };

  getValue() {
    const { values } = this.state;
    return values.join('');
  }

  onKeyDown = event => {
    const key = event.keyCode || event.charCode;
    if (BLOCK_KEYCODES.indexOf(key) !== -1) {
      event.preventDefault();
    }
  };

  onKeyUp = (event, index) => {
    const key = event.keyCode || event.charCode;
    const { values } = this.state;
    if (key === BACKSPACE_KEYCODE) {
      values[index] = '';
      this.setState({ values }, this.skipTo(index - 1));
    }
  };

  skipTo(index) {
    const { length, onChange } = this.props;

    if (index < 0) {
      return;
    }

    if (onChange) {
      onChange(this.getValue());
    }

    if (index >= length) {
      document.activeElement.blur();
      return;
    }

    const id = getId(index);
    const element = document.getElementById(id);
    element.focus();
  }

  render() {
    const { values } = this.state;
    const { isWrong, onFocus, onBlur, length } = this.props;

    return (
      <div className="token-input">
        {_.times(length, i => (
          <input
            value={values[i] || ''}
            key={getId(i)}
            id={getId(i)}
            className={isWrong ? 'wrong' : ''}
            onChange={evt => this.onChange(evt, i)}
            onKeyUp={evt => this.onKeyUp(evt, i)}
            onKeyDown={this.onKeyDown}
            onFocus={onFocus || null}
            onBlur={onBlur || null}
            type="number"
          />
        ))}
      </div>
    );
  }
}

TokenInput.defaultProps = {
  length: 4,
  onFocus: undefined,
  onBlur: undefined
};

TokenInput.propTypes = {
  onChange: PropTypes.func.isRequired,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  length: PropTypes.number,
  isWrong: PropTypes.bool.isRequired
};

export default TokenInput;

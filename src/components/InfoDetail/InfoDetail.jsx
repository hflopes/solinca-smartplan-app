import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Image from '../Image';
import './InfoDetail.scss';

function InfoDetail({ label, value, isEditable, isPassword, isList, field }) {
  return (
    <div className="info-detail">
      <div>
        <span>{label}</span>
        {isPassword ? <input type="password" value={value} disabled /> : <b>{value}</b>}
      </div>

      {!isEditable ? null : (
        <Link
          to={{
            pathname: `/profile/edit/${field}`,
            state: { value }
          }}
          className="link"
        >
          <Image src="/img/icons/edit.svg" alt="" />
        </Link>
      )}

      {!isList ? null : (
        <Link
          to={{
            pathname: `/profile/list/${field}`,
            state: { value }
          }}
          className="link"
        >
          <Image src="/img/icons/edit.svg" alt="" />
        </Link>
      )}
    </div>
  );
}

InfoDetail.defaultProps = {
  isEditable: false,
  isPassword: false,
  isList: false,
  field: ''
};

InfoDetail.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  isEditable: PropTypes.bool,
  isPassword: PropTypes.bool,
  isList: PropTypes.bool,
  field: PropTypes.string
};

export default InfoDetail;

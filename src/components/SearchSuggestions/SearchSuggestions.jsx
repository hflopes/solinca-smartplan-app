import React from 'react';
import PropTypes from 'prop-types';

import ModalityCard from '../cards/ModalityCard';
import './SearchSuggestions.scss';

function SearchSuggestions({ suggestions, filters, onGymSelected, onModalitySelected }) {
  const { modalities, gyms } = suggestions;

  const hasGyms = gyms && gyms.length > 0;
  const hasModalities = modalities && modalities.length > 0;

  const isModalityFilter = modality => filters.modalities.map(m => m.id).includes(modality.id);
  const isGymFilter = gym => filters.gyms.map(g => g.id).includes(gym.id);

  if (!hasGyms && !hasModalities) {
    return (
      <div className="section--empty">
        <span className="empty-section__label">Não há resultados</span>
      </div>
    );
  }

  const modalitiesSection = !hasModalities ? null : (
    <div className="section--modalities">
      <span className="section__label">Aulas</span>
      <ul className="section__list">
        {modalities.map(modality => (
          <li key={modality.id} className="list-item">
            <ModalityCard
              modality={modality}
              onClick={() => onModalitySelected(modality)}
              unfocused={Boolean(isModalityFilter(modality))}
            />
          </li>
        ))}
      </ul>
    </div>
  );

  const gymsSection = !hasGyms ? null : (
    <div className="section--gyms">
      <span className="section__label">Clubes</span>
      <ul className="section__list">
        {gyms.map(gym => (
          <li key={gym.id} className="list-item">
            <button
              className={`list-item__button ${isGymFilter(gym) ? '-disabled' : ''}`}
              type="button"
              onClick={() => onGymSelected(gym)}
            >
              <b className="gym__title">{gym.name}</b>
              {gym.distance && <span className="gym__info">{`${gym.distance.toFixed(1)} km`}</span>}
            </button>
          </li>
        ))}
      </ul>
    </div>
  );

  return (
    <>
      {modalitiesSection}
      {gymsSection}
    </>
  );
}

SearchSuggestions.propTypes = {
  suggestions: PropTypes.shape({
    modalities: PropTypes.arrayOf(PropTypes.any),
    gyms: PropTypes.arrayOf(PropTypes.any)
  }).isRequired,
  filters: PropTypes.shape({
    modalities: PropTypes.arrayOf(PropTypes.any),
    gyms: PropTypes.arrayOf(PropTypes.any)
  }).isRequired,
  onGymSelected: PropTypes.func.isRequired,
  onModalitySelected: PropTypes.func.isRequired
};

export default SearchSuggestions;

import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Image from '../Image';
import './Badge.scss';

function Badge({ size, image, style }) {
  return <Image className={classNames('container', size)} style={style} alt="badge" src={image} />;
}

Badge.defaultProps = {
  size: 'small',
  style: {},
  image: ''
};

Badge.propTypes = {
  size: PropTypes.string,
  style: PropTypes.shape({}),
  image: PropTypes.string
};

export default Badge;

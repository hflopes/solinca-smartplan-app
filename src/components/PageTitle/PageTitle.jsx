import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import './PageTitle.scss';

function PageTitle({ text, textHighlight, alignment }) {
  const className = classNames({
    'page-title': true,
    '-left': alignment === 'left',
    '-right': alignment === 'right',
    '-center': alignment === 'center'
  });

  return (
    <h1 className={className}>
      {text}
      <br />
      <b>{textHighlight}</b>
    </h1>
  );
}

PageTitle.defaultProps = {
  alignment: 'left'
};

PageTitle.propTypes = {
  text: PropTypes.string.isRequired,
  textHighlight: PropTypes.string.isRequired,
  alignment: PropTypes.string
};

export default PageTitle;

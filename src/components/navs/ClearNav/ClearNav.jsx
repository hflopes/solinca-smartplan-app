import React from 'react';
import PropTypes from 'prop-types';
import Image from '../../Image';
import './ClearNav.scss';

function ClearNav({ onNavigateBack, theme }) {
  const image = theme === 'dark' ? 'back_black.svg' : 'back_white.svg';

  return (
    <nav id="clear-nav" className="clear-nav">
      <button className="back-btn" type="button" onClick={onNavigateBack}>
        <Image alt="<" src={`/img/icons/${image}`} />
      </button>
    </nav>
  );
}

ClearNav.defaultProps = {
  onNavigateBack: undefined,
  theme: 'light'
};

ClearNav.propTypes = {
  onNavigateBack: PropTypes.func,
  theme: PropTypes.string
};

export default ClearNav;

import React from 'react';
import PropTypes from 'prop-types';
import Image from '../../Image';
import { openMenu } from '../../../utils/navigation';
import './Hamburger.scss';

const onMenuClicked = () => {
  setTimeout(openMenu, 200);
};

const Hamburger = ({ theme }) => {
  const image = theme === 'dark' ? 'menu_black.svg' : 'menu_white.svg';

  return (
    <nav id="hamburger-nav" className="hamburger__nav">
      <div id="notch" />
      <button className="hamburger__btn" type="button" onClick={onMenuClicked}>
        <Image alt="" src={`/img/icons/${image}`} />
      </button>
    </nav>
  );
};

Hamburger.defaultProps = {
  theme: 'light'
};

Hamburger.propTypes = {
  theme: PropTypes.string
};

export default Hamburger;

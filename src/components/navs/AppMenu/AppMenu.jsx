import React, { useState, Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import _ from 'lodash';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import ConditionalWrap from 'conditional-wrap';
import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';
import Image from '../../Image';
import socialLinks from '../../../config/social';
import { closeMenu } from '../../../utils/navigation';
import { userIsLoggedIn } from '../../../redux/selectors/auth';
import './AppMenu.scss';

import { VERSION, BUILD } from '../../../config/app';

const links = t => {
  return [
    {
      label: t('components.appMenu.links.home'),
      icon: '/img/icons/menu_home_gray.svg',
      url: '/home',
      auth: true
    },
    {
      label: t('components.appMenu.links.welcome'),
      icon: '/img/icons/menu_home_gray.svg',
      url: '/welcome',
      auth: false
    },
    {
      label: t('components.appMenu.links.appointments'),
      icon: '/img/icons/menu_appointments_gray.svg',
      submenu: [
        {
          label: t('components.appMenu.links.appointment.classes'),
          url: '/classes'
        },
        // {
        //   label: t('components.appMenu.links.appointment.gym'),
        //   url: '/welcome'
        // },
        {
          label: t('components.appMenu.links.appointment.nutrition'),
          url: '/nutrition'
        }
      ],
      auth: true
    },
    {
      label: t('components.appMenu.links.plan'),
      icon: '/img/icons/menu_plan_gray.svg',
      submenu: [
        {
          label: t('components.appMenu.links.planStart'),
          url: '/plan'
        },
        {
          label: t('components.appMenu.links.planRequest'),
          url: '/plan/renew'
        }
      ],
      auth: true
    },
    // {
    //   label: t('components.appMenu.links.gamification'),
    //   icon: '/img/icons/menu_challenges_gray.svg',
    //   url: '/gamification',
    //   auth: true
    // },
    {
      label: t('components.appMenu.links.activity'),
      icon: '/img/icons/menu_activity_gray.svg',
      url: '/dashboard/calendar',
      auth: true
    },
    {
      label: t('components.appMenu.links.metrics'),
      icon: '/img/icons/menu_metrics_gray.svg',
      submenu: [
        {
          label: t('components.appMenu.links.metric.weight'),
          url: '/dashboard/evolution/weight'
        },
        {
          label: t('components.appMenu.links.metric.imc'),
          url: '/dashboard/evolution/imc'
        },
        {
          label: t('components.appMenu.links.metric.fat'),
          url: '/dashboard/evolution/fat'
        }
      ],
      auth: true
    },
    {
      label: t('components.appMenu.links.profile'),
      icon: '/img/icons/menu_profile_gray.svg',
      url: '/profile',
      auth: true
    },

    {
      label: t('components.appMenu.links.about'),
      icon: '/img/icons/menu_solinca_gray.svg',
      url: '/about'
    },
    {
      label: t('components.appMenu.links.blog'),
      icon: '/img/icons/menu_blog_gray.svg',
      url: '/about/blog'
    },
    {
      label: t('components.appMenu.links.contact'),
      icon: '/img/icons/menu_contact_gray.svg',
      url: '/support/contact'
    }
  ];
};

function MenuLink({ url, icon, label, submenu, isHighlighted, onExpanded }) {
  const [isExpanded, setIsExpanded] = useState(false);

  const onClick = () => {
    if (submenu) {
      setIsExpanded(i => !i);

      if (!isExpanded) {
        onExpanded();
      }
    } else {
      closeMenu();
    }
  };

  const imgUrl = isHighlighted ? icon.replace('gray', 'pink') : icon;

  return (
    <ConditionalWrap condition={!submenu} wrap={children => <Link to={url}>{children}</Link>}>
      <li key={label}>
        <button type="button" onClick={onClick} className={isHighlighted ? 'highlighted' : ''}>
          <img src={imgUrl} alt="" />
          {label}
        </button>
        {submenu &&
          isExpanded &&
          submenu.map(s => (
            <Link key={s.url} className="sub-link" to={s.url}>
              <button type="button" onClick={closeMenu}>
                {s.label}
              </button>
            </Link>
          ))}
      </li>
    </ConditionalWrap>
  );
}

class AppMenu extends Component {
  state = {
    highlightedIndex: -1
  };

  componentWillReceiveProps(nextProps) {
    const { location } = nextProps;
    const { highlightedIndex } = this.state;

    if (location) {
      const { pathname } = location;

      _.map(links(nextProps.t), (l, i) => {
        if (l.url === pathname && i !== highlightedIndex) {
          this.setState({ highlightedIndex: i });
        }

        if (l.submenu && l.submenu.map(s => s.url) === pathname) {
          this.setState({ highlightedIndex: i });
        }
      });
    }
  }

  onSubmenuExpanded = i => {
    this.setState({ highlightedIndex: i });
  };

  render() {
    const { t, isLoggedIn } = this.props;
    const { highlightedIndex } = this.state;

    const menuClassName = !isLoggedIn ? 'centered' : '';

    return (
      <div className="app-menu full-height-flex">
        <div id="notch" />
        <div className="menu-nav-panel">
          <button type="button" className="close-btn" onClick={closeMenu}>
            <Image src="/img/icons/clear.svg" alt="X" />
          </button>

          <ul className={menuClassName}>
            {links(t).map(({ label, icon, url, auth, submenu }, i) => {
              // If auth is true and isnt logged in
              // Or auth is false and is logged in
              if (auth !== undefined && auth !== isLoggedIn) {
                return null;
              }

              return (
                <MenuLink
                  key={label}
                  label={label}
                  url={url}
                  icon={icon}
                  submenu={submenu}
                  isHighlighted={highlightedIndex === i}
                  onExpanded={() => this.onSubmenuExpanded(i)}
                />
              );
            })}
          </ul>

          {isLoggedIn && (
            <Link to="/logout">
              <button type="button" className="logout-btn" onClick={closeMenu}>
                <Image src="/img/icons/logout.svg" alt="" />
                <b>Log out</b>
              </button>
            </Link>
          )}

          <div className="socials">
            {_.map(socialLinks, s => (
              <a key={s.label} className="social-link" href={s.url}>
                <Image src={s.img} alt="" />
              </a>
            ))}
          </div>
          <span className="appVersion">{`${VERSION} - ${BUILD}`}</span>
        </div>
      </div>
    );
  }
}

MenuLink.defaultProps = {
  submenu: undefined,
  url: ''
};

MenuLink.propTypes = {
  url: PropTypes.string,
  icon: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  submenu: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      url: PropTypes.string
    })
  ),
  isHighlighted: PropTypes.bool.isRequired,
  onExpanded: PropTypes.func.isRequired
};

AppMenu.propTypes = {
  t: PropTypes.func.isRequired,
  isLoggedIn: PropTypes.bool.isRequired,
  location: PropTypes.shape({
    pathname: PropTypes.string
  }).isRequired
};

const mapStateToProps = state => {
  const isLoggedIn = userIsLoggedIn(state);
  return { isLoggedIn };
};

export default compose(withTranslation(), connect(mapStateToProps), withRouter)(AppMenu);

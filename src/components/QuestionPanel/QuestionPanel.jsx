import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';
import Image from '../Image';
import './QuestionPanel.scss';

class QuestionPanel extends Component {
  componentWillReceiveProps(nextProps) {
    const { answers, onSelect, selectedAnswerIndex } = this.props;

    const newAnswers = JSON.stringify(nextProps.answers);
    const oldAnswers = JSON.stringify(answers);

    if (newAnswers !== oldAnswers && selectedAnswerIndex === -1) {
      onSelect(-1);
    }
  }

  highlight = index => {
    const { onSelect, selectedAnswerIndex } = this.props;
    const newIndex = selectedAnswerIndex === index ? -1 : index;

    onSelect(newIndex);
  };

  renderAnswer = ({ isHighlighted, text, subtext, index }) => {
    return (
      <li key={`answer-${index}`} className={isHighlighted ? 'highlighted' : ''}>
        <button
          type="button"
          onClick={() => {
            this.highlight(index);
          }}
        >
          <div>
            <b>{text}</b>
            {subtext && <span>{subtext}</span>}
          </div>
          {isHighlighted && <Image src="/img/icons/check_pink.svg" alt="" />}
        </button>
      </li>
    );
  };

  render() {
    const { title, answers, selectedAnswerIndex } = this.props;

    return (
      <div className="question-panel">
        <b className="question-title">{title}</b>
        <ul>
          {answers.map((a, i) =>
            this.renderAnswer({
              text: a.title,
              subtext: a.subtitle,
              isHighlighted: i === selectedAnswerIndex,
              index: i
            })
          )}
        </ul>
      </div>
    );
  }
}

QuestionPanel.propTypes = {
  title: PropTypes.string.isRequired,
  onSelect: PropTypes.func.isRequired,
  answers: PropTypes.arrayOf(PropTypes.any).isRequired,
  selectedAnswerIndex: PropTypes.number.isRequired
};

export default withTranslation()(QuestionPanel);

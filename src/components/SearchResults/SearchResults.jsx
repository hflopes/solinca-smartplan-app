import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { withTranslation } from 'react-i18next';
import _ from 'lodash';
import GymHeader from '../GymHeader';
import ClassCard from '../cards/ClassCard';
import { getMonth, getDayOfWeek } from '../../utils/dates';
import './SearchResults.scss';
import VisibleElement from '../VisibleElement';

const RESULT_CARDS_DAYS = 2;

const getDateString = (t, date) => {
  const dayOfWeek = getDayOfWeek(date.getDay());
  const day = date.getDate();
  const month = getMonth(date.getMonth());

  return t('screens.Classes.headerDate', { dayOfWeek, day, month });
};

class SearchResults extends Component {
  state = {
    toShow: RESULT_CARDS_DAYS
  };

  incrementToShow = () => {
    const { toShow } = this.state;
    this.setState({ toShow: toShow + RESULT_CARDS_DAYS });
  };

  getItemsToLoad = () => {
    const { results } = this.props;
    const { toShow } = this.state;
    const allKeys = _.keys(results);
    const filteredKeys = allKeys.slice(0, toShow);
    const loadMore = filteredKeys.length < allKeys.length;
    return { items: _.pick(results, filteredKeys), loadMore };
  };

  render() {
    const { t } = this.props;
    const { items, loadMore } = this.getItemsToLoad();
    return (
      <>
        {_.map(items, (gyms, date) => (
          <div key={`result-${date}`} className="search-result">
            <span className="search-result__title">{getDateString(t, new Date(date))}</span>
            {_.map(gyms, ({ gym, classes }) => (
              <div key={`gym-${gym.id}`} className="gym__list">
                <GymHeader gymName={gym.name} darkMode />
                {classes.map(c => (
                  <ClassCard key={c.id} classData={c} />
                ))}
              </div>
            ))}
          </div>
        ))}
        {loadMore && <VisibleElement onVisible={this.incrementToShow} />}
      </>
    );
  }
}

SearchResults.propTypes = {
  results: PropTypes.shape({}).isRequired,
  t: PropTypes.func.isRequired
};

export default compose(withTranslation())(SearchResults);

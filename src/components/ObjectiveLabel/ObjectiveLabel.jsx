import React from 'react';
import { useSelector } from 'react-redux';
import classNames from 'classnames';
import { getObjective } from '../../redux/selectors/plan';
import { getUserStatus } from '../../redux/selectors/user';
import './ObjectiveLabel.scss';

function ObjectiveLabel() {
  const objective = useSelector(state => getObjective(state));
  const status = useSelector(state => getUserStatus(state));

  return <div className={classNames('objective-label', { [`-${status}`]: true })}>{objective}</div>;
}

export default ObjectiveLabel;

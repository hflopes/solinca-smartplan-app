import React, { Component } from 'react';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';
import { IonSpinner } from '@ionic/react';
import withBackNavigation from '../hoc/withBackNavigation';
import './PDF.scss';

class TermsPDF extends Component {
  state = {
    loading: true,
    iframeKey: 0
  };

  componentDidMount() {
    this.interval = setInterval(this.checkLoading, 5000);
  }

  checkLoading = () => {
    const { loading, iframeKey } = this.state;
    if (loading) {
      this.setState({ iframeKey: iframeKey + 1 });
    } else {
      clearInterval(this.interval);
    }
  };

  hideLoading = () => {
    this.setState({ loading: false });
  };

  render() {
    const { loading, iframeKey } = this.state;
    const { url, title } = this.props;

    return (
      <div className="pdf-container full-height-flex">
        <div id="notch" />
        <div className="top">
          <h1>{title}</h1>
        </div>
        {loading && <IonSpinner name="crescent" />}
        <iframe
          title={title}
          onLoad={this.hideLoading}
          key={iframeKey}
          src={`https://docs.google.com/viewer?url=${url}&embedded=true`}
          height="100%"
          width="100%"
        />
      </div>
    );
  }
}

TermsPDF.propTypes = {
  url: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired
};

export default compose(withTranslation(), withBackNavigation('dark'))(TermsPDF);

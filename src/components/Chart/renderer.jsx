import React, { Fragment } from 'react';
import _ from 'lodash';

export function getScales(intervals, points, renderConfig) {
  const { width, height, inverted } = renderConfig;

  // Calculate the max and min values of the datapoints
  const { minX, maxX, minY, maxY } = calculateBounds(points);

  // Define the real max and min values (intervals override datapoints)
  const realMinY = intervals ? Math.min(intervals[0].minBound, minY) : minY;
  const realMaxY = intervals ? Math.max(intervals[intervals.length - 1].maxBound, maxY) : maxY;

  const xDiff = maxX - minX;
  const yDiff = realMaxY - realMinY;

  // This function scales any given x to correctly fit the chart
  function scaleX(x) {
    return Math.round(x === 0 ? 0 : ((x - minX) / xDiff) * width);
  }

  // This function scales any given y to correctly fit the chart
  function scaleY(y) {
    const scaled = ((y - realMinY) / yDiff) * height;
    return inverted ? Math.round(height - scaled) : Math.round(scaled);
  }

  return { scaleX, scaleY };
}

/**
 * Scale an array of points to fit the chart
 */
export function scalePoints(points, scales) {
  const { scaleX, scaleY } = scales;
  return points.map(p => ({ x: scaleX(p.x), y: scaleY(p.y) }));
}

/**
 * Scale an array of intervals' areas to fit the chart
 */
export function scaleAreas(intervals, scales) {
  if (!intervals) {
    return [];
  }

  return intervals.map(i => ({
    startX: 0,
    endX: 0,
    startY: scales.scaleY(i.minBound),
    endY: scales.scaleY(i.maxBound),
    ...i
  }));
}

/**
 * Calculate the min and max values for every x and y of the dataset
 */
function calculateBounds(points) {
  const minX = Math.min(...points.map(p => p.x));
  const maxX = Math.max(...points.map(p => p.x));
  const minY = Math.min(...points.map(p => p.y));
  const maxY = Math.max(...points.map(p => p.y));

  return { minX, maxX, minY, maxY };
}

/**
 * Creates a polygon spaning the entire area underneath the chart line
 */
export function createAreaPolygon(points, height) {
  const pathPoints = [];
  let pathString = '';

  pathPoints.push({ x: 0, y: height }); // Add left-bottom point
  pathPoints.push(...points); // Add all line points
  pathPoints.push({ x: points[points.length - 1].x, y: height }); // Add right-bottom point

  pathPoints.forEach(p => {
    pathString += `${p.x},${p.y} `;
  });

  return { points: pathString };
}

/**
 * Creates all the chart lines (connecting the points)
 */
function createLines(points) {
  const lines = [];
  const lineStyle = { stroke: 'black', strokeWidth: 1 };

  for (let i = 0; i < points.length - 1; i += 1) {
    const startPoint = points[i];
    const endPoint = points[i + 1];

    lines.push({
      key: `line-(${startPoint.x},${startPoint.y})`,
      x1: startPoint.x,
      y1: startPoint.y,
      x2: endPoint.x,
      y2: endPoint.y,
      style: lineStyle
    });
  }

  return lines;
}

/**
 * Creates all the points on the chart
 */
function createIntersections(points) {
  const color = 'black';
  const lineStyle = { stroke: color, strokeWidth: 2, fill: color };

  return points.map(p => ({
    key: `circle-(${p.x},${p.y})`,
    cx: p.x,
    cy: p.y,
    r: 4,
    style: lineStyle
  }));
}

/**
 * Creates all the interval areas
 */
export function createAreas(areas) {
  return areas.map(a => {
    const yDiff = a.endY - a.startY;
    const y = yDiff < 0 ? a.endY - 1 : a.startY - 1;

    return {
      key: `area-(${a.startX},${a.startY})`,
      x: 0,
      y,
      width: '100%',
      height: Math.abs(yDiff) + 1,
      fill: a.fill || 'none',
      label: a.label,
      description: a.description
    };
  });
}

export function renderLines(points) {
  return createLines(points).map(l => <line {...l} />);
}

export function renderIntersections(points) {
  return createIntersections(points).map(c => <circle {...c} />);
}

export function renderIntervalAreas(areas) {
  return createAreas(areas).map(r => (
    <Fragment key={`${r.key}-ctr`}>
      <rect {...r} />
      <rect {...r} key={`${r.key}-sep`} height="1px" fill="white" />
    </Fragment>
  ));
}

export function renderShadow(points, height) {
  return (
    <>
      <defs>
        <linearGradient id="grad1" x1="0%" y1="100%" x2="0%" y2="0%">
          <stop offset="0%" style={{ stopColor: 'black', stopOpacity: 0.1 }} />
          <stop offset="100%" style={{ stopColor: 'black', stopOpacity: 0 }} />
        </linearGradient>
      </defs>
      <polygon {...createAreaPolygon(points, height)} fill="url(#grad1)" />
    </>
  );
}

export function renderLabels(areas) {
  return createAreas(areas).map(r => (
    <div key={`${r.key}-label`} className="chart-label__container" style={{ top: r.y, height: r.height }}>
      <div className="chart-label">{r.label}</div>
    </div>
  ));
}

export function renderDescriptions(areas) {
  return createAreas(areas).map(r => (
    <div
      key={`${r.key}-description`}
      className="chart-description__container"
      style={{ top: r.y, height: r.height }}
    >
      <span>{r.description}</span>
    </div>
  ));
}

export function renderLegend(intervals) {
  return _.uniqBy(intervals, 'legend').map(i => (
    <div key={i.legend} className="chart-legend">
      <div className="legend-dot" style={{ background: i.fill }} />
      <span className="legend-text">{i.legend}</span>
    </div>
  ));
}

import React, { useState, useCallback, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import SwipeableViews from 'react-swipeable-views';
import { withTranslation } from 'react-i18next';
import {
  scalePoints,
  scaleAreas,
  getScales,
  renderShadow,
  renderLines,
  renderIntersections,
  renderIntervalAreas,
  renderLabels,
  renderDescriptions,
  renderLegend
} from './renderer';
import './Chart.scss';

function Chart({ dataPoints, intervals, onDataPointSelected, renderConfig, t }) {
  const {
    title,
    width,
    height,
    showIntervalDescription,
    showLegend,
    showLabel,
    showShadow,
    showModal,
    modalHide
  } = renderConfig;

  const [selectedPointIndex, setSelectedPointIndex] = useState(dataPoints.length - 1);
  const isDragging = useRef(false);

  function handleTouchStart() {
    if (showModal) {
      modalHide();
    }
    isDragging.current = true;
  }

  function handleTouchEnd() {
    isDragging.current = false;
    attemptSnap();
  }

  function handleScroll() {
    const wrapper = document.getElementById('chart-wrapper');
    const labels = document.getElementById('chart-labels');
    const { scrollLeft } = wrapper;

    if (!labels) {
      return;
    }

    if (scrollLeft > width * 0.9) {
      if (!labels.classList.contains('-expand')) {
        labels.classList.add('-expand');
      }
    } else if (labels.classList.contains('-expand')) {
      labels.classList.remove('-expand');
    }
  }

  function handleInitialScroll() {
    setTimeout(() => {
      const wrapper = document.getElementById('chart-wrapper');
      wrapper.scrollTo(width * 2, 0);
      wrapper.style.visibility = 'visible';
    }, 100);
  }

  /**
   * After an attempt of point-snapping has been made,
   * it will re-attempt every 100ms until the scrolling
   * has completely stopped (or the user started scrolling again)
   * and snap the scrolling to the nearest point
   */
  function attemptSnap() {
    const wrapper = document.getElementById('chart-wrapper');
    const startX = wrapper.scrollLeft;

    setTimeout(() => {
      const curX = wrapper.scrollLeft;

      if (curX !== startX && !isDragging.current) {
        attemptSnap();
      } else {
        const closestPoint = [...scaledPoints].sort((a, b) => Math.abs(curX - a.x) - Math.abs(curX - b.x))[0];
        const selectedIndex = scaledPoints.findIndex(p => p.x === closestPoint.x);

        if (scaledPoints.length < 2) {
          return;
        }

        onDataPointSelected(selectedIndex);
        setSelectedPointIndex(selectedIndex);

        wrapper.scrollTo({
          left: closestPoint.x,
          behavior: 'smooth'
        });
      }
    }, 100);
  }

  function scrollToSelected() {
    const wrapper = document.getElementById('chart-wrapper');
    const selectedPoint = scaledPoints[selectedPointIndex];

    wrapper.scrollTo(selectedPoint.x, 0);
  }

  const scales = getScales(intervals, dataPoints, renderConfig);

  const styles = {
    chartStyle: { width, height },
    overlayStyle: { height, top: 10 },
    wrapperStyle: { padding: `10px 50vw`, visibility: 'hidden' },
    verticalLineStyle: { stroke: 'black', strokeWidth: 2, strokeDasharray: '2,7' },
    horizontalLineStyle: { stroke: 'black', strokeWidth: 1 }
  };

  const scaledPoints = scalePoints(dataPoints, scales);
  const scaledAreas = scaleAreas(intervals, scales);

  const onResize = useCallback(scrollToSelected, [selectedPointIndex, scaledPoints]);

  useEffect(handleInitialScroll, []);
  useEffect(onResize, [width]);

  return (
    <div className="chart__container">
      <svg className="chart__background" style={styles.overlayStyle}>
        {intervals && renderIntervalAreas(scaledAreas)}
      </svg>

      <SwipeableViews disabled={!showIntervalDescription}>
        <div
          id="chart-wrapper"
          className="chart__wrapper"
          onTouchStart={handleTouchStart}
          onTouchEnd={handleTouchEnd}
          onScroll={handleScroll}
          style={styles.wrapperStyle}
        >
          {showModal ? (
            <div className="modal">
              <img src="/img/icons/arrow_horiz.svg" alt="" />
              <p>{t('screens.Dashboard.modal')}</p>
            </div>
          ) : null}
          {/* SVG has a bug where it only re-renders on re-mount */}
          {/* So we need to bind its rendering to the width and height , to ensure */}
          {/* it draws properly when the width and height of the chart changes */}
          <svg key={`${width},${height}`} className="chart" style={styles.chartStyle}>
            {showShadow && renderShadow(scaledPoints, height)}

            {renderLines(scaledPoints)}
            {renderIntersections(scaledPoints)}
          </svg>

          <svg className="chart__overlay" style={styles.overlayStyle}>
            <line x1="50%" y1={0} x2="50%" y2={height} style={styles.verticalLineStyle} />
            <line x1={0} y1={height} x2="100%" y2={height} style={styles.horizontalLineStyle} />
          </svg>

          <img className="arrow" src="/img/icons/arrow_chart.svg" alt="" />
        </div>

        <div>
          {intervals && (
            <div id="chart_descriptions" className="chart__descriptions" style={styles.overlayStyle}>
              {renderDescriptions(scaledAreas)}
            </div>
          )}
        </div>
      </SwipeableViews>

      {intervals && (
        <div id="chart-labels" className="chart__labels" style={styles.overlayStyle}>
          {renderLabels(scaledAreas)}
        </div>
      )}

      <b className="chart-title">{title}</b>

      {showLegend && (
        <>
          <div className="chart-legend__container">{intervals && renderLegend(intervals)}</div>
        </>
      )}

      {showLabel && (
        <>
          <p className="chart-legend__ascm">{t('screens.Dashboard.ascm')}</p>
        </>
      )}
    </div>
  );
}

Chart.defaultProps = {
  intervals: undefined
};

Chart.propTypes = {
  dataPoints: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  intervals: PropTypes.arrayOf(PropTypes.shape()),
  onDataPointSelected: PropTypes.func.isRequired,
  renderConfig: PropTypes.shape().isRequired,
  t: PropTypes.func.isRequired
};

export default withTranslation()(Chart);

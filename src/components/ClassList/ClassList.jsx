import React from 'react';
import PropTypes from 'prop-types';
import { IonList } from '@ionic/react';
import ClassCard from '../cards/ClassCard';
import GymHeader from '../GymHeader';

function ClassList({ gym, classes }) {
  return (
    <>
      <GymHeader gymName={gym.name} />
      <IonList className="classes-list">
        {classes.map(c => (
          <ClassCard key={c.id} classData={c} />
        ))}
      </IonList>
    </>
  );
}

ClassList.propTypes = {
  gym: PropTypes.shape({
    name: PropTypes.string
  }).isRequired,
  classes: PropTypes.arrayOf(PropTypes.any).isRequired
};

export default ClassList;

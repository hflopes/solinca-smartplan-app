import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Image from '../../Image';
import MaterialTabs from '../../tabs/MaterialTabs';
import Avatar from '../../Avatar';
import { getMonth, getDayOfWeek } from '../../../utils/dates';
import { getSelectedGym } from '../../../redux/selectors/gyms';
import { isOffPeak } from '../../../redux/selectors/user';
import './ClassesHeader.scss';

const GymSelector = ({ selectedGymName, isUserOffPeak, isShowingGymList, onToggleGymList }) => {
  if (!selectedGymName) {
    return null;
  }

  const className = classNames({
    'gym-selector__icon': true,
    '-inverted': isShowingGymList
  });

  const onClick = () => {
    if (!isUserOffPeak) {
      onToggleGymList();
    }
  };

  return (
    <button className="gym-selector" type="button" onClick={onClick}>
      <h4 className="gym-selector__name">{selectedGymName}</h4>
      {!isUserOffPeak && <Image className={className} alt="<" src="/img/icons/dropdown_white.svg" />}
    </button>
  );
};

class ClassesHeader extends Component {
  state = {
    dateString: ''
  };

  componentWillMount() {
    const { t } = this.props;

    const currentDate = new Date();

    const dayOfWeek = getDayOfWeek(currentDate.getDay());
    const day = currentDate.getDate();
    const month = getMonth(currentDate.getMonth());

    const dateString = t('screens.Classes.headerDate', { dayOfWeek, day, month });

    this.setState({
      dateString
    });
  }

  render() {
    const { dateString } = this.state;
    const {
      tabs,
      onSelect,
      selectedGymName,
      onToggleGymList,
      isShowingGymList,
      defaultIndex,
      isUserOffPeak
    } = this.props;

    return (
      <div id="classes-header" className="classes-header__container">
        <div id="classes-header-wrapper" className="header__wrapper">
          <div className="header__avatar">
            <Link to="/profile">
              <Avatar />
            </Link>
          </div>
          <div className="header__info">
            <GymSelector
              selectedGymName={selectedGymName}
              isUserOffPeak={isUserOffPeak}
              isShowingGymList={isShowingGymList}
              onToggleGymList={onToggleGymList}
            />
            <p className="header__date">{dateString}</p>
          </div>
        </div>
        <MaterialTabs defaultIndex={defaultIndex} tabs={tabs} onSelect={onSelect} />
      </div>
    );
  }
}

ClassesHeader.propTypes = {
  onSelect: PropTypes.func.isRequired,
  onToggleGymList: PropTypes.func.isRequired,
  isShowingGymList: PropTypes.bool.isRequired,
  tabs: PropTypes.arrayOf(PropTypes.string).isRequired,
  selectedGymName: PropTypes.string.isRequired,
  isUserOffPeak: PropTypes.bool.isRequired,
  defaultIndex: PropTypes.number.isRequired,
  t: PropTypes.func.isRequired
};

GymSelector.propTypes = {
  onToggleGymList: PropTypes.func.isRequired,
  isShowingGymList: PropTypes.bool.isRequired,
  selectedGymName: PropTypes.string.isRequired,
  isUserOffPeak: PropTypes.bool.isRequired
};

const mapStateToProps = state => {
  const selectedGym = getSelectedGym(state);
  const selectedGymName = selectedGym ? selectedGym.name : '';
  const isUserOffPeak = isOffPeak(state);
  return { selectedGymName, isUserOffPeak };
};

export default compose(withTranslation(), connect(mapStateToProps))(ClassesHeader);

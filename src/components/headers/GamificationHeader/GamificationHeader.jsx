import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import SwipeableViews from 'react-swipeable-views';
import MaterialTabs from '../../tabs/MaterialTabs';
import Avatar from '../../Avatar';
import ChallengesHeader from './Challenges';
import HomeHeader from './Home';
import { getUnlockedBadgesCount, getLastestUnlockBadge } from '../../../redux/selectors/badges';
import { getUserPoints } from '../../../redux/selectors/user';
import './GamificationHeader.scss';

// TODO: JP: testa este header quanto o fetchBadges não vier vazio
const GamificationHeader = ({ tabs, onSelect, defaultIndex }) => {
  const badgesUnlocked = useSelector(getUnlockedBadgesCount);
  const latestBadgeUnlocked = useSelector(getLastestUnlockBadge);
  const points = useSelector(getUserPoints);

  return (
    <div id="gamification-header" className="gamification-header__container">
      <div id="notch" />
      <div id="gamification-header-wrapper" className="header__wrapper">
        <div className="header__avatar">
          <Link to="/profile">
            <Avatar />
          </Link>
        </div>
        <div id="header-hide" className="header__info">
          {badgesUnlocked ? (
            <SwipeableViews className="swipe-container" index={defaultIndex} disabled>
              <HomeHeader points={points} />
              <ChallengesHeader latestBadgeUnlocked={latestBadgeUnlocked} />
            </SwipeableViews>
          ) : (
            <HomeHeader points={points} />
          )}
        </div>
      </div>
      <MaterialTabs defaultIndex={defaultIndex} tabs={tabs} onSelect={onSelect} />
    </div>
  );
};

GamificationHeader.propTypes = {
  onSelect: PropTypes.func.isRequired,
  tabs: PropTypes.arrayOf(PropTypes.string).isRequired,
  defaultIndex: PropTypes.number.isRequired
};

export default GamificationHeader;

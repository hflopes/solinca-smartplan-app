import React from 'react';
import { withTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
import _ from 'lodash';
import Badge from '../../../Badge';
import './Challenges.scss';

function Challenges({ t, latestBadgeUnlocked }) {
  if (!latestBadgeUnlocked) {
    return null;
  }

  return (
    <div className="header__challenges-container">
      <div className="header__challenges">
        <div className="header__challenges-hexagons">
          {_.times(2, i => (
            <div key={`hexagon-challenges-${i}`} />
          ))}
          <Badge image={latestBadgeUnlocked.image} size="biggest" />
        </div>
        <p className="header__caption">{t('screens.Gamification.header.challenges.lastBadge')}</p>
      </div>
    </div>
  );
}

Challenges.propTypes = {
  t: PropTypes.func.isRequired,
  latestBadgeUnlocked: PropTypes.shape().isRequired
};

export default withTranslation()(Challenges);

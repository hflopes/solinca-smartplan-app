import React from 'react';
import { withTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
import _ from 'lodash';
import './Home.scss';

function Home({ points, t }) {
  return (
    <div className="header__home-container">
      <div className="header__home">
        <div className="header__home-count">{points}</div>
        <div className="header__home-hexagons">
          {_.times(3, i => (
            <div key={`hexagon-home-${i}`} />
          ))}
        </div>
        <p className="header__caption">{t('screens.Gamification.header.home.points')}</p>
        {!points && <p className="header__empty">{t('screens.Gamification.header.home.empty')}</p>}
      </div>
    </div>
  );
}

Home.defaultProps = {
  points: 0
};

Home.propTypes = {
  t: PropTypes.func.isRequired,
  points: PropTypes.number
};

export default withTranslation()(Home);

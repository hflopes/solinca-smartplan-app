import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { withTranslation } from 'react-i18next';
import Avatar from '../../Avatar';
import { getMonth, getDayOfWeek } from '../../../utils/dates';
import { getBaseGym } from '../../../redux/selectors/gyms';
import './ContactHeader.scss';

function getDateString(t) {
  const currentDate = new Date();

  const dayOfWeek = getDayOfWeek(currentDate.getDay());
  const day = currentDate.getDate();
  const month = getMonth(currentDate.getMonth());

  return t('screens.Contact.headerDate', { dayOfWeek, day, month });
}

function ContactHeader({ t, isLoggedIn }) {
  const baseGym = useSelector(state => getBaseGym(state));
  const date = useMemo(() => getDateString(t), [t]);

  const gymName = baseGym ? baseGym.name : '';

  return (
    <div id="contact-header" className="contact-header">
      <div id="notch" />
      {isLoggedIn && (
        <Link to="/profile">
          <Avatar size={36} />
        </Link>
      )}
      <h1 id="contact-header-title" className="contact-title">
        {t('components.contactHeader')}
      </h1>
      <div id="contact-header-info" className="contact-info">
        <b className="gym-name">{gymName}</b>
        <span className="date">{date}</span>
      </div>
    </div>
  );
}

ContactHeader.propTypes = {
  isLoggedIn: PropTypes.bool.isRequired,
  t: PropTypes.func.isRequired
};

export default withTranslation()(ContactHeader);

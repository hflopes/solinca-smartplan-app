import React from 'react';
import { IonGrid, IonRow } from '@ionic/react';
import Image from '../../Image';
import './AuthHeader.scss';

function AuthHeader() {
  return (
    <div className="auth-header">
      <IonGrid>
        <IonRow>
          <div />
        </IonRow>
        <IonRow>
          <Image className="logo" alt="Solinca" src="/img/logo_new.svg" />
        </IonRow>
      </IonGrid>
    </div>
  );
}

export default AuthHeader;

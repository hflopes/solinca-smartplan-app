import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
// import { Link } from 'react-router-dom';
import { withTranslation } from 'react-i18next';
import { Plugins, CameraResultType } from '@capacitor/core';
import PropTypes from 'prop-types';
// import classNames from 'classnames';
import Image from '../../Image';
import MaterialTabs from '../../tabs/MaterialTabs';
import Avatar from '../../Avatar';
import { getMonth, getDayOfWeek } from '../../../utils/dates';
import { getUserFirstName, getUserPhoto, getUserStatus } from '../../../redux/selectors/user';
import updateUserPhoto from '../../../redux/modules/user/actions/updatePhoto';
import { getBaseGym } from '../../../redux/selectors/gyms';
// import STATUSES from '../../../config/statuses';
import './ProfileHeader.scss';

const { Camera } = Plugins;

// function getStatuses(t) {
//   return STATUSES.map(s => {
//     return {
//       key: s,
//       title: t(`screens.Statuses.${s}`, { returnObjects: true }).title,
//       icon: `/img/big_icons/status_${s}.svg`
//     };
//   });
// }

const ProfilePicture = ({ photo, onPictureClicked }) => {
  const buttonStyle = {
    pointerEvents: photo ? 'none' : 'initial'
  };

  return (
    <div className="profile__picture">
      <div className="picture__line" />
      <button className="picture__avatar" type="button" style={buttonStyle} onClick={onPictureClicked}>
        <Avatar size="large" />
        {!photo && (
          <div className="badge__add">
            <Image alt="Add" src="/img/icons/plus.svg" />
          </div>
        )}
      </button>
    </div>
  );
};

const ProfileInfo = ({ t, status, greeting, name, gymName, date }) => {
  // const statuses = getStatuses(t);
  // const currentIndex = statuses.map(s => s.key).indexOf(status);

  return (
    <div className="profile__info">
      <h1 className="profile__greeting">
        {greeting}
        <b className="profile__greeting-bold">{name}</b>
      </h1>
      <h4 className="profile__gym">{gymName}</h4>
      <p className="profile__date">{date}</p>

      {/* <div className="status-container">
        <b className="status-label">O seu estatuto</b>
        <Link className="status-link" to="/profile/statuses">
          <Image src="/img/icons/dropdown_gray.svg" alt="" />
        </Link>
        <div className="status-list">
          <div className="line" />
          {statuses.map((s, i) => (
            <div className={classNames('status-badge', { '-faded': i > currentIndex })}>
              <img className={`badge-icon -${s.key}`} src={`/img/big_icons/status_${s.key}.svg`} alt="" />
              <span className="badge-label">{s.title}</span>
            </div>
          ))}
        </div>
      </div> */}
    </div>
  );
};

class ProfileHeader extends Component {
  state = {
    dateString: ''
  };

  componentWillMount() {
    const { t } = this.props;

    const currentDate = new Date();

    const dayOfWeek = getDayOfWeek(currentDate.getDay());
    const day = currentDate.getDate();
    const month = getMonth(currentDate.getMonth());

    const dateString = t('screens.Profile.headerDate', { dayOfWeek, day, month });

    this.setState({
      dateString
    });
  }

  takePicture = async () => {
    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: false,
      resultType: CameraResultType.Base64
    });

    const { base64String } = image;
    const { updatePhoto } = this.props;
    updatePhoto({ photo: base64String });
  };

  render() {
    const { t, status, onSelect, tabs, name, photo, baseGymName, defaultIndex } = this.props;
    const { dateString } = this.state;
    const greeting = t('screens.Profile.greeting');

    return (
      <>
        <div id="profile-header" className="profile-header__container">
          <div id="notch" />
          <div id="profile-header-wrapper" className="header__wrapper">
            <ProfilePicture photo={photo} onPictureClicked={this.takePicture} />
            <ProfileInfo
              t={t}
              status={status}
              greeting={greeting}
              name={name}
              gymName={baseGymName}
              date={dateString}
            />
          </div>
          <MaterialTabs tabs={tabs} onSelect={onSelect} defaultIndex={defaultIndex} />
        </div>
      </>
    );
  }
}

ProfileInfo.propTypes = {
  greeting: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  gymName: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired
};

ProfilePicture.propTypes = {
  photo: PropTypes.string.isRequired,
  onPictureClicked: PropTypes.func.isRequired
};

ProfileHeader.defaultProps = {
  photo: undefined,
  defaultIndex: 0
};

ProfileHeader.propTypes = {
  t: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  photo: PropTypes.string,
  updatePhoto: PropTypes.func.isRequired,
  onSelect: PropTypes.func.isRequired,
  tabs: PropTypes.arrayOf(PropTypes.string).isRequired,
  baseGymName: PropTypes.string.isRequired,
  status: PropTypes.string.isRequired,
  defaultIndex: PropTypes.number
};

const mapStateToProps = state => {
  const photo = getUserPhoto(state);
  const status = getUserStatus(state);
  const baseGym = getBaseGym(state);
  const baseGymName = baseGym ? baseGym.name : '';

  return {
    name: getUserFirstName(state),
    photo,
    status,
    baseGymName
  };
};

const mapDispatchToProps = {
  updatePhoto: updateUserPhoto
};

export default compose(withTranslation(), connect(mapStateToProps, mapDispatchToProps))(ProfileHeader);

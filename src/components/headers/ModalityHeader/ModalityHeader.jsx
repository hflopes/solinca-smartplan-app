import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import _ from 'lodash';
import { withTranslation } from 'react-i18next';
import { hexToCSS } from '../../../utils/colors';
import { DEFAULT_MODALITY } from '../../../config/visuals';
import './ModalityHeader.scss';

function overlayGradient({ color1, color2 }) {
  const isValidColors = color1 && color2;

  const firstColor = isValidColors ? color1 : DEFAULT_MODALITY.colorScheme.start;
  const secondColor = isValidColors ? color2 : DEFAULT_MODALITY.colorScheme.end;

  const start = firstColor;
  const middle = hexToCSS(secondColor, 0.8);
  const end = hexToCSS(secondColor, 0);

  return {
    background: `radial-gradient(circle at bottom center, ${start} 0%, ${middle} 50%, ${end} 100%)`
  };
}

function backgroundImage({ photoUrl }) {
  const isValidUrl = photoUrl && photoUrl !== '';

  return {
    backgroundSize: 'cover',
    backgroundImage: `url(${isValidUrl ? photoUrl : DEFAULT_MODALITY.imageUrl})`,
    backgroundPosition: 'left top, center'
  };
}

function ModalityIntensity({ intensity, label }) {
  const dashClass = i => classNames({ intensity__dash: true, on: i < intensity });

  return (
    <>
      {_.times(5, i => (
        <div key={i} className={dashClass(i)} />
      ))}
      <span className="intensity__label">{label}</span>
    </>
  );
}

function ModalityHeader({ modality, gym, instructor, t, paddingBottom }) {
  const { name, intensity, fullName } = modality;

  return (
    <div className="modality__header">
      <div className="modality__banner" style={backgroundImage(modality)}>
        <div className="modality__banner-overlay" style={overlayGradient(modality)} />
      </div>
      <div className="modality__header-wrapper" style={{ paddingBottom }}>
        <div className="gym__header">
          <div className="gym__line" />
          {gym && <b className="gym__name">{gym.name}</b>}
        </div>
        <h1 className="modality__title">{name || fullName.split(' ')[0]}</h1>
        {instructor && (
          <h2 className="modality__instructor">{t('components.modalityCard.instructor', { instructor })}</h2>
        )}
        <div className="modality__intensity">
          <ModalityIntensity
            intensity={intensity}
            label={t(`components.modalityCard.intensity.${intensity}`)}
          />
        </div>
      </div>
    </div>
  );
}

ModalityHeader.defaultProps = {
  modality: {
    intensity: 2
  },
  instructor: '',
  paddingBottom: null
};

ModalityIntensity.propTypes = {
  label: PropTypes.string.isRequired,
  intensity: PropTypes.number.isRequired
};

ModalityHeader.propTypes = {
  gym: PropTypes.shape({
    name: PropTypes.string
  }).isRequired,
  instructor: PropTypes.string,
  modality: PropTypes.shape({
    name: PropTypes.string,
    intensity: PropTypes.number,
    description: PropTypes.string,
    fullName: PropTypes.string,
    color1: PropTypes.string,
    color2: PropTypes.string
  }),
  t: PropTypes.func.isRequired,
  paddingBottom: PropTypes.string
};

export default withTranslation()(ModalityHeader);

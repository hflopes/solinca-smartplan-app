import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import className from 'classnames';
import Image from '../Image';
import { getUserPhoto, getUserStatus } from '../../redux/selectors/user';
import './Avatar.scss';

const DEFAULT_PROFILE_IMG = '/img/icons/menu_profile_gray.svg';

function Avatar({ size }) {
  const photo = useSelector(state => getUserPhoto(state));
  const status = useSelector(state => getUserStatus(state));

  const image = photo && photo.length > 0 ? photo : DEFAULT_PROFILE_IMG;

  return (
    <div className={className('avatar', { [`-${size}`]: true, [`-${status}`]: true })}>
      <div className="avatar-img__container">
        <Image className="avatar-img" src={image} alt="User" />
      </div>

      {status !== 'none' && (
        <Image className="avatar-status" src={`/img/big_icons/status_${status}.svg`} alt="" />
      )}
    </div>
  );
}

Avatar.defaultProps = {
  size: 'medium'
};

Avatar.propTypes = {
  size: PropTypes.string
};

export default Avatar;

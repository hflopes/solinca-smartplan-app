import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import './SwipeButton.scss';

const HANDLE_SIZE = 45;

function SwipeButton({ text, onSwipe }) {
  const screenWidth = useRef(0);
  const hasSwiped = useRef(false);

  function handleTouchEnd() {
    const handle = document.getElementById('handle');
    handle.style.transition = '0.2s';

    setTimeout(() => {
      const endPosition = screenWidth.current * 0.86 - HANDLE_SIZE - 10;
      const snapX = hasSwiped.current ? endPosition : 0;
      handle.style.transform = `translateX(${snapX}px)`;
    }, 100);

    setTimeout(() => {
      handle.style.transition = 'none';
    }, 350);
  }

  function handleTouchMove(e) {
    const xOffset = Math.max(0, e.changedTouches[0].clientX - HANDLE_SIZE);
    const goal = screenWidth.current * 0.86 - HANDLE_SIZE;

    const handle = document.getElementById('handle');
    handle.style.transform = `translateX(${Math.min(xOffset, goal + 10)}px)`;

    if (xOffset >= goal) {
      hasSwiped.current = true;

      setTimeout(() => {
        onSwipe();
      }, 300);
    }
  }

  useEffect(() => {
    screenWidth.current = window.innerWidth;
  }, []);

  return (
    <div className="swipe-button">
      <div id="handle" className="swipe-handle" onTouchEnd={handleTouchEnd} onTouchMove={handleTouchMove}>
        <img src="/img/icons/go_white.svg" alt="" />
      </div>
      {text}
    </div>
  );
}

SwipeButton.propTypes = {
  text: PropTypes.string.isRequired,
  onSwipe: PropTypes.func.isRequired
};

export default SwipeButton;

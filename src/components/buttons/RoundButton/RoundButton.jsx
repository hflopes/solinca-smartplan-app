import React from 'react';
import PropTypes from 'prop-types';
import Image from '../../Image';
import './RoundButton.scss';

function RoundButton({ icon, color, backgroundColor, text, textDisabled, onClick, disabled }) {
  const iconUrl = `/img/icons/${icon}.svg`;
  return (
    <div className="action-btn">
      <button disabled={disabled} type="button" onClick={onClick}>
        <div>
          <Image src={iconUrl} style={{ backgroundColor }} alt="" />
        </div>
        <b style={{ color }}>{disabled ? textDisabled : text}</b>
      </button>
    </div>
  );
}

RoundButton.defaultProps = {
  color: '#776e71',
  backgroundColor: '#776e71',
  textDisabled: '',
  disabled: false
};

RoundButton.propTypes = {
  icon: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  textDisabled: PropTypes.string,
  color: PropTypes.string,
  backgroundColor: PropTypes.string,
  onClick: PropTypes.func.isRequired,
  disabled: PropTypes.bool
};

export default RoundButton;

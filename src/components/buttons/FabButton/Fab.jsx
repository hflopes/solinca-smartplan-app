import React from 'react';
import PropTypes from 'prop-types';
import { IonRippleEffect } from '@ionic/react';
import classNames from 'classnames';
import Image from '../../Image';
import './Fab.scss';

function Fab({ icon, onClick, isDark }) {
  const callback = evt => {
    if (onClick) {
      evt.stopPropagation();
      setTimeout(onClick, 300);
    }
  };

  const className = classNames({
    'fab': true,
    'ion-activatable': true,
    '-dark': isDark
  });

  return (
    <button className={className} type="button" onClick={callback}>
      <Image src={`/img/icons/${icon}.svg`} alt="" />
      <IonRippleEffect />
    </button>
  );
}

Fab.defaultProps = {
  onClick: undefined,
  isDark: false
};

Fab.propTypes = {
  icon: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  isDark: PropTypes.bool
};

export default Fab;

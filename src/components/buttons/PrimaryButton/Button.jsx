import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import classNames from 'classnames';
import { IonRippleEffect } from '@ionic/react';
import Image from '../../Image';
import { getContrastColor } from '../../../utils/colors';
import './Button.scss';

const BASE_ICON_URL = '/img/icons/';
const DEFAULT_GRADIENT = { start: '#f33b30', end: '#c31a40' };

function buildIcon(icon) {
  if (!icon) {
    return null;
  }

  const iconUrl = `${BASE_ICON_URL + icon}.svg`;

  return (
    <div className="btn-action">
      <Image alt="" src={iconUrl} />
    </div>
  );
}

function Button({
  history,
  isLoading,
  isSecondary,
  isDisabled,
  link,
  text,
  icon,
  onClick,
  colorScheme,
  smartContrast,
  type
}) {
  const { start, end } = colorScheme;
  const iconName = isLoading ? 'loading' : icon;
  const gradient = isSecondary ? 'none' : `linear-gradient(90deg, ${start} 0%, ${end} 100%)`;
  const textColor = smartContrast ? getContrastColor(colorScheme) : 'white';

  const className = classNames({
    btn: !isSecondary,
    'disabled-btn': isDisabled || isLoading,
    'loading-btn': isLoading,
    'btn-secondary': isSecondary,
    'ion-activatable': true
  });

  const callback = () => {
    if (onClick) {
      setTimeout(onClick, 200);
    }

    if (link) {
      setTimeout(() => {
        history.push(link);
      }, 200);
    }
  };

  return (
    <button
      type={type}
      onClick={callback}
      disabled={isDisabled || isLoading}
      className={className}
      style={{ background: gradient, color: textColor }}
    >
      {text}
      {buildIcon(iconName)}
      <IonRippleEffect />
    </button>
  );
}

Button.defaultProps = {
  isSecondary: false,
  isDisabled: false,
  isLoading: false,
  icon: undefined,
  onClick: null,
  colorScheme: DEFAULT_GRADIENT,
  smartContrast: false,
  link: undefined,
  type: 'button'
};

Button.propTypes = {
  isSecondary: PropTypes.bool,
  isLoading: PropTypes.bool,
  isDisabled: PropTypes.bool,
  text: PropTypes.string.isRequired,
  icon: PropTypes.string,
  onClick: PropTypes.func,
  link: PropTypes.string,
  colorScheme: PropTypes.shape({
    start: PropTypes.string,
    end: PropTypes.string
  }),
  smartContrast: PropTypes.bool,
  history: PropTypes.shape({
    push: PropTypes.func
  }).isRequired,
  type: PropTypes.string
};

export default withRouter(Button);

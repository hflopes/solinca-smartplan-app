import React, { Component } from 'react';
import PropTypes from 'prop-types';

class VisibleElement extends Component {
  componentDidMount() {
    this.addObserver();
  }

  addObserver = () => {
    this.observer = new IntersectionObserver(this.interSectionCallback, {
      threshold: [1]
    });
    this.observer.observe(this.visible);
  };

  interSectionCallback = entries => {
    entries.forEach(entry => {
      if (entry.intersectionRatio >= 1) {
        const { onVisible } = this.props;
        onVisible();
      }
    });
  };

  render() {
    const { height } = this.props;
    return (
      <div
        ref={i => {
          this.visible = i;
        }}
        style={{ height }}
      />
    );
  }
}

VisibleElement.defaultProps = {
  height: '10vh'
};

VisibleElement.propTypes = {
  height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  onVisible: PropTypes.func.isRequired
};

export default VisibleElement;

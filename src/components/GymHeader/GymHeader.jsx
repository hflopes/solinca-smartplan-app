import React from 'react';
import PropTypes from 'prop-types';
import Image from '../Image';
import './GymHeader.scss';

function GymHeader({ gymName, darkMode }) {
  return (
    <div className={`gym-header ${darkMode ? '-dark-mode' : ''}`}>
      <h3>{gymName}</h3>
      <Image src="/img/icons/location.svg" alt="" />
    </div>
  );
}

GymHeader.defaultProps = {
  darkMode: false
};

GymHeader.propTypes = {
  gymName: PropTypes.string.isRequired,
  darkMode: PropTypes.bool
};

export default GymHeader;

import React, { Component } from 'react';
import { IonAlert } from '@ionic/react';
import { withTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
import { formatSeconds } from '../../utils/dates';
import Fab from '../buttons/FabButton';
import './ExerciseTimer.scss';

function formatTime(elapsedSeconds, duration) {
  const seconds = duration > 0 ? duration - elapsedSeconds : elapsedSeconds;
  return formatSeconds(seconds);
}

// For testing purposes, increase
// this value for faster timers
const TICKS_PER_SECOND = 1;

class ExerciseTimer extends Component {
  state = {
    elapsedSeconds: 0,
    running: false
  };

  componentWillReceiveProps(nextProps) {
    const { show } = this.props;

    if (show !== nextProps.show) {
      if (nextProps.show) {
        this.start();
      } else {
        this.stop();
      }
    }
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  start = () => {
    this.setState(
      {
        running: true,
        elapsedSeconds: 0
      },
      () => {
        this.interval = setInterval(this.tick, 1000 / TICKS_PER_SECOND);
      }
    );
  };

  stop = () => {
    this.setState(
      {
        running: false,
        elapsedSeconds: 0
      },
      () => {
        clearInterval(this.interval);
      }
    );
  };

  tick = () => {
    const { show, duration } = this.props;
    const { elapsedSeconds, running } = this.state;

    if (!running || !show) {
      return;
    }

    const newElapsed = elapsedSeconds + 1;

    // If descending timer hits it's goal duration,
    // auto-complete itself
    if (duration > 0 && newElapsed >= duration) {
      this.setState(
        {
          elapsedSeconds: newElapsed
        },
        this.complete
      );
    } else {
      this.setState({
        elapsedSeconds: newElapsed
      });
    }
  };

  complete = () => {
    const { onComplete } = this.props;
    const { elapsedSeconds } = this.state;

    onComplete(elapsedSeconds);

    this.setState({
      running: false
    });
  };

  dismiss = () => {
    const { onDismiss } = this.props;
    onDismiss();

    this.setState({
      running: false,
      elapsedSeconds: 0,
      confirmDismiss: false
    });
  };

  confirmDismiss = () => {
    this.setState({
      confirmDismiss: true,
      running: false
    });
  };

  resume = () => {
    this.setState({
      running: true
    });
    this.tick();
  };

  render() {
    const { t, show, duration } = this.props;
    const { elapsedSeconds, confirmDismiss } = this.state;

    if (!show) {
      return null;
    }

    return (
      <div
        className="exercise-timer"
        onClick={this.confirmDismiss}
        onKeyDown={() => {}}
        role="button"
        tabIndex="0"
      >
        <div className="exercise-timer-loading" />
        <div className="exercise-timer-digits">
          <h1 id="timer-digits">{formatTime(elapsedSeconds, duration)}</h1>
          <Fab icon="check_white" onClick={this.complete} />
        </div>
        <IonAlert
          isOpen={confirmDismiss}
          onDidDismiss={this.resume}
          header={t('components.exerciseTimer.alert.header')}
          message={t('components.exerciseTimer.alert.message')}
          buttons={[
            {
              text: t('global.actions.cancel'),
              role: 'cancel',
              cssClass: 'secondary',
              handler: () => {
                this.setState({
                  confirmDismiss: false
                });
              }
            },
            {
              text: t('global.actions.confirm'),
              handler: () => {
                this.dismiss();
              }
            }
          ]}
        />
      </div>
    );
  }
}

ExerciseTimer.defaultProps = {
  duration: 0
};

ExerciseTimer.propTypes = {
  duration: PropTypes.number,
  show: PropTypes.bool.isRequired,
  onComplete: PropTypes.func.isRequired,
  onDismiss: PropTypes.func.isRequired,
  t: PropTypes.func.isRequired
};

export default withTranslation()(ExerciseTimer);

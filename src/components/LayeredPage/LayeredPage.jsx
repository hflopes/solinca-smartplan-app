import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import './LayeredPage.scss';

const MIN_DRAG = 80;
const MAX_DRAG = 120;

class LayeredPage extends Component {
  onTouchStart = evt => {
    this.originY = evt.touches[0].clientY;

    const background = document.getElementById('bg');
    background.style.transition = 'none';

    const foreground = document.getElementById('fg');
    foreground.style.transition = 'none';

    const edge = document.getElementById('edge');

    if (edge) {
      edge.style.transition = 'none';
    }
  };

  onTouchMove = evt => {
    const currentY = evt.changedTouches[0].clientY;
    const diff = currentY - this.originY;
    const deltaY = Math.max(MIN_DRAG, Math.min(MAX_DRAG, diff)) - MIN_DRAG;

    const container = document.getElementById('ctr');

    if (deltaY <= 0 || container.scrollTop > 0) {
      return;
    }

    const background = document.getElementById('bg');
    const foreground = document.getElementById('fg');
    const edge = document.getElementById('edge');

    const bgScale = `scale(${1 + deltaY / 200})`;
    background.style.transform = bgScale;

    const fgTranslate = `translate3d(0, ${deltaY}px, 0)`;
    foreground.style.transform = fgTranslate;

    if (edge) {
      const edgeTranslate = `translate3d(0, ${deltaY}px, 0)`;
      edge.style.transform = edgeTranslate;
    }
  };

  onTouchEnd = () => {
    const background = document.getElementById('bg');
    const foreground = document.getElementById('fg');
    const edge = document.getElementById('edge');

    background.style.transition = '0.3s';
    background.style.transform = 'none';

    foreground.style.transition = '0.3s';
    foreground.style.transform = 'none';

    if (edge) {
      edge.style.transition = '0.3s';
      edge.style.transform = 'none';
    }
  };

  render() {
    const { className, rounded, background, foreground, edge, edgeGap, cta, gap, extra } = this.props;

    const containerClass = classNames({
      layered__container: true,
      [className]: true,
      rounded
    });

    const backgroundStyle = {
      maxHeight: `calc(${gap} + ${extra})`,
      height: `calc(${gap} + ${extra})`
    };

    const foregroundStyle = {
      marginTop: `calc(${gap} - ${rounded ? '0px' : '-28px'})`
    };

    const edgeStyle = {
      transform: `translate3d(0, calc(-${edgeGap} - ${rounded ? 28 : 0}px), 0)`
    };

    return (
      <div
        id="ctr"
        className={containerClass}
        onTouchStart={this.onTouchStart}
        onTouchMove={this.onTouchMove}
        onTouchEnd={this.onTouchEnd}
      >
        <div id="bg" className="layered__background" style={backgroundStyle}>
          {background}
        </div>
        <div id="fg" className="layered__foreground" style={foregroundStyle}>
          <div className="foreground__container">{foreground}</div>
          <div className="edge__container" style={edgeStyle}>
            {edge}
          </div>
        </div>
        {cta && (
          <div id="cta" className="layered__cta">
            {cta}
          </div>
        )}
      </div>
    );
  }
}

LayeredPage.defaultProps = {
  cta: undefined,
  edge: undefined,
  edgeGap: undefined,
  extra: '0'
};

LayeredPage.propTypes = {
  className: PropTypes.string.isRequired,
  rounded: PropTypes.bool.isRequired,
  background: PropTypes.node.isRequired,
  foreground: PropTypes.node.isRequired,
  cta: PropTypes.node,
  edge: PropTypes.node,
  gap: PropTypes.string.isRequired,
  extra: PropTypes.string,
  edgeGap: PropTypes.string
};

export default LayeredPage;

import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import Tab from '@material/react-tab';
import TabBar from '@material/react-tab-bar';
import './BadgeTabs.scss';

function initialScroll(index) {
  if (!index) return;

  const badgeTab = document.querySelector(`#badge-tab-${index}`);
  if (badgeTab) {
    const leftScroll = document.querySelector(`#badge-tab-${index}`).offsetLeft;
    if (leftScroll) {
      document.querySelector('.badge-tabs .mdc-tab-scroller div').scrollLeft = leftScroll;
    } else {
      setTimeout(initialScroll, 500);
    }
  } else {
    setTimeout(initialScroll, 500);
  }
}

function BadgeTabs({ tabs, selectedIndex, onSelected }) {
  useEffect(() => initialScroll(selectedIndex), [selectedIndex]);

  return (
    <div className="badge-tabs">
      <TabBar className="badge-tabs" activeIndex={selectedIndex} handleActiveIndexUpdate={onSelected}>
        {tabs.map((tab, index) => (
          <Tab id={`badge-tab-${index}`} key={`${tab}-${index}`}>
            {tab}
          </Tab>
        ))}
      </TabBar>
    </div>
  );
}

BadgeTabs.propTypes = {
  tabs: PropTypes.arrayOf(PropTypes.string).isRequired,
  selectedIndex: PropTypes.number.isRequired,
  onSelected: PropTypes.func.isRequired
};

export default BadgeTabs;

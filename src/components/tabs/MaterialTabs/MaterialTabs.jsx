import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Tab from '@material/react-tab';
import TabBar from '@material/react-tab-bar';
import './MaterialTabs.scss';

class MaterialTabs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeIndex: props.defaultIndex || 0
    };
  }

  selectIndex = index => {
    this.setState({ activeIndex: index });

    const { onSelect } = this.props;
    onSelect(index);
  };

  render() {
    const { tabs } = this.props;
    const { activeIndex } = this.state;

    return (
      <TabBar activeIndex={activeIndex} handleActiveIndexUpdate={this.selectIndex} className="material-tabs">
        {tabs.map(tab => (
          <Tab key={tab}>{tab}</Tab>
        ))}
      </TabBar>
    );
  }
}

MaterialTabs.propTypes = {
  tabs: PropTypes.arrayOf(PropTypes.string).isRequired,
  defaultIndex: PropTypes.number.isRequired,
  onSelect: PropTypes.func.isRequired
};

export default MaterialTabs;

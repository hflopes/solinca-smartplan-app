import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import _ from 'lodash';
import PropTypes from 'prop-types';
import Checkbox from '../Checkbox';
import { getContactTOS } from '../../redux/selectors/terms';
import './PrivacyForm.scss';

export class PrivacyForm extends Component {
  constructor(props) {
    super(props);

    const { tosData } = props;

    if (!tosData) {
      this.state = {};
      return;
    }

    const { authorizeText } = tosData;

    if (!authorizeText || !authorizeText.length) {
      this.state = {};
      return;
    }

    this.state = {
      checkedItems: authorizeText.map(() => false)
    };
  }

  onCheckToggle = index => {
    const { onChanged } = this.props;
    const { checkedItems } = this.state;

    checkedItems[index] = !checkedItems[index];

    const isAllAccepted = checkedItems.length === _.compact(checkedItems).length;

    this.setState({ checkedItems }, () => {
      onChanged(isAllAccepted);
    });
  };

  render() {
    const { checkedItems } = this.state;
    const { tosData, onVisitLink } = this.props;

    if (!tosData || !checkedItems) {
      return null;
    }

    const { authorizeText, title, consentText, files } = tosData;

    return (
      <div className="privacy-form__container">
        <h3 className="tos__title">{title}</h3>
        <p className="tos__text">{consentText}</p>
        <ul className="tos__files">
          {files.map(file => (
            <Link
              onClick={onVisitLink}
              key={file.url}
              to={{
                pathname: '/terms',
                state: { ...file }
              }}
            >
              <li className="tos__file-item">{file.title}</li>
            </Link>
          ))}
        </ul>

        {authorizeText.map((text, index) => (
          <Checkbox
            key={text}
            onToggle={() => this.onCheckToggle(index)}
            checked={checkedItems.length ? checkedItems[index] : false}
            label={text}
          />
        ))}
      </div>
    );
  }
}

PrivacyForm.propTypes = {
  tosData: PropTypes.shape({
    title: PropTypes.string,
    consentText: PropTypes.string,
    authorizeText: PropTypes.arrayOf(PropTypes.string),
    files: PropTypes.arrayOf(
      PropTypes.shape({
        title: PropTypes.string,
        url: PropTypes.string
      })
    )
  }).isRequired,
  onChanged: PropTypes.func.isRequired,
  onVisitLink: PropTypes.func.isRequired
};

const mapStateToProps = state => {
  const tosData = getContactTOS(state);
  return { tosData };
};

export default connect(mapStateToProps)(PrivacyForm);

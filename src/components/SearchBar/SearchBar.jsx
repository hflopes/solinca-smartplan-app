import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withRouter } from 'react-router';
import { withTranslation } from 'react-i18next';
import clearResultsAction from '../../redux/modules/search/actions/clear';
import Image from '../Image';
import './SearchBar.scss';

const SearchBar = ({ value, placeholder, onClear, onChange, history, darkMode, clearResults, t }) => (
  <div className="search-bar-container">
    <div className="search-bar">
      <Image className="search-bar__image" src="/img/icons/search_gray.svg" alt="" />
      <input
        value={value}
        className="search-bar__input"
        placeholder={placeholder}
        onChange={onChange}
        type="text"
      />
      {value && value.length > 0 && (
        <button className="search-bar__button" type="button" onClick={onClear}>
          <Image src="/img/icons/clear.svg" alt="" />
        </button>
      )}
    </div>
    <button
      type="button"
      onClick={() => {
        clearResults();
        history.goBack();
      }}
      className={`full btn-text ${darkMode ? 'dark' : ''}`}
    >
      {t('screens.Search.cancel')}
    </button>
  </div>
);

SearchBar.propTypes = {
  t: PropTypes.func.isRequired,
  clearResults: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  darkMode: PropTypes.bool.isRequired,
  onClear: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  history: PropTypes.shape({
    goBack: PropTypes.func
  }).isRequired
};

const mapDispatchToProps = {
  clearResults: clearResultsAction
};

export default compose(withTranslation(), withRouter, connect(null, mapDispatchToProps))(SearchBar);

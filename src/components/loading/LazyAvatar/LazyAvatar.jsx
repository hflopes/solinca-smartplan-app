import React from 'react';
import PropTypes from 'prop-types';
import { RoundShape } from 'react-placeholder/lib/placeholders';

import 'react-placeholder/lib/reactPlaceholder.css';

function LazyAvatar({ size }) {
  return (
    <div className="lazy-avatar" style={{ width: size, height: size }}>
      <RoundShape color="#E0E0E0" showLoadingAnimation />
    </div>
  );
}

LazyAvatar.propTypes = {
  size: PropTypes.number.isRequired
};

export default LazyAvatar;

import React from 'react';
import { TextRow, RoundShape } from 'react-placeholder/lib/placeholders';
import { IonItemSliding, IonItem } from '@ionic/react';

import 'react-placeholder/lib/reactPlaceholder.css';
import './LazyClassCard.scss';

const LazyClassCard = () => (
  <div className="class-card">
    <IonItemSliding>
      <IonItem>
        <button type="button" className="class-card__foreground">
          <div className="foreground__left">
            <div className="left__dot">
              <RoundShape className="show-loading-animation loading-available" color="#E0E0E0" />
            </div>
            <span className="left__time">
              <TextRow className="show-loading-animation loading-time" color="#E0E0E0" />
            </span>
            <div className="left__chip" />
          </div>
          <div className="foreground__middle">
            <div className="middle__separator -left" />
            <h2 className="middle__title">
              <TextRow className="show-loading-animation loading-name" color="#E0E0E0" />
            </h2>
            <div className="middle__separator -right" />
          </div>
          <div className="foreground__right">
            <span className="right__studio">
              <TextRow className="show-loading-animation loading-space" color="#E0E0E0" />
            </span>
            <span className="right__duration">
              <TextRow className="show-loading-animation loading-duration" color="#E0E0E0" />
            </span>
          </div>
        </button>
      </IonItem>
    </IonItemSliding>
  </div>
);

export default LazyClassCard;

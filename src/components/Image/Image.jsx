import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ImageCache } from 'capacitor-image-cache';
import { connect } from 'react-redux';
import { getPlatform } from '../../redux/selectors/app';

class Image extends Component {
  constructor(props) {
    super(props);
    this.state = {
      remote: props.src.startsWith('http'),
      gif: props.src.endsWith('gif'),
      file: ''
    };
  }

  componentDidMount() {
    const { remote, gif } = this.state;
    if (remote && !gif) {
      this.fetchImage();
    }
  }

  fetchImage = async () => {
    const { src } = this.props;
    const cache = new ImageCache();
    const { value } = await cache.get({ src });
    this.setState({ file: value });
  };

  render() {
    const { src, alt, style, className, platform } = this.props;
    const { remote, file, gif } = this.state;

    if (!remote || gif || platform === 'ios') {
      return <img alt={alt} src={src} style={style} className={className} />;
    }

    return <img alt={alt} src={file} style={style} className={className} />;
  }
}

Image.defaultProps = {
  platform: '',
  alt: '',
  className: '',
  style: {}
};

Image.propTypes = {
  platform: PropTypes.string,
  alt: PropTypes.string,
  className: PropTypes.string,
  src: PropTypes.string.isRequired,
  style: PropTypes.shape({})
};

const mapStateToProps = state => {
  const platform = getPlatform(state);
  return { platform };
};

export default connect(mapStateToProps)(Image);

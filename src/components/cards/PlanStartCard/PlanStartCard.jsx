import React from 'react';
import { Link } from 'react-router-dom';
import './PlanStartCard.scss';

function PlanStartCard() {
  return (
    <Link to="/plan" className="plan-start-card">
      <div className="overlay">
        <img src="/img/big_icons/foot.svg" alt="" />
        <h1>Começar plano de treino</h1>
      </div>
    </Link>
  );
}

export default PlanStartCard;

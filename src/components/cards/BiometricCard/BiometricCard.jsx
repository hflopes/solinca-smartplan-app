import React from 'react';
import PropTypes from 'prop-types';
import './BiometricCard.scss';

function BiometricCard({ label, valuePrimary, valueSecondary }) {
  return (
    <div className="biometric-card">
      <div className="biometric-card__top">
        <div className="top-container">
          <b className="top-primary">{valuePrimary}</b>
          <span className="top-secondary">{valueSecondary}</span>
        </div>
      </div>
      <div className="biometric-card__bottom">
        <b className="bottom-label">{label}</b>
      </div>
    </div>
  );
}

BiometricCard.propTypes = {
  label: PropTypes.string.isRequired,
  valuePrimary: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  valueSecondary: PropTypes.string.isRequired
};

export default BiometricCard;

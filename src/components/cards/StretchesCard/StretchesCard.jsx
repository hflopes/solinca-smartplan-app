import React from 'react';
import PropTypes from 'prop-types';
import { IonGrid, IonRow } from '@ionic/react';
import './StretchesCard.scss';
import Image from '../../Image';

function StretchesCard({ exercise }) {
  const { name, instructions, complete } = exercise;

  return (
    <div className="stretches-card">
      <IonGrid>
        <IonRow className="stretches-header">
          <Image className="icon" src="/img/big_icons/exercise.svg" alt="" />
          <h1>{name}</h1>
          {complete && (
            <div className="complete-check">
              <Image src="/img/icons/check_pink.svg" alt="" />
            </div>
          )}
        </IonRow>
        <IonRow className="stretches-info">
          <p>{instructions}</p>
        </IonRow>
      </IonGrid>
    </div>
  );
}

StretchesCard.propTypes = {
  exercise: PropTypes.shape({
    name: PropTypes.string,
    instructions: PropTypes.string,
    complete: PropTypes.bool
  }).isRequired
};

export default StretchesCard;

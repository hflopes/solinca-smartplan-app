import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import moment from 'moment';
import readComplaint from '../../../redux/modules/complaints/actions/read';
import './NewComplaintCard.scss';

function NewComplaintCard({ id, date, message, replies }) {
  const dispatch = useDispatch();
  const text = replies.length && replies.length > 0 ? replies[replies.length - 1].message : message;

  return (
    <div className="complaint-card-row">
      <Link to={`/profile/complaints/${id}`}>
        <div className="new-complaint-card">
          {/* <span className="top">Reclamação</span> */}
          <div className="middle">
            <div className="dot" />
            <b className="title">Reclamação</b>
            <span className="date">{moment(date).format('DD/MM/YY')}</span>
          </div>
          <p className="bottom">{text}</p>
        </div>
      </Link>
      <button
        type="button"
        onClick={() => {
          dispatch(readComplaint(id));
        }}
        className="close-btn"
      >
        <img src="/img/icons/clear.svg" alt="" />
      </button>
    </div>
  );
}

NewComplaintCard.defaultProps = {
  message: '',
  replies: [],
  date: new Date()
};

NewComplaintCard.propTypes = {
  id: PropTypes.string.isRequired,
  message: PropTypes.string,
  replies: PropTypes.arrayOf(
    PropTypes.shape({
      message: PropTypes.string
    })
  ),
  date: PropTypes.instanceOf(Date)
};

export default NewComplaintCard;

import React from 'react';
import PropTypes from 'prop-types';
import { IonItemOptions, IonItemOption } from '@ionic/react';
import classNames from 'classnames';
import { withTranslation } from 'react-i18next';
import Image from '../../../../Image';
import './SideButton.scss';

const BUTTON_TYPES = {
  favorite: {
    icon: '/img/icons/heart_filled_white.svg',
    labelKey: 'global.actions.add',
    side: 'start'
  },
  unfavorite: {
    icon: '/img/icons/heart_filled.svg',
    labelKey: 'global.actions.remove',
    side: 'start'
  },
  book: {
    icon: '/img/icons/check_white.svg',
    labelKey: 'global.actions.book',
    side: 'end'
  },
  unavailable: {
    icon: '/img/icons/clear_white.svg',
    labelKey: 'global.actions.unavailable',
    side: 'end'
  },
  cancel: {
    icon: '/img/icons/clear_white.svg',
    labelKey: 'global.actions.cancel',
    side: 'end'
  },
  counter: {
    icon: '/img/logo_small_color.svg',
    labelKey: 'global.actions.kiosk',
    side: 'end'
  }
};

function SideButton({ t, type, onClick, isCounterOnly }) {
  const buttonType = isCounterOnly ? 'counter' : type;
  const { side, icon, labelKey } = BUTTON_TYPES[buttonType];

  const className = classNames({
    'side-button__container': true,
    [`-${buttonType}`]: true
  });

  const onButtonClicked = e => {
    if (isCounterOnly) {
      return;
    }

    onClick(e);
  };

  return (
    <IonItemOptions className={className} side={side}>
      <IonItemOption className="side-button__option" onClick={onButtonClicked}>
        <button className="side-button" type="button">
          <Image className="side-button__icon" src={icon} alt="" />
          <b className="side-button__label">{t(labelKey)}</b>
        </button>
      </IonItemOption>
    </IonItemOptions>
  );
}

SideButton.defaultProps = {
  isCounterOnly: false
};

SideButton.propTypes = {
  t: PropTypes.func.isRequired,
  onClick: PropTypes.func.isRequired,
  type: PropTypes.string.isRequired,
  isCounterOnly: PropTypes.bool
};

export default withTranslation()(SideButton);

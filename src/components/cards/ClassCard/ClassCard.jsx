import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import moment from 'moment';
import classNames from 'classnames';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';
import { IonItemSliding, IonItem } from '@ionic/react';
import Image from '../../Image';
import { getTime } from '../../../utils/dates';
import { hasFavoritedClass } from '../../../redux/selectors/classes';
import bookAction from '../../../redux/modules/classes/actions/book';
import cancelAction from '../../../redux/modules/classes/actions/cancel';
import toggleFavoriteAction from '../../../redux/modules/classes/actions/toggleFavorite';
import { isPremium } from '../../../redux/selectors/user';
import SideButton from './components/SideButton';
import { REGULAR_BOOKING_GAP, PREMIUM_BOOKING_GAP } from '../../../config/classes';
import './ClassCard.scss';

function ClassCardIcon({ isFavorite, hasFavorited }) {
  if (!isFavorite && !hasFavorited) {
    return null;
  }

  return <Image className="class-card-icon" src="/img/icons/'heart_filled.svg'" alt="" />;
}

function StatusDot({ isAvailable, isPremiumLocked }) {
  const className = classNames({
    'status-dot': true,
    '-premium': isPremiumLocked,
    '-available': !isPremiumLocked && isAvailable,
    '-unavailable': !isPremiumLocked && !isAvailable
  });

  return <div className={className} />;
}

function StatusChip({ isBooked, isCounterOnly }) {
  if (!isBooked && !isCounterOnly) {
    return null;
  }

  const className = classNames({
    'status-chip': true,
    '-booked': isBooked,
    '-counter': !isBooked && isCounterOnly
  });

  return <div className={className}>{isBooked ? 'marcada' : 'quiosque'}</div>;
}

class ClassCard extends PureComponent {
  closeSlider = () => {
    this.slider.close();
  };

  bookClass = () => {
    const { book, classData } = this.props;
    book(classData);

    this.closeSlider();
  };

  cancelClass = () => {
    const { cancel, classData } = this.props;
    cancel(classData);

    this.closeSlider();
  };

  toggleFavorite = () => {
    const { toggleFavorite, classData } = this.props;
    toggleFavorite(classData);

    this.closeSlider();
  };

  addRef = i => {
    this.slider = i;
  };

  render() {
    const { classData, hasFavorited, isUserPremium, t, showDate } = this.props;
    const {
      id,
      modality,
      date,
      studio,
      duration,
      isAvailable,
      isBooked,
      isFavorite,
      fullName,
      instructor,
      maxMinutesAllowedToCancelClass
    } = classData;

    const leftType = isFavorite || hasFavorited ? 'unfavorite' : 'favorite';
    const leftOnClick = this.toggleFavorite;

    let rightType = isBooked ? 'cancel' : 'book';
    const rightOnClick = isBooked ? this.cancelClass : this.bookClass;

    // If the class starts in less than maxMinutesAllowedToCancelClass minutes,
    // it can only be booked in the gym's counter
    const timeLeft = moment(date).diff(moment(), 'minutes');
    const dateFormat = moment(date).format('DD/MM');
    const isCounterOnly = timeLeft < maxMinutesAllowedToCancelClass;
    const isPremiumLocked =
      !isUserPremium && timeLeft < PREMIUM_BOOKING_GAP * 60 && timeLeft > REGULAR_BOOKING_GAP * 60;

    if (!isAvailable && !isPremiumLocked && !isBooked) {
      rightType = 'unavailable';
    }

    const dateTime = getTime(date);
    const fixedName = modality ? modality.name : fullName.split(' ')[0];

    return (
      <div className="class-card">
        <IonItemSliding ref={this.addRef}>
          <SideButton type={leftType} onClick={leftOnClick} />
          <IonItem>
            <Link to={`/classes/${id}`}>
              <button type="button" className="class-card__foreground">
                <div className="foreground__left">
                  {showDate ? (
                    <h2 className="card__date">{dateFormat}</h2>
                  ) : (
                    <div className="left__dot">
                      <StatusDot isAvailable={isAvailable} isPremiumLocked={isPremiumLocked} />
                    </div>
                  )}
                  <span className="left__time">{dateTime}</span>
                  <div className="left__chip">
                    <StatusChip isBooked={isBooked} isCounterOnly={isCounterOnly} />
                  </div>
                </div>
                <div className="foreground__middle">
                  <ClassCardIcon isFavorite={isFavorite} hasFavorited={hasFavorited} />
                  <div className="middle__separator -left" />
                  <div className="middle__content">
                    <h2 className="middle__title">{fixedName}</h2>
                    {instructor && (
                      <h2 className="middle__instructor">
                        {t('components.modalityCard.instructor', { instructor })}
                      </h2>
                    )}
                  </div>
                  <div className="middle__separator -right" />
                </div>
                <div className="foreground__right">
                  <span className="right__studio">{studio}</span>
                  <span className="right__duration">{`${duration} min`}</span>
                </div>
              </button>
            </Link>
          </IonItem>
          <SideButton type={rightType} onClick={rightOnClick} isCounterOnly={isCounterOnly} />
        </IonItemSliding>
      </div>
    );
  }
}

StatusChip.propTypes = {
  isBooked: PropTypes.bool.isRequired,
  isCounterOnly: PropTypes.bool.isRequired
};

StatusDot.propTypes = {
  isAvailable: PropTypes.bool.isRequired,
  isPremiumLocked: PropTypes.bool.isRequired
};

ClassCardIcon.propTypes = {
  isFavorite: PropTypes.bool.isRequired,
  hasFavorited: PropTypes.bool.isRequired
};

ClassCard.defaultProps = {
  showDate: false
};

ClassCard.propTypes = {
  classData: PropTypes.shape({
    id: PropTypes.string,
    modality: PropTypes.objectOf(PropTypes.any),
    date: PropTypes.instanceOf(Date),
    duration: PropTypes.number,
    studio: PropTypes.string,
    isAvailable: PropTypes.bool,
    isBooked: PropTypes.bool,
    isFavorite: PropTypes.bool,
    fullName: PropTypes.string,
    instructor: PropTypes.string,
    maxMinutesAllowedToCancelClass: PropTypes.number
  }).isRequired,
  t: PropTypes.func.isRequired,
  showDate: PropTypes.bool,
  isUserPremium: PropTypes.bool.isRequired,
  toggleFavorite: PropTypes.func.isRequired,
  hasFavorited: PropTypes.bool.isRequired,
  book: PropTypes.func.isRequired,
  cancel: PropTypes.func.isRequired
};

const mapDispatchToProps = {
  toggleFavorite: toggleFavoriteAction,
  book: bookAction,
  cancel: cancelAction
};

const mapStateToProps = (state, ownProps) => {
  const { classData } = ownProps;
  const { id } = classData;
  const hasFavorited = hasFavoritedClass(state, id);
  const isUserPremium = isPremium(state);
  return { hasFavorited, isUserPremium };
};

export default compose(withTranslation(), connect(mapStateToProps, mapDispatchToProps))(ClassCard);

import React from 'react';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { getMonth } from '../../../utils/dates';
import './ChallengeHighlightedCard.scss';

function getDuration(dateBegin, dateEnd) {
  const pad = num => (num < 10 ? `0${num}` : num);

  const startDate = dateBegin.getDate();
  const startMonth = dateBegin.getMonth();
  const endDate = dateEnd.getDate();
  const endMonth = dateEnd.getMonth();

  const start = `${pad(startDate)} ${getMonth(startMonth).slice(0, 3)}.`;
  const end = `${pad(endDate)} ${getMonth(endMonth).slice(0, 3)}.`;

  return `${start} - ${end}`;
}

function ChallengeHighlightedCard({ challenge, t }) {
  const { id, name, description, group, isNew } = challenge;
  const challengeName = name || description;
  const challengeImage = group && group.url_image_nobackground;

  const duration = getDuration(new Date(), new Date());

  const overlayBg = {
    backgroundImage: `linear-gradient(rgba(142, 132, 137, 0.5) 0%, rgba(142, 132, 137, 0.5) 100%), url("${challengeImage}")`,
    backgroundSize: 'cover',
    backgroundPosition: 'center'
  };

  return (
    <Link className="remove-link" key={id} to={`/gamification/challenge/${id}`}>
      <div className="challenge-highlighted-card">
        <div className="card-overlay" style={overlayBg} />
        <div className="card__top">
          {isNew && <div className="new">{t('components.ChallengeCard.new')}</div>}
        </div>
        <div className="card__middle">
          <h1 className="card-title">{challengeName}</h1>
        </div>
        <div className="card__bottom">
          <span className="card-duration">{duration}</span>
        </div>
      </div>
    </Link>
  );
}

ChallengeHighlightedCard.propTypes = {
  challenge: PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    description: PropTypes.string,
    isNew: PropTypes.bool,
    progress: PropTypes.number,
    dateBegin: PropTypes.instanceOf(Date),
    datePublish: PropTypes.instanceOf(Date),
    dateEnd: PropTypes.instanceOf(Date),
    group: PropTypes.shape({
      url_image_nobackground: PropTypes.string
    })
  }).isRequired,
  t: PropTypes.func.isRequired
};

export default withTranslation()(ChallengeHighlightedCard);

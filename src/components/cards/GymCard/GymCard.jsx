import React from 'react';
import PropTypes from 'prop-types';
import { IonGrid, IonRow } from '@ionic/react';
import Image from '../../Image';
import './GymCard.scss';

const formatPhoneNumber = phone => {
  const formatNumber = num => {
    return num.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ');
  };

  if (phone.includes(')')) {
    const split = phone.split(')');
    const extention = `${split[0]})`;
    const number = split[1];

    const formatted = `${extention} ${formatNumber(number)}`;

    return formatted;
  }

  return formatNumber(phone);
};

function GymCard({ gym, highlighted }) {
  const { name, phone, email } = gym;

  return (
    <div className={`gym-card ${highlighted ? 'highlighted' : ''}`}>
      <IonGrid>
        <IonRow>
          <Image src="/img/logo_small.svg" alt="" />
          <h1>{name}</h1>
        </IonRow>
        <IonRow>
          <span>{formatPhoneNumber(phone)}</span>
          <span>{email}</span>
        </IonRow>
      </IonGrid>
    </div>
  );
}

GymCard.propTypes = {
  gym: PropTypes.shape({
    name: PropTypes.string,
    phone: PropTypes.string,
    email: PropTypes.string
  }).isRequired,
  highlighted: PropTypes.bool.isRequired
};

export default GymCard;

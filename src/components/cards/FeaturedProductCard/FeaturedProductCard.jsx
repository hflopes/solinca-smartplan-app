import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { getMonth } from '../../../utils/dates';
import './FeaturedProductCard.scss';

function getDuration(dateBegin, dateEnd) {
  const pad = num => (num < 10 ? `0${num}` : num);

  const startDate = dateBegin.getDate();
  const startMonth = dateBegin.getMonth();
  const endDate = dateEnd.getDate();
  const endMonth = dateEnd.getMonth();

  const start = `${pad(startDate)} ${getMonth(startMonth).slice(0, 3)}.`;
  const end = `${pad(endDate)} ${getMonth(endMonth).slice(0, 3)}.`;

  return `${start} - ${end}`;
}

function FeaturedProductCard({ product }) {
  const { t } = useTranslation();

  const { id, name, points, imageURL, isNew, dateHourStart, dateHourEnd } = product;
  const duration = getDuration(dateHourStart, dateHourEnd);

  const overlayBg = {
    backgroundImage: `linear-gradient(rgba(142, 132, 137, 0.5) 0%, rgba(142, 132, 137, 0.5) 100%), url("${imageURL}")`,
    backgroundSize: 'cover',
    backgroundPosition: 'center'
  };

  return (
    <Link className="remove-link" key={id} to={`/gamification/reward/${id}`}>
      <div className="product-featured-card">
        <div className="card-overlay" style={overlayBg} />
        <div className="card__top">
          {isNew && <div className="new">{t('components.ChallengeCard.new')}</div>}
        </div>
        <div className="card__middle">
          <h1 className="card-title">{name}</h1>
          <b className="card-pts">{`${points} pts`}</b>
        </div>
        <div className="card__bottom">
          <span className="card-duration">{duration}</span>
        </div>
      </div>
    </Link>
  );
}

FeaturedProductCard.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    name: PropTypes.string,
    imageURL: PropTypes.string,
    isNew: PropTypes.bool,
    points: PropTypes.number,
    dateHourStart: PropTypes.instanceOf(Date),
    dateHourEnd: PropTypes.instanceOf(Date)
  }).isRequired
};

export default FeaturedProductCard;

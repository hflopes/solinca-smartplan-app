import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import Image from '../../Image';
import { getMonth } from '../../../utils/dates';
import './ProductCard.scss';

function getDuration(dateBegin, dateEnd) {
  const pad = num => (num < 10 ? `0${num}` : num);

  const startDate = dateBegin.getDate();
  const startMonth = dateBegin.getMonth();
  const endDate = dateEnd.getDate();
  const endMonth = dateEnd.getMonth();

  const start = `${pad(startDate)} ${getMonth(startMonth).slice(0, 3)}.`;
  const end = `${pad(endDate)} ${getMonth(endMonth).slice(0, 3)}.`;

  return `${start} - ${end}`;
}

function ProductCard({ product }) {
  const { t } = useTranslation();

  const { id, name, points, imageURL, isNew, dateHourStart, dateHourEnd } = product;
  const duration = getDuration(dateHourStart, dateHourEnd);

  return (
    <Link className="remove-link" key={id} to={`/gamification/reward/${id}`}>
      <div className="product-card">
        {isNew && (
          <span className="product-card__label">
            <p>{t('components.ChallengeCard.new')}</p>
          </span>
        )}
        <div className="product-card__top">
          <Image className="product-card__image" src={imageURL} alt="" />
        </div>
        <div className="product-card__bottom">
          <h3 className="product-card__title">{name}</h3>
          <b className="product-card__pts">{`${points} pts`}</b>
          <p className="product-card__caption">{duration}</p>
        </div>
      </div>
    </Link>
  );
}

ProductCard.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    name: PropTypes.string,
    imageURL: PropTypes.string,
    isNew: PropTypes.bool,
    points: PropTypes.number,
    dateHourStart: PropTypes.instanceOf(Date),
    dateHourEnd: PropTypes.instanceOf(Date)
  }).isRequired
};

export default ProductCard;

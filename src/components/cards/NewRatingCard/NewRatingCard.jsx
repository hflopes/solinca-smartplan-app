import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import _ from 'lodash';
import { useTranslation } from 'react-i18next';
import './NewRatingCard.scss';
import Image from '../../Image';
import reviewClass, {
  closeReviewClass,
  viewReviewClass
} from '../../../redux/modules/classes/actions/review';
import { isReviewingClasses } from '../../../redux/selectors/classes';

function NewRatingCard({ id, day, hour, name, classData }) {
  const [rating, setRating] = useState(0);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(viewReviewClass(id));
  }, [dispatch, id]);
  const { t } = useTranslation();
  const isReviewing = useSelector(state => isReviewingClasses(state));

  const disabled = rating === 0 || isReviewing;

  return (
    <div className="new-rating-card">
      <img className="icon" src="/img/big_icons/exercise.svg" alt="" />
      <div className="content">
        <p className="text">
          {t('screens.Review.title')}
          <b>{`${day} ${hour} | ${name}`}</b>
        </p>
        <div className="rating">
          {_.times(5, i => (
            <button
              key={i}
              type="button"
              onClick={() => {
                setRating(i + 1);
              }}
              className={classNames('star', disabled && 'disabled')}
            >
              <Image src={`/img/icons/star_${i + 1 <= rating ? 'filled' : 'outline'}.svg`} alt="" />
            </button>
          ))}
        </div>
        <button
          type="button"
          disabled={disabled}
          className={classNames('underline-btn', disabled && 'disabled')}
          onClick={() => {
            dispatch(reviewClass(classData, rating));
          }}
        >
          {t('global.actions.confirm')}
        </button>
      </div>
      <button
        type="button"
        onClick={() => {
          dispatch(closeReviewClass(id));
        }}
      >
        <img className="close-btn" src="/img/icons/clear.svg" alt="" />
      </button>
    </div>
  );
}

NewRatingCard.propTypes = {
  hour: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  day: PropTypes.string.isRequired,
  classData: PropTypes.shape({}).isRequired
};

export default NewRatingCard;

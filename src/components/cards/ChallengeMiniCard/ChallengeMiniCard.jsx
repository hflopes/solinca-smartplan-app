import React from 'react';
import PropTypes from 'prop-types';
import { motion, AnimatePresence } from 'framer-motion';
import Image from '../../Image';
import Badge from '../../Badge';
import './ChallengeMiniCard.scss';

function ChallengeMiniCard({ challenge, onClose }) {
  const { id, name, description } = challenge;
  const challengeName = name || description;

  return (
    <AnimatePresence exitBeforeEnter>
      <motion.div
        initial={{ opacity: 0, scale: 0 }}
        animate={{ opacity: 1, scale: 1 }}
        exit={{ opacity: 0, scale: 0.75 }}
        key={id}
      >
        {id && (
          <div className="challenge-mini-card">
            <Badge size="small" image={null} />
            <div className="challenge-mini-card__text">
              <button className="challenge-mini-card__close" type="button" onClick={() => onClose(id)}>
                <Image src="/img/icons/clear.svg" alt="" />
              </button>
              <p className="challenge-mini-card__text">{`Parabéns, acabou de conquistar o desafio ${challengeName}`}</p>
            </div>
          </div>
        )}
      </motion.div>
    </AnimatePresence>
  );
}

ChallengeMiniCard.defaultProps = {
  challenge: {
    id: null,
    name: '',
    description: '',
    points: 0
  }
};

ChallengeMiniCard.propTypes = {
  challenge: PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    description: PropTypes.string,
    points: PropTypes.number
  }),
  onClose: PropTypes.func.isRequired
};

export default ChallengeMiniCard;

import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';
import moment from 'moment';
import classNames from 'classnames';
import { getTime } from '../../../utils/dates';
import { isPremium } from '../../../redux/selectors/user';
import { REGULAR_BOOKING_GAP, PREMIUM_BOOKING_GAP } from '../../../config/classes';
import './ClassMiniCard.scss';

function StatusIndicator({ isAvailable, isBooked, isCounterOnly, isPremiumLocked }) {
  if (isBooked || isCounterOnly) {
    const chipClass = classNames({
      'status-chip': true,
      '-booked': isBooked,
      '-counter': !isBooked && isPremiumLocked
    });

    return <div className={chipClass}>{isBooked ? 'marcada' : 'quiosque'}</div>;
  }

  const dotClass = classNames({
    'status-dot': true,
    '-premium': isPremiumLocked,
    '-available': !isPremiumLocked && isAvailable,
    '-unavailable': !isPremiumLocked && !isAvailable
  });

  return <div className={dotClass} />;
}

function ClassMiniCard({ classData, isSelected, isDisabled, t, isUserPremium, index, onClick }) {
  const { date, duration, studio, isAvailable, isBooked, maxMinutesAllowedToCancelClass } = classData;

  // If the class starts in less than maxMinutesAllowedToCancelClass minutes,
  // it can only be booked in the gym's counter
  const timeLeft = moment(date).diff(moment(), 'minutes');
  const classDate = moment(date).format('DD/MM');
  const isCounterOnly = timeLeft < maxMinutesAllowedToCancelClass;
  const isPremiumLocked =
    !isUserPremium && timeLeft < PREMIUM_BOOKING_GAP * 60 && timeLeft > REGULAR_BOOKING_GAP * 60;

  const timeFormatted = getTime(date);
  const durationFormatted = t('components.classMiniCard.duration', { duration });

  const onCardClicked = () => {
    if (index > -1 && onClick) {
      onClick(index);
    }
  };

  const cardClass = classNames({
    'class-mini-card': true,
    '-disabled': isDisabled,
    '-selected': isSelected
  });

  return (
    <button type="button" className={cardClass} onClick={onCardClicked}>
      <div className="class-mini-card__top">
        <div className="card__indicator">
          <StatusIndicator
            isBooked={isBooked}
            isAvailable={isAvailable}
            isCounterOnly={isCounterOnly}
            isPremiumLocked={isPremiumLocked}
          />
        </div>
        <span className="card__date">{classDate}</span>
        <b className="card__time">{timeFormatted}</b>
        <span className="card__duration">{durationFormatted}</span>
      </div>
      <div className="class-mini-card__bottom">
        <span className="card__studio">{studio}</span>
      </div>
    </button>
  );
}

StatusIndicator.propTypes = {
  isBooked: PropTypes.bool.isRequired,
  isCounterOnly: PropTypes.bool.isRequired,
  isPremiumLocked: PropTypes.bool.isRequired,
  isAvailable: PropTypes.bool.isRequired
};

ClassMiniCard.defaultProps = {
  onClick: undefined,
  index: -1,
  isDisabled: false,
  isSelected: false
};

ClassMiniCard.propTypes = {
  onClick: PropTypes.func,
  index: PropTypes.number,
  isDisabled: PropTypes.bool,
  isSelected: PropTypes.bool,
  classData: PropTypes.shape({
    date: PropTypes.instanceOf(Date),
    duration: PropTypes.number,
    maxMinutesAllowedToCancelClass: PropTypes.number,
    studio: PropTypes.string,
    isAvailable: PropTypes.bool,
    isBooked: PropTypes.bool
  }).isRequired,
  t: PropTypes.func.isRequired,
  isUserPremium: PropTypes.bool.isRequired
};

const mapStateToProps = state => {
  const isUserPremium = isPremium(state);
  return { isUserPremium };
};

export default compose(withTranslation(), connect(mapStateToProps))(ClassMiniCard);

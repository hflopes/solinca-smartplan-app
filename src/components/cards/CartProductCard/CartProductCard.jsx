import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Image from '../../Image';
import './CartProductCard.scss';

function CartProductCard({ product, onRemoveClicked, onSelectRedeem }) {
  const { id, name, points, imageURL } = product;

  return (
    <div className="cart-product-card">
      <Link className="remove-link" key={id} to={`/gamification/reward/${id}`}>
        <div className="cart-product-card__top">
          <Image className="cart-product-card__image" src={imageURL} alt="" />
        </div>
      </Link>
      <div className="cart-product-card__bottom">
        <Link className="remove-link" key={id} to={`/gamification/reward/${id}`}>
          <h3 className="cart-product-card__title">{name}</h3>
          <b className="cart-product-card__pts">{`${points} pts`}</b>
        </Link>
        <button type="button" className="cart-product-card__cta" onClick={() => onSelectRedeem(id)}>
          Redimir
        </button>
      </div>
      <button type="button" className="cart-product-card__btn" onClick={onRemoveClicked}>
        <Image className="cart-product-card__image" src="/img/icons/trash.svg" alt="" />
      </button>
    </div>
  );
}

CartProductCard.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    imageURL: PropTypes.string,
    isNew: PropTypes.bool,
    points: PropTypes.number
  }).isRequired,
  onRemoveClicked: PropTypes.func.isRequired,
  onSelectRedeem: PropTypes.func.isRequired
};

export default CartProductCard;

import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { useTranslation } from 'react-i18next';
import Image from '../../Image';
import './ProgressCard.scss';

const CONFIGS = {
  plans: {
    icon: '/img/big_icons/foot.svg',
    label: 'components.progressCard.plans',
    className: 'red'
  },
  recommendedClasses: {
    icon: '/img/big_icons/recommended_classes.svg',
    label: 'components.progressCard.recommendedClasses',
    className: 'orange'
  },
  lesMillsClasses: {
    icon: '/img/big_icons/lesmills_classes.svg',
    label: 'components.progressCard.lesMillsClasses',
    className: 'pink'
  },
  otherClasses: {
    icon: '/img/big_icons/other_classes.svg',
    label: 'components.progressCard.otherClasses',
    className: 'purple'
  }
};

function ProgressCard({ type, value, goal }) {
  const { t } = useTranslation();
  const config = CONFIGS[type];

  const cardClass = classNames({
    'progress-card': true,
    [`-${config.className}`]: true
  });

  const percent = goal && Math.min(100, (value / goal) * 100);

  return (
    <div className={cardClass}>
      <Image className="progress-card__icon" src={config.icon} alt="" />
      <div className="progress-card__value">
        <b className="value-current">{value}</b>
        {goal && goal !== -1 ? <span className="value-goal">{`/${goal}`}</span> : null}
      </div>
      <span className="progress-card__label">{t(config.label)}</span>
      {goal && goal !== -1 ? (
        <div className="progress-card__bar">
          <div className="bar__filled" style={{ width: `${percent}%` }} />
        </div>
      ) : null}
    </div>
  );
}

ProgressCard.defaultProps = {
  goal: undefined
};

ProgressCard.propTypes = {
  type: PropTypes.string.isRequired,
  value: PropTypes.number.isRequired,
  goal: PropTypes.number
};

export default ProgressCard;

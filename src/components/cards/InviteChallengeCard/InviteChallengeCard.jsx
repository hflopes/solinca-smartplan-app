import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import './InviteChallengeCard.scss';

const InviteChallengeCard = ({ challenge }) => {
  if (!challenge) return null;
  return (
    <Link className="remove-link" to={`/gamification/challenge/${challenge.id}`}>
      <div className="invite-challenge-card">
        <div className="invite-challenge-card__overlay" />
        <h3 className="invite-challenge-card__title">CONVIDAR AMIGOS</h3>
      </div>
    </Link>
  );
};

InviteChallengeCard.propTypes = {
  challenge: PropTypes.shape({
    id: PropTypes.number
  }).isRequired
};

export default InviteChallengeCard;

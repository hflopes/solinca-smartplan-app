import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import _ from 'lodash';
import { hexToCSS } from '../../../utils/colors';
import { DEFAULT_MODALITY } from '../../../config/visuals';
import './ModalityCard.scss';

function overlayGradient({ color1, color2 }) {
  const isValidColors = color1 && color2;

  const firstColor = isValidColors ? color1 : DEFAULT_MODALITY.colorScheme.start;
  const secondColor = isValidColors ? color2 : DEFAULT_MODALITY.colorScheme.end;

  const start = firstColor;
  const middle = hexToCSS(secondColor, 0.8);
  const end = hexToCSS(secondColor, 0);

  return {
    background: `radial-gradient(circle at bottom center, ${start} 0%, ${middle} 50%, ${end} 100%)`
  };
}

function backgroundImage({ photoUrl }) {
  const isValidUrl = photoUrl && photoUrl !== '';

  return {
    backgroundSize: 'cover',
    backgroundImage: `url(${isValidUrl ? photoUrl : DEFAULT_MODALITY.imageUrl})`,
    backgroundPosition: 'left top, center'
  };
}

function ModalityCardBody({ modality }) {
  const { name, intensity } = modality;
  return (
    <>
      <div className="card__background" style={backgroundImage(modality)}>
        <div className="card__overlay" style={overlayGradient(modality)} />
      </div>

      <div className="card__wrapper">
        <h2 className="card__title">{name}</h2>
        <div className="card__intensity">
          {_.times(5, i => (
            <div key={i} className={i < intensity ? 'dash active' : 'dash'} />
          ))}
        </div>
      </div>
    </>
  );
}

function ModalityCard({ modality, onClick, unfocused }) {
  const { id } = modality;

  const containerClass = classNames({
    modality__card: true,
    unfocused
  });

  return (
    <div className={containerClass}>
      {onClick ? (
        <button type="button" onClick={onClick}>
          <ModalityCardBody modality={modality} />
        </button>
      ) : (
        <Link to={`/classes/modalities/${id}`}>
          <ModalityCardBody modality={modality} />
        </Link>
      )}
    </div>
  );
}

ModalityCard.defaultProps = {
  onClick: undefined,
  unfocused: false
};

ModalityCard.propTypes = {
  modality: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    intensity: PropTypes.number,
    photoUrl: PropTypes.string
  }).isRequired,
  onClick: PropTypes.func,
  unfocused: PropTypes.bool
};

ModalityCardBody.propTypes = {
  modality: PropTypes.shape({
    name: PropTypes.string,
    intensity: PropTypes.number
  }).isRequired
};

export default ModalityCard;

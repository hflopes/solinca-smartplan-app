import React from 'react';
import './NewStatusCard.scss';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { getUserStatus } from '../../../redux/selectors/user';
import { statusChecked } from '../../../redux/modules/user/actions/status';

function NewStatusCard() {
  const dispatch = useDispatch();
  const status = useSelector(state => getUserStatus(state));
  const { t } = useTranslation();

  const clearNotification = () => {
    dispatch(statusChecked());
  };

  return (
    <div className="status-card-row">
      <Link onClick={clearNotification} to="/profile">
        <div className="new-status-card">
          <img className="icon" src={`/img/big_icons/status_${status}.svg`} alt="" />
          <p className="text">
            {t('screens.Status.upgraded')}
            <b>{` ${t(`screens.Statuses.${status}.title`)}`}</b>
          </p>
        </div>
      </Link>
      <button type="button" onClick={clearNotification} className="close-btn">
        <img src="/img/icons/clear.svg" alt="" />
      </button>
    </div>
  );
}

export default NewStatusCard;

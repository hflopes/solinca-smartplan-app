import React from 'react';
import PropTypes from 'prop-types';
import { IonGrid, IonRow } from '@ionic/react';
import classNames from 'classnames';
import Image from '../../Image';
import './SelectCard.scss';

function SelectCard({ title, description, imageUrl, highlighted, selected, onClick, disabled }) {
  const className = classNames({
    disabled,
    'select-card': true,
    highlighted,
    selected
  });

  return (
    <div className={className}>
      <button disabled={disabled} type="button" onClick={onClick}>
        <div className="img-wrapper">
          <Image src="/img/icons/check_pink.svg" alt="" />
        </div>
        <IonGrid>
          <IonRow>
            <div
              className="card-header"
              style={{
                background: `url('${imageUrl}')`,
                backgroundSize: 'cover',
                backgroundPosition: 'left top, center'
              }}
            >
              <div className="overlay">
                <h3>{title}</h3>
              </div>
            </div>
          </IonRow>
          {description && (
            <IonRow>
              <p className="card-description">{description}</p>
            </IonRow>
          )}
        </IonGrid>
      </button>
    </div>
  );
}

SelectCard.defaultProps = {
  imageUrl: '/img/backgrounds/modality.jpg'
};

SelectCard.propTypes = {
  title: PropTypes.string.isRequired,
  imageUrl: PropTypes.string,
  description: PropTypes.string.isRequired,
  highlighted: PropTypes.bool.isRequired,
  selected: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
  disabled: PropTypes.bool.isRequired
};

export default SelectCard;

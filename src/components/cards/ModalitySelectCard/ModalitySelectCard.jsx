import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { DEFAULT_MODALITY } from '../../../config/visuals';
import { hexToCSS } from '../../../utils/colors';
import Image from '../../Image';
import './ModalitySelectCard.scss';

function overlayGradient({ color1, color2 }) {
  const isValidColors = color1 && color2;

  const firstColor = isValidColors ? color1 : DEFAULT_MODALITY.colorScheme.start;
  const secondColor = isValidColors ? color2 : DEFAULT_MODALITY.colorScheme.end;

  const start = firstColor;
  const middle = hexToCSS(secondColor, 0.8);
  const end = hexToCSS(secondColor, 0);

  return {
    background: `radial-gradient(circle at bottom center, ${start} 0%, ${middle} 50%, ${end} 100%)`
  };
}

function backgroundImage({ photoUrl }) {
  const isValidUrl = photoUrl && photoUrl !== '';

  return {
    backgroundSize: 'cover',
    backgroundImage: `url(${isValidUrl ? photoUrl : DEFAULT_MODALITY.imageUrl})`,
    backgroundPosition: 'left top, center'
  };
}

function ModalitySelectCard({ modality, isFocused, isSelected, onClick }) {
  const { name, description } = modality;

  const containerClass = classNames({
    'modality-select__card': true,
    '-unfocused': !isFocused,
    '-selected': isSelected
  });

  return (
    <div className={containerClass} onClick={onClick}>
      <div className="modality-select__header">
        <div className="overlay-wrapper" style={backgroundImage(modality)}>
          <div className="overlay" style={overlayGradient(modality)} />
        </div>
        <h1 className="modality__title">{name}</h1>
      </div>
      <div className="modality-select__wrapper">
        <div className="modality-select__content">
          <div className="handle" />
          <div className="description">{description}</div>
        </div>
      </div>
      <div className="modality-select__check">
        <Image src="/img/icons/check_pink.svg" alt="" />
      </div>
    </div>
  );
}

ModalitySelectCard.propTypes = {
  modality: PropTypes.shape({
    name: PropTypes.string,
    description: PropTypes.string
  }).isRequired,
  isFocused: PropTypes.bool.isRequired,
  isSelected: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired
};

export default ModalitySelectCard;

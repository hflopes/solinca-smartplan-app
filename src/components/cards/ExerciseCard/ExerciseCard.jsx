import React from 'react';
import { Link } from 'react-router-dom';
import { compose } from 'redux';
import { connect } from 'react-redux';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';
import { isExerciseCompleted } from '../../../redux/selectors/plan';
import { startExercise } from '../../../redux/modules/plan/actions/execute';
import { formatSeconds } from '../../../utils/dates';
import Image from '../../Image';
import Fab from '../../buttons/FabButton';
import ScrollAnchor from '../../ScrollAnchor';
import './ExerciseCard.scss';

const DARK_GRADIENT = 'linear-gradient(0deg, rgba(0,0,0,0.4) 0%, rgba(0,0,0,0.4) 100%)';

function CompleteCheck({ isCompleted }) {
  const className = classNames({
    'complete-check': true,
    '-hidden': !isCompleted
  });

  return (
    <div className={className}>
      <Image className="complete-check__icon" src="/img/icons/check_pink.svg" alt="" />
    </div>
  );
}

function ExerciseDuration({ duration, isCompleted }) {
  const className = classNames({
    'exercise-duration': true,
    '-hidden': isCompleted
  });

  return (
    <div className={className}>
      <Image className="exercise-duration__icon" src="/img/icons/stopwatch_gray.svg" alt="" />
      <b className="exercise-duration__value">{duration}</b>
    </div>
  );
}

function ExerciseCard({ t, exercise, isCompleted, startPlanExercise }) {
  const { id, image, name, equipment, pse, duration, params } = exercise;

  const bgImageStyle = {
    background: `${DARK_GRADIENT}, url("${image}")`,
    backgroundSize: 'cover',
    backgroundPosition: 'left top, center'
  };

  const onStart = () => startPlanExercise({ exercise });
  const time = formatSeconds(duration);

  return (
    <div className="exercise-card">
      <ScrollAnchor />
      <Link to={`/plan/exercise/${id}`}>
        <div className="exercise-card__header" style={image ? bgImageStyle : null}>
          <div className="overlay">
            <h1 className="exercise-card__title">{name}</h1>
          </div>
        </div>
        <div className="exercise-card__info">
          {equipment &&
            <div className="info-column -first">
              <span className="info-column__label">{t('global.trainingPlan.equipment.short')}</span>
              <b className="info-column__value">{equipment || '---'}</b>
            </div>
          }
          <div className="info-column">
            <span className="info-column__label">{t('global.trainingPlan.pse.short')}</span>
            <b className="info-column__value">{pse || '---'}</b>
          </div>
          {params.map(({ label, value }) => (
            <div className="info-column" key={label}>
              <span className="info-column__label">{t(`global.trainingPlan.${label}.short`)}</span>
              <b className="info-column__value">{value}</b>
            </div>
          ))}
        </div>
      </Link>
      <div className="exercise-card__overlays">
        <Fab icon="stopwatch_start_white" onClick={onStart} isDark={isCompleted} />
        <CompleteCheck isCompleted={isCompleted} />
        <ExerciseDuration duration={time} isCompleted={isCompleted} />
      </div>
    </div>
  );
}

CompleteCheck.propTypes = {
  isCompleted: PropTypes.bool.isRequired
};

ExerciseDuration.propTypes = {
  isCompleted: PropTypes.bool.isRequired,
  duration: PropTypes.string.isRequired
};

ExerciseCard.propTypes = {
  exercise: PropTypes.shape({
    id: PropTypes.string,
    image: PropTypes.string,
    name: PropTypes.string,
    equipment: PropTypes.string,
    pse: PropTypes.number,
    duration: PropTypes.number,
    params: PropTypes.arrayOf(PropTypes.any)
  }).isRequired,
  t: PropTypes.func.isRequired,
  startPlanExercise: PropTypes.func.isRequired,
  isCompleted: PropTypes.bool.isRequired
};

const mapStateToProps = (state, ownProps) => {
  const exerciseId = ownProps.exercise.id;
  const isCompleted = isExerciseCompleted(state, exerciseId);
  return { isCompleted };
};

const mapDispatchToProps = {
  startPlanExercise: startExercise
};

export default compose(withTranslation(), connect(mapStateToProps, mapDispatchToProps))(ExerciseCard);

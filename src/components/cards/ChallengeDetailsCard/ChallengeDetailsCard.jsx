import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import moment from 'moment';
import { withTranslation } from 'react-i18next';
import { getMonth } from '../../../utils/dates';
import './ChallengeDetailsCard.scss';

function getDaysLeft(startDate, endDate) {
  return moment(endDate).diff(moment(startDate), 'days');
}

function ChallengeDetailsCard({ challenge, hasJoined, hasFinished, t }) {
  const { dateBegin, dateEnd } = challenge;

  const beginMonth = getMonth(dateBegin.getMonth());
  const endMonth = getMonth(dateEnd.getMonth());

  const startDate = `${dateBegin.getDate()} ${beginMonth.substring(0, 3)}`;
  let endDate = `${dateEnd.getDate()} ${endMonth.substring(0, 3)}`;

  const daysLeft = getDaysLeft(dateBegin, dateEnd);
  const showDays = daysLeft < 365;
  if (!showDays) {
    endDate = null;
  }

  return (
    <div
      className={classNames({
        'challenge-details__card': true,
        '-joined': hasJoined,
        '-finished': hasFinished
      })}
    >
      <div className="top">
        {hasJoined ? (
          <div className="row">
            {showDays ? (
              <>
                <span className="label">{t('screens.ChallengeDetails.ends')}</span>
                <b className="value">{t('components.ChallengeCard.days', { days: daysLeft })}</b>
              </>
            ) : (
              <>
                <span className="label">{t('screens.ChallengeDetails.started')}</span>
                <b className="value">{startDate}</b>
              </>
            )}
          </div>
        ) : (
          <>
            <div className="row">
              <span className="label">{t('screens.ChallengeDetails.start')}</span>
              <b className="value">{startDate}</b>
            </div>
            {endDate && (
              <div className="row">
                <span className="label">{t('screens.ChallengeDetails.end')}</span>
                <b className="value">{endDate}</b>
              </div>
            )}
          </>
        )}
      </div>
      {/* <div className="bottom">
        <b className="challenge-points">{`${points} pts`}</b>
      </div> */}
    </div>
  );
}

ChallengeDetailsCard.propTypes = {
  challenge: PropTypes.shape({
    points: PropTypes.number,
    dateBegin: PropTypes.instanceOf(Date),
    dateEnd: PropTypes.instanceOf(Date)
  }).isRequired,
  hasJoined: PropTypes.bool.isRequired,
  hasFinished: PropTypes.bool.isRequired,
  t: PropTypes.func.isRequired
};

export default withTranslation()(ChallengeDetailsCard);

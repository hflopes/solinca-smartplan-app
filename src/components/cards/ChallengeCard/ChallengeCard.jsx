import React from 'react';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import moment from 'moment';
import Image from '../../Image';
import './ChallengeCard.scss';

function ChallengeCard({ challenge, t }) {
  const { id, name, description, progress, dateBegin, dateEnd, group, isNew } = challenge;
  const challengeName = name || description;
  const days = moment(dateEnd).diff(moment(dateBegin), 'days');
  const showDays = days < 365;
  const hasProgression = progress > 0;
  const badge = group && group.url_image_enabled;

  return (
    <Link className="remove-link" key={id} to={`/gamification/challenge/${id}`}>
      <div className={classNames('challenge-card', hasProgression && 'progression')}>
        {isNew && !hasProgression && (
          <span className="challenge-card__label">
            <p>{t('components.ChallengeCard.new')}</p>
          </span>
        )}
        {hasProgression && (
          <div className="challenge-card__progress-bar">
            <span className="challenge-card__progress-bar-fill" style={{ width: `${progress}%` }} />
          </div>
        )}
        <Image className="challenge-card__badge" src={badge} alt="" />
        <h3 className="challenge-card__title">{challengeName.toUpperCase()}</h3>
        {showDays && (
          <>
            <span className="challenge-card__separator" />
            <p className="challenge-card__caption">{t('components.ChallengeCard.days', { days })}</p>
          </>
        )}
      </div>
    </Link>
  );
}

ChallengeCard.propTypes = {
  challenge: PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    description: PropTypes.string,
    progress: PropTypes.number,
    dateBegin: PropTypes.instanceOf(Date),
    dateEnd: PropTypes.instanceOf(Date),
    isNew: PropTypes.bool,
    group: PropTypes.shape({
      url_image_enabled: PropTypes.string
    })
  }).isRequired,
  t: PropTypes.func.isRequired
};

export default withTranslation()(ChallengeCard);

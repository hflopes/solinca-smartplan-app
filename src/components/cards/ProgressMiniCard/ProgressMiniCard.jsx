import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Image from '../../Image';
import './ProgressMiniCard.scss';

const CONFIGS = {
  plans: {
    icon: '/img/big_icons/foot.svg',
    className: 'red'
  },
  recommendedClasses: {
    icon: '/img/big_icons/recommended_classes.svg',
    className: 'orange'
  },
  lesMillsClasses: {
    icon: '/img/big_icons/lesmills_classes.svg',
    className: 'pink'
  },
  otherClasses: {
    icon: '/img/big_icons/other_classes.svg',
    className: 'purple'
  }
};

function ProgressCard({ type, value, goal }) {
  const config = CONFIGS[type];

  const cardClass = classNames({
    'progress-mini-card': true,
    [`-${config.className}`]: true
  });

  return (
    <div className={cardClass}>
      <Image className="progress-mini-card__icon" src={config.icon} alt="" />
      <div className="progress-mini-card__value">
        <b className="value-current">{value}</b>
        {goal && goal !== -1 ? <span className="value-goal">{`/${goal}`}</span> : null}
      </div>
    </div>
  );
}

ProgressCard.defaultProps = {
  goal: undefined
};

ProgressCard.propTypes = {
  type: PropTypes.string.isRequired,
  value: PropTypes.number.isRequired,
  goal: PropTypes.number
};

export default ProgressCard;

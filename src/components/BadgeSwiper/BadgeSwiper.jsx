import React, { Component } from 'react';
import _ from 'lodash';
import { IonGrid, IonRow } from '@ionic/react';
import PropTypes from 'prop-types';
import SwipeableViews from 'react-swipeable-views';
import './BadgeSwiper.scss';
import Image from '../Image';

class BadgeSwiper extends Component {
  state = {
    selectedTabIndex: 0
  };

  selectTab = index => {
    this.setState({
      selectedTabIndex: index
    });
  };

  render() {
    const { swipes } = this.props;
    const { selectedTabIndex } = this.state;

    return (
      <div className="badge-swiper">
        <SwipeableViews index={selectedTabIndex} onChangeIndex={this.selectTab}>
          {swipes.map(s => (
            <IonGrid key={s.title} className="swiper">
              <IonRow>
                <div className="badge">
                  <Image src={s.icon} alt="" />
                </div>
              </IonRow>
              <IonRow>
                <h2>{s.title}</h2>
              </IonRow>
              <IonRow>
                <p>{s.description}</p>
              </IonRow>
            </IonGrid>
          ))}
        </SwipeableViews>
        <div className="swiper-bullets">
          {_.times(swipes.length, i => (
            <div key={i} className={`bullet ${i === selectedTabIndex ? 'highlighted' : ''}`} />
          ))}
        </div>
      </div>
    );
  }
}

BadgeSwiper.propTypes = {
  swipes: PropTypes.arrayOf(PropTypes.any).isRequired
};

export default BadgeSwiper;

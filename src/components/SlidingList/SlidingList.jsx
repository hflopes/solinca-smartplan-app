import React, { Component } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import Image from '../Image';
import reloadAction from '../../redux/reload/action';
import './SlidingList.scss';

class SlidingList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedIndex: props.selectedIndex || 0
    };
  }

  componentWillReceiveProps(nextProps) {
    const { isShowing: wasShowing } = this.props;
    const { isShowing, reload, type } = nextProps;

    if (!wasShowing && isShowing && type === 'gyms') {
      reload('userPosition');
    }
  }

  selectOption = index => {
    this.setState({
      selectedIndex: index
    });

    const { onSelect } = this.props;
    onSelect(index);
  };

  render() {
    const { selectedIndex } = this.state;
    const { list, copy, isShowing, id, onDismiss } = this.props;
    const { listTitle, selectedTitle, optionsTitle } = copy;

    const selectedOption = list[selectedIndex];

    const bgClassName = classNames({
      'sliding-list-bg': true,
      showing: isShowing
    });

    const fgClassName = classNames({
      'sliding-list': true,
      showing: isShowing
    });

    return (
      <>
        <div className={bgClassName} onClick={onDismiss} />
        <div id={id} className={fgClassName}>
          <h3>{listTitle}</h3>
          {selectedIndex < 0 ? null : (
            <div className="selected-option">
              <span>{selectedTitle}</span>
              <div>
                <b>{selectedOption.title}</b>
                <Image src="/img/icons/check_pink.svg" alt="" />
              </div>
            </div>
          )}
          <div className="list-options">
            <span>{optionsTitle}</span>
            <ul>
              {list.map(({ title, info }, i) =>
                (() => {
                  if (i === selectedIndex) return null;
                  return (
                    <li key={title}>
                      <button type="button" onClick={() => this.selectOption(i)}>
                        <b>{title}</b>
                        {info && <span>{info}</span>}
                      </button>
                    </li>
                  );
                })()
              )}
            </ul>
          </div>
        </div>
      </>
    );
  }
}

SlidingList.defaultProps = {
  type: 'gyms',
  selectedIndex: 0,
  isShowing: false,
  id: undefined,
  onDismiss: undefined
};

SlidingList.propTypes = {
  type: PropTypes.string,
  list: PropTypes.arrayOf(PropTypes.any).isRequired,
  selectedIndex: PropTypes.number,
  reload: PropTypes.func.isRequired,
  onSelect: PropTypes.func.isRequired,
  onDismiss: PropTypes.func,
  copy: PropTypes.shape({
    listTitle: PropTypes.string,
    selectedTitle: PropTypes.string,
    optionsTitle: PropTypes.string
  }).isRequired,
  isShowing: PropTypes.bool,
  id: PropTypes.string
};

const mapDispatchToProps = {
  reload: reloadAction
};

export default connect(null, mapDispatchToProps)(SlidingList);

import React from 'react';
import PropTypes from 'prop-types';
import Image from '../Image';
import './Empty.scss';

const Empty = ({ text, image, imageSize, style }) => {
  if (!text) {
    return null;
  }
  return (
    <div className="empty" style={style}>
      <p className="text">{text}</p>
      {image && <Image className="image" src={image} style={imageSize ? { height: imageSize } : {}} />}
    </div>
  );
};

Empty.defaultProps = {
  style: {},
  text: '',
  image: '',
  imageSize: false
};

Empty.propTypes = {
  style: PropTypes.shape({}),
  text: PropTypes.string,
  imageSize: PropTypes.oneOfType([PropTypes.bool, PropTypes.string, PropTypes.number]),
  image: PropTypes.string
};

export default Empty;

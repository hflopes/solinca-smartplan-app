import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { handleBackNavigation } from '../../utils/navigation';
import setBackNavigationAction from '../../redux/modules/app/actions/setBackNavigation';
import ClearNav from '../navs/ClearNav';

export default function withBackNavigation(theme = 'light', showButton = true) {
  return WrappedComponent => {
    class BackNavigationContainer extends Component {
      navigateBack = () => {
        const { history, location } = this.props;
        handleBackNavigation(history, location);
      };

      setOnNavigateBack = callback => {
        const { setBackNavigation } = this.props;
        setBackNavigation(callback);
      };

      render() {
        return (
          <>
            <WrappedComponent {...this.props} onNavigateBack={this.setOnNavigateBack} />
            {showButton && <ClearNav onNavigateBack={this.navigateBack} theme={theme} />}
          </>
        );
      }
    }

    const mapDispatchToProps = {
      setBackNavigation: setBackNavigationAction
    };

    BackNavigationContainer.propTypes = {
      setBackNavigation: PropTypes.func.isRequired,
      history: PropTypes.shape({
        goBack: PropTypes.func
      }).isRequired,
      location: PropTypes.shape({}).isRequired
    };

    return compose(withRouter, connect(null, mapDispatchToProps))(BackNavigationContainer);
  };
}

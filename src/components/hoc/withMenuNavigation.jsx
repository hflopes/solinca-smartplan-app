import React from 'react';
import { withRouter } from 'react-router';
import HamburgerNav from '../navs/HamburgerNav';

export default function withMenuNavigation(theme = 'light', showButton = true) {
  return WrappedComponent => {
    const MenuNavigationContainer = props => {
      return (
        <>
          <WrappedComponent {...props} />
          {showButton && <HamburgerNav theme={theme} />}
        </>
      );
    };
    return withRouter(MenuNavigationContainer);
  };
}

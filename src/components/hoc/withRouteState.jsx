import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { getComponentStateStack } from '../../redux/selectors/app';
import saveStateAction from '../../redux/modules/app/actions/saveState';

export default function withRouteState(componentName = 'undefined') {
  return function hoc(WrappedComponent) {
    class ComponentStateContainer extends Component {
      onSaveState = state => {
        const { saveState } = this.props;

        saveState({
          component: componentName,
          data: { ...state }
        });
      };

      render() {
        const { lastState } = this.props;
        return <WrappedComponent {...this.props} saveState={this.onSaveState} lastState={lastState} />;
      }
    }

    const mapStateToProps = state => {
      const stack = getComponentStateStack(state);
      const componentStack = stack[componentName];
      return { lastState: componentStack ? componentStack.data : {} };
    };

    const mapDispatchToProps = {
      saveState: saveStateAction
    };

    ComponentStateContainer.defaultProps = {
      lastState: {}
    };

    ComponentStateContainer.propTypes = {
      saveState: PropTypes.func.isRequired,
      lastState: PropTypes.shape({})
    };

    return compose(connect(mapStateToProps, mapDispatchToProps))(ComponentStateContainer);
  };
}

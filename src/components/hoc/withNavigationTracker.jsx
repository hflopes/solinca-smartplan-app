import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { screen, event } from '../../utils/analytics';

const withNavigationTracker = WrappedComponent => {
  const trackPage = page => {
    screen(page);
    event('screen_view', { firebase_screen: page });
  };

  const HOC = class extends Component {
    static propTypes = {
      location: PropTypes.shape({
        pathname: PropTypes.string
      }).isRequired
    };

    componentDidMount() {
      const {
        location: { pathname: page }
      } = this.props;
      trackPage(page);
    }

    componentWillReceiveProps(nextProps) {
      const {
        location: { pathname: currentPage }
      } = this.props;
      const {
        location: { pathname: nextPage }
      } = nextProps;

      if (currentPage !== nextPage) {
        trackPage(nextPage);
      }
    }

    render() {
      return <WrappedComponent {...this.props} />;
    }
  };

  return HOC;
};

export default withNavigationTracker;

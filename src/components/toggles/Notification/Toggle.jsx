import React from 'react';
import PropTypes from 'prop-types';
import { IonToggle, IonItem, IonLabel } from '@ionic/react';
import './Toggle.scss';

const NotificationToggle = ({ label, type, description, checked, onChange }) => {
  return (
    <IonItem lines="full">
      <IonLabel>
        {label}
        <>{description ? <span>{description}</span> : null}</>
      </IonLabel>
      <IonToggle mode="ios" value={type} checked={checked} onIonChange={onChange} />
    </IonItem>
  );
};

NotificationToggle.defaultProps = {
  description: ''
};

NotificationToggle.propTypes = {
  description: PropTypes.string,
  label: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  checked: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired
};

export default NotificationToggle;

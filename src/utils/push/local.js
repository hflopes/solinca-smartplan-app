// import moment from 'moment';
import { Plugins } from '@capacitor/core';

const { LocalNotifications } = Plugins;

// This plugin only accepts Number as id for the notification
const generateNumberId = string => string.replace(/[^0-9]/g, '');

/**
 * Schedule a local push notification to be sent
 * This is used for reminders
 *
 * @param {String} id used to track the added notification (for later cancel if needed)
 * @param {String} title first line, should be able to recognize what it is
 * @param {String} [body] descriptive information, can be optional
 * @param {Date} date when should the push be sent
 */
export const scheduleLocalNotification = async ({ id, title, body = '', date, extra = {} }) => {
  try {
    const { value: enabled } = await LocalNotifications.areEnabled();
    if (!enabled) {
      await LocalNotifications.requestPermissions();
    }

    await LocalNotifications.schedule({
      notifications: [
        {
          title,
          body,
          id: generateNumberId(id),
          schedule: { at: date },
          extra
        }
      ]
    });
  } catch (error) {
    console.log('utils/push/local/scheduleLocalNotification');
    console.log(error);
  }
};

/**
 * Cancel a scheduled local push notification
 *
 * @param {String} id used on creation
 */
export const cancelLocalNotification = id => {
  LocalNotifications.cancel({ notifications: [{ id: generateNumberId(id) }] });
};

const actionPerformed = actionData => {
  // const {
  //   actionId,
  //   notification: { id, extra }
  // } = actionData;
  // TODO: track event?
};

const addListener = () => {
  LocalNotifications.addListener('localNotificationActionPerformed', actionPerformed);
};

// const testLocalPush = () => {
//   scheduleLocalNotification({
//     id: '1',
//     title: 'Local Notification',
//     body: 'Body with more text',
//     date: moment()
//       .add(10, 'seconds')
//       .toDate()
//   });
// };

/**
 * Initialize Local Push Notifications
 * Checks if Local Push are enabled
 * Registers the listener for push action performed
 */
const init = async () => {
  try {
    addListener();
  } catch (error) {
    console.log('utils/push/local/init');
    console.log(error);
  }

  // testLocalPush();
};

export default init;

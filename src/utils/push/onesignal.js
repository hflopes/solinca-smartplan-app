/**
 *  OneSignal Integration
 */

const APP_ID = '187f61c0-e8b5-4cfb-9dfc-19213da96b20';

const opened = result => {
  console.log(JSON.stringify(result));
};

export const sendTag = (key, value) => {
  try {
    window.plugins.OneSignal.sendTag(key, value);
  } catch (error) {
    console.log('utils/push/onesignal/sendTag');
    console.log(error);
  }
};

export const sendTags = tags => {
  try {
    window.plugins.OneSignal.sendTags(tags);
    console.log('sent tags', tags);
  } catch (error) {
    console.log('utils/push/onesignal/sendTag');
    console.log(error);
  }
};

/**
 * Setup userId for OneSignal
 * This allow us to not store the player id from OneSignal
 * Meaning we can use our own id's to send push
 *
 * If your system assigns unique identifiers to users, it can be annoying to have to also remember their OneSignal user ID's as well.
 * To make things easier, OneSignal now allows you to set an external_id for your users.
 * Simply call this method, pass in your custom user ID (as a string), and from now on when you send a push notification,
 * you can use include_external_user_ids instead of include_player_ids.
 *
 * @param {String} userId
 */
export const setupUserId = userId => {
  try {
    window.plugins.OneSignal.setExternalUserId(userId.toString());
  } catch (error) {
    console.log('utils/push/onesignal/setupUserId');
    console.log(error);
  }
};

/**
 * Initialize OneSignal SDK
 * Registers the listener for push opened
 * TODO: UserPrivacyConsent
 *
 * @param {String} [userId]
 */
const init = userId => {
  console.log('Initialize OneSignal');
  if (window.plugins && window.plugins.OneSignal) {
    try {
      window.plugins.OneSignal.startInit(APP_ID)
        .handleNotificationOpened(opened)
        .endInit();
      if (userId) {
        setupUserId(userId);
      }
    } catch (error) {
      console.log('utils/push/onesignal/init');
      console.log(error);
    }
  } else {
    console.log('Platform not supported');
  }
};

export default init;

export default function isDevelopmentMode() {
  return !process.env.NODE_ENV || process.env.NODE_ENV === 'development';
}

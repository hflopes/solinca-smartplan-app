import i18n from 'i18next';
import traverse from 'traverse';
import moment from 'moment';
import { createTransform } from 'redux-persist';

export function getDayPeriod() {
  const hour = new Date().getHours();
  let period = 'morning';
  if (hour >= 12 && hour <= 18) {
    period = 'afternoon';
  }
  if (hour >= 19 || hour <= 5) {
    period = 'night';
  }

  return period;
}

export function getMonth(index) {
  return i18n.t(`global.dates.months.${index + 1}`);
}

export function getDayOfWeek(index) {
  return i18n.t(`global.dates.daysOfWeek.${index}`);
}

export function getTime(date) {
  const pad = num => {
    if (num > 9) {
      return num;
    }

    return `0${num}`;
  };

  const hours = date.getHours();
  const minutes = date.getMinutes();

  return `${pad(hours)}:${pad(minutes)}`;
}

export function getDaysAhead() {
  const days = [];

  const currentDate = new Date();
  const dayOfWeek = currentDate.getDay();
  let dayInc = 0;

  for (let i = dayOfWeek; i < 7; i += 1) {
    const day = {
      index: i,
      date: moment()
        .add(dayInc, 'days')
        .format('DD/MM'),
      name: getDayOfWeek(i)
    };

    days.push(day);
    dayInc += 1;
  }

  const daysLeft = 7 - days.length;

  for (let i = 0; i < daysLeft; i += 1) {
    const day = {
      index: i,
      date: moment()
        .add(dayInc, 'days')
        .format('DD/MM'),
      name: getDayOfWeek(i)
    };

    days.push(day);
    dayInc += 1;
  }

  days.push({
    index: days.length,
    date: moment()
      .add(dayInc, 'days')
      .format('DD/MM'),
    name: getDayOfWeek(dayOfWeek)
  });

  return days;
}

/**
 * Persist saves dates as strings.
 * This transform should convert ISO date strings back to
 * their original Date object type.
 */
export const dateTransformPersistor = createTransform(null, outboundState => {
  return traverse(outboundState).forEach(val => {
    if (typeof val === 'string' && moment(val, moment.ISO_8601).isValid()) {
      return new Date(val);
    }

    return val;
  });
});

export function formatSeconds(seconds) {
  const mins = Math.floor(seconds / 60);
  const secs = seconds % 60;

  const appendZero = number => {
    return number < 10 ? `0${number}` : number;
  };

  return `${appendZero(mins)}:${appendZero(secs)}`;
}

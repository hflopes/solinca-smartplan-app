import { Plugins } from '@capacitor/core';
import store from '../redux/store';

const MENU_OPEN_CLASS = 'menu-open';

export function handleBackNavigation(history, location) {
  const callback = store.getState().app.backNavigationCallback;

  // If the route has a defined callback,
  // with a valid returned value (true),
  // do not navigate backwards
  if (callback && callback()) {
    return;
  }

  if (canExit(location)) {
    try {
      Plugins.App.exitApp();
    } catch (error) {
      console.log(error);
      console.log('exit');
    }
  } else {
    history.goBack();
  }
}

export function toggleMenu() {
  const app = document.getElementById('app');
  const menu = document.getElementById('menu');

  app.classList.toggle(MENU_OPEN_CLASS);
  menu.classList.toggle(MENU_OPEN_CLASS);
}

export function openMenu() {
  const app = document.getElementById('app');
  const menu = document.getElementById('menu');

  app.classList.add(MENU_OPEN_CLASS);
  menu.classList.add(MENU_OPEN_CLASS);
}

export function closeMenu() {
  const app = document.getElementById('app');
  const menu = document.getElementById('menu');

  app.classList.remove(MENU_OPEN_CLASS);
  menu.classList.remove(MENU_OPEN_CLASS);
}

export function canExit(location) {
  const { pathname } = location;
  return pathname === '/welcome' || pathname === '/home';
}

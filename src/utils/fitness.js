export function getFatTier(age, gender, fatPercent) {
  if (gender === 'M') {
    return getMaleFatTier(age, fatPercent);
  }

  return getFemaleFatTier(age, fatPercent);
}

function getFemaleFatTier(age, fatPercent) {
  if (age <= 29) {
    if (isInRange(fatPercent, 11.4, 15)) {
      return 'Muito magro';
    } else if (isInRange(fatPercent, 15.1, 16.7)) {
      return 'Excelente';
    } else if (isInRange(fatPercent, 16.8, 20.5)) {
      return 'Bom';
    } else if (isInRange(fatPercent, 20.6, 24.1)) {
      return 'Médio';
    } else if (isInRange(fatPercent, 24.2, 30.4)) {
      return 'Mau';
    } else if (fatPercent >= 30.5) {
      return 'Muito mau';
    }
  } else if (isInRange(age, 30, 39)) {
    if (isInRange(fatPercent, 11.2, 15.4)) {
      return 'Muito magro';
    } else if (isInRange(fatPercent, 15.5, 17.4)) {
      return 'Excelente';
    } else if (isInRange(fatPercent, 17.5, 21.9)) {
      return 'Bom';
    } else if (isInRange(fatPercent, 22, 25.7)) {
      return 'Médio';
    } else if (isInRange(fatPercent, 25.8, 31.4)) {
      return 'Mau';
    } else if (fatPercent >= 31.5) {
      return 'Muito mau';
    }
  } else if (isInRange(age, 40, 49)) {
    if (isInRange(fatPercent, 12.1, 16.7)) {
      return 'Muito magro';
    } else if (isInRange(fatPercent, 16.8, 19.4)) {
      return 'Excelente';
    } else if (isInRange(fatPercent, 19.5, 24.5)) {
      return 'Bom';
    } else if (isInRange(fatPercent, 24.6, 28.3)) {
      return 'Médio';
    } else if (isInRange(fatPercent, 28.4, 33.3)) {
      return 'Mau';
    } else if (fatPercent >= 33.4) {
      return 'Muito mau';
    }
  } else if (isInRange(age, 50, 59)) {
    if (isInRange(fatPercent, 13.9, 19)) {
      return 'Muito magro';
    } else if (isInRange(fatPercent, 19.1, 22.2)) {
      return 'Excelente';
    } else if (isInRange(fatPercent, 22.3, 27.5)) {
      return 'Bom';
    } else if (isInRange(fatPercent, 27.6, 30.7)) {
      return 'Médio';
    } else if (isInRange(fatPercent, 30.8, 34.9)) {
      return 'Mau';
    } else if (fatPercent >= 35) {
      return 'Muito mau';
    }
  } else if (isInRange(age, 60, 69)) {
    if (isInRange(fatPercent, 13.9, 20.1)) {
      return 'Muito magro';
    } else if (isInRange(fatPercent, 20.2, 23.2)) {
      return 'Excelente';
    } else if (isInRange(fatPercent, 23.3, 28.2)) {
      return 'Bom';
    } else if (isInRange(fatPercent, 28.3, 31.4)) {
      return 'Médio';
    } else if (isInRange(fatPercent, 31.5, 35.5)) {
      return 'Mau';
    } else if (fatPercent >= 35.6) {
      return 'Muito mau';
    }
  } else if (age >= 70) {
    if (isInRange(fatPercent, 11.7, 18.2)) {
      return 'Muito magro';
    } else if (isInRange(fatPercent, 18.3, 22.4)) {
      return 'Excelente';
    } else if (isInRange(fatPercent, 22.5, 27.5)) {
      return 'Bom';
    } else if (isInRange(fatPercent, 27.6, 30.9)) {
      return 'Médio';
    } else if (isInRange(fatPercent, 31, 35.2)) {
      return 'Mau';
    } else if (fatPercent >= 35.3) {
      return 'Muito mau';
    }
  }

  return '??';
}

function getMaleFatTier(age, fatPercent) {
  if (age <= 29) {
    if (isInRange(fatPercent, 4.2, 7.8)) {
      return 'Muito magro';
    } else if (isInRange(fatPercent, 7.9, 11.4)) {
      return 'Excelente';
    } else if (isInRange(fatPercent, 11.5, 15.7)) {
      return 'Bom';
    } else if (isInRange(fatPercent, 15.8, 19.6)) {
      return 'Médio';
    } else if (isInRange(fatPercent, 19.7, 24.8)) {
      return 'Mau';
    } else if (fatPercent >= 24.9) {
      return 'Muito mau';
    }
  } else if (isInRange(age, 30, 39)) {
    if (isInRange(fatPercent, 7.3, 12.3)) {
      return 'Muito magro';
    } else if (isInRange(fatPercent, 12.4, 15.8)) {
      return 'Excelente';
    } else if (isInRange(fatPercent, 15.9, 19.1)) {
      return 'Bom';
    } else if (isInRange(fatPercent, 19.2, 22.3)) {
      return 'Médio';
    } else if (isInRange(fatPercent, 22.4, 26.3)) {
      return 'Mau';
    } else if (fatPercent >= 26.4) {
      return 'Muito mau';
    }
  } else if (isInRange(age, 40, 49)) {
    if (isInRange(fatPercent, 9.5, 14.9)) {
      return 'Muito magro';
    } else if (isInRange(fatPercent, 15.0, 18.4)) {
      return 'Excelente';
    } else if (isInRange(fatPercent, 18.5, 21.3)) {
      return 'Bom';
    } else if (isInRange(fatPercent, 21.4, 24.1)) {
      return 'Médio';
    } else if (isInRange(fatPercent, 24.2, 27.7)) {
      return 'Mau';
    } else if (fatPercent >= 27.8) {
      return 'Muito mau';
    }
  } else if (isInRange(age, 50, 59)) {
    if (isInRange(fatPercent, 11, 16.9)) {
      return 'Muito magro';
    } else if (isInRange(fatPercent, 17, 20.1)) {
      return 'Excelente';
    } else if (isInRange(fatPercent, 20.2, 22.9)) {
      return 'Bom';
    } else if (isInRange(fatPercent, 23, 25.5)) {
      return 'Médio';
    } else if (isInRange(fatPercent, 25.6, 29.1)) {
      return 'Mau';
    } else if (fatPercent >= 29.2) {
      return 'Muito mau';
    }
  } else if (isInRange(age, 60, 69)) {
    if (isInRange(fatPercent, 11.9, 18)) {
      return 'Muito magro';
    } else if (isInRange(fatPercent, 18.1, 20.9)) {
      return 'Excelente';
    } else if (isInRange(fatPercent, 21, 23.5)) {
      return 'Bom';
    } else if (isInRange(fatPercent, 23.6, 26.3)) {
      return 'Médio';
    } else if (isInRange(fatPercent, 26.4, 29.7)) {
      return 'Mau';
    } else if (fatPercent >= 29.8) {
      return 'Muito mau';
    }
  } else if (age >= 70) {
    if (isInRange(fatPercent, 13.6, 17.4)) {
      return 'Muito magro';
    } else if (isInRange(fatPercent, 17.5, 20.9)) {
      return 'Excelente';
    } else if (isInRange(fatPercent, 21, 23.6)) {
      return 'Bom';
    } else if (isInRange(fatPercent, 23.7, 25.7)) {
      return 'Médio';
    } else if (isInRange(fatPercent, 25.8, 29.3)) {
      return 'Mau';
    } else if (fatPercent >= 29.4) {
      return 'Muito mau';
    }
  }

  return '??';
}

function isInRange(num, start, end) {
  return num >= start && num <= end;
}

import moment from 'moment';

export function hasCalendarPermissions(cb) {
  try {
    window.plugins.calendar.hasReadWritePermission(result => {
      if (result) {
        cb();
      }
    });
  } catch (error) {
    console.log('utils/calendar/hasCalendarPermissions');
    console.log(error);
  }
}

export function checkCalendarPermissions() {
  try {
    window.plugins.calendar.hasReadWritePermission(result => {
      if (!result) {
        window.plugins.calendar.requestReadWritePermission();
      }
    });
  } catch (error) {
    console.log('utils/calendar/checkCalendarPermissions');
    console.log(error);
  }
}

export function buildEvent(classData) {
  const { date, duration, studio, modality, gym, fullName } = classData;
  const start = moment(date);

  const startDate = start.toDate();
  const endDate = start.add(duration, 'minutes').toDate();
  const title = `${modality ? modality.name : fullName.split(' ')[0]} - ${studio}`;
  const location = gym.name;

  return { startDate, endDate, title, location, notes: '' };
}

export function findEvent(event, onSuccess, onFailure) {
  const { startDate, endDate, title, location, notes } = event;
  try {
    window.plugins.calendar.findEvent(title, location, notes, startDate, endDate, events => {
      if (events && events.length > 0) {
        onSuccess(events);
      }
    });
  } catch (error) {
    if (onFailure) onFailure();
    return;
  }
  if (onFailure) onFailure();
}

export function addEvent(event, onSuccess, onFailure) {
  const { startDate, endDate, title, location, notes } = event;
  try {
    // TODO: we should create the event without interactivity, to prevent data changes
    window.plugins.calendar.createEventInteractively(
      title,
      location,
      notes,
      startDate,
      endDate,
      onSuccess,
      onFailure
    );
  } catch (error) {
    if (onFailure) onFailure();
  }
}

import validationConfigurations from '../config/validations';
import isDevelopmentMode from './development';

export const validateEmail = email => {
  if (isDevelopmentMode()) return true;
  const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  const valid = regex.test(String(email).toLowerCase());
  return valid;
};

export const validatePhone = phone => {
  if (isDevelopmentMode()) return true;
  const regex = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s./0-9]*$/;
  const valid = regex.test(String(phone));
  return valid;
};

export const validatePassword = password => {
  if (isDevelopmentMode()) return true;
  const {
    password: { minLength, requireLength, requireNumber, requireSymbol, requireCaps }
  } = validationConfigurations;

  if (requireLength) {
    const validLength = password.length >= minLength;
    if (!validLength) return false;
  }

  if (requireCaps) {
    const regex = /\w*[A-Z]/;
    const validCaps = regex.test(String(password));
    if (!validCaps) return false;
  }

  if (requireNumber) {
    const regex = /\w*[0-9]/;
    const validNumber = regex.test(String(password));
    if (!validNumber) return false;
  }

  if (requireSymbol) {
    const regex = /.*[^A-Za-z0-9].*/;
    const validSymbol = regex.test(String(password));
    if (!validSymbol) return false;
  }

  return true;
};

/* eslint-disable no-bitwise */
import getPixels from 'get-pixels';

// The darkness threshold, at which, if inferior
// than it, the colors are considered to be "dark"
const DARK_COLOR_THRESHOLD = 186;

// The rate at which we should ignore
// pixels to inspect and transform
//
// Ex: 4 = Accept 1 pixel in every 4 pixels in
// the image (ignore the next 3)
const PIXEL_IGNORE_RATE = 4;

/**
 * Converts hex color codes to a rgb object
 */
export function hexToRGB(hex) {
  const color = parseInt(hex.replace('#', ''), 16);

  return {
    red: (color >> 16) & 255,
    green: (color >> 8) & 255,
    blue: color & 255
  };
}

export function hexToCSS(hex, alpha = 1) {
  const color = hexToRGB(hex);
  return `rgba(${color.red}, ${color.green}, ${color.blue}, ${alpha})`;
}

/**
 * Converts a rgb color to the integer equivalent
 */
export function RGBToInt({ red, green, blue }) {
  return ((red & 0x0ff) << 16) | ((green & 0x0ff) << 8) | (blue & 0x0ff);
}

/**
 * Finds the average color in between two gradient points
 */
function blendColors(colorA, colorB) {
  const avg = (a, b) => {
    return (a + b) / 2;
  };

  const rgbA = hexToRGB(colorA);
  const rgbB = hexToRGB(colorB);

  return {
    red: avg(rgbA.red, rgbB.red),
    green: avg(rgbA.green, rgbB.green),
    blue: avg(rgbA.blue, rgbB.blue)
  };
}

/**
 * Gets the brightness level, the modifier values
 * are based on the W3C formula for calculating
 * the perceived brightness of a color
 *
 * Source: https://www.w3.org/TR/AERT/#color-contrast
 */
function getBrightness({ red, green, blue }) {
  return red * 0.299 + green * 0.587 + blue * 0.114;
}

/**
 * Gets the corresponding contrasting color
 * from a given two-point gradient.
 */
export function getContrastColor(gradient) {
  const blendedColor = blendColors(gradient.start, gradient.end);
  const brightness = getBrightness(blendedColor);

  return brightness < DARK_COLOR_THRESHOLD ? 'white' : '#282828';
}

export function isDark(imageUrl, callback) {
  // TODO: add result caching
  getImageAverageColor(
    imageUrl,
    avg => {
      callback(getBrightness(avg));
    },
    () => {
      callback(false);
    }
  );
}

/**
 * Calculates the average rgb color of
 * a given imageUrl
 */
function getImageAverageColor(imageUrl, onSuccess, onFailure) {
  getPixels(imageUrl, (err, pixels) => {
    if (err) {
      return onFailure(err);
    }

    const rgbArray = transformPixels(pixels.data);
    const average = getAverageColor(rgbArray);
    return onSuccess(average);
  });
}

/**
 * Calculates the average rgb color of
 * a given rgb color array
 */
function getAverageColor(rgbArray) {
  let count = 0;
  let totalRed = 0;
  let totalGreen = 0;
  let totalBlue = 0;

  rgbArray.forEach(({ red, green, blue }) => {
    count += 1;
    totalRed += red;
    totalGreen += green;
    totalBlue += blue;
  });

  return {
    red: totalRed / count,
    green: totalGreen / count,
    blue: totalBlue / count
  };
}

/**
 * Transforms an array of pixel data from the
 * get-pixels library into an array of rgb objects
 */
function transformPixels(pixelData) {
  const rgbArray = [];

  for (let i = 0; i < pixelData.length; i += 4 * PIXEL_IGNORE_RATE) {
    rgbArray.push({
      red: pixelData[i],
      green: pixelData[i + 1],
      blue: pixelData[i + 2]
    });
  }

  return rgbArray;
}

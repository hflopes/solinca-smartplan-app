export function mapStudioName(original) {
  if (!original || original.length === 0) {
    return 'N/A';
  }

  return capitalize(
    original
      .toLowerCase()
      .replace('estúdio', 'Est.')
      .replace('zona boxe', 'Z. Boxe')
      .replace('sala de exercício', 'Sala Ex.')
  );
}

export function escapeRegexCharacters(str) {
  return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}

export function capitalize(str) {
  return str
    .toLowerCase()
    .split(' ')
    .map(s => s.charAt(0).toUpperCase() + s.substring(1))
    .join(' ');
}

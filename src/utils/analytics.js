import { Capacitor } from '@capacitor/core';
import { FirebaseAnalytics } from '@capacitor-community/firebase-analytics';

/**
 * Send an app screen view to Firebase
 * @param {String} name Screen name
 */
export const screen = name => {
  if (Capacitor.platform === 'web') return;
  try {
    FirebaseAnalytics.setScreenName({ screenName: name });
    try {
      window.plugins.OneSignal.addTrigger('screen', name);
    } catch (error) {
      console.log('utils/analtyics/addTrigger');
      console.log(error);
    }
  } catch (error) {
    console.log('utils/analtyics/screen');
    console.log(error);
  }
};

/**
 * Send an app event to Firebase
 * @param {String} name Event name
 * @param {Object} [params] Event data
 */
export const event = (name, params = {}) => {
  if (Capacitor.platform === 'web') return;
  try {
    FirebaseAnalytics.logEvent({
      name,
      params
    });
    try {
      window.plugins.OneSignal.addTrigger('event', name);
    } catch (error) {
      console.log('utils/analtyics/addTrigger');
      console.log(error);
    }
  } catch (error) {
    console.log('utils/analtyics/event');
    console.log(error);
  }
};

/**
 * Setup userId for Firebase
 * @param {String} userId
 */
export const setupUserId = userId => {
  if (Capacitor.platform === 'web') return;
  try {
    FirebaseAnalytics.setUserId({ value: userId.toString() });
  } catch (error) {
    console.log('utils/analtyics/setupUserId');
    console.log(error);
  }
};

/**
 * Setup userProps for Firebase
 * @param {Object} userProp Stored in a key ->value format. Note: Google don't allow use of sensitive data like names, emails, card numbers, etc.
 * @param {String} userProps.key
 * @param {String} userProps.value
 */
export const setupUserProp = ({ key, value }) => {
  if (Capacitor.platform === 'web') return;
  try {
    FirebaseAnalytics.setUserProperty({ key: key.toString(), value: value.toString() });
  } catch (error) {
    console.log('utils/analtyics/setupUserProp');
    console.log(error);
  }
};

/**
 * Setup userId or/and userProps for Firebase
 * @param {String} userId
 * @param {Array} [userProps] Array containing all the userProp information
 * @param {Object} userProp Stored in a key ->value format. Note: Google don't allow use of sensitive data like names, emails, card numbers, etc.
 * @param {String} userProps.key
 * @param {String} userProps.value
 * Google don't allow use of sensitive data like names, emails, card numbers, etc.
 */
export const setup = ({ userId, userProps = [] }) => {
  if (Capacitor.platform === 'web') return;
  if (userId) {
    setupUserId(userId);
  }

  if (userProps && userProps.length) {
    userProps.map(({ key, value }) => {
      if (key && value) {
        return setupUserProp({ key, value });
      }
      console.log('UserProp format incorrect. Need key and value');
      return null;
    });
  }
};

import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import ptPT from './locales/pt-PT';

const defaultLanguage = 'pt';

const resources = {
  pt: ptPT
};

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources,
    lng: defaultLanguage,

    keySeparator: '.',

    interpolation: {
      escapeValue: false // react already safes from xss
    }
  });

export default i18n;

module.exports = {
  root: {
    blacklist: [
      'app',
      'alerts',
      'device',
      'reload',
      'toasts',
      'register',
      'token',
      'resetPassword',
      'support',
      'complaints',
      'login',
      'challenges',
      'badges',
      'rewards',
      'dashboard',
      'classes',
      'gyms',
      'modalities',
      'user',
      'plan',
      'search',
      'campaigns',
      'snackbar',
      'terms'
    ]
  },
  modules: {
    app: {
      whitelist: ['language', 'routeStateStack', 'currentRoute', 'version', 'apiURL']
    },
    login: {
      whitelist: [
        'leaseToken',
        'leaseExpiraton',
        'isLoggedIn',
        'isCanceledOrSuspended',
        'subscriptionType',
        'restrictionType',
        'isNewUser',
        'showOnboarding',
        'user'
      ]
    },
    classes: {
      whitelist: ['upcoming', 'favorites', 'booked', 'alert', 'favoriteClasses', 'reviewClasses']
    },
    gyms: {
      whitelist: ['selectedGymId', 'data']
    },
    modalities: {
      whitelist: ['data']
    },
    search: {
      whitelist: ['recentQueries']
    },
    user: {
      whitelist: ['data', 'notifications', 'url3dBody']
    },
    plan: {
      whitelist: [
        'hasFetched',
        'currentPlan',
        'currentSession',
        'data',
        'setup',
        'hasVisited',
        'plans',
        'planError'
      ]
    },
    campaigns: {
      whitelist: ['data']
    },
    reload: {
      whitelist: ['lastUpdates']
    },
    challenges: {
      whitelist: ['data']
    },
    complaints: {
      whitelist: ['data']
    },
    badges: {
      whitelist: ['data']
    },
    rewards: {
      whitelist: ['data', 'cart']
    },
    dashboard: {
      whitelist: [
        'classesHistory',
        'plansHistory',
        'evaluationsHistory',
        'seenFatGraph',
        'seenIMCGraph',
        'seenWeightGraph'
      ]
    }
  }
};

import axios from 'axios';

// export const URLS = ['https://appapimemory.scfitness.ga/']; // DEBUG
// export const URLS = ['https://5d5cc9871a42.ngrok.io/']; // LOCAL
// export const URLS = ['https://appapi.solinca.ga/', 'https://appapilight.solinca.ga/']; // QA
export const URLS = ['https://appapi.scfitness.ga/', 'https://appapilight.scfitness.ga/']; // PROD

let URL = URLS[0];
const VERSION = 'v1';

export function setURL(url) {
  URL = url;
}

export function baseUrl() {
  return `${URL}${VERSION}`;
}

export default function() {
  return {
    // auth
    login: `${baseUrl()}/session/login/`,
    logout: `${baseUrl()}/session/logout/`,
    sync: `${baseUrl()}/session/sync/`,
    register: `${baseUrl()}/session/register/`,
    verifyToken: `${baseUrl()}/session/verify/`,
    resetPassword: `${baseUrl()}/session/recoverPassword/`,

    // classes
    fetchUpcomingClasses: `${baseUrl()}/classes/upcoming/`,
    fetchFavoriteClasses: `${baseUrl()}/classes/favorites/`,
    fetchBookedClasses: `${baseUrl()}/classes/booked/`,
    fetchReviewClasses: `${baseUrl()}/classes/lastClassesNotRatted/`,
    reviewClass: `${baseUrl()}/classes/rateClass`,
    fetchSearch: `${baseUrl()}/classes/upcomingGrouped`,
    bookClass: `${baseUrl()}/classes/book`,
    cancelClass: `${baseUrl()}/classes/cancel`,
    toggleClassFavorite: `${baseUrl()}/classes/favorites`,
    terms: `${baseUrl()}/terms`,
    acceptTerms: `${baseUrl()}/terms/read`,

    // others
    fetchModalities: `${baseUrl()}/classes/modalities/`,
    fetchPlan: `${baseUrl()}/plans/list`,
    fetchFullPlan: `${baseUrl()}/plans/listComplete`,
    fetchGyms: `${baseUrl()}/gyms/list`,
    fetchCampaign: `${baseUrl()}/campaigns/`,
    fetchUserData: `${baseUrl()}/users/details`,
    fetchUserTags: `${baseUrl()}/users/tags`,
    // fetchLastReceipt: `${baseUrl()}/users/documentLast`,
    fetchReceiptsList: `${baseUrl()}/users/documentList`,
    fetch3dBody: `${baseUrl()}/plan/3dbody`,

    // update user data
    updateUserDetails: `${baseUrl()}/users/details`,
    updateUserPhoto: `${baseUrl()}/users/updatePhoto`,
    updateUserPassword: `${baseUrl()}/session/changePassword`,

    // support
    sendLead: `${baseUrl()}/support/lead`,
    sendTicket: `${baseUrl()}/support/ticket`,
    sendNutrition: `${baseUrl()}/support/nutrition`,
    complaints: `${baseUrl()}/support/complaints`,
    complaintsRead: `${baseUrl()}/support/complaints/read`,
    renewPlan: `${baseUrl()}/support/requestPlan`,

    // data collection
    dataCompleteExercise: `${baseUrl()}/plans/completeExercise`,

    // gamification
    challenges: `${baseUrl()}/gamification/challenges`,
    challenge: `${baseUrl()}/gamification/challenge`,
    joinChallenge: `${baseUrl()}/gamification/join`,
    leaveChallenge: `${baseUrl()}/gamification/leave`,
    badges: `${baseUrl()}/gamification/userbadges`,
    rewards: `${baseUrl()}/gamification/rewards`,
    rewardPurchases: `${baseUrl()}/gamification/rewards/purchased`,
    redeemReward: `${baseUrl()}/gamification/rewards/redeem`,

    // dashboard
    fetchClassesHistory: `${baseUrl()}/classes/history`,
    fetchPlansHistory: `${baseUrl()}/plans/history`,
    fetchEvaluationsHistory: `${baseUrl()}/plans/historyEvaluation`,

    // notifications
    notifications: `${baseUrl()}/notifications/config/`,
    // GET /notifications/config/ - buscar
    // PUT /notifications/config/ - buscar

    ping: `${baseUrl()}/test/ping`
  };
}

export function updateToken(token) {
  axios.defaults.headers.common.token = token;
}

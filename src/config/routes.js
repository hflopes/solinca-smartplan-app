import Index from '../screens/general/Index';
import NotFound from '../screens/general/NotFound';
import Home from '../screens/general/Home';
import Welcome from '../screens/general/Welcome';
import TermsOfService from '../screens/general/TermsOfService';
import OnBoarding from '../screens/general/OnBoarding';

import Login from '../screens/authentication/Login';
import LoginRedirect from '../screens/authentication/LoginRedirect';
import Logout from '../screens/authentication/Logout';
import Register from '../screens/authentication/Register';
import ResetPassword from '../screens/authentication/ResetPassword';
import Token from '../screens/authentication/Token';

import ClassesHome from '../screens/classes/Home';
import ClassDetails from '../screens/classes/ClassDetails';
import BecomePremium from '../screens/general/BecomePremium';
import ModalityDetails from '../screens/classes/ModalityDetails';
import Search from '../screens/classes/Search';

import Profile from '../screens/profile/Profile';
import ComplaintDetails from '../screens/profile/Complaint';
import List from '../screens/profile/List';
import EditProfile from '../screens/profile/Edit';
import Statuses from '../screens/profile/Statuses';
import PDF3dBody from '../screens/profile/3dBody';

import PlanWelcome from '../screens/plan/Welcome';
import PlanError from '../screens/plan/Error';
import PlanQuestionnaire from '../screens/plan/Questionnaire';
import PlanSelectExerciseType from '../screens/plan/SelectExerciseType';
import PlanClasses from '../screens/plan/SelectClasses';
import PlanConfirmaton from '../screens/plan/Confirmation';
import PlanHome from '../screens/plan/Home';
import PlanFinish from '../screens/plan/Finish';
import PlanRenew from '../screens/plan/Renew';
import ExerciseDetails from '../screens/plan/ExerciseDetails';

import Gamification from '../screens/gamification/Home';
import GamificationChallengeDetails from '../screens/gamification/ChallengeDetails';
import GamificationCatalogFilters from '../screens/gamification/CatalogFilters';
import GamificationRewardDetails from '../screens/gamification/RewardDetails';

import Support from '../screens/support/Contact';
import Lead from '../screens/support/Lead';

import Terms from '../screens/general/Terms';

import About from '../screens/about/About';
import Blog from '../screens/about/Blog';
import AboutGyms from '../screens/about/Gyms';
import AboutGymDetails from '../screens/about/GymDetails';

import DashboardCalendar from '../screens/dashboard/Calendar';
import DashboardEvolution from '../screens/dashboard/Evolution';

import Nutrition from '../screens/nutrition/Nutrition';

const routes = {
  index: {
    path: '/',
    component: Index
  },
  home: {
    path: '/home',
    component: Home,
    auth: true
  },
  termsOfService: {
    path: '/terms-of-service',
    component: TermsOfService,
    auth: true
  },
  terms: {
    path: '/terms',
    component: Terms
  },
  becomePremium: {
    path: '/become-premium',
    component: BecomePremium,
    auth: true
  },
  welcome: {
    path: '/welcome',
    component: Welcome
  },
  login: {
    path: '/login',
    component: Login
  },
  logout: {
    path: '/logout',
    component: Logout
  },
  loginRedirect: {
    path: '/login-redirect',
    component: LoginRedirect,
    auth: true
  },
  register: {
    path: '/register',
    component: Register
  },
  leadGym: {
    path: '/leads/:gymId',
    component: Lead
  },
  lead: {
    path: '/leads/',
    component: Lead
  },
  resetPassword: {
    path: '/reset-password',
    component: ResetPassword
  },
  token: {
    path: '/token',
    component: Token
  },
  onBoarding: {
    path: '/on-boarding',
    component: OnBoarding,
    auth: true
  },
  classes: {
    path: '/classes',
    component: ClassesHome,
    auth: true
  },
  modalityDetails: {
    path: '/classes/modalities/:id',
    component: ModalityDetails,
    auth: true
  },
  classDetails: {
    path: '/classes/:id',
    component: ClassDetails,
    auth: true
  },
  search: {
    path: '/search',
    component: Search,
    auth: true
  },
  profile: {
    path: '/profile',
    component: Profile,
    auth: true
  },
  editProfile: {
    path: '/profile/edit/:type',
    component: EditProfile,
    auth: true
  },
  pdf3dBody: {
    path: '/3dbody',
    component: PDF3dBody
  },
  complaint: {
    path: '/profile/complaints/:id',
    component: ComplaintDetails,
    auth: true
  },
  list: {
    path: '/profile/list/:type',
    component: List,
    auth: true
  },
  statuses: {
    path: '/profile/statuses',
    component: Statuses,
    auth: true
  },
  exerciseDetails: {
    path: '/plan/exercise/:id',
    component: ExerciseDetails,
    auth: true
  },
  plan: {
    path: '/plan/',
    component: PlanHome,
    auth: true
  },
  planError: {
    path: '/plan/error',
    component: PlanError,
    auth: true
  },
  planWelcome: {
    path: '/plan/welcome',
    component: PlanWelcome,
    auth: true
  },
  planQuestionnaire: {
    path: '/plan/questionnaire',
    component: PlanQuestionnaire,
    auth: true
  },
  planSelectExercises: {
    path: '/plan/select/exercise-type',
    component: PlanSelectExerciseType,
    auth: true
  },
  planClasses: {
    path: '/plan/select/classes',
    component: PlanClasses,
    auth: true
  },
  planConfirmation: {
    path: '/plan/confirmation',
    component: PlanConfirmaton,
    auth: true
  },
  planFinish: {
    path: '/plan/finish',
    component: PlanFinish,
    auth: true
  },
  planRenwew: {
    path: '/plan/renew',
    component: PlanRenew,
    auth: true
  },
  gamification: {
    path: '/gamification/:section?',
    component: Gamification,
    auth: true
  },
  gamificationCatalogFilters: {
    path: '/gamification/catalog/filters',
    component: GamificationCatalogFilters,
    auth: true
  },
  gamificationChallenge: {
    path: '/gamification/challenge/:id',
    component: GamificationChallengeDetails,
    auth: true
  },
  gamificationReward: {
    path: '/gamification/reward/:id',
    component: GamificationRewardDetails,
    auth: true
  },
  support: {
    path: '/support/contact',
    component: Support
  },
  about: {
    path: '/about',
    component: About
  },
  blog: {
    path: '/about/blog',
    component: Blog
  },
  aboutGyms: {
    path: '/about/gyms',
    component: AboutGyms
  },
  aboutGymDetails: {
    path: '/about/gyms/:id',
    component: AboutGymDetails
  },
  dashboardCalendar: {
    path: '/dashboard/calendar',
    component: DashboardCalendar
  },
  dashboardEvolution: {
    path: '/dashboard/evolution/:metric',
    component: DashboardEvolution
  },
  nutrition: {
    path: '/nutrition',
    component: Nutrition,
    auth: true
  },
  pageNotFound: {
    // If no other route is rendered,
    // render the 404 - not found page
    uniqueKey: '404',
    component: NotFound
  }
};

export default routes;

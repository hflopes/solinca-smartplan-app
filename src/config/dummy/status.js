module.exports = {
  active: {
    emailExists: true,
    subActive: true,
    isRegistered: true
  },
  lead: {
    emailExists: false
  },
  noSubscription: {
    emailExists: true,
    subActive: false
  },
  notRegistered: {
    emailExists: true,
    subActive: true,
    isRegistered: false
  }
};

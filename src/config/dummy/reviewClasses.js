export default [
  {
    bookingId: null,
    cancelId: null,
    date: new Date('2020-07-28 07:15'),
    dateStart: new Date('2020-07-28 07:15'),
    duration: 15,
    fullName: 'ERM - ABS 3ª 07H45',
    gymId: '16',
    id: '8e2b0ccc134b76715e7406ed217c1e3c',
    instructor: '',
    isAlert: false,
    isAvailable: false,
    isBooked: false,
    isFavorite: true,
    modalityId: 'de58fcdf-20cb-4eb4-9218-2aebcc6a088f',
    studio: 'N/A'
  },
  {
    bookingId: null,
    cancelId: null,
    date: new Date('2020-12-02 07:15'),
    dateStart: new Date('2020-12-02 07:15'),
    duration: 15,
    fullName: 'ERM - YOGA 3ª 07H45',
    gymId: '16',
    id: 'cdca3876-34a6-11eb-adc1-0242ac120002',
    instructor: '',
    isAlert: false,
    isAvailable: false,
    isBooked: false,
    isFavorite: true,
    modalityId: '06b3e9e8-0622-426f-b70e-03b7038a0511',
    studio: 'N/A'
  }
];

module.exports = {
  token: '123456QWERTY',
  email: 'luiz.ottino@comon.pt',
  name: 'Luiz Ottino',
  photo: null,
  phone: '210 000 000',
  address: 'Rua 3 da Matinha',
  memberNumber: 123456,
  birthdate: '30/02/1986',
  nif: 987654321,
  position: {
    lat: 38.747079,
    lon: -9.098122
  },
  premium: false
};

export default [
  {
    challengeDh__oid: 81,
    userId: 'dd693d0e-c0ec-4b07-9d85-72fdaead8b93',
    percentage: 100.0,
    dateBegin: '2020-04-20T00:00:00',
    dateEnd: '2020-05-20T00:00:00',
    dateLastUpdate: '2020-04-22T10:00:38',
    badges: '1',
    active: 0,
    ranking: 80
  },
  {
    challengeDh__oid: 202,
    userId: 'dd693d0e-c0ec-4b07-9d85-72fdaead8b93',
    percentage: 100.0,
    dateBegin: '2020-04-20T00:00:00',
    dateEnd: '2020-05-20T00:00:00',
    dateLastUpdate: '2020-04-22T10:07:33',
    badges: '1',
    active: 0,
    ranking: 10
  }
];

export default {
  '95ac5eea-b104-4168-913a-ebc0c2b65119': {
    id: '95ac5eea-b104-4168-913a-ebc0c2b65119',
    modality: {
      id: '10369e95-a841-4be2-a50d-417d9c098127',
      name: 'PILATES',
      intensity: 3
    },
    studio: 3,
    date: new Date('2019-08-28 11:30'),
    duration: 55,
    isAvailable: false,
    isBooked: false,
    isAlert: false,
    isFavorite: false,
    isScheduled: false,
    gymId: '710ea381-eef3-4ecc-bd8a-c88b8d27fa41'
  },
  'c92b8b03-49f7-4973-b5be-d3f0bf34dbc2': {
    id: 'c92b8b03-49f7-4973-b5be-d3f0bf34dbc2',
    modality: {
      id: '0229975b-ea85-40ff-9608-b8cb2fd12c52',
      name: 'BODY PUMP',
      intensity: 4
    },
    studio: 5,
    date: new Date('2019-08-28 11:30'),
    duration: 25,
    isAvailable: true,
    isBooked: false,
    isAlert: false,
    isFavorite: true,
    isScheduled: false,
    gymId: '117d2f5d-5366-4699-9c35-49cb05fbf949'
  },
  '85f5cc30-964a-4b68-97c0-1b55c94afd74': {
    id: '85f5cc30-964a-4b68-97c0-1b55c94afd74',
    modality: {
      id: '10369e95-a841-4be2-a50d-417d9c098127',
      name: 'PILATES',
      intensity: 3
    },
    studio: 2,
    date: new Date('2019-11-07 16:15'),
    duration: 40,
    isAvailable: true,
    isBooked: true,
    isAlert: false,
    isFavorite: true,
    isScheduled: false,
    gymId: '710ea381-eef3-4ecc-bd8a-c88b8d27fa41'
  },
  'f7759321-db2e-431b-a756-ff6b38e81e4e': {
    id: 'f7759321-db2e-431b-a756-ff6b38e81e4e',
    modality: {
      id: '10369e95-a841-4be2-a50d-417d9c098127',
      name: 'PILATES',
      intensity: 3
    },
    studio: 2,
    date: new Date('2019-09-23 13:30'),
    duration: 35,
    isAvailable: true,
    isBooked: true,
    isAlert: false,
    isFavorite: true,
    isScheduled: false,
    gymId: 'c14593fa-b5df-4040-8222-13b54b5d9280'
  }
};

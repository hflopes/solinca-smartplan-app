export default {
  ok: true,
  results: {
    82: [
      {
        id: '9f90fdd31b63a4df0467f3c466f87a5f',
        bookingId: null,
        cancelId: null,
        gymId: '82',
        modalityId: '1b46a044-5b20-4213-aff4-dabc1dec77cb',
        fullName: 'TRX 2F 12:40',
        studio: '',
        isAvailable: false,
        isAlert: false,
        isFavorite: false,
        dateStart: '2019-09-02T07:15:00',
        duration: 50
      },
      {
        id: '2def99f0bfefe36279f037efca28ed5b',
        bookingId: 'c60f4da9-6e39-499e-9b79-b6acf2b8ea47',
        cancelId: null,
        gymId: '82',
        modalityId: '1b46a044-5b20-4213-aff4-dabc1dec77cb',
        fullName: 'TRX 2F 12:40',
        studio: '',
        isAvailable: true,
        isAlert: false,
        isFavorite: false,
        dateStart: '2019-09-02T19:05:00',
        duration: 30
      }
    ],
    91: [
      {
        id: '062dcba24202600fddca1532106b9bf7',
        bookingId: '8f891206-923b-4110-a715-480523b2a9ac',
        cancelId: 'c685a3cb-7c82-4db8-9df3-37aa74f86c87',
        gymId: '91',
        modalityId: '1b46a044-5b20-4213-aff4-dabc1dec77cb',
        fullName: 'TRX 2F 12:40',
        studio: 'Estúdio 2',
        isAvailable: true,
        isAlert: false,
        isFavorite: false,
        dateStart: '2019-09-02T19:10:00',
        duration: 30
      }
    ]
  }
};

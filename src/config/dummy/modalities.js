export default {
  '10369e95-a841-4be2-a50d-417d9c098127': {
    id: '10369e95-a841-4be2-a50d-417d9c098127',
    name: 'PILATES',
    intensity: 3
  },
  '0229975b-ea85-40ff-9608-b8cb2fd12c52': {
    id: '0229975b-ea85-40ff-9608-b8cb2fd12c52',
    name: 'BODY PUMP',
    intensity: 4
  },
  'bbf613e7-a0af-45e0-ac35-e232e879ee89': {
    id: 'bbf613e7-a0af-45e0-ac35-e232e879ee89',
    name: 'BODY BALANCE',
    intensity: 5
  },
  'd82fd007-f3a9-4e5e-9cb4-86636bf36859': {
    id: 'd82fd007-f3a9-4e5e-9cb4-86636bf36859',
    name: 'YOGA',
    intensity: 3
  },
  '62f43a92-0337-4f01-b4e0-dec7a8acd016': {
    id: '62f43a92-0337-4f01-b4e0-dec7a8acd016',
    name: 'BODY COMBAT',
    intensity: 2
  },
  '5939eb4a-48ab-4315-8134-f597e1088da3': {
    id: '5939eb4a-48ab-4315-8134-f597e1088da3',
    name: 'RPM',
    intensity: 4
  }
};

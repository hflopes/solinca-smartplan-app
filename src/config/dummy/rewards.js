export default [
  // {
  //   id: 'df8a93e8-4c08-410b-a0c5-cac086b1d373',
  //   name: 'Garrafa Bobble Carry',
  //   description: 'Garrafa BOBBLE Carry Cap (50 cl - Com filtro - Rosa)',
  //   characteristics: 'Plástico BPA Free',
  //   image: 'https://www.worten.pt/i/1739bfc89a9ef1bdfe006a86bd9a693b6c38026b.jpg',
  //   category: 'Acessórios',
  //   brand: {
  //     name: 'Sport Zone',
  //     image:
  //       'https://www.marshopping.com/matosinhos/-/media/Images/B2C/Portugal/Matosinhos/Images-Stores/Sport-Zone/Sport_Zone_logo.ashx?h=282&iar=0&mw=650&w=410&hash=D1472651D789E27E398FF9A537AE4122'
  //   },
  //   points: 125,
  //   datePublished: new Date('11-30-2020'),
  //   dateStart: new Date('12-06-2020'),
  //   dateEnd: new Date('01-6-2021')
  // },
  // {
  //   id: '5030c3bc-6eb2-4814-8798-4904537a04d6',
  //   name: 'Tapete de Yoga Yesfit Roxo',
  //   description: 'Tapete de Yoga Yesfit Roxo - Algodão',
  //   characteristics: 'Algodão',
  //   image: 'https://www.worten.pt/i/ac97737abed841cb4bc5600a77c33cbb31a7e273.jpg',
  //   category: 'Acessórios',
  //   brand: {
  //     name: 'Modalfa',
  //     image: 'https://upload.wikimedia.org/wikipedia/commons/d/d0/Mo.png'
  //   },
  //   points: 500,
  //   datePublished: new Date('04-11-2020'),
  //   dateStart: new Date('12-06-2020'),
  //   dateEnd: new Date('01-6-2021')
  // },
  // {
  //   id: '2768ae3d-0af4-44d9-9b2f-e9ea9714c48a',
  //   name: 'Tenis Adidas Core Black',
  //   description: 'Tenis Adidas Core Black',
  //   characteristics: '',
  //   image:
  //     'https://assets.adidas.com/images/w_600,f_auto,q_auto/12bddf80db0e417498daaa9b01521627_9366/Tenis_Energyfalcon_Preto_EE9863_01_standard.jpg',
  //   category: 'Calçado',
  //   brand: {
  //     name: 'Sport Zone',
  //     image:
  //       'https://www.marshopping.com/matosinhos/-/media/Images/B2C/Portugal/Matosinhos/Images-Stores/Sport-Zone/Sport_Zone_logo.ashx?h=282&iar=0&mw=650&w=410&hash=D1472651D789E27E398FF9A537AE4122'
  //   },
  //   points: 500,
  //   datePublished: new Date('03-12-2020'),
  //   dateStart: new Date('12-06-2020'),
  //   dateEnd: new Date('01-6-2021')
  // }
];

export default {
  heartRateRest: '56',
  heartRateActive: '126-139',
  bloodPreassure: '118-69',
  idealWeight: '70.15 a 0',
  fatExcess: '12.25 a 12.25',
  fatPercentage: '23.48%',
  weight: '82.40',
  falta: '12.25 a 12.25',
  bmi: '26.60'
};

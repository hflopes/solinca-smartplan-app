export default [
  {
    id: '1',
    title: 'Solinca Laranjeiras',
    date: new Date(2020, 0, 1),
    message: `Lorem ipsum dolor sit amet, consectetur adipiscing elit.
       Maecenas vel pellentesque massa, eu tem`,
    isUnread: true,
    isReplied: true
  },
  {
    id: '2',
    title: 'Solinca Colombo',
    date: new Date(2020, 3, 1),
    message: `Lorem ipsum dolor sit amet, consectetur adipiscing elit.
       Maecenas vel pellentesque massa, eu tem`,
    isReplied: true,
    isUnread: false
  },
  {
    id: '3',
    title: 'Solinca Laranjeiras',
    date: new Date(2020, 2, 1),
    message: `Lorem ipsum dolor sit amet, consectetur adipiscing elit.
       Maecenas vel pellentesque massa, eu tem`,
    isReplied: false,
    isUnread: false
  }
];

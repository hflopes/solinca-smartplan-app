module.exports = [
  {
    label: 'Março 2019',
    url: 'https://gitlab.com'
  },
  {
    label: 'Fevereiro 2019',
    url: 'https://gitlab.com'
  },
  {
    label: 'Janeiro 2019',
    url: 'https://gitlab.com'
  },
  {
    label: 'Dezembro 2018',
    url: 'https://gitlab.com'
  },
  {
    label: 'Novembro 2018',
    url: 'https://gitlab.com'
  },
  {
    label: 'Outubro 2018',
    url: 'https://gitlab.com'
  },
  {
    label: 'Setembro 2018',
    url: 'https://gitlab.com'
  },
  {
    label: 'Agosto 2018',
    url: 'https://gitlab.com'
  },
  {
    label: 'Julho 2018',
    url: 'https://gitlab.com'
  },
  {
    label: 'Junho 2018',
    url: 'https://gitlab.com'
  },
  {
    label: 'Maio 2018',
    url: 'https://gitlab.com'
  }
];

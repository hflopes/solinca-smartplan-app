module.exports = {
  NOT_REGISTERED: {
    isRegistered: false,
    isActive: false
  },
  REGISTERED_NOT_ACTIVE: {
    isRegistered: true,
    isActive: false
  },
  REGISTERED: {
    isRegistered: true,
    isActive: true
  }
};

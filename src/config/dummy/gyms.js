module.exports = {
  '710ea381-eef3-4ecc-bd8a-c88b8d27fa41': {
    id: '710ea381-eef3-4ecc-bd8a-c88b8d27fa41',
    name: 'Laranjeiras',
    pos: {
      lat: 38.7541994,
      lon: -9.1690555
    }
  },
  'c14593fa-b5df-4040-8222-13b54b5d9280': {
    id: 'c14593fa-b5df-4040-8222-13b54b5d9280',
    name: 'Loures',
    pos: {
      lat: 38.8334012,
      lon: -9.1588587
    }
  },
  'd7535887-b137-4b45-854b-593bd46f894b': {
    id: 'd7535887-b137-4b45-854b-593bd46f894b',
    name: 'Oeiras',
    pos: {
      lat: 38.6865752,
      lon: -9.3254416
    }
  },
  '117d2f5d-5366-4699-9c35-49cb05fbf949': {
    id: '117d2f5d-5366-4699-9c35-49cb05fbf949',
    name: 'Vasco da Gama',
    pos: {
      lat: 38.7671358,
      lon: -9.0998541
    }
  },
  '117d2f5d-5366-4699-9c35-49cb05fbf949123': {
    id: '117d2f5d-5366-4699-9c35-49cb05fbf949123',
    name: 'Braga',
    pos: {
      lat: 41.527887,
      lon: -8.448889
    }
  }
};

module.exports = {
  toast: {
    initialState: {
      visible: false,
      toast: {
        title: '',
        message: '',
        duration: 0,
        type: ''
      }
    }
  },
  login: {
    variables: {
      success: {
        email: 'uilque.ferreira@comon.pt',
        password: 123
      },
      failure: {
        email: 'emailquenaoexiste@dominioquenaoexiste.com',
        password: 1
      },
      canceled: {
        email: 'joao.capelo@comon.pt',
        password: 123
      }
    },
    errors: {
      login: 8001
    },
    initialState: {
      isLoggingIn: false,
      isSyncing: false,
      isLoggedIn: false,
      isCanceledOrSuspended: false,
      isNewUser: false,
      shouldReadTos: false,
      hasSynced: false,
      leaseToken: null,
      restrictionType: '',
      subscriptionType: '',
      user: null,
      error: null,
      tries: 0,
      waitingReset: false
    },
    properties: {
      request: ['isCanceledOrSuspended', 'isLoggedIn', 'isNewUser', 'leaseToken', 'shouldReadTos', 'user'],
      user: ['address', 'birthDate', 'defaultGym', 'email', 'memberNumber', 'name', 'nif', 'phone']
    }
  },
  register: {
    variables: {
      failure: {
        email: 'emailquenaoexiste@dominioquenaoexiste.com',
        password: 1,
        passwordConfirmation: 2
      },
      success: {
        email: 'emailqueexiste@dominioqueexiste.com',
        password: '12345',
        passwordConfirmation: '12345'
      }
    },
    initialState: {
      isRegistering: false,
      hasRequested: false,
      isActive: false,
      isRegistered: false,
      error: null,
      email: '',
      password: ''
    }
  },
  resetPassword: {
    initialState: {
      isResetting: false,
      hasRequestedReset: false,
      isActive: false,
      isRegistered: false,
      error: null,
      email: '',
      password: ''
    },
    variables: {
      failure: {
        email: 'emailquenaoexiste@dominioquenaoexiste.com',
        password: 1,
        passwordConfirmation: 2
      },
      success: {
        email: 'joao.pereira@comon.pt',
        password: '123',
        passwordConfirmation: '123'
      }
    }
  },
  token: {
    initialState: {
      isVerifyingToken: false,
      hasRequested: false,
      isTokenVerified: false,
      error: null
    },
    errors: {
      invalidToken: 8002
    }
  },
  classes: {
    variables: {
      book: {
        bookingId: 'YY',
        gymId: 'XX',
        isBooked: false,
        date: new Date()
      },
      cancel: {
        id: 'ZYX',
        cancelId: 'YY',
        gymId: 'XX'
      }
    },
    errors: {
      booked: 6001,
      notBooked: 6002,
      premium: 6003,
      auth: 1000
    },
    initialState: {
      isFetchingUpcoming: false,
      isFetchingFavorites: false,
      isFetchingBooked: false,
      isBooking: false,
      isCanceling: false,
      isTogglingFavorite: false,
      isTogglingAlert: false,
      error: null,
      upcoming: {},
      favorites: {},
      booked: {},
      alert: [],
      favoriteClasses: []
    },
    properties: [
      'bookingId',
      'cancelId',
      'date',
      'dateStart',
      'duration',
      'fullName',
      'gymId',
      'id',
      'isAlert',
      'isAvailable',
      'isBooked',
      'isFavorite',
      'modalityId',
      'studio'
    ]
  },
  modalities: {
    initialState: {
      isFetching: false,
      error: null,
      data: {}
    },
    properties: [
      'benefits',
      'category',
      'chooseYourGoal',
      'color1',
      'color2',
      'description',
      'id',
      'intensity',
      'name',
      'photoUrl'
    ]
  },
  gyms: {
    initialState: {
      isFetching: false,
      error: null,
      selectedGymId: null,
      data: {}
    },
    properties: {
      request: [
        'address',
        'coordinates',
        'email',
        'id',
        'initials',
        'name',
        'observations',
        'phone',
        'postalCode',
        'timeSchedule',
        'website'
      ],
      coordinates: ['latitude', 'longitude']
    }
  },
  app: {
    initialState: {
      language: 'pt-PT',
      componentStateStack: {},
      fetchedInitialData: false,
      isFetchingInitialData: false,
      errorFetchingInitialData: false,
      error: null,
      offline: false,
      isMenuOpen: false,
      loadingModules: [],
      platform: '',
      backNavigationCallback: null
    }
  },
  campaigns: {
    initialState: {
      isFetching: false,
      data: {
        title: '',
        description: '',
        highlightText: '',
        fileUrl: ''
      }
    },
    properties: {
      request: ['title', 'description', 'highlightText', 'fileUrl']
    }
  },
  support: {
    initialState: {
      isSendingTicket: false,
      isSendingLead: false,
      hasSentLead: false,
      hasSentTicket: false,
      error: null,
      form: {}
    },
    variables: {
      email: 'emailquenaoexiste@dominioquenaoexiste.com',
      name: 'Nome',
      phone: '210 000 000',
      gym: {
        id: 'XX'
      },
      gymId: 'XX',
      message: 'Commodo sunt in irure occaecat ex nisi nisi pariatur in eiusmod.',
      motive: 'ABC'
    }
  }
};

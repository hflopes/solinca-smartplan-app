export const DEFAULT_MODALITY = {
  imageUrl: '/img/backgrounds/modality.jpg',
  colorScheme: {
    start: '#62DEA8',
    end: '#a9cf8f'
  }
};

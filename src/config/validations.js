module.exports = {
  password: {
    retries: 9,
    minLength: 3,
    requireLength: true,
    requireCaps: false,
    requireNumber: false,
    requireSymbol: false
  }
};

module.exports = [
  {
    label: 'Facebook',
    url: 'https://pt-pt.facebook.com/Solinca',
    img: '/img/icons/facebook.svg'
  },
  {
    label: 'Instagram',
    url: 'https://www.instagram.com/solincahf/',
    img: '/img/icons/instagram.svg'
  },
  {
    label: 'Youtube',
    url: 'https://www.youtube.com/channel/UCr59qpthBNzx_Tm-jI7aP-Q',
    img: '/img/icons/youtube.svg'
  }
];

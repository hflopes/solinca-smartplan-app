import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import reducers from './reducers';
import persistors from '../config/persistors';
import { dateTransformPersistor } from '../utils/dates';

const hasPersistConfiguration = module => {
  if (persistors.modules[module]) {
    return persistReducer(
      {
        key: module,
        storage,
        transforms: [dateTransformPersistor],
        ...persistors.modules[module]
      },
      reducers[module]
    );
  }

  return reducers[module];
};

export default combineReducers({
  app: hasPersistConfiguration('app'),
  device: hasPersistConfiguration('device'),
  alerts: hasPersistConfiguration('alerts'),
  toasts: hasPersistConfiguration('toasts'),
  snackbar: hasPersistConfiguration('snackbar'),
  register: hasPersistConfiguration('register'),
  token: hasPersistConfiguration('token'),
  login: hasPersistConfiguration('login'),
  resetPassword: hasPersistConfiguration('resetPassword'),
  challenges: hasPersistConfiguration('challenges'),
  badges: hasPersistConfiguration('badges'),
  dashboard: hasPersistConfiguration('dashboard'),
  classes: hasPersistConfiguration('classes'),
  gyms: hasPersistConfiguration('gyms'),
  modalities: hasPersistConfiguration('modalities'),
  user: hasPersistConfiguration('user'),
  plan: hasPersistConfiguration('plan'),
  support: hasPersistConfiguration('support'),
  complaints: hasPersistConfiguration('complaints'),
  search: hasPersistConfiguration('search'),
  campaigns: hasPersistConfiguration('campaigns'),
  reload: hasPersistConfiguration('reload'),
  terms: hasPersistConfiguration('terms'),
  rewards: hasPersistConfiguration('rewards')
});

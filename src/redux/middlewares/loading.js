import { addLoading, removeLoading } from '../modules/app/actions/toggleLoading';
import { RELOAD_DECLINED } from '../reload/reducer';
import {
  FETCH_UPCOMING_CLASSES_SUCCESS,
  FETCH_UPCOMING_CLASSES_FAILURE,
  FETCH_CLASSES_BOOKED_SUCCESS,
  FETCH_CLASSES_BOOKED_FAILURE,
  BOOK_CLASS_REQUEST,
  BOOK_CLASS_FAILURE,
  CANCEL_CLASS_REQUEST,
  CANCEL_CLASS_FAILURE
} from '../modules/classes/reducer';
import {
  LOGIN_SUCCESS,
  LOGIN_SYNC_SUCCESS,
  LOGIN_REQUEST,
  LOGIN_FAILURE
} from '../modules/auth/reducers/login';
import { REGISTER_REQUEST, REGISTER_FAILURE, REGISTER_SUCCESS } from '../modules/auth/reducers/register';
import {
  RESET_PASSWORD_REQUEST,
  RESET_PASSWORD_FAILURE,
  RESET_PASSWORD_SUCCESS
} from '../modules/auth/reducers/resetPassword';
import {
  VERIFY_TOKEN_REQUEST,
  VERIFY_TOKEN_FAILURE,
  VERIFY_TOKEN_SUCCESS
} from '../modules/auth/reducers/token';
import { FETCH_FULL_PLAN_SUCCESS, FETCH_FULL_PLAN_FAILURE } from '../modules/plan/reducer';
import { FETCH_USER_INFO_SUCCESS, FETCH_USER_INFO_FAILURE } from '../modules/user/reducer';
import {
  FETCH_SEARCH_RESULTS_REQUEST,
  FETCH_SEARCH_RESULTS_FAILURE,
  FETCH_SEARCH_RESULTS_SUCCESS
} from '../modules/search/reducer';
import {
  SUBMIT_LEAD_REQUEST,
  SUBMIT_LEAD_SUCCESS,
  SUBMIT_LEAD_FAILURE,
  SEND_TICKET_REQUEST,
  SEND_TICKET_SUCCESS,
  SEND_TICKET_SUCCESS_WITH_ALERT,
  SEND_TICKET_FAILURE
} from '../modules/support/reducer';

const FETCH_UPCOMING_DECLINED = RELOAD_DECLINED('upcomingClasses');
const FETCH_BOOKED_DECLINED = RELOAD_DECLINED('bookedClasses');
const FETCH_PLAN_DECLINED = RELOAD_DECLINED('plan');
const FETCH_USER_DATA_DECLINED = RELOAD_DECLINED('userData');

const loadingMiddleware = store => next => action => {
  const { type } = action;

  switch (type) {
    case LOGIN_REQUEST: {
      // store.dispatch(addLoading(['login']));
      break;
    }

    case LOGIN_FAILURE: {
      const { checkedAllUrls } = store.getState().app;
      if (checkedAllUrls) {
        store.dispatch(removeLoading(['login']));
      }
      break;
    }

    case LOGIN_SUCCESS:
    case LOGIN_SYNC_SUCCESS: {
      store.dispatch(removeLoading(['login']));
      // Let's not block users for this info
      // We have placeholders for loading items, so let's use them
      // store.dispatch(addLoading(['userData', 'upcomingClasses', 'bookedClasses', 'plan']));
      // store.dispatch(addLoading(['userData', 'plan']));
      break;
    }

    case FETCH_UPCOMING_CLASSES_SUCCESS:
    case FETCH_UPCOMING_CLASSES_FAILURE:
    case FETCH_UPCOMING_DECLINED: {
      store.dispatch(removeLoading(['upcomingClasses', 'bookClass', 'cancelClass']));
      break;
    }

    case FETCH_CLASSES_BOOKED_SUCCESS:
    case FETCH_CLASSES_BOOKED_FAILURE:
    case FETCH_BOOKED_DECLINED: {
      store.dispatch(removeLoading(['bookedClasses']));
      break;
    }

    case FETCH_FULL_PLAN_SUCCESS:
    case FETCH_FULL_PLAN_FAILURE:
    case FETCH_PLAN_DECLINED: {
      store.dispatch(removeLoading(['plan']));
      break;
    }

    case FETCH_USER_INFO_SUCCESS:
    case FETCH_USER_INFO_FAILURE:
    case FETCH_USER_DATA_DECLINED: {
      store.dispatch(removeLoading(['userData']));
      break;
    }

    case BOOK_CLASS_REQUEST: {
      store.dispatch(addLoading(['bookClass']));
      break;
    }

    case BOOK_CLASS_FAILURE: {
      store.dispatch(removeLoading(['bookClass']));
      break;
    }

    case CANCEL_CLASS_REQUEST: {
      store.dispatch(addLoading(['cancelClass']));
      break;
    }

    case CANCEL_CLASS_FAILURE: {
      store.dispatch(removeLoading(['cancelClass']));
      break;
    }

    case FETCH_SEARCH_RESULTS_REQUEST: {
      store.dispatch(addLoading(['searchResults']));
      break;
    }

    case FETCH_SEARCH_RESULTS_SUCCESS:
    case FETCH_SEARCH_RESULTS_FAILURE: {
      store.dispatch(removeLoading(['searchResults']));
      break;
    }

    case SUBMIT_LEAD_REQUEST: {
      store.dispatch(addLoading(['submitLead']));
      break;
    }
    case SUBMIT_LEAD_SUCCESS:
    case SUBMIT_LEAD_FAILURE: {
      store.dispatch(removeLoading(['submitLead']));
      break;
    }

    case SEND_TICKET_REQUEST: {
      store.dispatch(addLoading(['sendTicket']));
      break;
    }
    case SEND_TICKET_SUCCESS:
    case SEND_TICKET_SUCCESS_WITH_ALERT:
    case SEND_TICKET_FAILURE: {
      store.dispatch(removeLoading(['sendTicket']));
      break;
    }

    case REGISTER_REQUEST: {
      store.dispatch(addLoading(['register']));
      break;
    }

    case REGISTER_SUCCESS: {
      store.dispatch(removeLoading(['register']));
      break;
    }

    case REGISTER_FAILURE: {
      const { isActive, isRegistered } = action;
      if (isActive && isRegistered) {
        store.dispatch(removeLoading(['register']));
      } else {
        const { checkedAllUrls } = store.getState().app;
        if (checkedAllUrls) {
          store.dispatch(removeLoading(['register']));
        }
      }
      break;
    }

    case RESET_PASSWORD_REQUEST: {
      store.dispatch(addLoading(['resetPassword']));
      break;
    }

    case RESET_PASSWORD_SUCCESS: {
      store.dispatch(removeLoading(['resetPassword']));
      break;
    }

    case RESET_PASSWORD_FAILURE: {
      const { checkedAllUrls } = store.getState().app;
      if (checkedAllUrls) {
        store.dispatch(removeLoading(['resetPassword']));
      }
      break;
    }

    case VERIFY_TOKEN_REQUEST: {
      store.dispatch(addLoading(['verifyToken']));
      break;
    }

    case VERIFY_TOKEN_SUCCESS: {
      store.dispatch(removeLoading(['verifyToken']));
      break;
    }

    case VERIFY_TOKEN_FAILURE: {
      const { checkedAllUrls } = store.getState().app;
      if (checkedAllUrls) {
        store.dispatch(removeLoading(['verifyToken']));
      }
      break;
    }

    default:
  }
  return next(action);
};

export default loadingMiddleware;

import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import snackbarMiddleware from './snackbar';
import { online, offline } from '../modules/app/actions/toggleConnected';
import { app as AppConfiguration } from '../../config/tests';
import { SHOW_SNACKBAR, HIDE_SNACKBAR } from '../modules/snackbar/reducer';
import { APP_ONLINE, APP_OFFLINE } from '../modules/app/reducer';

const { initialState } = AppConfiguration;

const middlewares = [thunk, snackbarMiddleware];
const mockStore = configureMockStore(middlewares);
const store = mockStore({ app: { ...initialState } });

describe('Middleware Tests', () => {
  jest.setTimeout(30000);

  describe('Snackbar', () => {
    beforeEach(() => {
      store.clearActions();
    });

    describe('Connection', () => {
      it('should show when the user loses connection', async () => {
        const expectedActions = [{ type: SHOW_SNACKBAR }, { type: APP_OFFLINE }];
        await store.dispatch(offline());
        store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
      });
      it('should disappear when the user is connected', async () => {
        const expectedActions = [{ type: HIDE_SNACKBAR }, { type: APP_ONLINE }];
        await store.dispatch(online());
        store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
      });
    });
  });
});

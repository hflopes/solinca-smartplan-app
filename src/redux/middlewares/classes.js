import { CHANGE_SELECTED_GYM } from '../modules/gyms/reducer';
import { BOOK_CLASS_SUCCESS, CANCEL_CLASS_SUCCESS } from '../modules/classes/reducer';
import fetchUpcoming from '../modules/classes/actions/fetchUpcoming';
import reload from '../reload/action';

const classesMiddleware = store => next => action => {
  const { type } = action;

  switch (type) {
    case CHANGE_SELECTED_GYM: {
      const prevSelectGymId = store.getState().gyms.selectedGymId;
      const nextState = next(action);
      if (String(prevSelectGymId) !== String(action.selectedGymId)) {
        store.dispatch(fetchUpcoming());
      }

      return nextState;
    }

    case BOOK_CLASS_SUCCESS:
    case CANCEL_CLASS_SUCCESS: {
      store.dispatch(reload('upcomingClasses', true));
      break;
    }

    default:
  }

  return next(action);
};

export default classesMiddleware;

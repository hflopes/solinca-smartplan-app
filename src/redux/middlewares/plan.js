import {
  SETUP_PLAN_EXERCISE_TYPE,
  SETUP_PLAN_MODALITIES_PLAN,
  SETUP_PLAN_QUESTIONNAIRE
} from '../modules/plan/reducer';
import {
  setupExercisePlan,
  setupRecommendedModalities,
  setupFinish,
  setupExerciseType
} from '../modules/plan/actions/setup';
import { USER_TYPES, USER_OBJECTIVES, EXERCISE_TYPES } from '../modules/plan/constants';

const planMiddleware = store => next => action => {
  const { type } = action;

  switch (type) {
    case SETUP_PLAN_QUESTIONNAIRE: {
      const nextState = next(action);
      const { setup } = store.getState().plan;
      const { userObjective } = setup;

      if (userObjective === USER_OBJECTIVES.BALANCE.key) {
        store.dispatch(setupExerciseType(EXERCISE_TYPES.BODY_WEIGHT.key));
      }

      return nextState;
    }

    case SETUP_PLAN_EXERCISE_TYPE: {
      const nextState = next(action);
      const { setup } = store.getState().plan;
      const { userType, userObjective } = setup;
      store.dispatch(setupExercisePlan());

      if (userType === USER_TYPES.EXPERIENCED_USER.key && userObjective === USER_OBJECTIVES.MUSCLE.key) {
        store.dispatch(setupFinish());
      } else {
        store.dispatch(setupRecommendedModalities());
      }

      return nextState;
    }

    case SETUP_PLAN_MODALITIES_PLAN: {
      const nextState = next(action);
      store.dispatch(setupFinish());
      return nextState;
    }

    default:
  }

  return next(action);
};

export default planMiddleware;

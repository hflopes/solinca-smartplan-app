import i18n from 'i18next';
import { REGISTER_FAILURE_ERROR, REGISTER_SUCCESS } from '../modules/auth/reducers/register';
import {
  LOGIN_FAILURE,
  LOGIN_INVALID_EMAIL,
  LOGIN_INVALID_PASSWORD,
  LOGIN_WAIT
} from '../modules/auth/reducers/login';
import { VERIFY_TOKEN_SUCCESS, VERIFY_TOKEN_FAILURE } from '../modules/auth/reducers/token';
import { RESET_PASSWORD_FAILURE_ERROR, RESET_PASSWORD_SUCCESS } from '../modules/auth/reducers/resetPassword';
import {
  UPDATE_INFO_FAILURE,
  UPDATE_INFO_SUCCESS,
  UPDATE_PASSWORD_FAILURE,
  UPDATE_PASSWORD_SUCCESS
} from '../modules/user/reducer';
import {
  SUBMIT_LEAD_FAILURE,
  SUBMIT_LEAD_SUCCESS,
  SEND_TICKET_SUCCESS,
  SEND_TICKET_FAILURE,
  RENEW_PLAN_FAILURE,
  RENEW_PLAN_SUCCESS
} from '../modules/support/reducer';
import {
  TOGGLE_CLASS_FAVORITE_SUCCESS,
  TOGGLE_CLASS_FAVORITE_FAILURE,
  BOOK_CLASS_SUCCESS,
  CANCEL_CLASS_SUCCESS,
  BOOK_CLASS_FAILURE,
  CANCEL_CLASS_FAILURE,
  ADD_CLASS_ALERT,
  REMOVE_CLASS_ALERT,
  REVIEW_CLASS_SUCCESS,
  REVIEW_CLASS_FAILURE
} from '../modules/classes/reducer';
import { MAX_GYMS_FILTERS, MAX_MODALITIES_FILTERS, NO_RESULTS } from '../modules/search/reducer';
import {
  ADD_TO_CART,
  REMOVE_FROM_CART,
  REDEEM_REWARD_SUCCESS,
  REDEEM_REWARD_FAILURE
} from '../modules/gamification/rewards/reducer';
import mapError from '../utils/errorMapping';
import showToast from '../modules/toasts/actions/show';
import {
  CANCEL_CHALLENGE_SUCCESS,
  JOIN_CHALLENGE_SUCCESS,
  JOIN_CHALLENGE_FAILURE,
  CANCEL_CHALLENGE_FAILURE
} from '../modules/gamification/challenges/reducer';

const toastMiddleware = store => next => action => {
  const { type } = action;
  switch (type) {
    case UPDATE_INFO_SUCCESS: {
      const { field } = action;
      const toast = {
        type: 'success',
        title: i18n.t(`toasts.updateProfileInfoSuccess.${field}`)
      };
      store.dispatch(showToast({ ...toast }));
      break;
    }

    case UPDATE_INFO_FAILURE: {
      const { field } = action;
      const toast = {
        type: 'error',
        title: i18n.t(`toasts.updateProfileInfoError.${field}`)
      };
      store.dispatch(showToast({ ...toast }));
      break;
    }

    case UPDATE_PASSWORD_SUCCESS: {
      const toast = {
        type: 'success',
        title: i18n.t('toasts.updateProfileInfoSuccess.password')
      };
      store.dispatch(showToast({ ...toast }));
      break;
    }

    case UPDATE_PASSWORD_FAILURE: {
      const toast = {
        type: 'error',
        title: i18n.t('toasts.updateProfileInfoError.password')
      };
      store.dispatch(showToast({ ...toast }));
      break;
    }

    case REGISTER_FAILURE_ERROR: {
      const toast = {
        type: 'error',
        title: action.error.message || action.error
      };
      store.dispatch(showToast({ ...toast }));
      break;
    }

    case REGISTER_SUCCESS: {
      const toast = {
        type: 'success',
        title: i18n.t('toasts.registerSuccess')
      };
      store.dispatch(showToast({ ...toast }));
      break;
    }

    case LOGIN_FAILURE: {
      const { isAuto } = action;
      const { checkedAllUrls } = store.getState().app;

      if (!isAuto && checkedAllUrls) {
        const toast = {
          type: 'error',
          title: action.error.message || action.error
        };
        store.dispatch(showToast({ ...toast }));
      }
      break;
    }

    case VERIFY_TOKEN_FAILURE: {
      const toast = {
        type: 'error',
        title: action.error.message || action.error
      };
      store.dispatch(showToast({ ...toast }));
      break;
    }

    case VERIFY_TOKEN_SUCCESS: {
      const toast = {
        type: 'success',
        title: i18n.t(`toasts.verifyTokenSuccess`)
      };
      store.dispatch(showToast({ ...toast }));
      break;
    }

    case RESET_PASSWORD_FAILURE_ERROR: {
      const toast = {
        type: 'error',
        title: action.error.message || action.error
      };
      store.dispatch(showToast({ ...toast }));
      break;
    }

    case RESET_PASSWORD_SUCCESS: {
      const toast = {
        type: 'success',
        title: i18n.t(`toasts.resetPasswordSuccess`)
      };
      store.dispatch(showToast({ ...toast }));
      break;
    }

    case SEND_TICKET_SUCCESS: {
      const toast = {
        type: 'success',
        title: i18n.t(`toasts.sendTicketSuccess`)
      };
      store.dispatch(showToast({ ...toast }));
      break;
    }

    case SEND_TICKET_FAILURE: {
      const toast = {
        type: 'error',
        title: action.error.message || action.error
      };
      store.dispatch(showToast({ ...toast }));

      break;
    }

    case BOOK_CLASS_SUCCESS: {
      const { minutes } = action;
      const toast = {
        type: 'success',
        title: i18n.t('toasts.bookClassSuccess', { minutes })
      };
      store.dispatch(showToast({ ...toast }));
      break;
    }

    case ADD_CLASS_ALERT: {
      const { minutes } = action;
      const toast = {
        type: 'success',
        title: i18n.t(`toasts.addAlertClassSuccess`, { minutes })
      };
      store.dispatch(showToast({ ...toast }));
      break;
    }

    case REMOVE_CLASS_ALERT: {
      const toast = {
        type: 'success',
        title: i18n.t(`toasts.removeAlertClassSuccess`)
      };
      store.dispatch(showToast({ ...toast }));
      break;
    }

    case REVIEW_CLASS_SUCCESS: {
      const toast = {
        type: 'success',
        title: i18n.t(`toasts.reviewClassSuccess`)
      };
      store.dispatch(showToast({ ...toast }));
      break;
    }

    case REVIEW_CLASS_FAILURE: {
      const toast = {
        type: 'error',
        title: i18n.t(`toasts.reviewClassFailure`)
      };
      store.dispatch(showToast({ ...toast }));
      break;
    }

    case TOGGLE_CLASS_FAVORITE_SUCCESS: {
      const { classData } = action;
      const { isFavorite } = classData;
      let toast = {};
      if (isFavorite) {
        toast = {
          type: 'success',
          title: i18n.t(`toasts.removedFavoriteSuccess`)
        };
      } else {
        toast = {
          type: 'success',
          title: i18n.t(`toasts.addedFavoriteSuccess`)
        };
      }
      store.dispatch(showToast({ ...toast }));
      break;
    }

    case TOGGLE_CLASS_FAVORITE_FAILURE: {
      const toast = {
        type: 'error',
        title: i18n.t(`toasts.favoriteError`)
      };
      store.dispatch(showToast({ ...toast }));
      break;
    }

    case CANCEL_CLASS_SUCCESS: {
      const toast = {
        type: 'success',
        title: i18n.t(`toasts.cancelClassSuccess`)
      };
      store.dispatch(showToast({ ...toast }));
      break;
    }

    case BOOK_CLASS_FAILURE: {
      // Don't show a toast for the premium related errors
      // as an alert will be shown for them
      if (action.error && action.error === mapError(6003)) {
        break;
      }

      let title = i18n.t(`toasts.bookClassError`);

      if (action.error && (action.error === mapError(1101) || action.error === mapError(6007))) {
        title = action.error;
      }

      const toast = {
        type: 'error',
        title
      };

      store.dispatch(showToast({ ...toast }));
      break;
    }

    case CANCEL_CLASS_FAILURE: {
      const toast = {
        type: 'error',
        title: i18n.t(`toasts.cancelClassError`)
      };
      store.dispatch(showToast({ ...toast }));
      break;
    }

    case LOGIN_WAIT: {
      const toast = {
        type: 'error',
        title: i18n.t(`toasts.loginTimeout`)
      };
      store.dispatch(showToast({ ...toast }));
      break;
    }

    case LOGIN_INVALID_EMAIL: {
      const toast = {
        type: 'error',
        title: i18n.t(`toasts.invalidEmail`)
      };
      store.dispatch(showToast({ ...toast }));
      break;
    }

    case LOGIN_INVALID_PASSWORD: {
      const toast = {
        type: 'error',
        title: i18n.t(`toasts.invalidPassword`)
      };
      store.dispatch(showToast({ ...toast }));
      break;
    }

    case MAX_GYMS_FILTERS: {
      const toast = {
        type: 'error',
        title: i18n.t(`toasts.search.maxGymsFilter`)
      };
      store.dispatch(showToast({ ...toast }));
      break;
    }
    case MAX_MODALITIES_FILTERS: {
      const toast = {
        type: 'error',
        title: i18n.t(`toasts.search.maxModalitiesFilter`)
      };
      store.dispatch(showToast({ ...toast }));
      break;
    }

    case NO_RESULTS: {
      const toast = {
        type: 'error',
        title: i18n.t(`toasts.search.noResults`)
      };
      store.dispatch(showToast({ ...toast }));
      break;
    }

    case SUBMIT_LEAD_FAILURE: {
      const toast = {
        type: 'error',
        title: action.error.message
      };
      store.dispatch(showToast({ ...toast }));

      break;
    }

    case SUBMIT_LEAD_SUCCESS: {
      const toast = {
        type: 'success',
        title: i18n.t(`toasts.sendLeadSuccess`)
      };
      store.dispatch(showToast({ ...toast }));

      break;
    }

    case ADD_TO_CART: {
      const toast = {
        type: 'success',
        title: i18n.t(`toasts.addRewardToCartSuccess`)
      };
      store.dispatch(showToast({ ...toast }));

      break;
    }

    case REMOVE_FROM_CART: {
      const toast = {
        type: 'success',
        title: i18n.t(`toasts.removeRewardFromCartSuccess`)
      };
      store.dispatch(showToast({ ...toast }));
      break;
    }

    case REDEEM_REWARD_FAILURE: {
      const toast = {
        type: 'error',
        title: i18n.t(`toasts.redeemRewardError`)
      };
      store.dispatch(showToast({ ...toast }));
      break;
    }

    case REDEEM_REWARD_SUCCESS: {
      const toast = {
        type: 'success',
        title: i18n.t(`toasts.redeemRewardSuccess`)
      };
      store.dispatch(showToast({ ...toast }));
      break;
    }

    case RENEW_PLAN_FAILURE: {
      const toast = {
        type: 'error',
        title: i18n.t(`toasts.renewPlanError`)
      };
      store.dispatch(showToast({ ...toast }));
      break;
    }

    case RENEW_PLAN_SUCCESS: {
      const toast = {
        type: 'success',
        title: i18n.t(`toasts.renewPlanSuccess`)
      };
      store.dispatch(showToast({ ...toast }));
      break;
    }

    case JOIN_CHALLENGE_SUCCESS: {
      const toast = {
        type: 'success',
        title: i18n.t(`toasts.joinChallengeSuccess`)
      };
      store.dispatch(showToast({ ...toast }));
      break;
    }

    case JOIN_CHALLENGE_FAILURE: {
      const toast = {
        type: 'error',
        title: i18n.t(`toasts.joinChallengeError`)
      };
      store.dispatch(showToast({ ...toast }));
      break;
    }

    case CANCEL_CHALLENGE_SUCCESS: {
      const toast = {
        type: 'success',
        title: i18n.t(`toasts.cancelChallengeSuccess`)
      };
      store.dispatch(showToast({ ...toast }));
      break;
    }

    case CANCEL_CHALLENGE_FAILURE: {
      const toast = {
        type: 'error',
        title: i18n.t(`toasts.cancelChallengeError`)
      };
      store.dispatch(showToast({ ...toast }));
      break;
    }

    default:
      break;
  }

  return next(action);
};

export default toastMiddleware;

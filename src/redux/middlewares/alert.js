import i18n from 'i18next';
import showAlert from '../modules/alerts/actions/show';
import { LOGIN_SUCCESS, LOGIN_FAILURE, LOGIN_SYNC_SUCCESS } from '../modules/auth/reducers/login';
import mapError from '../utils/errorMapping';
import { BOOK_CLASS_FAILURE } from '../modules/classes/reducer';
import { FETCH_INITIAL_DATA_FAILURE } from '../modules/app/reducer';
import { SEND_TICKET_SUCCESS_WITH_ALERT } from '../modules/support/reducer';

const alertMiddleware = store => next => action => {
  const { type } = action;
  switch (type) {
    case FETCH_INITIAL_DATA_FAILURE: {
      const { offline } = store.getState().app;
      if (!offline) {
        // const alert = {
        //   persist: true,
        //   title: i18n.t('alerts.pingDown.title'),
        //   message: i18n.t('alerts.pingDown.message'),
        //   icon: 'logo_dark.svg'
        // };
        // store.dispatch(showAlert({ ...alert }));
      }
      break;
    }
    case LOGIN_FAILURE: {
      const { isAuto } = action;
      if (isAuto) {
        const alert = {
          persist: true,
          title: i18n.t('alerts.registerAutoLogin.title'),
          message: i18n.t('alerts.registerAutoLogin.message'),
          icon: 'big_icons/exercise.svg',
          cta: i18n.t('alerts.registerAutoLogin.cta'),
          redirectUrl: '/reset-password'
        };
        store.dispatch(showAlert({ ...alert }));
      }
      break;
    }

    case LOGIN_SUCCESS:
    case LOGIN_SYNC_SUCCESS: {
      const { isCanceledOrSuspended } = action;

      if (isCanceledOrSuspended) {
        const alert = {
          persist: true,
          title: i18n.t('alerts.suspended.title'),
          message: i18n.t('alerts.suspended.message'),
          icon: 'big_icons/class.svg',
          cta: i18n.t('alerts.suspended.cta'),
          redirectUrl: '/about/gyms'
        };
        store.dispatch(showAlert({ ...alert }));
      }

      break;
    }

    case BOOK_CLASS_FAILURE: {
      if (action.error && action.error === mapError(6003)) {
        const alert = {
          persist: false,
          title: i18n.t('alerts.premium.title'),
          message: i18n.t('alerts.premium.message'),
          icon: 'big_icons/premium_gold.svg',
          cta: i18n.t('alerts.premium.cta'),
          redirectUrl: '/become-premium',
          colorScheme: {
            start: '#C9B951',
            end: '#968735'
          }
        };
        store.dispatch(showAlert({ ...alert }));
      }
      break;
    }

    case SEND_TICKET_SUCCESS_WITH_ALERT: {
      const { email, phone } = action.formInfo;
      const phoneNumber = phone.length === 12 ? phone.split('351')[1] : phone;
      const alert = {
        persist: false,
        title: i18n.t('alerts.nutrition.title'),
        message: `${i18n.t('alerts.nutrition.message-part1')} <span class="bold">${email}</span>${i18n.t(
          'alerts.nutrition.message-part2'
        )}<span class="bold">${phoneNumber}</span>${i18n.t('alerts.nutrition.message-part3')}`,
        icon: 'big_icons/exercise.svg',
        cta: i18n.t('alerts.nutrition.cta'),
        redirectUrl: '/nutrition',
        colorScheme: {
          start: '#f33b30',
          end: '#c31a40'
        }
      };
      store.dispatch(showAlert({ ...alert }));
      break;
    }

    default:
  }

  return next(action);
};

export default alertMiddleware;

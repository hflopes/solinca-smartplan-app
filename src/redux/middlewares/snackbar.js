import i18n from 'i18next';
import { APP_OFFLINE, APP_ONLINE } from '../modules/app/reducer';
import showSnackBar from '../modules/snackbar/actions/show';
import hideSnackBar from '../modules/snackbar/actions/hide';

const snackBarMiddleware = store => next => action => {
  const { type } = action;
  switch (type) {
    case APP_OFFLINE: {
      const snackbar = {
        type: 'error',
        message: i18n.t(`snackbar.offline`)
      };
      store.dispatch(showSnackBar({ ...snackbar }));
      break;
    }
    case APP_ONLINE: {
      store.dispatch(hideSnackBar());
      break;
    }

    default:
      break;
  }

  return next(action);
};

export default snackBarMiddleware;

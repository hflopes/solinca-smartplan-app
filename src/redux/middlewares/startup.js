import {
  FETCH_INITIAL_DATA_SUCCESS,
  APP_START,
  APP_ONLINE,
  APP_OFFLINE,
  FETCH_INITIAL_DATA_FAILURE,
  SET_API_URL,
  RESET_API_URL
} from '../modules/app/reducer';
import { FETCH_USER_INFO_SUCCESS } from '../modules/user/reducer';
import { LOGIN_SUCCESS, LOGIN_SYNC_SUCCESS, LOGIN_SYNC_FAILURE } from '../modules/auth/reducers/login';

import sync from '../modules/auth/actions/sync';
import resetReload from '../modules/app/actions/resetReload';
import setVersion from '../modules/app/actions/setVersion';
import toggleApiUrl from '../modules/app/actions/toggleApiUrl';
import reload from '../reload/action';
import toggleConnected from '../modules/app/actions/toggleConnected';
import logout from '../modules/auth/actions/logout';

import { VERSION } from '../../config/app';
import { setURL } from '../../config/api';

const startupMiddleware = store => next => action => {
  const { type } = action;
  switch (type) {
    /**
     * After the app starts, check for
     * an internet connection
     */
    case APP_START: {
      store.dispatch(toggleConnected());
      store.dispatch(reload('initialData'));
      break;
    }

    case RESET_API_URL:
    case SET_API_URL: {
      store.dispatch(reload('initialData'));
      break;
    }

    /**
     * If the app has an internet connection,
     * load the initial data, which will trigger
     * every other necessary api fetch
     */
    case APP_ONLINE: {
      const { offline } = store.getState().app;
      if (offline) {
        store.dispatch(reload('initialData'));
      }
      break;
    }

    /**
     * After the initial data is fetched,
     * attempt to sync login-data (if logged in)
     * and also fetch gyms and modalities
     * (these two modules are useful regardless
     * of login-state, so load them either way)
     */
    case FETCH_INITIAL_DATA_SUCCESS: {
      const { version, apiURL } = store.getState().app;

      if (!apiURL) {
        store.dispatch(toggleApiUrl());
      } else {
        setURL(apiURL);
      }

      if (version) {
        const [major, minor] = version.split('.');
        const [newMajor, newMinor] = VERSION.split('.');
        if (newMajor > major || newMinor > minor) {
          store.dispatch(resetReload());
          store.dispatch(setVersion(VERSION));
        }
      } else {
        store.dispatch(setVersion(VERSION));
      }

      const { isLoggedIn } = store.getState().login;
      if (isLoggedIn) {
        store.dispatch(sync());
      }

      store.dispatch(reload('campaigns'));
      store.dispatch(reload('terms'));
      store.dispatch(reload('gyms'));
      store.dispatch(reload('modalities'));
      break;
    }

    case APP_OFFLINE:
    case FETCH_INITIAL_DATA_FAILURE: {
      const { isLoggedIn } = store.getState().login;
      if (!isLoggedIn) {
        store.dispatch(logout());
      }
      break;
    }

    case LOGIN_SYNC_FAILURE: {
      const { isLoggedIn } = store.getState().login;
      if (isLoggedIn) {
        store.dispatch(logout());
      }
      break;
    }

    /**
     * If either login or sync were done successfully,
     * fetch terms of service if necessary and also
     * fetch any necessary content
     */
    case LOGIN_SUCCESS:
    case LOGIN_SYNC_SUCCESS: {
      // Go to next state to ensure
      // the loadings get added before
      // they are removed
      const nextState = next(action);
      store.dispatch(reload('gyms'));
      store.dispatch(reload('modalities'));
      store.dispatch(reload('userData'));
      store.dispatch(reload('notificationSettings'));
      store.dispatch(reload('favoriteClasses'));
      store.dispatch(reload('bookedClasses'));
      store.dispatch(reload('plan'));
      // store.dispatch(reload('fullPlan'));
      store.dispatch(reload('classesHistory'));
      store.dispatch(reload('plansHistory'));
      store.dispatch(reload('evaluationsHistory'));
      // store.dispatch(reload('badges'));
      // store.dispatch(reload('rewards'));
      // store.dispatch(reload('purchases'));
      // store.dispatch(reload('lastReceipt'));
      // store.dispatch(reload('3dbody'));
      store.dispatch(reload('receiptsList'));
      store.dispatch(reload('classesReview'));
      store.dispatch(reload('complaints'));
      store.dispatch(reload('userTags'));

      return nextState;
    }

    /**
     * Since upcomingClasses depends on "selectedGymId", which
     * starts out as the user's default gym.
     *
     * We can only load upcoming classes after the user data has
     * been loaded
     */
    case FETCH_USER_INFO_SUCCESS: {
      const nextState = next(action);
      const { isFetchingUpcoming } = store.getState().classes;
      if (!isFetchingUpcoming) {
        store.dispatch(reload('upcomingClasses'));
        store.dispatch(reload('challenges'));
      }
      const { selectedGymId, data } = store.getState().gyms;
      if (!data[selectedGymId]) {
        store.dispatch(reload('gyms', true));
      }
      return nextState;
    }

    default:
  }

  return next(action);
};

export default startupMiddleware;

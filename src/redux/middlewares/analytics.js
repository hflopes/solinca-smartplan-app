import {
  BOOK_CLASS_SUCCESS,
  REVIEW_CLASS_CLOSE,
  REVIEW_CLASS_SUCCESS,
  TOGGLE_CLASS_FAVORITE_SUCCESS,
  REVIEW_CLASS_VIEW
} from '../modules/classes/reducer';
import { LOGIN_SUCCESS, LOGIN_SYNC_SUCCESS } from '../modules/auth/reducers/login';
import { REGISTER_SUCCESS } from '../modules/auth/reducers/register';
import { SETUP_PLAN_FINISH, START_PLAN, COMPLETE_PLAN } from '../modules/plan/reducer';
import { SUBMIT_LEAD_SUCCESS, SEND_TICKET_SUCCESS, RENEW_PLAN_SUCCESS } from '../modules/support/reducer';
import { SHARE_CHALLENGE } from '../modules/gamification/challenges/reducer';
import { event } from '../../utils/analytics';

const analyticsMiddleware = store => next => action => {
  const { type } = action;
  switch (type) {
    case BOOK_CLASS_SUCCESS: {
      const { classData } = action;
      const {
        gym: { name: gymName },
        modality: { name: modalityName }
      } = classData;
      event('class_book_completed', { class_book_type: modalityName, class_book_gym: gymName });
      break;
    }
    case LOGIN_SYNC_SUCCESS:
      event('logged_in_user');
      break;
    case LOGIN_SUCCESS:
      event('login');
      break;
    case SHARE_CHALLENGE: {
      const { id, group, name } = action;
      event('share_challenge', { id, group, name });
      break;
    }
    case SETUP_PLAN_FINISH:
      event('workout_plan_creation_completed');
      break;
    case START_PLAN: {
      const { plan } = store.getState();
      const { hasVisited } = plan;
      if (hasVisited) {
        event('workout_plan_started');
      } else {
        event('workout_inical_plan_started');
      }
      break;
    }
    case COMPLETE_PLAN:
      event('workout_plan_completed');
      break;
    case SUBMIT_LEAD_SUCCESS:
      event('contact', { type: 'generate_lead' });
      break;
    case REGISTER_SUCCESS: {
      event('create_account_complete');
      break;
    }
    case SEND_TICKET_SUCCESS: {
      const { login } = store.getState();
      const { isLoggedIn } = login;
      if (isLoggedIn) {
        event('contact', { type: 'suport_user_authenticated' });
      } else {
        event('contact', { type: 'suport_new_user' });
      }
      break;
    }
    case RENEW_PLAN_SUCCESS: {
      const { gym } = action;
      event('renew_plan', { renew_plan_gym: gym });
      break;
    }
    case TOGGLE_CLASS_FAVORITE_SUCCESS: {
      const { classData } = action;
      const {
        modality: { name: modalityName }
      } = classData;
      event('class_add_favorite', { class_name: modalityName });
      break;
    }
    case REVIEW_CLASS_SUCCESS: {
      const { classId } = action;
      event('class_evaluation_complete', { class_id: classId });
      break;
    }
    case REVIEW_CLASS_VIEW: {
      const { classId } = action;
      event('class_evaluation_show', { class_id: classId });
      break;
    }
    case REVIEW_CLASS_CLOSE: {
      const { classId } = action;
      event('class_evaluation_close', { class_id: classId });
      break;
    }
    default:
      break;
  }

  return next(action);
};

export default analyticsMiddleware;

import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import moment from 'moment';
import alertMiddleware from './alert';
import bookClassAction from '../modules/classes/actions/book';
import loginAction from '../modules/auth/actions/login';
import {
  login as LoginConfiguration,
  classes as ClassesConfiguration,
  app as AppConfiguration
} from '../../config/tests';
import { SHOW_ALERT } from '../modules/alerts/reducer';
import { BOOK_CLASS_REQUEST, BOOK_CLASS_FAILURE } from '../modules/classes/reducer';
import mapError from '../utils/errorMapping';
import { LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE } from '../modules/auth/reducers/login';
import { FETCH_INITIAL_DATA_FAILURE } from '../modules/app/reducer';
import { initialDataFailure } from '../modules/app/actions/fetchInitialData';

const {
  initialState: classesInitialState,
  variables: classesVariables,
  errors: classesErrors
} = ClassesConfiguration;
const {
  initialState: initialLoginState,
  variables: loginVariables,
  errors: loginErrors
} = LoginConfiguration;
const { initialState: initialAppState } = AppConfiguration;

const middlewares = [thunk, alertMiddleware];
const mockStore = configureMockStore(middlewares);
const store = mockStore({
  classes: { ...classesInitialState },
  login: { ...initialLoginState, subscriptionType: 'basic' },
  app: { ...initialAppState }
});

describe('Middleware Tests', () => {
  jest.setTimeout(30000);

  describe('Snackbar', () => {
    beforeEach(() => {
      store.clearActions();
    });

    describe('Login', () => {
      it('should show if the user is canceled or suspended', async () => {
        const expectedActions = [{ type: LOGIN_REQUEST }, { type: SHOW_ALERT }, { type: LOGIN_SUCCESS }];
        const { email, password } = loginVariables.canceled;

        await store.dispatch(loginAction({ email, password }));
        store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
        const [, , success] = store.getActions();
        expect(Object.prototype.hasOwnProperty.call(success, 'isCanceledOrSuspended')).toBe(true);
        const { isCanceledOrSuspended } = success;
        expect(isCanceledOrSuspended).toBe(true);
      });
      it('should show if the user tries to register with a correct email and different password', async () => {
        const expectedActions = [{ type: LOGIN_REQUEST }, { type: SHOW_ALERT }, { type: LOGIN_FAILURE }];
        const { email } = loginVariables.success;

        await store.dispatch(loginAction({ email, password: 'other', isAuto: true }));
        store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
        const [, , failure] = store.getActions();
        expect(Object.prototype.hasOwnProperty.call(failure, 'isAuto')).toBe(true);
        expect(Object.prototype.hasOwnProperty.call(failure, 'error')).toBe(true);
        const { isAuto, error } = failure;
        expect(isAuto).toBe(true);
        expect(error).toBe(mapError(loginErrors.login));
      });
    });

    describe('Initial data', () => {
      it('should show on fetch initial data error', async () => {
        const expectedActions = [{ type: SHOW_ALERT }, { type: FETCH_INITIAL_DATA_FAILURE }];

        await store.dispatch(initialDataFailure());
        store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
      });
    });

    describe('Book classes', () => {
      it('should show on class book error - only premium', async () => {
        const expectedActions = [{ type: SHOW_ALERT }, { type: BOOK_CLASS_FAILURE }];
        const variables = classesVariables.book;
        await store.dispatch(
          bookClassAction({
            ...variables,
            date: moment()
              .add(12, 'hours')
              .toDate()
          })
        );
        store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
        const [, error] = store.getActions();
        expect(error.error).toEqual(mapError(classesErrors.premium));
      });
    });
  });
});

import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import moment from 'moment';
import mapError from '../utils/errorMapping';
import toastMiddleware from './toast';
import {
  LOGIN_FAILURE,
  LOGIN_REQUEST,
  LOGIN_INVALID_PASSWORD,
  LOGIN_INVALID_EMAIL,
  LOGIN_WAIT
} from '../modules/auth/reducers/login';
import { SHOW_TOAST } from '../modules/toasts/reducer';
import loginAction, {
  loginInvalidEmail,
  loginInvalidPassword,
  loginWait
} from '../modules/auth/actions/login';
import { maxGymsFilters, maxModalitiesFilters, noResults } from '../modules/search/actions/submit';
import bookClassAction from '../modules/classes/actions/book';
import cancelClassAction from '../modules/classes/actions/cancel';
import verifyTokenAction from '../modules/auth/actions/verifyToken';
import resetPasswordAction from '../modules/auth/actions/resetPassword';
import registerAction, { registerSuccess } from '../modules/auth/actions/register';
import sendLeadAction from '../modules/support/actions/sendLead';
import sendTicketAction from '../modules/support/actions/sendTicket';
import {
  login as LoginConfiguration,
  toast as ToastConfiguration,
  support as SupportConfiguration,
  register as RegisterConfiguration,
  resetPassword as ResetConfiguration,
  classes as ClassesConfiguration
} from '../../config/tests';
import {
  SUBMIT_LEAD_FAILURE,
  SUBMIT_LEAD_REQUEST,
  SUBMIT_LEAD_SUCCESS,
  SEND_TICKET_SUCCESS,
  SEND_TICKET_FAILURE,
  SEND_TICKET_REQUEST
} from '../modules/support/reducer';
import { REGISTER_FAILURE, REGISTER_SUCCESS } from '../modules/auth/reducers/register';
import {
  RESET_PASSWORD_FAILURE,
  RESET_PASSWORD_SUCCESS,
  RESET_PASSWORD_REQUEST
} from '../modules/auth/reducers/resetPassword';
import { VERIFY_TOKEN_FAILURE } from '../modules/auth/reducers/token';
import { MAX_MODALITIES_FILTERS, MAX_GYMS_FILTERS, NO_RESULTS } from '../modules/search/reducer';
import {
  BOOK_CLASS_REQUEST,
  BOOK_CLASS_FAILURE,
  CANCEL_CLASS_REQUEST,
  CANCEL_CLASS_FAILURE
} from '../modules/classes/reducer';

const { initialState: initialLoginState, variables: loginVariables } = LoginConfiguration;
const { initialState: initialToastState } = ToastConfiguration;
const { initialState: initialSupportState, variables: supportVariables } = SupportConfiguration;
const { initialState: initialRegisterState, variables: registerVariables } = RegisterConfiguration;
const { initialState: initialResetState, variables: resetVariables } = ResetConfiguration;
const {
  initialState: initialClassesState,
  variables: classesVariables,
  errors: classesErrors
} = ClassesConfiguration;

const middlewares = [thunk, toastMiddleware];
const mockStore = configureMockStore(middlewares);
const store = mockStore({
  support: { ...initialSupportState },
  login: { ...initialLoginState, subscriptionType: 'basic' },
  toasts: { ...initialToastState },
  register: { ...initialRegisterState },
  classes: { ...initialClassesState },
  resetPassword: { ...initialResetState }
});

describe('Middleware Tests', () => {
  jest.setTimeout(30000);

  describe('Toasts', () => {
    beforeEach(() => {
      store.clearActions();
    });

    describe('Login', () => {
      it('should show on login error - invalid email', async () => {
        const expectedActions = [{ type: SHOW_TOAST }, { type: LOGIN_INVALID_EMAIL }];
        await store.dispatch(loginInvalidEmail());
        store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
      });
      it('should show on login error - invalid password', async () => {
        const expectedActions = [{ type: SHOW_TOAST }, { type: LOGIN_INVALID_PASSWORD }];
        await store.dispatch(loginInvalidPassword());
        store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
      });
      it('should show on login error - invalid credentials', async () => {
        const expectedActions = [{ type: LOGIN_REQUEST }, { type: SHOW_TOAST }, { type: LOGIN_FAILURE }];
        const { email, password } = loginVariables.failure;
        await store.dispatch(loginAction({ email, password }));
        store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
      });
      it('should show on login waiting', async () => {
        const expectedActions = [{ type: SHOW_TOAST }, { type: LOGIN_WAIT }];
        await store.dispatch(loginWait());
        store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
      });
    });

    describe('Reset Password', () => {
      it('should show on reset password error - passwords not matching', async () => {
        const expectedActions = [{ type: SHOW_TOAST }, { type: RESET_PASSWORD_FAILURE }];
        const variables = resetVariables.failure;
        await store.dispatch(resetPasswordAction(variables));
        store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
      });
      it('should show on reset password success', async () => {
        const expectedActions = [
          { type: RESET_PASSWORD_REQUEST },
          { type: SHOW_TOAST },
          { type: RESET_PASSWORD_SUCCESS }
        ];
        const variables = resetVariables.success;
        await store.dispatch(resetPasswordAction(variables));
        store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
      });
    });

    describe('Leads', () => {
      it('should show on lead error - missing data', async () => {
        const expectedActions = [{ type: SHOW_TOAST }, { type: SUBMIT_LEAD_FAILURE }];
        const { name, email, gymId } = supportVariables;
        await store.dispatch(sendLeadAction({ email, name, gymId }));
        store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
      });
      it('should show on lead success', async () => {
        const expectedActions = [
          { type: SUBMIT_LEAD_REQUEST },
          { type: SHOW_TOAST },
          { type: SUBMIT_LEAD_SUCCESS }
        ];
        const { name, email, phone, gymId } = supportVariables;
        await store.dispatch(sendLeadAction({ email, name, phone, gymId }));
        store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
      });
    });

    describe('Support', () => {
      it('should show on ticket support error - missing data', async () => {
        const expectedActions = [{ type: SHOW_TOAST }, { type: SEND_TICKET_FAILURE }];
        const { name, email, gymId, phone } = supportVariables;
        await store.dispatch(sendTicketAction({ email, name, gymId, phone }));
        store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
      });
      it('should show on ticket support success', async () => {
        const expectedActions = [
          { type: SEND_TICKET_REQUEST },
          { type: SHOW_TOAST },
          { type: SEND_TICKET_SUCCESS }
        ];
        const { name, email, gym, phone, message } = supportVariables;
        await store.dispatch(sendTicketAction({ email, name, phone, gym, message }));
        store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
      });
    });

    describe('Search', () => {
      it('should show on max filters for gyms', async () => {
        const expectedActions = [{ type: SHOW_TOAST }, { type: MAX_GYMS_FILTERS }];
        await store.dispatch(maxGymsFilters());
        store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
      });
      it('should show on max filters for modalities', async () => {
        const expectedActions = [{ type: SHOW_TOAST }, { type: MAX_MODALITIES_FILTERS }];
        await store.dispatch(maxModalitiesFilters());
        store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
      });
      it('should show on no results', async () => {
        const expectedActions = [{ type: SHOW_TOAST }, { type: NO_RESULTS }];
        await store.dispatch(noResults());
        store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
      });
    });

    describe('Classes', () => {
      describe('Book', () => {
        it('should show on class book error - already booked', async () => {
          const expectedActions = [{ type: SHOW_TOAST }, { type: BOOK_CLASS_FAILURE }];
          const variables = classesVariables.book;
          await store.dispatch(bookClassAction({ ...variables, isBooked: true }));
          store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
          const [, error] = store.getActions();
          expect(error.error).toEqual(mapError(classesErrors.booked));
        });
        it('should not show on class book error - only premium', async () => {
          const expectedActions = [{ type: BOOK_CLASS_FAILURE }];
          const variables = classesVariables.book;
          await store.dispatch(
            bookClassAction({
              ...variables,
              date: moment()
                .add(12, 'hours')
                .toDate()
            })
          );
          store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
          const [error] = store.getActions();
          expect(error.error).toEqual(mapError(classesErrors.premium));
        });
        it('should show on class book error - auth error', async () => {
          const expectedActions = [
            { type: BOOK_CLASS_REQUEST },
            { type: SHOW_TOAST },
            { type: BOOK_CLASS_FAILURE }
          ];
          const variables = classesVariables.book;
          await store.dispatch(bookClassAction({ ...variables }));
          store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
          const [, , error] = store.getActions();
          expect(error.error).toEqual(mapError(classesErrors.auth));
        });
      });
      describe('Cancel', () => {
        it('should show on class cancel error - id error', async () => {
          const expectedActions = [
            { type: CANCEL_CLASS_REQUEST },
            { type: SHOW_TOAST },
            { type: CANCEL_CLASS_FAILURE }
          ];
          const variables = classesVariables.cancel;
          await store.dispatch(cancelClassAction({ ...variables, cancelId: '' }));
          store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
          const [, , error] = store.getActions();
          expect(error.error).toEqual(mapError(classesErrors.notBooked));
        });
        it('should show on class cancel error - auth error', async () => {
          const expectedActions = [
            { type: CANCEL_CLASS_REQUEST },
            { type: SHOW_TOAST },
            { type: CANCEL_CLASS_FAILURE }
          ];
          const variables = classesVariables.cancel;
          await store.dispatch(cancelClassAction({ ...variables }));
          store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
          const [, , error] = store.getActions();
          expect(error.error).toEqual(mapError(classesErrors.auth));
        });
      });
    });

    describe('Register', () => {
      it('should show on register error - passwords not matching', async () => {
        const expectedActions = [{ type: SHOW_TOAST }, { type: REGISTER_FAILURE }];
        const variables = registerVariables.failure;
        await store.dispatch(registerAction(variables));
        store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
      });
      it('should show on token verification error', async () => {
        const expectedActions = [{ type: SHOW_TOAST }, { type: VERIFY_TOKEN_FAILURE }];
        await store.dispatch(verifyTokenAction());
        store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
      });
      it('should show on register success', async () => {
        const expectedActions = [{ type: SHOW_TOAST }, { type: REGISTER_SUCCESS }];
        await store.dispatch(registerSuccess({ isRegistered: true, isActive: true }));
        store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
      });
    });
  });
});

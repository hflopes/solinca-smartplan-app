import { REGISTER_FAILURE } from '../modules/auth/reducers/register';
import { LOGIN_SUCCESS, LOGOUT, LOGIN_WAIT, LOGIN_FAILURE } from '../modules/auth/reducers/login';
import { FETCH_USER_INFO_SUCCESS, UPDATE_PHOTO_SUCCESS } from '../modules/user/reducer';
import { APP_START, APP_ONLINE } from '../modules/app/reducer';
import fetchUser, { fetchTemporaryUserSuccess } from '../modules/user/actions/fetchData';
import { loginFailureError, loginWaitReset } from '../modules/auth/actions/login';
import { statusUpgraded } from '../modules/user/actions/status';
import { updateToken } from '../../config/api';
import STATUSES from '../../config/statuses';
import { setupUserId } from '../../utils/push/onesignal';
import toggleAPIURL from '../modules/app/actions/toggleApiUrl';
import { registerFailureError } from '../modules/auth/actions/register';
import { RESET_PASSWORD_FAILURE } from '../modules/auth/reducers/resetPassword';
import { resetPasswordFailureError } from '../modules/auth/actions/resetPassword';

const authMiddleware = store => next => action => {
  const { type } = action;

  switch (type) {
    case APP_ONLINE:
    case APP_START: {
      const { login } = store.getState();
      const { leaseToken } = login;
      updateToken(leaseToken);
      break;
    }

    case LOGOUT: {
      updateToken(null);
      break;
    }

    case LOGIN_FAILURE: {
      const { checkedAllUrls } = store.getState().app;
      if (!checkedAllUrls) {
        store.dispatch(toggleAPIURL());
      } else {
        store.dispatch(loginFailureError(action.error, action.isAuto));
      }
      break;
    }

    case RESET_PASSWORD_FAILURE: {
      const { checkedAllUrls } = store.getState().app;
      if (!checkedAllUrls) {
        store.dispatch(toggleAPIURL());
      } else {
        store.dispatch(resetPasswordFailureError(action.error));
      }
      break;
    }

    case LOGIN_WAIT: {
      setTimeout(() => {
        store.dispatch(loginWaitReset());
      }, 10000);
      break;
    }

    case LOGIN_SUCCESS: {
      const { user, leaseToken } = action;
      updateToken(leaseToken);
      store.dispatch(fetchTemporaryUserSuccess(user));
      break;
    }

    case REGISTER_FAILURE: {
      const { isActive, isRegistered } = action;
      if (isActive && isRegistered) {
        store.dispatch(registerFailureError(action.error, { isActive, isRegistered }));
      } else {
        const { checkedAllUrls } = store.getState().app;
        if (!checkedAllUrls) {
          store.dispatch(toggleAPIURL());
        } else {
          store.dispatch(registerFailureError(action.error, { isActive, isRegistered }));
        }
      }
      break;
    }

    case FETCH_USER_INFO_SUCCESS: {
      const userStore = store.getState().user;
      const status = userStore.data.status || 'none';
      if (status) {
        const newStatus = action.user.status || 'none';
        const statusIndex = STATUSES.findIndex(s => s === status);
        const newStatusIndex = STATUSES.findIndex(s => s === newStatus);
        if (newStatusIndex > statusIndex) {
          store.dispatch(statusUpgraded());
        }
      }

      // TODO: Test this
      const { cedisId } = userStore.data;
      if (cedisId) {
        setupUserId(cedisId);
      }
      break;
    }

    case UPDATE_PHOTO_SUCCESS: {
      store.dispatch(fetchUser());
      break;
    }

    default:
  }

  return next(action);
};

export default authMiddleware;

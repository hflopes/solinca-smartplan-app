import i18n from 'i18next';

/*
- 1000 range: APPAPI auth errors
- 5000 range: APPAPI handled errors
- 6000 range: APP internal errors
- 7000 range: APPAPPI-Backoffice communication errors
- 8000 range: CEDIS errors
*/

const ERROR_CODES = {
  0: 'errors.default',
  1000: 'errors.noAuth',
  1001: 'errors.noAccess',
  1100: 'errors.absencesBlocked',
  1101: 'errors.absencesBlocked',
  6000: 'errors.passwordsDontMatch',
  6001: 'errors.classAlreadyBooked',
  6002: 'errors.classNotBooked',
  6003: 'errors.premiumBlocked',
  6004: 'errors.invalidLeadParams',
  6005: 'errors.invalidTicketParams',
  6006: 'errors.invalidUser',
  6007: 'errors.classUnavailable',
  8000: 'errors.wrongParameters',
  8001: 'errors.login',
  8002: 'errors.invalidToken',
  8003: 'errors.duplicatedRegister'
};

export default function mapError(code) {
  const message = ERROR_CODES[code] || ERROR_CODES[0];
  return i18n.t(message) || message;
}

export function editList(list, id, params) {
  if (!list[id]) {
    return list;
  }

  let newValue;

  // If value isn't provided, consider this field
  // a boolean and toggle its value
  if (typeof params === 'string') {
    newValue = { ...list[id], [params]: !list[id][params] };
  } else {
    newValue = { ...list[id], ...params };
  }

  return { ...list, [id]: newValue };
}

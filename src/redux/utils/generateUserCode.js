import Chance from 'chance';

const CODE_LENGTH = 6;

export default function generateUserCode(userId) {
  if (!userId) throw Error('Missing userId to generate code');

  const userSeed = new Chance(userId);
  const code = userSeed.hash({ length: CODE_LENGTH, casing: 'upper' });
  return code;
}

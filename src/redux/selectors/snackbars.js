import { createSelector } from 'reselect';

const snackbarState = state => state.snackbar;

export const isSnackbarVisible = createSelector([snackbarState], snackbarData => snackbarData.visible);
export const getSnackbar = createSelector([snackbarState], snackbarData => snackbarData.snackbar);

import { createSelector } from 'reselect';
import _ from 'lodash';

const termsState = state => state.terms.data;

export const getTOS = createSelector([termsState], terms => _.find(terms, t => t.type === 'termsconditions'));

export const getContactTOS = createSelector([termsState], terms =>
  _.find(terms, t => t.type === 'inforight')
);

/* eslint-disable import/prefer-default-export */
import { createSelector } from 'reselect';

const reloadState = state => state.reload;

export const upcomingClassesLastUpdate = createSelector(
  [reloadState],
  state => state.lastUpdates.upcomingClasses
);

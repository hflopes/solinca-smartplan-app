import { createSelector } from 'reselect';

const loginState = state => state.login;
const registerState = state => state.register;
const resetPasswordState = state => state.resetPassword;
const tokenState = state => state.token;

export const getLastLogin = createSelector([loginState], login => login.lastLogin);

export const userIsSyncing = createSelector([loginState], login => login.isSyncing);
export const userHasSynced = createSelector([loginState], login => login.hasSynced);
export const userIsLoggedIn = createSelector([loginState], login => login.isLoggedIn);
export const userIsLoggingIn = createSelector([loginState], login => login.isLoggingIn);
export const userIsWaitingReset = createSelector([loginState], login => login.waitingReset);
export const userIsCanceledOrSuspended = createSelector([loginState], login => login.isCanceledOrSuspended);
export const userIsNewUser = createSelector([loginState], login => login.isNewUser);
export const showOnboarding = createSelector([loginState], login => login.showOnboarding);
export const shouldReadTos = createSelector([loginState], login => login.shouldReadTos);

export const userIsRegistering = createSelector([registerState], register => register.isRegistering);
export const userIsActive = createSelector([registerState], register => register.isActive);
export const userIsRegistered = createSelector([registerState], register => register.isRegistered);
export const userHasRequestedRegister = createSelector([registerState], register => register.hasRequested);

export const userIsResetting = createSelector([resetPasswordState], reset => reset.isResetting);
export const userHasRequestedReset = createSelector([resetPasswordState], reset => reset.hasRequestedReset);

export const userIsVerifyingToken = createSelector([tokenState], token => token.isVerifyingToken);
export const userHasRequestedToken = createSelector([tokenState], token => token.hasRequested);
export const tokenIsVerified = createSelector([tokenState], token => token.isTokenVerified);

export const cancelClassMinutes = createSelector([loginState], login => login.maxMinutesAllowedToCancelClass);
export const hasBlockedWarningMessage = createSelector([loginState], login => login.blockedWarningMessage);

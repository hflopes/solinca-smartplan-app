import { createSelector } from 'reselect';
import moment from 'moment';
import _ from 'lodash';
import { getUserNumber } from './user';

const challengesState = state => state.challenges;
const badgesState = state => state.badges;

export const getChallenges = createSelector([challengesState], ({ data }) => data);
export const getBadges = createSelector([badgesState], ({ data }) => data);
export const getChallengesList = createSelector(
  [getChallenges, getBadges, getUserNumber],
  (challenges, badges, userNumber) => {
    return _.compact(
      _.map(challenges, challenge => {
        const tracker = badges[challenge.id];
        return format(challenge, tracker, userNumber);
      })
    );
  }
);

export const getChallengesGrouped = createSelector([getChallengesList], challenges => {
  const categoryMap = {};

  Object.values(challenges).forEach(challenge => {
    const { group, hasFinished, challengeType } = challenge;
    if (hasFinished || challengeType === 'notvisible') return;
    let code = 'other';
    let name = '';
    if (group) {
      ({ code, name } = challenge.group);
      if (code === 'convidar_amigo') return;
    }
    if (categoryMap[code]) {
      categoryMap[code].challenges.push(challenge);
    } else {
      categoryMap[code] = {
        groupDescription: name,
        badge: challenge.badge || '',
        challenges: [challenge]
      };
    }
  });

  return categoryMap;
});

export const getHighlightedChallenges = createSelector([getChallengesList], challenges => {
  return challenges.filter(c => Boolean(c.highlight));
});

export const getInviteFriendsChallenge = createSelector([getChallengesList], challenges => {
  return challenges.find(c => Boolean(c.group && c.group.code === 'convidar_amigo'));
});

export const getOngoingChallenges = createSelector([getChallengesList], challenges => {
  return challenges.filter(c => c.enrolled && !c.hasFinished);
});

export const getFinishedChallenges = createSelector([getChallengesList], challenges => {
  return challenges.filter(c => c.hasFinished);
});

export const getLastestFinishedChallenge = createSelector(
  [getChallengesList, getBadges],
  (challenges, badges) => {
    const challenge = _.first(challenges.filter(c => c.completedSinceLastSession));
    return badges[challenge.id];
  }
);

export const getChallenge = (state, id) => {
  const obj = getChallenges(state)[id];
  const tracker = getBadges(state)[id];
  const memberNumber = getUserNumber(state);

  return format(obj, tracker, memberNumber);
};

export const isJoiningOrCancelling = createSelector(
  [challengesState],
  ({ isJoining, isCanceling }) => isJoining || isCanceling
);

function format(challenge, tracker, memberNumber) {
  if (challenge.challengeType === 'notvisible') return null;
  const toBePubslished = moment(challenge.datePublish).isAfter();
  const publishedInTheLastWeek = moment().diff(moment(challenge.datePublish), 'days') < 7;
  const isNew = toBePubslished || publishedInTheLastWeek;
  const inviteCode = challenge.invitefriend_code ? memberNumber : '';
  const inviteURL = challenge.invitefriend_pageurl
    ? challenge.invitefriend_pageurl.replace('%CODE%', inviteCode)
    : '';
  const inviteText = challenge.invitefriend_share
    ? challenge.invitefriend_share.replace('%CODE%', inviteCode)
    : '';
  const totalStamps = challenge.lesmills_numberclasses || 0;
  const filledStamps = challenge.lesmills_classestaken || 0;
  const progress = Math.min(100, tracker ? tracker.percentage : challenge.user_percentage);
  const hasFinished = progress >= 100;
  const hasJoined = challenge.enrolled;
  const hasRanking = Boolean(challenge.ranking_challenge);
  const userRanking = challenge.user_ranking;
  const usersInChallenge = challenge.totalEnrolled;
  const dateLastUpdate = !_.isEmpty(tracker) ? tracker.dateLastUpdate : null;
  return {
    ...challenge,
    tracker,
    isNew,
    inviteCode,
    inviteURL,
    inviteText,
    totalStamps,
    filledStamps,
    progress,
    hasFinished,
    hasJoined,
    hasRanking,
    userRanking,
    usersInChallenge,
    dateLastUpdate
  };
}

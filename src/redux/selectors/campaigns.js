import { createSelector } from 'reselect';

const campaignsState = state => state.campaigns;

export const isFetchingCampaign = createSelector([campaignsState], ({ isFetching }) => isFetching);
export const getCampaignData = createSelector([campaignsState], ({ data }) => data);

import { createSelector } from 'reselect';
import _ from 'lodash';
import moment from 'moment';
import { getModalities } from './modalities';
import { capitalize } from '../../utils/strings';
import { getDayOfWeek } from '../../utils/dates';

const PARAM_FIELDS = ['series', 'repetitions', 'level', 'speed', 'weight'];
const GOALS_PER_MONTH = 4;

export const planState = state => state.plan;

export const isExecutingExercise = createSelector([planState], state => state.isExecutingExercise);
export const isLoadingPlans = createSelector([planState], ({ hasFetched }) => !hasFetched);
export const hasFailedToFetchPlan = createSelector(
  [planState],
  ({ hasFetched, error }) => hasFetched && !!error
);
export const hasVisited = createSelector([planState], state => state.hasVisited);
export const isResting = createSelector([planState], state => state.isResting);

export const getSetup = createSelector([planState], ({ setup }) => setup);
export const getCurrentSession = createSelector([planState], ({ currentSession }) => currentSession);
export const getCurrentPlan = createSelector([planState], ({ currentPlan }) => currentPlan);
export const isExecutingPlan = createSelector([getCurrentSession], session => session !== null);

export const getPlans = createSelector([planState], ({ data }) => data.plans);
export const getObjective = createSelector([planState], ({ data }) => data.objective);

export const getGoalPlans = createSelector([planState], ({ data }) =>
  data.goals && data.goals.plans ? data.goals.plans.length * GOALS_PER_MONTH : -1
);
export const getGoalClasses = createSelector([planState], ({ data }) =>
  data.goals && data.goals.classes ? data.goals.classes.length * GOALS_PER_MONTH : -1
);
export const getPlanClasses = createSelector([planState], ({ data }) =>
  data.goals && data.goals.classes ? data.goals.classes : []
);

export const getBiometricData = createSelector([planState], state => {
  return state && state.data && state.data.biometric ? state.data.biometric.data : null;
});

export const getBiometricDate = createSelector([planState], state => {
  return state && state.data && state.data.biometric ? state.data.biometric.date : null;
});

export const getBiometricDateGap = createSelector([getBiometricDate], biometricDate => {
  const currentDate = new Date();
  const plusOneMonth = moment(biometricDate).add(1, 'month');

  if (!biometricDate) {
    return [null, null];
  }

  if (currentDate < plusOneMonth.toDate()) {
    return [biometricDate, plusOneMonth.toDate()];
  }

  const futureBiometricDate = moment(biometricDate)
    .set({ month: currentDate.getMonth() })
    .toDate();

  if (currentDate < futureBiometricDate) {
    const before = moment(futureBiometricDate).subtract(1, 'month');
    return [before.toDate(), futureBiometricDate];
  }

  const after = moment(futureBiometricDate).add(1, 'month');
  return [futureBiometricDate, after.toDate()];
});

export const hasPlan = createSelector([getPlans], plans => {
  return plans && plans.length > 0;
});

export const hasErrorCoach = createSelector([planState], state => {
  return state.planError;
});

export const getRecommendedModalities = createSelector(
  [getModalities, getPlanClasses],
  (modalities, planClasses) => planClasses.map(id => modalities[id] || null).filter(m => m)
);

export const getCurrentExercises = createSelector([getCurrentPlan], currentPlan => {
  const newExercices = [];

  if (!currentPlan || !currentPlan.exercises) {
    return [];
  }

  currentPlan.exercises.forEach(ex => {
    const newEx = { ...ex };
    newEx.params = [];

    // Convert any PARAM FIELDS into a params array
    // Ex: params: [{label: 'series', value: 5}, {label: 'repetitions', value: 2}]
    PARAM_FIELDS.forEach(f => {
      if (newEx[f]) {
        newEx.params.push({
          label: f,
          value: newEx[f]
        });

        delete newEx[f];
      }
    });

    // Capitalize only the first letter of each word
    Object.keys(newEx).forEach(key => {
      if (
        key !== 'instructions' &&
        key !== 'observations' &&
        key !== 'id' &&
        key !== 'image' &&
        key !== 'gif' &&
        key !== 'clipUrl' &&
        typeof newEx[key] === 'string'
      ) {
        newEx[key] = capitalize(newEx[key]);
      }
    });

    newExercices.push(newEx);
  });

  return newExercices;
});

export const getCompletedExercises = createSelector([getCurrentSession], session =>
  session ? session.exercises.filter(e => e.duration > 0) : []
);

export const getProgress = createSelector(
  [getCurrentSession, getCompletedExercises],
  (session, completed) => {
    return session ? `${completed.length}/${session.exercises.length}` : '';
  }
);

export const getNextExercise = createSelector([getCurrentSession], session => {
  if (!session) return null;

  const completedIndexes = [];

  session.exercises.forEach((e, i) => {
    if (e.duration > 0) {
      completedIndexes.push(i);
    }
  });

  if (completedIndexes.length === 0) {
    return null;
  }

  const maxIndex = Math.max(...completedIndexes);

  if (maxIndex + 1 >= session.exercises.length) {
    return null;
  }

  return session.exercises[maxIndex + 1];
});

export const isPlanCompleted = createSelector(
  [getCurrentSession, getCompletedExercises],
  (session, completed) => {
    return completed.length === session.exercises.length;
  }
);

export const getCurrentSessionExercise = createSelector([getCurrentSession], session => {
  if (!session) return null;
  return session.currentExercise;
});

export const getExercise = (state, id) => {
  const exercises = getCurrentExercises(state);
  const matches = exercises.filter(e => e.id === id);

  if (matches && matches.length > 0) {
    return matches[0];
  }

  return null;
};

export const isExerciseCompleted = (state, id) => {
  const currentSession = getCurrentSession(state);

  if (!currentSession) {
    return false;
  }

  const { exercises } = currentSession;
  const matches = exercises.filter(e => e.id === id);

  if (matches && matches.length > 0) {
    return matches[0].duration > 0;
  }

  return false;
};

export const getWeekPlans = createSelector([planState], ({ data }) => {
  // const startDate = moment()
  //   .startOf('day')
  //   .toDate();

  // const endDate = moment()
  //   .startOf('day')
  //   .add(1, 'week')
  //   .toDate();

  const plans = [];
  const allPlans = {};
  const days = [];

  data.plans.forEach(p => {
    p.dates.forEach(date => {
      if (moment(date).isBefore(moment().startOf('week')) || moment(date).isAfter(moment().endOf('week'))) {
        return;
      }
      const day = getDayOfWeek(date.getDay()).substr(0, 3);
      const hasDay = days.indexOf(day) !== -1;
      // if (date >= startDate && date < endDate) {
      if (!hasDay) {
        days.push(day);
        plans.push({
          name: p.name,
          day,
          date,
          exercises: p.exercises,
          modalities: p.modalities
        });
      }
      const key = `${p.name}-${day}`;
      if (!allPlans[key]) {
        allPlans[key] = {
          ...p,
          day,
          date
        };
      }
    });
  });

  return plans.length
    ? plans.sort((a, b) => a.date - b.date)
    : _.flatMap(allPlans).sort((a, b) => a.date - b.date);
});

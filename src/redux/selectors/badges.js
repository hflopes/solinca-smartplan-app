import { createSelector } from 'reselect';
import _ from 'lodash';

const badgesState = state => state.badges;
const challengesState = state => state.challenges;

const getChallenges = createSelector([challengesState], ({ data }) => data);

export const getBadges = createSelector([badgesState], ({ data }) => data);
export const getBadgesList = createSelector([getBadges, getChallenges], (badges, challenges) => {
  return _.compact(
    _.map(badges, badge => {
      const challengeInfo = challenges[badge.challengeId];
      if (!challengeInfo) return null;
      const { group, points, challengeType } = challengeInfo;
      if (challengeType === 'notvisible') return null;
      const unlocked = true;
      let image = '';
      let name = '';
      if (group) {
        ({ name } = group);
        image = unlocked ? group.url_image_enabled : group.url_image_disabled;
      }
      return {
        id: badge.challengeId,
        name,
        points,
        unlocked,
        date: badge.dateEnd,
        image
      };
    })
  );
});

export const getUnlockedBadges = createSelector([getBadgesList], badges => badges.filter(b => b.unlocked));
export const getLastestUnlockBadge = createSelector(
  [getUnlockedBadges],
  badges => badges.sort((a, b) => a.date - b.date)[0]
);
export const getUnlockedBadgesCount = createSelector([getUnlockedBadges], badges => badges.length);

export const getRandomBadge = createSelector([getBadgesList], badges => {
  if (!badges || badges.length === 0) {
    return null;
  }

  return badges[Math.floor(Math.random() * badges.length) + 1];
});

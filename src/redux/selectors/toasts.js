import { createSelector } from 'reselect';

const toastsState = state => state.toasts;

export const isToastVisible = createSelector([toastsState], toastData => toastData.visible);
export const getToast = createSelector([toastsState], toastData => toastData.toast);

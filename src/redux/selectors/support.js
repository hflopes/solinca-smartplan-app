import { createSelector } from 'reselect';

const supportState = state => state.support;

export const hasSentLead = createSelector([supportState], support => support.hasSentLead);
export const hasSentTicket = createSelector([supportState], support => support.hasSentTicket);
export const isSendingLead = createSelector([supportState], support => support.isSendingLead);
export const isSendingTicket = createSelector([supportState], support => support.isSendingTicket);
export const getContactForm = createSelector([supportState], support => support.form);

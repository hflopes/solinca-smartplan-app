import { createSelector } from 'reselect';
import moment from 'moment';
import { getModalitiesList, getModalities } from './modalities';
import { getGymsListOrderedByDistance, getGyms } from './gyms';

const searchState = state => state.search;

export const isFetching = createSelector([searchState], state => state.isFetching);
export const getRecentQueries = createSelector([searchState], ({ recentQueries }) => recentQueries);
export const getResults = createSelector([searchState], ({ results }) => results);

export const getTerms = createSelector(
  [getModalitiesList, getGymsListOrderedByDistance],
  (modalities, gyms) => {
    return { modalities, gyms };
  }
);

export const getRecentTerms = createSelector(
  [getRecentQueries, getModalitiesList, getGymsListOrderedByDistance],
  (queries, modalities, gyms) => {
    let gymIds = [];
    let modalityIds = [];

    // Collect all gym and modality ids from recent queries
    queries.forEach(query => {
      gymIds.push(...query.filter(q => q.type === 'gym').map(q => q.id));
      modalityIds.push(...query.filter(q => q.type === 'modality').map(q => q.id));
    });

    // Ensure the ids are unique and only
    // appear once, even if in multiple queries
    gymIds = [...new Set(gymIds)];
    modalityIds = [...new Set(modalityIds)];

    // Reverse the arrays so more recent ids
    // show up first on the list
    gymIds = gymIds.reverse();
    modalityIds = modalityIds.reverse();

    // Get the gym and modalities objects, filtered by id
    // and add the "type" field to them, for easier rendering
    const recentGyms = gyms.filter(g => gymIds.includes(g.id));
    const recentModalities = modalities.filter(m => modalityIds.includes(m.id));

    return {
      gyms: recentGyms,
      modalities: recentModalities
    };
  }
);

/**
 * [date]: {
 *  82: {
 *        gym: {},
 *        classes: [{}, {}],
 *      }
 *  89: {
 *        gym: {},
 *        classes: [{}, {}],
 *      }
 * }
 */
export const getFormattedResults = createSelector(
  [getResults, getGyms, getModalities],
  (results, gyms, modalities) => {
    const obj = {};
    const flatResults = Object.values(results).flat();

    flatResults.forEach(r => {
      const { dateStart, gymId } = r;
      const date = moment(dateStart).toDate();
      const result = { ...r, modality: { ...modalities[r.modalityId] } };

      date.setMinutes(0);
      date.setHours(0);
      date.setSeconds(0);

      if (obj[date]) {
        if (obj[date][gymId]) {
          obj[date][gymId] = { ...obj[date][gymId], classes: [...obj[date][gymId].classes, result] };
        } else {
          obj[date][gymId] = { gym: gyms[gymId], classes: [result] };
        }
      } else {
        obj[date] = {
          [gymId]: { gym: gyms[gymId], classes: [result] }
        };
      }
    });
    if (Object.fromEntries) {
      const orderedResults = new Map();
      Object.keys(obj).forEach(k => {
        orderedResults.set(k, obj[k]);
      });
      return Object.fromEntries(orderedResults);
    }

    return obj;
  }
);

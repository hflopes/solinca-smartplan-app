import { createSelector } from 'reselect';

const appState = state => state.app;

export const isAppOffline = createSelector([appState], ({ offline }) => offline);
export const getLoadingModules = createSelector([appState], ({ loadingModules }) => loadingModules);

export const getPlatform = createSelector([appState], ({ platform }) => platform.toLowerCase());

export const appApiURL = createSelector([appState], ({ apiURL }) => apiURL);
export const checkedAllURLs = createSelector([appState], ({ checkedAllUrls }) => checkedAllUrls);

export const getComponentStateStack = createSelector(
  [appState],
  ({ componentStateStack }) => componentStateStack
);

export const hasFetchedInitialData = createSelector(
  [appState],
  ({ fetchedInitialData }) => fetchedInitialData
);

export const hasErrorFetchingInitialData = createSelector(
  [appState],
  ({ errorFetchingInitialData }) => errorFetchingInitialData
);

import { createSelector } from 'reselect';
import moment from 'moment';

const userState = state => state.user;
const getSubscriptionType = state => state.login.subscriptionType;
const getRestrictionType = state => state.login.restrictionType;

export const getUserData = createSelector([userState], user => user.data);
export const isFetchingUser = createSelector([userState], user => user.isFetching);
export const userAuthToken = createSelector([userState], user => user.token);
export const getUserName = createSelector([getUserData], user => user.name || '');
export const getUserEmail = createSelector([getUserData], user => user.email || '');
export const getUserPhoto = createSelector([getUserData], user => user.photo);
export const getUserStatus = createSelector([getUserData], user => user.status || 'none');
export const getUserFirstName = createSelector([getUserData], user => user.name.split(' ')[0] || null);
export const getUserPosition = createSelector([getUserData], data => data.position);
export const getUserDefaultGymId = createSelector([getUserData], data => data.defaultGym);
export const getUserNumber = createSelector([getUserData], data => data.memberNumber);
export const getUserGender = createSelector([getUserData], data => data.sex);
export const getUserPoints = createSelector([getUserData], data => data.points || 0);

export const getUser3dBody = createSelector([userState], state => state.url3dBody);

export const getUserAge = createSelector([getUserData], user =>
  moment().diff(moment(user.birthDate), 'years')
);

export const getUserContactData = createSelector([getUserData], ({ defaultGym, email, phone, name }) => ({
  defaultGym,
  email,
  phone,
  name
}));

export const isPremium = createSelector([getSubscriptionType], subscriptionType => {
  return subscriptionType.toLowerCase() === 'premium';
});

export const isOffPeak = createSelector([getRestrictionType], restrictionType => {
  return restrictionType.toLowerCase() === 'offpeak';
});

export const userUpgradedStatus = createSelector([userState], user => user.upgradedStatus);

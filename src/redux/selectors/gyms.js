import { createSelector } from 'reselect';
import _ from 'lodash';
import { getUserPosition, getUserDefaultGymId } from './user';
import calculateDistance from '../utils/coordinates';

const gymsState = state => state.gyms;

export const getSelectedGymId = createSelector([gymsState], ({ selectedGymId }) => selectedGymId);
export const getGyms = createSelector([gymsState], ({ data }) => data);
export const getGymsList = createSelector([getGyms], gyms => _.values(gyms));

export const getGymsListOrderedByDistance = createSelector(
  [getGymsList, getUserPosition],
  (gyms, userCoordinates = {}) => {
    try {
      const latitude = userCoordinates.latitude || 0;
      const longitude = userCoordinates.longitude || 0;

      if (latitude && longitude) {
        return _.chain(gyms)
          .map(gym => ({
            ...gym,
            distance: calculateDistance(
              latitude,
              longitude,
              gym.coordinates.latitude,
              gym.coordinates.longitude
            )
          }))
          .value()
          .sort((a, b) => a.distance - b.distance);
      }
      return gyms;
    } catch (error) {
      return gyms;
    }
  }
);

export const getGymsListOrderedByName = createSelector([getGymsList], gyms =>
  _.sortBy(gyms, gym => gym.name)
);

export const getSelectedGym = createSelector([getGyms, getSelectedGymId], (gyms, selectedGymId) => {
  return gyms[selectedGymId];
});

export const getBaseGym = createSelector(
  [getUserDefaultGymId, getGymsList],
  (defaultGymId, gymList) => gymList.filter(g => g.id === defaultGymId)[0]
);

/**
 * Returns a specific gym, from a given gym id
 */
export const getGym = (state, id) => {
  const gyms = getGyms(state);
  return gyms[id];
};

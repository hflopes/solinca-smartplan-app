/* eslint-disable import/prefer-default-export */
import { createSelector } from 'reselect';

const deviceState = state => state.device;

export const keyboardIsUp = createSelector([deviceState], ({ isKeyboardUp }) => isKeyboardUp);

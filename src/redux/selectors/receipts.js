/* eslint-disable import/prefer-default-export */
import { createSelector } from 'reselect';
// import _ from 'lodash';

const userState = state => state.user;

export const getUserReceipts = createSelector([userState], user => user.receiptsList);
export const isFetchingUserReceipts = createSelector([userState], user => user.isFetchingReceipts);

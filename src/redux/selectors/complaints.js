import { createSelector } from 'reselect';
import moment from 'moment';

const complaintsState = state => state.complaints;

export const getComplaints = createSelector([complaintsState], complaints => complaints.data);
export const getComplaintsValues = createSelector([complaintsState], complaints =>
  Object.values(complaints.data)
);
export const isSending = createSelector([complaintsState], complaints => complaints.isSending);
export const isFetching = createSelector([complaintsState], complaints => complaints.isFetching);

// Remove the complaints with dates older than 1 year (12 months)
export const getLatestComplaints = createSelector([getComplaintsValues], complaints =>
  complaints.filter(complaint => moment(complaint.date).isAfter(moment().subtract(1, 'year')))
);

export const getUnreadComplaints = createSelector([getComplaintsValues], complaints =>
  complaints.filter(complaint => complaint.isUnread)
);

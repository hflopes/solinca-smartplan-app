import moment from 'moment';
import { createSelector } from 'reselect';
import i18n from 'i18next';
import { getRecommendedModalities } from './plan';
import { getModalities } from './modalities';
import { getUserAge, getUserGender } from './user';
import { getFatTier } from '../../utils/fitness';

const dashboard = state => state.dashboard;
const plansHistory = state => state.dashboard.plansHistory;
const classesHistory = state => state.dashboard.classesHistory;
const evaluationsHistory = state => state.dashboard.evaluationsHistory;

export const hasSeenFatGraph = createSelector([dashboard], state => state.seenFatGraph);

export const hasSeenIMCGraph = createSelector([dashboard], state => state.seenIMCGraph);

export const hasSeenWeightGraph = createSelector([dashboard], state => state.seenWeightGraph);

export const getWeightHistory = createSelector([evaluationsHistory], history => {
  return history
    .map(({ date, bodyComposition }) => {
      const biometricEntry = bodyComposition.find(b => b.title === 'Peso');
      const weight = biometricEntry && parseInt(biometricEntry.value, 10);
      return { date, weight };
    })
    .sort((a, b) => a.date - b.date);
});

export const getIMCHistory = createSelector([evaluationsHistory], history => {
  return history
    .map(({ date, bodyComposition }) => {
      const biometricEntry = bodyComposition.find(b => b.title === 'IMC');
      const imc = biometricEntry && parseFloat(biometricEntry.value);
      return { date, imc };
    })
    .sort((a, b) => a.date - b.date);
});

export const getFatHistory = createSelector([evaluationsHistory], history => {
  return history
    .map(({ date, bodyComposition }) => {
      const biometricEntry = bodyComposition.find(b => b.title === '% Massa Gorda');
      const fat = biometricEntry && parseFloat(biometricEntry.value);
      return { date, fat };
    })
    .sort((a, b) => a.date - b.date);
});

export const getContextMessage = createSelector(
  [evaluationsHistory, getUserAge, getUserGender],
  (history, userAge, gender) => {
    const threeMonthsAgo = moment()
      .subtract(3, 'month')
      .toDate();

    const recentEvals = history.filter(history => history.date > threeMonthsAgo);

    if (recentEvals.length > 0) {
      const latestEval = recentEvals[recentEvals.length - 1];
      const currentFat = latestEval.bodyComposition.find(b => b.title === '% Massa Gorda').value;
      const fatPercentageRating = getFatTier(userAge, gender, parseFloat(currentFat));

      if (recentEvals.length > 1) {
        const lastTwo = recentEvals.reverse().slice(0, 2);

        const startObj = lastTwo[0].bodyComposition.find(b => b.title === 'Peso');
        const endObj = lastTwo[1].bodyComposition.find(b => b.title === 'Peso');

        const startWeight = startObj ? parseFloat(startObj.value) : 0;
        const endWeight = endObj ? parseFloat(endObj.value) : 0;

        const weightDiff = endWeight - startWeight;

        // Has lost weight in between the last two evals
        if (weightDiff > 0) {
          return i18n.t('screens.Dashboard.contextMessage.lostWeight', { lostWeight: weightDiff.toFixed(2) });
        }
        return i18n.t('screens.Dashboard.contextMessage.fatProgress', { fatPercentageRating });
      }
      return i18n.t('screens.Dashboard.contextMessage.fatProgress', { fatPercentageRating });
    } else if (history.length > 0) {
      // Had a re-eval more than 3 months ago
      if (history.length > 1 && history[history.length - 1].date < threeMonthsAgo) {
        return i18n.t('screens.Dashboard.contextMessage.scheduleAFI');
      }
      return i18n.t('screens.Dashboard.contextMessage.redoAFI');
    }
    return i18n.t('screens.Dashboard.contextMessage.scheduleAFI');
  }
);

const getClassesHistory = createSelector([classesHistory, getModalities], (history, modalities) => {
  if (!history || !history.length) {
    return [];
  }

  return history.map(c => {
    let modalityData = modalities[c.modalityId];
    if (!modalityData) {
      modalityData = Object.values(modalities).find(m => m.name === c.className.split(' ')[0]);
    }

    return {
      date: c.dateInsert,
      modality: modalityData,
      modalityId: c.modalityId
    };
  });
});

const getPlansHistory = createSelector([plansHistory], history => history);

export function getActivityCount(state, dateStart, dateEnd, activityType) {
  const activities = getActivities(state).filter(
    ({ type, date }) =>
      (type === activityType || type.indexOf(activityType) !== -1) && date >= dateStart && date <= dateEnd
  );

  return activities.length;
}

export const getActivities = createSelector(
  [getClassesHistory, getPlansHistory, getRecommendedModalities],
  (classes, plans, recomendedModalities) => {
    const recommendedModalityIds = recomendedModalities.map(m => m.id);

    const classActivities = classes.map(({ date, modality, modalityId }) => ({
      date,
      type: getActivityType(modalityId, modality, recommendedModalityIds),
      name: modality ? modality.name : ''
    }));

    const planActivities = plans.map(({ date }) => ({
      date,
      type: 'plan',
      name: 'Plano de treino'
    }));

    const activities = [...classActivities, ...planActivities].sort(
      (a, b) => a.date.getTime() - b.date.getTime()
    );

    return activities;
  }
);

export function getPaddedActivities(state, startDate, endDate) {
  if (endDate <= startDate) {
    return null;
  }

  const startMoment = moment(startDate);
  const endMoment = moment(endDate);

  const indexMoment = startMoment.startOf('month').clone();
  const allActivities = getActivities(state);

  const paddedActivities = [];

  while (!indexMoment.add(1, 'days').isSame(endMoment.endOf('month'), 'day')) {
    const dayActivities = allActivities.filter(a => moment(a.date).isSame(indexMoment, 'day'));

    if (dayActivities && dayActivities.length) {
      paddedActivities.push(dayActivities);
    } else {
      paddedActivities.push([{ date: new Date(indexMoment.toISOString()), type: 'none' }]);
    }
  }

  return paddedActivities;
}

function getActivityType(modalityId, modality, recommendedIds) {
  let type = '';
  if (recommendedIds.includes(modalityId)) {
    type += 'recommendedClass-';
  }
  if (modality && modality.category && modality.category === 'Les Mills') {
    type += 'lesMillsClass';
  }
  if (!type) {
    type = 'otherClass';
  }
  return type;
}

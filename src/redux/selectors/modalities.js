import { createSelector } from 'reselect';

const modalitiesState = state => state.modalities;

export const getModalities = createSelector([modalitiesState], ({ data }) => data);

export const getModalitiesList = createSelector([getModalities], modalities => Object.values(modalities));

export const getModalitiesGroupByCategory = createSelector([getModalitiesList], modalities => {
  const categoryMap = {};

  Object.values(modalities).forEach(e => {
    const key = e.category;
    if (key === 'N/A') return;
    if (categoryMap[key]) {
      categoryMap[key].push(e);
    } else {
      categoryMap[key] = [e];
    }
  });

  return categoryMap;
});

export const getModality = (state, id) => {
  const obj = getModalities(state);
  return obj[id];
};

import moment from 'moment';
import { createSelector } from 'reselect';

const rewardsState = state => state.rewards;

export const getRewards = createSelector([rewardsState], ({ data }) => data);
export const getPurchased = createSelector([rewardsState], ({ purchases }) => purchases);
export const getFilters = createSelector([rewardsState], ({ filters }) => filters);
export const getCartIds = createSelector([rewardsState], ({ cart }) => cart);

export const getReward = (state, id) => getRewards(state)[id];

const getFiltersMap = createSelector([rewardsState], ({ filters }) => {
  const filterMap = {};

  filters.forEach(f => {
    if (f.key in filterMap) {
      filterMap[f.key] = [...filterMap[f.key], f.value];
    } else {
      filterMap[f.key] = [f.value];
    }
  });

  return filterMap;
});

const getBrandsFromProperties = properties => properties.filter(p => p.type === 'brand');

const getBrandNamesFromProperties = properties =>
  properties.filter(p => p.type === 'brand').map(brand => brand.name);

const getCategoriesFromProperties = properties => properties.filter(p => p.type === 'category');

const getCategoryNamesFromProperties = properties =>
  properties.filter(p => p.type === 'category').map(category => category.name);

export const getAllFilters = createSelector([rewardsState, getFiltersMap], ({ data }, filtersMap) => {
  const brands = [];
  const categories = [];

  if (!data) {
    return { brands, categories };
  }

  Object.values(data).forEach(d => {
    const { properties } = d;

    const rewardBrands = getBrandsFromProperties(properties);
    rewardBrands.map(brand => {
      const { name } = brand;
      if (!brands.find(b => b.title === name)) {
        const isToggled = !!(filtersMap.brand && filtersMap.brand.includes(brand));
        brands.push({ title: name, value: isToggled });
      }
      return null;
    });

    const rewardCategories = getCategoriesFromProperties(properties);
    rewardCategories.map(category => {
      const { name } = category;
      if (!categories.find(c => c.title === name)) {
        const isToggled = !!(filtersMap.category && filtersMap.category.includes(category));
        categories.push({ title: name, value: isToggled });
      }
      return null;
    });
  });

  return { brands, categories };
});

export const getFilteredRewards = createSelector([getRewards, getFiltersMap], (rewards, filterMap) => {
  // the last published 8 products should be featured products
  const FEATURED_SIZE = 8;

  return Object.values(rewards)
    .sort((a, b) => b.datePublished - a.datePublished)
    .map((r, i) => {
      const isNew = moment().diff(moment(r.datePublished), 'days') < 7;
      const isFeatured = i < FEATURED_SIZE;

      return { ...r, isNew, isFeatured };
    })
    .filter(r => {
      if (filterMap.brand) {
        const names = getBrandNamesFromProperties(r.properties);
        if (!filterMap.brand.some(filter => names.includes(filter))) {
          return false;
        }
      }

      if (filterMap.category) {
        const names = getCategoryNamesFromProperties(r.properties);
        if (!filterMap.category.some(filter => names.includes(filter))) {
          return false;
        }
      }

      return true;
    });
});

export const getCartRewards = createSelector([getRewards, getCartIds], (rewards, cartIds) => {
  // the last published 8 products should be featured products
  const FEATURED_SIZE = 8;

  return Object.values(rewards)
    .sort((a, b) => b.datePublished - a.datePublished)
    .map((r, i) => {
      const isNew = moment().diff(moment(r.datePublished), 'days') < 7;
      const isFeatured = i < FEATURED_SIZE;

      return { ...r, isNew, isFeatured };
    })
    .filter(r => cartIds.includes(isNaN(r.id) ? r.id : r.id.toString()));
});

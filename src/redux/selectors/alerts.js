import { createSelector } from 'reselect';

const alertsState = state => state.alerts;

export const isAlertVisible = createSelector([alertsState], ({ visible }) => visible);
export const getAlert = createSelector([alertsState], ({ alert }) => alert);

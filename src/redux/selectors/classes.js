import _ from 'lodash';
import moment from 'moment';
import { createSelector } from 'reselect';
import { getModalities } from './modalities';
import { getRecommendedModalities } from './plan';
import { getGyms } from './gyms';
import { getLastLogin } from './auth';
import { getResults } from './search';
import { getDaysAhead } from '../../utils/dates';

const classesState = state => state.classes;

const alertData = createSelector([classesState], ({ alert }) => alert);
const favoriteData = createSelector([classesState], ({ favoriteClasses }) => favoriteClasses);

export const isFetchingUpcomingClasses = createSelector(
  [classesState],
  ({ isFetchingUpcoming }) => isFetchingUpcoming
);

/**
 * Returns an indexed list (object) of upcoming classes.
 * This list is indexed by the class ids
 */
export const getUpcomingClasses = createSelector(
  [classesState, getModalities, getGyms],
  (state, modalities, gyms) =>
    _.map(state.upcoming, c => ({
      ...c,
      modality: modalities[c.modalityId],
      gym: gyms[c.gymId]
    })).sort((a, b) => a.date - b.date)
);

/**
 * Returns an indexed list (object) of recommended classes.
 * This list is indexed by the class ids
 */
export const getRecommendedClasses = createSelector(
  [classesState, getRecommendedModalities, getModalities, getGyms],
  (state, recommendedModalities, modalities, gyms) => {
    const recommendedModalityIds = recommendedModalities.map(m => m.id);

    const mappedClasses = _.map(state.upcoming, c => {
      return { ...c, modality: modalities[c.modalityId], gym: gyms[c.gymId] };
    });

    return mappedClasses
      .filter(c => recommendedModalityIds.includes(c.modalityId))
      .sort((a, b) => a.date - b.date);
  }
);

/**
 * Returns an indexed list (object) of recommended classes,
 * that are happening today (before midnight).
 * This list is indexed by the class ids
 */
export const getTodayRecommendedClasses = createSelector(
  [classesState, getRecommendedModalities, getModalities, getGyms],
  (state, recommendedModalities, modalities, gyms) => {
    const currentDate = new Date();
    const recommendedModalityIds = recommendedModalities.map(m => m.id);

    const mappedClasses = _.map(state.upcoming, c => {
      return { ...c, modality: modalities[c.modalityId], gym: gyms[c.gymId] };
    });

    return mappedClasses
      .filter(
        c => recommendedModalityIds.includes(c.modalityId) && currentDate.getDate() === c.date.getDate()
      )
      .sort((a, b) => a.date - b.date);
  }
);

/**
 * Returns an indexed list (object) of favorite classes.
 * This list is indexed by the class ids
 */
export const getFavoriteClasses = createSelector(
  [classesState, getModalities, getGyms],
  (state, modalities, gyms) => {
    return _.map(state.favorites, c => {
      return { ...c, modality: modalities[c.modalityId], gym: gyms[c.gymId] };
    });
  }
);

/**
 * Returns an indexed list (object) of booked classes.
 * This list is indexed by the class ids
 */
export const getBookedClasses = createSelector(
  [classesState, getModalities, getGyms],
  (state, modalities, gyms) => {
    return _.map(state.booked, c => {
      return { ...c, modality: modalities[c.modalityId], gym: gyms[c.gymId] };
    });
  }
);

/**
 * Returns an indexed list (object) of review classes.
 * This list is indexed by the class ids
 */
export const getReviewClasses = createSelector(
  [classesState, getModalities, getLastLogin],
  (state, modalities, lastLogin) => {
    // TODO: For now, it only shows classes that happened a maximum of 2 days before the last login
    //       We need to make sure this is the desired behavior in the future
    return _.map(state.reviewClasses, c => ({ ...c, modality: modalities[c.modalityId] })).filter(
      c => !lastLogin || moment(c.date).isAfter(moment(lastLogin).subtract(2, 'days'))
    );
  }
);

export const isReviewingClasses = createSelector([classesState], ({ isReviewing }) => isReviewing);

export const getSearchedClasses = createSelector(
  [getResults, getModalities, getGyms],
  (results, modalities, gyms) => {
    const allClasses = [];

    _.forEach(results, r => {
      allClasses.push(...r);
    });

    return allClasses.map(c => {
      return { ...c, modality: modalities[c.modalityId], gym: gyms[c.gymId] };
    });
  }
);

export const getClassesByDays = createSelector([getUpcomingClasses, getDaysAhead], (classes, days) => {
  return days.map(day => ({
    day,
    classes: classes.filter(c => moment(c.date).format('DD/MM') === day.date)
  }));
});

export const getRecommendedClassesByDays = createSelector(
  [getRecommendedClasses, getDaysAhead],
  (classes, days) => {
    return days.map(day => ({
      day,
      classes: classes.filter(c => c.date.getDay() === day.index)
    }));
  }
);

export const getClassesDays = createSelector([getDaysAhead], days => days);

/**
 * Returs all upcoming classes, from a given modalityId
 */
export const getUpcomingClassesByModality = (state, modalityId) => {
  const list = getUpcomingClasses(state);
  return _.filter(list, c => c.modality && c.modality.id === modalityId);
};

/**
 * Returns a specific class, from a given class id
 */
export const getClass = (state, id) => {
  const find = list => {
    const matches = list.filter(l => l.id === id);
    return matches && matches.length > 0 ? matches[0] : null;
  };

  const upcoming = getUpcomingClasses(state);
  const favorite = getFavoriteClasses(state);
  const booked = getBookedClasses(state);
  const searched = getSearchedClasses(state);

  return find(upcoming) || find(favorite) || find(booked) || find(searched);
};

/**
 * Returs if the class has been favorited (for search)
 */
export const hasFavoritedClass = (state, classId) => {
  const favorites = favoriteData(state);
  return favorites.indexOf(classId) !== -1;
};

/**
 * Returs if the class has an active alert
 */
export const hasClassAlert = (state, classId) => {
  const alerts = alertData(state);
  return alerts.indexOf(classId) !== -1;
};

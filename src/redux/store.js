import { createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import { persistReducer, persistStore } from 'redux-persist';

import thunk from 'redux-thunk';
import storage from 'redux-persist/lib/storage';
import reducers from './persistor';

import datesMiddleware from './middlewares/dates';
import authMiddleware from './middlewares/auth';
import alertMiddleware from './middlewares/alert';
import analyticsMiddleware from './middlewares/analytics';
import toastMiddleware from './middlewares/toast';
import snackbarMiddleware from './middlewares/snackbar';
import planMiddleware from './middlewares/plan';
import classesMiddleware from './middlewares/classes';
import reloadMiddleware from './reload/middleware';
import startupMiddleware from './middlewares/startup';
import loadingMiddleware from './middlewares/loading';

import persistors from '../config/persistors';
import { dateTransformPersistor } from '../utils/dates';

const KEYWORDS_TO_IGNORE = ['redux-reload', 'KEYBOARD_', 'REHYDRATE'];

const persistConfig = {
  key: 'root',
  blacklist: persistors.root.blacklist,
  storage,
  transforms: [dateTransformPersistor]
};

const persistedReducer = persistReducer(persistConfig, reducers);

const logger = createLogger({
  predicate: (getState, action) => {
    const { type } = action;

    let ignore = true;

    KEYWORDS_TO_IGNORE.forEach(k => {
      if (type.includes(k)) {
        ignore = false;
      }
    });

    return ignore;
  }
});

const store = createStore(
  persistedReducer,
  applyMiddleware(
    thunk,
    logger,
    datesMiddleware,
    authMiddleware,
    alertMiddleware,
    analyticsMiddleware,
    toastMiddleware,
    snackbarMiddleware,
    classesMiddleware,
    planMiddleware,
    reloadMiddleware,
    startupMiddleware,
    loadingMiddleware
  )
);

export const persistor = persistStore(store);

export default store;

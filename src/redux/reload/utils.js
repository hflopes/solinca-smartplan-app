export function convertModuleName(moduleName) {
  return moduleName.replace(/([A-Z])/g, g => `_${g[0].toLowerCase()}`).toUpperCase();
}

export const placeholder = null;

import RELOAD_MODULES from './config';
import { convertModuleName } from './utils';
import { RESET_RELOAD } from '../modules/app/reducer';
import { LOGOUT } from '../modules/auth/reducers/login';

export const RELOAD_BASE = 'redux-reload';
export const RELOAD_SILENT_BASE = 'redux-silent-reload';
export const RELOAD_START = `${RELOAD_BASE}/START`;

export const RELOAD_SILENT_ACCEPTED = moduleName => {
  return `${RELOAD_SILENT_BASE}/${convertModuleName(moduleName)}_ACCEPTED`;
};

export const RELOAD_ACCEPTED = moduleName => {
  return `${RELOAD_BASE}/${convertModuleName(moduleName)}_ACCEPTED`;
};

export const RELOAD_DECLINED = moduleName => {
  return `${RELOAD_BASE}/${convertModuleName(moduleName)}_DECLINED`;
};

const initialState = {
  lastUpdates: {}
};

const releaseModules = state => {
  const matches = RELOAD_MODULES.filter(m => m.releaseOnLogout);

  if (!matches.length) {
    return state;
  }

  const newLastUpdates = state.lastUpdates;

  // eslint-disable-next-line
  matches.map(match => {
    newLastUpdates[match.name] = null;
  });

  return { ...state, lastUpdates: newLastUpdates };
};

const getModule = type => {
  const matches = RELOAD_MODULES.filter(m => type === m.successState);

  if (!matches) {
    return null;
  }

  return matches[0];
};

export default function reloadsReducer(state = initialState, action) {
  const reloadModule = getModule(action.type);

  if (reloadModule) {
    return { ...state, lastUpdates: { ...state.lastUpdates, [reloadModule.name]: new Date() } };
  }

  switch (action.type) {
    case LOGOUT:
      return releaseModules(state);
    case RESET_RELOAD:
      return { ...state, lastUpdates: {} };
    default:
      break;
  }

  return state;
}

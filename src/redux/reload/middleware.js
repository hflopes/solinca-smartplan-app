import { RELOAD_ACCEPTED, RELOAD_SILENT_ACCEPTED } from './reducer';
import RELOAD_MODULES from './config';

const reloadMiddleware = store => next => action => {
  const { type } = action;

  RELOAD_MODULES.forEach(reloadModule => {
    const { name, reloadAction, reloadSilentAction } = reloadModule;
    if (name && type === RELOAD_SILENT_ACCEPTED(name)) {
      if (reloadSilentAction) {
        store.dispatch(reloadSilentAction());
      } else {
        store.dispatch(reloadAction());
      }
    } else if (name && type === RELOAD_ACCEPTED(name)) {
      store.dispatch(reloadAction());
    }
  });

  return next(action);
};

export default reloadMiddleware;

import { FETCH_INITIAL_DATA_REQUEST } from '../modules/app/reducer';
import { FETCH_MODALITIES_SUCCESS } from '../modules/modalities/reducer';
import { FETCH_GYMS_SUCCESS } from '../modules/gyms/reducer';
import { FETCH_FULL_PLAN_SUCCESS, FETCH_PLAN_SUCCESS } from '../modules/plan/reducer';
import { FETCH_CAMPAIGN_SUCCESS } from '../modules/campaigns/reducer';
import { FETCH_TERMS_SUCCESS } from '../modules/terms/reducer';
import {
  FETCH_POSITION_SUCCESS,
  FETCH_USER_INFO_SUCCESS,
  FETCH_USER_TAGS_SUCCESS,
  FETCH_NOTIFICATIONS_SUCCESS,
  FETCH_RECEIPTS_SUCCESS
  // FETCH_3DBODY_SUCCESS
} from '../modules/user/reducer';
import {
  FETCH_UPCOMING_CLASSES_SUCCESS,
  FETCH_CLASSES_FAVORITE_SUCCESS,
  FETCH_CLASSES_REVIEW_SUCCESS,
  FETCH_CLASSES_BOOKED_SUCCESS
} from '../modules/classes/reducer';
// import {
//   FETCH_CHALLENGES_SUCCESS,
//   DETAILS_CHALLENGE_SUCCESS
// } from '../modules/gamification/challenges/reducer';
// import { FETCH_BADGES_SUCCESS } from '../modules/gamification/badges/reducer';
// import { FETCH_REWARDS_SUCCESS, FETCH_PURCHASES_SUCCESS } from '../modules/gamification/rewards/reducer';
import {
  FETCH_CLASSES_HISTORY_SUCCESS,
  FETCH_PLANS_HISTORY_SUCCESS,
  FETCH_EVALUATIONS_HISTORY_SUCCESS
} from '../modules/dashboard/reducer';
import { FETCH_COMPLAINTS_SUCCESS } from '../modules/complaints/reducer';
import fetchInitialData from '../modules/app/actions/fetchInitialData';
import fetchUserPosition from '../modules/user/actions/fetchPosition';
import fetchNotificationSettings from '../modules/user/actions/fetchNotificationSettings';
import fetchModalities from '../modules/modalities/actions/fetch';
import fetchFullPlan from '../modules/plan/actions/fetchFull';
import fetchPlan from '../modules/plan/actions/fetch';
import fetchGyms from '../modules/gyms/actions/fetch';
import fetchUserData from '../modules/user/actions/fetchData';
import fetchUserTags from '../modules/user/actions/fetchTags';
// import fetchLastReceipt from '../modules/user/actions/fetchLastReceipt';
import fetchReceiptsList from '../modules/user/actions/fetchReceiptsList';
import fetchUpcomingClasses, { fetchClassesSilent } from '../modules/classes/actions/fetchUpcoming';
import fetchReviewClasses from '../modules/classes/actions/fetchReview';
import fetchComplaints from '../modules/complaints/actions/fetch';
import fetchBookedClasses from '../modules/classes/actions/fetchBooked';
import fetchFavoriteClasses from '../modules/classes/actions/fetchFavorites';
import fetchCampaigns from '../modules/campaigns/actions/fetch';
import fetchTerms from '../modules/terms/actions/fetch';
// import fetchChallenges from '../modules/gamification/challenges/actions/list';
// import fetchBadges from '../modules/gamification/badges/actions/list';
// import fetchRewards from '../modules/gamification/rewards/actions/list';
// import fetchPurchases from '../modules/gamification/rewards/actions/listPurchases';
// import fetchChallenge from '../modules/gamification/challenges/actions/details';
// import fetch3dBody from '../modules/user/actions/fetch3dBody';
import fetchClassesHistory from '../modules/dashboard/actions/fetchClassesHistory';
import fetchPlansHistory from '../modules/dashboard/actions/fetchPlansHistory';
import fetchEvaluationsHistory from '../modules/dashboard/actions/fetchEvaluationsHistory';

const RELOAD_MODULES = [
  {
    name: 'initialData',
    interval: 0,
    reloadAction: fetchInitialData,
    successState: FETCH_INITIAL_DATA_REQUEST
  },
  {
    name: 'userPosition',
    interval: 1,
    reloadAction: fetchUserPosition,
    successState: FETCH_POSITION_SUCCESS
  },
  {
    name: 'notificationSettings',
    interval: 0,
    reloadAction: fetchNotificationSettings,
    successState: FETCH_NOTIFICATIONS_SUCCESS
  },
  {
    name: 'userData',
    interval: 0,
    reloadAction: fetchUserData,
    successState: FETCH_USER_INFO_SUCCESS
  },
  {
    name: 'userTags',
    interval: 0,
    reloadAction: fetchUserTags,
    successState: FETCH_USER_TAGS_SUCCESS
  },
  /*   {
    name: 'lastReceipt',
    interval: 0,
    reloadAction: fetchLastReceipt,
    successState: FETCH_RECEIPTS_SUCCESS
  }, */
  {
    name: 'receiptsList',
    interval: 0,
    reloadAction: fetchReceiptsList,
    successState: FETCH_RECEIPTS_SUCCESS
  },
  // {
  //   name: '3dbody',
  //   interval: 10,
  //   reloadAction: fetch3dBody,
  //   successState: FETCH_3DBODY_SUCCESS
  // },
  // {
  //   name: 'challenges',
  //   interval: 60,
  //   reloadAction: fetchChallenges,
  //   successState: FETCH_CHALLENGES_SUCCESS,
  //   releaseOnLogout: true
  // },
  // {
  //   name: 'badges',
  //   interval: 60,
  //   reloadAction: fetchBadges,
  //   successState: FETCH_BADGES_SUCCESS,
  //   releaseOnLogout: true
  // },
  // {
  //   name: 'rewards',
  //   interval: 60,
  //   reloadAction: fetchRewards,
  //   successState: FETCH_REWARDS_SUCCESS,
  //   releaseOnLogout: true
  // },
  // {
  //   name: 'purchases',
  //   interval: 60,
  //   reloadAction: fetchPurchases,
  //   successState: FETCH_PURCHASES_SUCCESS,
  //   releaseOnLogout: true
  // },
  // {
  //   name: 'challenge',
  //   interval: 60,
  //   reloadAction: fetchChallenge,
  //   successState: DETAILS_CHALLENGE_SUCCESS,
  //   releaseOnLogout: true
  // },
  {
    name: 'modalities',
    interval: 0,
    reloadAction: fetchModalities,
    successState: FETCH_MODALITIES_SUCCESS
  },
  {
    name: 'gyms',
    interval: 300,
    reloadAction: fetchGyms,
    successState: FETCH_GYMS_SUCCESS
  },
  {
    name: 'upcomingClasses',
    interval: 5,
    reloadAction: fetchUpcomingClasses,
    reloadSilentAction: fetchClassesSilent,
    successState: FETCH_UPCOMING_CLASSES_SUCCESS,
    releaseOnLogout: true
  },
  {
    name: 'favoriteClasses',
    interval: 15,
    reloadAction: fetchFavoriteClasses,
    successState: FETCH_CLASSES_FAVORITE_SUCCESS,
    releaseOnLogout: true
  },
  {
    name: 'bookedClasses',
    interval: 0,
    reloadAction: fetchBookedClasses,
    successState: FETCH_CLASSES_BOOKED_SUCCESS,
    releaseOnLogout: true
  },
  {
    name: 'classesReview',
    interval: 5,
    reloadAction: fetchReviewClasses,
    successState: FETCH_CLASSES_REVIEW_SUCCESS,
    releaseOnLogout: true
  },
  {
    name: 'complaints',
    interval: 0,
    reloadAction: fetchComplaints,
    successState: FETCH_COMPLAINTS_SUCCESS,
    releaseOnLogout: true
  },
  {
    name: 'campaigns',
    interval: 60,
    reloadAction: fetchCampaigns,
    successState: FETCH_CAMPAIGN_SUCCESS
  },
  {
    name: 'fullPlan',
    interval: 5,
    reloadAction: fetchFullPlan,
    successState: FETCH_FULL_PLAN_SUCCESS,
    releaseOnLogout: true
  },
  {
    name: 'plan',
    interval: 5,
    reloadAction: fetchPlan,
    successState: FETCH_PLAN_SUCCESS,
    releaseOnLogout: true
  },
  {
    name: 'terms',
    interval: 0,
    reloadAction: fetchTerms,
    successState: FETCH_TERMS_SUCCESS
  },
  {
    name: 'classesHistory',
    interval: 5,
    reloadAction: fetchClassesHistory,
    successState: FETCH_CLASSES_HISTORY_SUCCESS,
    releaseOnLogout: true
  },
  {
    name: 'plansHistory',
    interval: 5,
    reloadAction: fetchPlansHistory,
    successState: FETCH_PLANS_HISTORY_SUCCESS,
    releaseOnLogout: true
  },
  {
    name: 'evaluationsHistory',
    interval: 5,
    reloadAction: fetchEvaluationsHistory,
    successState: FETCH_EVALUATIONS_HISTORY_SUCCESS,
    releaseOnLogout: true
  }
];

export default RELOAD_MODULES;

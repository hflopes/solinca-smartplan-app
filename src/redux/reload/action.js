import RELOAD_MODULES from './config';
import { RELOAD_ACCEPTED, RELOAD_DECLINED, RELOAD_SILENT_ACCEPTED } from './reducer';

function reloadAccepted(moduleName) {
  return {
    type: RELOAD_ACCEPTED(moduleName)
  };
}

function reloadSilentAccepted(moduleName) {
  return {
    type: RELOAD_SILENT_ACCEPTED(moduleName)
  };
}

function reloadDeclined(moduleName) {
  return {
    type: RELOAD_DECLINED(moduleName)
  };
}

export default function reload(moduleName, force = false) {
  return (dispatch, getState) => {
    if (force) {
      dispatch(reloadSilentAccepted(moduleName));
    } else if (shouldReload(getState(), moduleName)) {
      dispatch(reloadAccepted(moduleName));
    } else {
      dispatch(reloadDeclined(moduleName));
    }
  };
}

const findModule = name => {
  const matches = RELOAD_MODULES.filter(m => name === m.name);

  if (!matches || matches.length === 0) {
    return null;
  }

  return matches[0];
};

const shouldReload = (state, moduleName) => {
  const lastUpdate = state.reload.lastUpdates[moduleName];
  if (lastUpdate) {
    const now = new Date();

    const timeDiff = now.getTime() - lastUpdate.getTime();
    const minutesDiff = timeDiff / 1000 / 60;

    return minutesDiff >= findModule(moduleName).interval;
  }

  return true;
};

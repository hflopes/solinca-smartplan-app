// general
import app from '../modules/app/reducer';
import device from '../modules/device/reducer';
import reload from '../reload/reducer';

// modals
import alerts from '../modules/alerts/reducer';
import toasts from '../modules/toasts/reducer';
import snackbar from '../modules/snackbar/reducer';

// auth
import register from '../modules/auth/reducers/register';
import token from '../modules/auth/reducers/token';
import login from '../modules/auth/reducers/login';
import resetPassword from '../modules/auth/reducers/resetPassword';
import terms from '../modules/terms/reducer';

import challenges from '../modules/gamification/challenges/reducer';
import badges from '../modules/gamification/badges/reducer';
import rewards from '../modules/gamification/rewards/reducer';
import classes from '../modules/classes/reducer';
import gyms from '../modules/gyms/reducer';
import modalities from '../modules/modalities/reducer';
import user from '../modules/user/reducer';
import plan from '../modules/plan/reducer';
import search from '../modules/search/reducer';
import support from '../modules/support/reducer';
import complaints from '../modules/complaints/reducer';
import campaigns from '../modules/campaigns/reducer';
import dashboard from '../modules/dashboard/reducer';

export default {
  app,
  device,
  alerts,
  toasts,
  snackbar,
  register,
  token,
  login,
  resetPassword,
  classes,
  gyms,
  modalities,
  user,
  plan,
  support,
  complaints,
  search,
  campaigns,
  reload,
  terms,
  challenges,
  badges,
  dashboard,
  rewards
};

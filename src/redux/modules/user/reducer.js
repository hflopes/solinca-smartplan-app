import { LOGOUT } from '../auth/reducers/login';

export const FETCH_USER_INFO_REQUEST = 'smartplan/user/INFO_REQUEST';
export const FETCH_USER_INFO_SUCCESS = 'smartplan/user/INFO_SUCCESS';
export const FETCH_USER_INFO_FAILURE = 'smartplan/user/INFO_FAILURE';
export const FETCH_TEMPORARY_USER_INFO_SUCCESS = 'smartplan/user/TEMPORARY_INFO_SUCCESS';

export const FETCH_USER_TAGS_REQUEST = 'smartplan/user/TAGS_REQUEST';
export const FETCH_USER_TAGS_SUCCESS = 'smartplan/user/TAGS_SUCCESS';
export const FETCH_USER_TAGS_FAILURE = 'smartplan/user/TAGS_FAILURE';

export const FETCH_RECEIPTS_REQUEST = 'smartplan/user/RECEIPTS_REQUEST';
export const FETCH_RECEIPTS_SUCCESS = 'smartplan/user/RECEIPTS_SUCCESS';
export const FETCH_RECEIPTS_FAILURE = 'smartplan/user/RECEIPTS_FAILURE';

export const FETCH_3DBODY_REQUEST = 'smartplan/user/3DBODY_REQUEST';
export const FETCH_3DBODY_SUCCESS = 'smartplan/user/3DBODY_SUCCESS';
export const FETCH_3DBODY_FAILURE = 'smartplan/user/3DBODY_FAILURE';

export const FETCH_NOTIFICATIONS_REQUEST = 'smartplan/user/NOTIFICATIONS_REQUEST';
export const FETCH_NOTIFICATIONS_SUCCESS = 'smartplan/user/NOTIFICATIONS_SUCCESS';
export const FETCH_NOTIFICATIONS_FAILURE = 'smartplan/user/NOTIFICATIONS_FAILURE';

export const FETCH_POSITION_REQUEST = 'smartplan/user/POSITION_REQUEST';
export const FETCH_POSITION_SUCCESS = 'smartplan/user/POSITION_SUCCESS';
export const FETCH_POSITION_FAILURE = 'smartplan/user/POSITION_FAILURE';

export const UPDATE_INFO_REQUEST = 'smartplan/user/UPDATE_REQUEST';
export const UPDATE_INFO_SUCCESS = 'smartplan/user/UPDATE_SUCCESS';
export const UPDATE_INFO_FAILURE = 'smartplan/user/UPDATE_FAILURE';

export const UPDATE_PASSWORD_REQUEST = 'smartplan/user/UPDATE_PASSWORD_REQUEST';
export const UPDATE_PASSWORD_SUCCESS = 'smartplan/user/UPDATE_PASSWORD_SUCCESS';
export const UPDATE_PASSWORD_FAILURE = 'smartplan/user/UPDATE_PASSWORD_FAILURE';

export const UPDATE_PHOTO_REQUEST = 'smartplan/user/UPDATE_PHOTO_REQUEST';
export const UPDATE_PHOTO_SUCCESS = 'smartplan/user/UPDATE_PHOTO_SUCCESS';
export const UPDATE_PHOTO_FAILURE = 'smartplan/user/UPDATE_PHOTO_FAILURE';

export const CHANGE_NOTIFICATION_ALERT_REQUEST = 'smartplan/user/NOTIFICATION_ALERT_REQUEST';
export const CHANGE_NOTIFICATION_ALERT_SUCCESS = 'smartplan/user/NOTIFICATION_ALERT_SUCCESS';
export const CHANGE_NOTIFICATION_ALERT_FAILURE = 'smartplan/user/NOTIFICATION_ALERT_FAILURE';

export const STATUS_UPGRADED = 'smartplan/user/STATUS_UPGRADED';
export const STATUS_CHECKED = 'smartplan/user/STATUS_CHECKED';

const initialState = {
  isFetching: false,
  isFetchingPos: false,
  isFetchingReceipts: false,
  isFetching3dBody: false,
  isUpdatingInfo: false,
  isUpdatingPhoto: false,
  isUpdatingPassword: false,
  updatedPassword: false,
  updatedEmail: false,
  isUpdatingNotification: false,
  upgradedStatus: false,
  error: null,
  data: {
    token: null,
    email: '',
    name: '',
    photo: '',
    phone: '',
    address: '',
    memberNumber: '',
    birthDate: '',
    nif: '',
    position: { latitude: 0, longitude: 0 },
    premium: false,
    defaultGym: null
  },

  notifications: [],

  url3dBody: null,

  // lastReceipt: '',

  receiptsList: []
};

export default function userReducer(state = initialState, action) {
  switch (action.type) {
    case LOGOUT:
      return { ...initialState, data: { ...initialState.data, position: state.data.position } };

    case FETCH_USER_INFO_REQUEST:
      return { ...state, isFetching: true };
    case FETCH_USER_INFO_SUCCESS:
      return {
        ...state,
        isFetching: false,
        error: null,
        data: { ...state.data, ...action.user }
      };
    case FETCH_USER_INFO_FAILURE:
      return { ...state, isFetching: false, error: action.error };
    case FETCH_TEMPORARY_USER_INFO_SUCCESS:
      return {
        ...state,
        isFetching: false,
        error: null,
        data: { ...state.data, ...action.user }
      };

    case FETCH_RECEIPTS_REQUEST:
      return { ...state, isFetchingReceipts: true };
    case FETCH_RECEIPTS_SUCCESS:
      return {
        ...state,
        isFetchingReceipts: false,
        receiptsList: action.receiptsList
      };
    case FETCH_RECEIPTS_FAILURE:
      return { ...state, isFetchingReceipts: false, error: action.error };

    case FETCH_POSITION_REQUEST:
      return { ...state, isFetchingPos: true };
    case FETCH_POSITION_SUCCESS:
      return { ...state, isFetchingPos: false, data: { ...state.data, position: action.position } };
    case FETCH_POSITION_FAILURE:
      return { ...state, isFetchingPos: false, error: action.error };

    case FETCH_3DBODY_REQUEST:
      return { ...state, isFetching3dBody: true };
    case FETCH_3DBODY_SUCCESS:
      return { ...state, isFetching3dBody: false, url3dBody: action.url };
    case FETCH_3DBODY_FAILURE:
      return { ...state, isFetching3dBody: false, error: action.error };

    case FETCH_NOTIFICATIONS_REQUEST:
      return { ...state };
    case FETCH_NOTIFICATIONS_SUCCESS:
      return {
        ...state,
        error: null,
        notifications: action.notifications
      };
    case FETCH_NOTIFICATIONS_FAILURE:
      return { ...state, error: action.error };

    case UPDATE_INFO_REQUEST:
      return { ...state, isUpdatingInfo: true };
    case UPDATE_INFO_SUCCESS:
      return {
        ...state,
        isUpdatingInfo: false,
        updatedEmail: action.field === 'email',
        data: {
          ...state.data,
          [action.field]: action.value
        }
      };
    case UPDATE_INFO_FAILURE:
      return { ...state, isUpdatingInfo: false, error: action.error };

    case UPDATE_PHOTO_REQUEST:
      return { ...state, isUpdatingPhoto: true };
    case UPDATE_PHOTO_SUCCESS:
      return {
        ...state,
        isUpdatingPhoto: false,
        data: {
          ...state.data,
          photo: action.photo
        }
      };
    case UPDATE_PHOTO_FAILURE:
      return { ...state, isUpdatingPhoto: false, error: action.error };

    case UPDATE_PASSWORD_REQUEST:
      return { ...state, isUpdatingPassword: true };
    case UPDATE_PASSWORD_SUCCESS:
      return {
        ...state,
        isUpdatingPassword: false,
        updatedPassword: true
      };
    case UPDATE_PASSWORD_FAILURE:
      return { ...state, isUpdatingPassword: false, error: action.error, updatedPassword: false };

    case CHANGE_NOTIFICATION_ALERT_REQUEST:
      return { ...state, isUpdatingNotification: true };
    case CHANGE_NOTIFICATION_ALERT_SUCCESS:
      return {
        ...state,
        isUpdatingNotification: false,
        notifications: state.notifications.map(notification =>
          notification.notificationType === action.notificationType
            ? { ...notification, value: action.value }
            : notification
        )
      };
    case CHANGE_NOTIFICATION_ALERT_FAILURE:
      return { ...state, isUpdatingNotification: false, error: action.error };

    case STATUS_UPGRADED:
      return { ...state, upgradedStatus: true };

    case STATUS_CHECKED:
      return { ...state, upgradedStatus: false };

    default:
      return state;
  }
}

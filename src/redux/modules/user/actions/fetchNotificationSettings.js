import axios from 'axios';
import endpoints from '../../../../config/api';
import {
  FETCH_NOTIFICATIONS_REQUEST,
  FETCH_NOTIFICATIONS_SUCCESS,
  FETCH_NOTIFICATIONS_FAILURE
} from '../reducer';

function fetchNotificationSettingsRequest() {
  return {
    type: FETCH_NOTIFICATIONS_REQUEST
  };
}

function fetchNotificationSettingsSuccess(notifications) {
  const notificationsList = notifications.map(notification => {
    const { description, isChecked, title, userNeverUpdatedValue, usertag } = notification;
    const value = userNeverUpdatedValue ? true : isChecked;
    return {
      notificationType: usertag,
      value,
      title,
      description
    };
  });

  return {
    type: FETCH_NOTIFICATIONS_SUCCESS,
    notifications: notificationsList
  };
}

function fetchNotificationSettingsFailure(error) {
  return {
    type: FETCH_NOTIFICATIONS_FAILURE,
    error
  };
}

export default function fetchNotificationSettings() {
  return async dispatch => {
    dispatch(fetchNotificationSettingsRequest());

    axios
      .get(endpoints().notifications)
      .then(({ data }) => {
        return dispatch(fetchNotificationSettingsSuccess(data));
      })
      .catch(err => dispatch(fetchNotificationSettingsFailure(err)));
  };
}

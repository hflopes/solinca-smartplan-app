import axios from 'axios';
import endpoints from '../../../../config/api';
import mapError from '../../../utils/errorMapping';
import { FETCH_RECEIPTS_REQUEST, FETCH_RECEIPTS_SUCCESS, FETCH_RECEIPTS_FAILURE } from '../reducer';

function fetchReceiptsListRequest() {
  return {
    type: FETCH_RECEIPTS_REQUEST
  };
}

function fetchReceiptsListSuccess(receiptsList) {
  return {
    type: FETCH_RECEIPTS_SUCCESS,
    receiptsList
  };
}

function fetchReceiptsListFailure(error) {
  return {
    type: FETCH_RECEIPTS_FAILURE,
    error
  };
}

export default function fetchReceiptsList() {
  return dispatch => {
    dispatch(fetchReceiptsListRequest());

    axios
      .get(endpoints().fetchReceiptsList)
      .then(({ data }) => {
        const { documents, code } = data;
        if (documents) {
          dispatch(fetchReceiptsListSuccess(data.documents));
        } else {
          dispatch(fetchReceiptsListFailure(mapError(code)));
        }
      })
      .catch(err => dispatch(fetchReceiptsListFailure(err)));
  };
}

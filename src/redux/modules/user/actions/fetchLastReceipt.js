import axios from 'axios';
import _ from 'lodash';
import endpoints from '../../../../config/api';
import { FETCH_RECEIPTS_REQUEST, FETCH_RECEIPTS_SUCCESS, FETCH_RECEIPTS_FAILURE } from '../reducer';

function fetchLastReceiptRequest() {
  return {
    type: FETCH_RECEIPTS_REQUEST
  };
}

function fetchLastReceiptSuccess(lastReceipt) {
  return {
    type: FETCH_RECEIPTS_SUCCESS,
    lastReceipt
  };
}

function fetchLastReceiptFailure(error) {
  return {
    type: FETCH_RECEIPTS_FAILURE,
    error
  };
}

export default function fetchLastReceipt() {
  return dispatch => {
    dispatch(fetchLastReceiptRequest());

    axios
      .get(endpoints().fetchLastReceipt)
      .then(({ data }) => {
        const { ok, code } = data;
        console.log('Data:', data);
        if (ok) {
          dispatch(fetchLastReceiptSuccess(_.omit(data, 'ok')));
        } else {
          //dispatch(fetchLastReceiptFailure(mapError(code)));
          console.log('Erro:', data);
        }
      })
      .catch(err => dispatch(fetchLastReceiptFailure(err)));
  };
}

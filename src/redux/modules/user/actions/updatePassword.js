import axios from 'axios';
import { UPDATE_PASSWORD_REQUEST, UPDATE_PASSWORD_SUCCESS, UPDATE_PASSWORD_FAILURE } from '../reducer';
import endpoints from '../../../../config/api';
import mapError from '../../../utils/errorMapping';

function updatePasswordRequest() {
  return {
    type: UPDATE_PASSWORD_REQUEST
  };
}

export function updatePasswordSuccess() {
  return {
    type: UPDATE_PASSWORD_SUCCESS
  };
}

function updatePasswordFailure(err) {
  return {
    type: UPDATE_PASSWORD_FAILURE,
    err
  };
}

export default function updatePassword({ currentPassword, newPassword, newPasswordConfirmation }) {
  return async dispatch => {
    dispatch(updatePasswordRequest());

    const requestBody = { currentPassword, newPassword, newPasswordConfirmation };

    return axios
      .put(endpoints().updateUserPassword, requestBody)
      .then(({ data }) => {
        const { ok, code } = data;

        if (ok) {
          dispatch(updatePasswordSuccess());
        } else {
          dispatch(updatePasswordFailure(mapError(code)));
        }
      })
      .catch(err => dispatch(updatePasswordFailure(err)));
  };
}

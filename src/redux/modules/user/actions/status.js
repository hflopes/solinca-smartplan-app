import { STATUS_UPGRADED, STATUS_CHECKED } from '../reducer';

export function statusUpgraded() {
  return {
    type: STATUS_UPGRADED
  };
}

export function statusChecked() {
  return {
    type: STATUS_CHECKED
  };
}

import axios from 'axios';
import endpoints from '../../../../config/api';
import mapError from '../../../utils/errorMapping';
import { FETCH_3DBODY_REQUEST, FETCH_3DBODY_SUCCESS, FETCH_3DBODY_FAILURE } from '../reducer';

function fetch3dBodyRequest() {
  return { type: FETCH_3DBODY_REQUEST };
}

function fetch3dBodySuccess(url) {
  return { type: FETCH_3DBODY_SUCCESS, url };
}

function fetch3dBodyFailure(error) {
  return { type: FETCH_3DBODY_FAILURE, error };
}

export default function fetch3dBody() {
  return dispatch => {
    dispatch(fetch3dBodyRequest());

    axios
      .get(endpoints().fetch3dBody)
      .then(({ data }) => {
        const { ok, code, url } = data;
        if (ok) {
          dispatch(fetch3dBodySuccess(url));
        } else {
          dispatch(fetch3dBodyFailure(mapError(code)));
        }
      })
      .catch(err => dispatch(fetch3dBodyFailure(err)));
  };
}

import { Plugins } from '@capacitor/core';
import { FETCH_POSITION_REQUEST, FETCH_POSITION_FAILURE, FETCH_POSITION_SUCCESS } from '../reducer';

const { Geolocation } = Plugins;

async function getCurrentPosition(callback) {
  Geolocation.watchPosition({ timeout: 5000 }, (position, err) => {
    if (position && position.coords) {
      callback(position.coords);
    }
  });

  const position = await Geolocation.getCurrentPosition();
  if (position && position.coords) {
    callback(position.coords);
  }
}

function fetchPositionRequest() {
  return {
    type: FETCH_POSITION_REQUEST
  };
}

function fetchPositionSuccess(position) {
  return {
    type: FETCH_POSITION_SUCCESS,
    position
  };
}

function fetchPositionFailure(error) {
  return {
    type: FETCH_POSITION_FAILURE,
    error
  };
}

export default function fetchPosition() {
  return async dispatch => {
    dispatch(fetchPositionRequest());

    try {
      getCurrentPosition(coords => {
        dispatch(fetchPositionSuccess(coords));
      });
    } catch (error) {
      dispatch(fetchPositionFailure(error));
    }
  };
}

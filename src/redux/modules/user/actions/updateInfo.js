import axios from 'axios';
import { UPDATE_INFO_REQUEST, UPDATE_INFO_SUCCESS, UPDATE_INFO_FAILURE } from '../reducer';
import endpoints from '../../../../config/api';
import mapError from '../../../utils/errorMapping';

function updateInfoRequest() {
  return {
    type: UPDATE_INFO_REQUEST
  };
}

export function updateInfoSuccess({ field, value }) {
  return {
    type: UPDATE_INFO_SUCCESS,
    field,
    value
  };
}

function updateInfoFailure({ field, error }) {
  return {
    type: UPDATE_INFO_FAILURE,
    field,
    error
  };
}

export default function updateInfo({ field, value }) {
  return dispatch => {
    dispatch(updateInfoRequest());

    const requestBody = {
      address: '',
      phone: '',
      email: '',
      [field]: value // this should override any of the above empty fields
    };

    return axios
      .post(endpoints().updateUserDetails, requestBody)
      .then(({ data }) => {
        const { ok, code } = data;

        if (ok) {
          dispatch(updateInfoSuccess({ field, value }));
        } else {
          dispatch(updateInfoFailure({ field, error: mapError(code) }));
        }
      })
      .catch(err => dispatch(updateInfoFailure({ field, error: err })));
  };
}

import axios from 'axios';
import _ from 'lodash';
import endpoints from '../../../../config/api';
import mapError from '../../../utils/errorMapping';
import {
  FETCH_USER_INFO_REQUEST,
  FETCH_USER_INFO_SUCCESS,
  FETCH_USER_INFO_FAILURE,
  FETCH_TEMPORARY_USER_INFO_SUCCESS
} from '../reducer';

function fetchUserRequest() {
  return {
    type: FETCH_USER_INFO_REQUEST
  };
}

function fetchUserSuccess(user) {
  return {
    type: FETCH_USER_INFO_SUCCESS,
    user
  };
}

function fetchUserFailure(error) {
  return {
    type: FETCH_USER_INFO_FAILURE,
    error
  };
}

export function fetchTemporaryUserSuccess(user) {
  return {
    type: FETCH_TEMPORARY_USER_INFO_SUCCESS,
    user
  };
}

export default function fetchUser() {
  return dispatch => {
    dispatch(fetchUserRequest());

    axios
      .get(endpoints().fetchUserData)
      .then(({ data }) => {
        const { ok, code } = data;

        if (ok) {
          dispatch(fetchUserSuccess(_.omit(data, 'ok')));
        } else {
          dispatch(fetchUserFailure(mapError(code)));
        }
      })
      .catch(err => dispatch(fetchUserFailure(err)));
  };
}

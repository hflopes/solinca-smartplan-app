import axios from 'axios';
import endpoints from '../../../../config/api';
import {
  CHANGE_NOTIFICATION_ALERT_REQUEST,
  CHANGE_NOTIFICATION_ALERT_SUCCESS,
  CHANGE_NOTIFICATION_ALERT_FAILURE
} from '../reducer';
import { sendTag } from '../../../../utils/push/onesignal';

function changeNotificationAlertRequest() {
  return {
    type: CHANGE_NOTIFICATION_ALERT_REQUEST
  };
}

export function changeNotificationAlertSuccess({ notificationType, value }) {
  sendTag(notificationType, value);
  return {
    type: CHANGE_NOTIFICATION_ALERT_SUCCESS,
    notificationType,
    value
  };
}

function changeNotificationAlertFailure(error) {
  return {
    type: CHANGE_NOTIFICATION_ALERT_FAILURE,
    error
  };
}

export default function changeNotificationAlert({ notificationType, value }) {
  return async dispatch => {
    dispatch(changeNotificationAlertRequest());

    const requestBody = {};
    requestBody[notificationType] = value;

    axios
      .put(endpoints().notifications, requestBody)
      .then(() => {
        dispatch(changeNotificationAlertSuccess({ notificationType, value }));
      })
      .catch(err => dispatch(changeNotificationAlertFailure(err)));
  };
}

import axios from 'axios';
import { UPDATE_PHOTO_REQUEST, UPDATE_PHOTO_SUCCESS, UPDATE_PHOTO_FAILURE } from '../reducer';
import endpoints from '../../../../config/api';
import mapError from '../../../utils/errorMapping';

function updatePhotoRequest() {
  return {
    type: UPDATE_PHOTO_REQUEST
  };
}

export function updatePhotoSuccess({ photo }) {
  return {
    type: UPDATE_PHOTO_SUCCESS,
    photo
  };
}

function updatePhotoFailure(error) {
  return {
    type: UPDATE_PHOTO_FAILURE,
    error
  };
}

export default function updatePhoto({ photo }) {
  return async dispatch => {
    dispatch(updatePhotoRequest());

    const requestBody = {
      extension: 'jpg',
      base64Source: photo
    };

    return axios
      .post(endpoints().updateUserPhoto, requestBody)
      .then(({ data }) => {
        const { ok, code } = data;

        if (ok) {
          dispatch(updatePhotoSuccess());
        } else {
          dispatch(updatePhotoFailure(mapError(code)));
        }
      })
      .catch(err => dispatch(updatePhotoFailure(err)));
  };
}

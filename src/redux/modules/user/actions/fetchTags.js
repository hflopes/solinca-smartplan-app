import axios from 'axios';
import { sendTags } from '../../../../utils/push/onesignal';
import endpoints from '../../../../config/api';
import mapError from '../../../utils/errorMapping';
import { FETCH_USER_TAGS_REQUEST, FETCH_USER_TAGS_SUCCESS, FETCH_USER_TAGS_FAILURE } from '../reducer';

function fetchTagsRequest() {
  return {
    type: FETCH_USER_TAGS_REQUEST
  };
}

function fetchTagsSuccess(tags) {
  return {
    type: FETCH_USER_TAGS_SUCCESS,
    tags
  };
}

function fetchTagsFailure(error) {
  return {
    type: FETCH_USER_TAGS_FAILURE,
    error
  };
}

export default function fetchTags() {
  return dispatch => {
    dispatch(fetchTagsRequest());

    axios
      .get(endpoints().fetchUserTags)
      .then(({ data }) => {
        const { ok, code, tags } = data;

        if (ok) {
          // Send these tags to OneSignal via their SDK
          sendTags(tags);

          dispatch(fetchTagsSuccess(tags));
        } else {
          dispatch(fetchTagsFailure(mapError(code)));
        }
      })
      .catch(err => dispatch(fetchTagsFailure(err)));
  };
}

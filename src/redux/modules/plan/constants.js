export const USER_TYPES = {
  NEW_USER_1: {
    key: 'NEW_USER_1',
    title: 'Sem actividade física regular'
  },
  NEW_USER_2: {
    key: 'NEW_USER_2',
    title: '1 a 3 vezes por semana',
    subtitle: 'Ou totalizando 75 a 150 minutos semanais de treino'
  },
  EXPERIENCED_USER: {
    key: 'EXPERIENCED_USER',
    title: '4 ou mais vezes por semana',
    subtitle: 'Ou totalizando 150 minutos semanais de treino'
  }
};

export const USER_FREQUENCIES = {
  NONE: {
    key: 'NONE',
    title: 'Nenhum ginásio'
  },
  ONE: {
    key: 'ONE',
    title: 'Um ginásio'
  },
  FEW: {
    key: 'FEW',
    title: 'Dois ou mais ginásios'
  }
};

/**
 * Solinca is aware of the current chooseYourGoal keys,
 * if in the future new plans are created,
 * the app needs to be updated
 */
export const USER_OBJECTIVES = {
  WEIGHT: {
    key: 'WEIGHT',
    chooseYourGoalKey: 'SW',
    title: 'Perder peso',
    plan: 'Smart Weight'
  },
  FIT: {
    key: 'FIT',
    chooseYourGoalKey: 'SF',
    title: 'Tonificar e definir',
    plan: 'Smart Fit'
  },
  MUSCLE: {
    key: 'MUSCLE',
    chooseYourGoalKey: 'SM',
    title: 'Aumento de massa muscular / Hipertrofia',
    plan: 'N/A'
  },
  AGE: {
    key: 'AGE',
    chooseYourGoalKey: 'SA',
    title: 'Aumentar energia e resistência',
    plan: 'Smart Age'
  },
  BALANCE: {
    key: 'BALANCE',
    chooseYourGoalKey: 'SB',
    title: 'Aumentar o bem-estar / Gerir o stress',
    plan: 'Smart Balance'
  }
};

export const EXERCISE_TYPES = {
  MACHINES: {
    title: 'Máquinas',
    description: 'Exercícios em que utilizam equipamentos de ginásio como por exemplo passadeiras',
    key: 'MACHINES'
  },
  BODY_WEIGHT: {
    title: 'Peso corporal',
    description:
      'Exercícios em que apenas é utilizado o peso do seu corpo para trabalhar todos os grupos musculares',
    key: 'BODY_WEIGHT'
  }
};

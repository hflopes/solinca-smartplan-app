export const EXERCISES = {
  MUSCLE: {
    BODY_WEIGHT: [
      {
        equipment: 'PASSADEIRA',
        goal: 'Performance',
        id: '824e4bd1-5481-4966-b59b-a21b4a730ddd',
        level: 2,
        pse: 5,
        speed: 6,
        duration: 300,
        cedisId: 597,
        name: 'CAMINHAR NA PASSADEIRA',
        instructions:
          'Caminhar de forma controlada. Por questões de segurança sugerimos que mantenha sempre o corpo à distância de um braço, o olhar em frente e se necessário, pode-se apoiar.'
      },
      {
        equipment: 'REMO',
        goal: 'Performance',
        id: 'c2513186-b55d-4a6e-a49b-c4fcb3b70681',
        level: 8,
        pse: 5,
        duration: 300,
        cedisId: 599,
        name: 'REMO',
        instructions:
          'Mantenha os pés apoiados e fixos no respectivo suporte e realize o movimento identico ao remar (extensão de pernas -flexão de braços -flexão de pernas- extensão de braços).'
      },
      {
        equipment: 'CROSSOVER',
        goal: 'Performance',
        id: '373c5c52-5bdf-4768-970f-cc803583911b',
        level: 2,
        pse: 5,
        speed: 80,
        duration: 300,
        cedisId: 595,
        name: 'CROSSOVER',
        instructions:
          'Por questões de segurança, apoie os pés junto ao bordo anterior e interior do respectivo apoio para os pés e as mãos, sobre os respectivos apoios fixos ou móveis. Realize o movimento similar ao caminhar/marchar/ correr sem impacto.'
      },
      {
        equipment: 'BICICLETA VERTICAL',
        goal: 'Performance',
        id: 'daa9feb5-ecaf-4e1a-94d7-76b7c062f74b',
        level: 2,
        pse: 5,
        speed: 50,
        duration: 300,
        cedisId: 594,
        name: 'PEDALAR NA BICICLETA VERTICAL',
        instructions:
          'Juste o banco, mantendo uma ligeira flexão do joelho no ponto máximo, realize o movimento similar ao pedalar.'
      },
      {
        equipment: 'CAIXA/ STEP',
        goal: 'Performance',
        id: 'f3404c32-6e6d-4e96-8245-46b7a3259f12',
        pse: 6,
        repetitions: 10,
        series: 2,
        duration: 120,
        cedisId: 716,
        name: 'SQUAT COM APOIO',
        instructions:
          'Realize o movimento de sentar e levantar de uma caixa/step. Empurre a anca para trás e para baixo, realizando a flexão de anca e joelhos mantendo sempre os calcanhares apoiados no chão e o olhar em frente.'
      },
      {
        equipment: 'NA',
        goal: 'Performance',
        id: '7e8f8a59-27ae-433f-ab61-c52c35a4dbaf',
        pse: 6,
        repetitions: 10,
        series: 2,
        duration: 120,
        cedisId: 693,
        name: 'LUNGE',
        instructions:
          'Inicie em pé, com os pés à largura da anca. Dê um passo grande atrás, com uma perna e realize a flexão e extensão de joelhos, como se o corpo se movesse para baixo e para cima (não para frente e trás). Repita o exercício com a outra perna.'
      },
      {
        equipment: 'COLCHÃO',
        goal: 'Performance',
        id: '7e9179ec-3a0c-4562-9b8c-71b1aa1fd14f',
        pse: 6,
        repetitions: 10,
        series: 2,
        duration: 120,
        cedisId: 664,
        name: 'FLEXÃO (PUSH UP)',
        instructions:
          'Deitado sobre um tapete, apoie as mãos mesmo por baixo dos ombros. Realize o movimento de extensão e flexão de cotovelos, assegurando que as suas costas permanecem sempre longas /direitas e o olhar sobre o chão. Pode optar por apoiar os joelhos ou a ponto dos dedos do pés no chão.'
      },
      {
        equipment: 'TRX',
        goal: 'Performance',
        id: '8264c0f0-0bd9-4b86-8f0c-20938ae05f21',
        pse: 6,
        repetitions: 10,
        series: 2,
        duration: 120,
        cedisId: 730,
        name: 'REMADA FECHADA NO TRX',
        instructions:
          'Coloque o trx no ajuste médio. Mantenha o olhar no ponto de ancoragem e as fitas sempre em extensão. Realize a flexão e extensão de cotovelos, mantendo os cotovelos a passar sempre junto ao tronco (axilas fexhadas)'
      },
      {
        equipment: 'CAIXA/ STEP',
        goal: 'Performance',
        id: '86bf582d-025d-4d15-a6ec-cef613990903',
        pse: 6,
        repetitions: 10,
        series: 2,
        duration: 120,
        cedisId: 678,
        name: 'FUNDOS',
        instructions:
          'Sente-se sobre um step/caixa e mantenha as mãos por baixo das nadegas. Dê um passo em frente (cerca de 50cm), afastando a anca do step/caixa. Realize a flexão e extensão de cotovelos, apontando os cotovelos sempre para trás (e não para fora).'
      },
      {
        equipment: 'COLCHÃO',
        goal: 'Performance',
        id: 'f8d4c873-b557-4a60-8c34-eb5048133061',
        pse: 6,
        repetitions: 10,
        series: 2,
        duration: 120,
        cedisId: 668,
        name: 'AB CRUNCH',
        instructions:
          'Deitado sobre o tapete, mantenha as penas flectidas e os pés apoiados no chão. Cruze os braços sobre o peito ou apoie as mãos sobre as temporas. Realize a flexão de tronco, mantendo sempre a lombar apoiada no chão e olhar sobre o umbigo.'
      },
      {
        equipment: 'COLCHÃO',
        goal: 'Performance',
        id: '498b141c-7239-4e8d-922f-88f107af45e0',
        pse: 6,
        repetitions: 10,
        series: 2,
        duration: 120,
        cedisId: 752,
        name: 'SUPERMAN ALTERNADO',
        instructions:
          'Deitado sobre o tapete, mantendo o corpo em extensão, eleve de forma alternada, o braço direito e perna esquerda e o braço esquerdo e perna direita. Os gluteos devem permanecer contraídos para proteger a zona lombar e o olhar deve manter-se sobre o chão.'
      },
      {
        equipment: 'COLCHÃO',
        pse: 3,
        duration: 180,
        id: 'df2f3d17-6e95-4ed6-9f8a-015bc1c93bad',
        name: 'ALONGAMENTOS',
        instructions: 'Diriga-se à zona de alongamentos do seu clube'
      }
    ],
    MACHINES: [
      {
        equipment: 'PASSADEIRA',
        goal: 'Performance',
        id: '7e5a7e39-1744-4732-88b4-8c17bce644b6',
        level: 2,
        pse: 5,
        speed: 7,
        duration: 300,
        cedisId: 597,
        name: 'CAMINHAR NA PASSADEIRA',
        instructions:
          'Caminhar de forma controlada. Por questões de segurança sugerimos que mantenha sempre o corpo à distância de um braço, o olhar em frente e se necessário, pode-se apoiar.'
      },
      {
        equipment: 'CROSSOVER',
        goal: 'Performance',
        id: 'e7c85833-f955-4e00-86ae-7319f27a2ec2',
        level: 2,
        pse: 5,
        speed: 7,
        duration: 300,
        cedisId: 595,
        name: 'CROSSOVER',
        instructions:
          'Por questões de segurança, apoio os pés junto ao bordo anterior e interior do respetivo apoio para os pés e as mãos, sobre os respetivos apoios fixo e móveis. Realize o movimento similar ao caminhar/ marchar/ correr sem impacto.'
      },
      {
        equipment: 'REMO',
        goal: 'Performance',
        id: '88eed691-5169-4e33-8b07-3ec622072261',
        level: 6,
        pse: 5,
        duration: 300,
        cedisId: 599,
        name: 'REMO',
        instructions:
          'Mantenha os pés apoiados e fixos no respetivo suporte e realize o movimento idêntico ao remar (extensão de pernas - flexão de braços - flexão de perna – extensão de braços).'
      },
      {
        equipment: 'BICICLETA VERTICAL',
        goal: 'Performance',
        id: '893caed7-31eb-4b98-90ee-450fc7b29834',
        level: 3,
        pse: 5,
        speed: 50,
        duration: 300,
        cedisId: 594,
        name: 'PEDALAR NA BICICLETA VERTICAL',
        instructions:
          'Ajuste o banco, mantendo uma ligeira flexão do joelho no ponto máximo, realize o movimento similar ao pedalar.'
      },
      {
        equipment: 'LEG PRESS',
        goal: 'Performance',
        id: 'b9b526e9-3e7c-4912-8304-29858ebe1497',
        pse: 6,
        repetitions: 10,
        series: 2,
        duration: 120,
        cedisId: 775,
        name: 'LEG PRESS',
        instructions:
          'Ajuste o banco até ao ponto máximo de flexão do joelho. As costas devem permanecer, totalmente, apoiadas contra o banco e as mãos sobre os apoios, realize a extensão quase completa dos joelhos.'
      },
      {
        equipment: 'LOW ROW',
        goal: 'Performance',
        id: 'f79108a5-4930-4086-9de6-5b24e9773e5a',
        pse: 6,
        repetitions: 10,
        series: 2,
        duration: 120,
        cedisId: 774,
        name: 'LOW ROW',
        instructions:
          'Ajuste o banco mantendo a almofada de apoio à altura do peito, permanecendo em contacto durante todo o movimento. Realize a extensão e flexão de cotovelo.'
      },
      {
        equipment: 'CHEST PRESS',
        goal: 'Performance',
        id: '2f2f20fc-b0f1-440d-a725-91702321a7b0',
        pse: 6,
        repetitions: 10,
        series: 2,
        duration: 120,
        cedisId: 770,
        name: 'CHEST PRESS',
        instructions:
          'Ajuste o banco de forma a manter os puxadores em linha com o peito. As costas devem permanecer apoiadas contra o banco e o olhar em frente. Realize a extensão e flexão de cotovelos de forma controlada.'
      },
      {
        equipment: 'LEG CURL',
        goal: 'Performance',
        id: 'd9dbf567-6c61-4a64-9cc2-64adbda54fae',
        pse: 6,
        repetitions: 10,
        series: 2,
        duration: 120,
        cedisId: 780,
        name: 'LEG CURL',
        instructions:
          'Ajuste o banco mantendo os joelhos alinhados com o centro da roldana, ajuste o rolo para manter a perna apoiada (acima da linha do tornozelo). Realize a extensão e flexão de joelhos de forma controlada.'
      },
      {
        equipment: 'DETS MACHINE',
        goal: 'Performance',
        id: 'bee0cdae-0778-4d9d-9996-cfca561b3720',
        pse: 6,
        repetitions: 10,
        series: 2,
        duration: 120,
        cedisId: 784,
        name: 'DETS MACHINE',
        instructions:
          'Ajuste o banco mantendo os cotovelos à altura dos apoio, empurre os cotovelos para cima (até à altura dos ombros) e desça de forma controlada.'
      },
      {
        equipment: 'ABDOMINAL CRUNCH',
        goal: 'Performance',
        id: 'e1f272d9-48e4-43a4-842c-55c97df0a84d',
        pse: 6,
        repetitions: 10,
        series: 2,
        duration: 120,
        cedisId: 761,
        name: 'ABDOMINAL CRUNCH',
        instructions:
          'Apoie os suportes sobre os ombros e segure com as mãos. Realize a flexão de tronco à frente, empurrando o umbigo contra as costas e enrolando o tronco como se quisesse aproximar o seu peito da sua anca.'
      },
      {
        equipment: 'COLCHÃO',
        level: 3,
        id: '9d1a9e10-862e-48d1-bf41-c2d1c989ccc6',
        name: 'ALONGAMENTOS',
        instructions: 'Diriga-se à zona de alongamentos do seu clube'
      }
    ]
  },
  AGE: {
    BODY_WEIGHT: [
      {
        equipment: 'CROSSOVER',
        goal: 'Saúde e condição física',
        id: '3cf494c3-2be4-4b9b-a69f-e04689c9fea4',
        level: 1,
        pse: 5,
        speed: 80,
        duration: 600,
        cedisId: 595,
        name: 'CROSSOVER',
        instructions: 'Movimento similar ao caminhar/marchar/ correr sem impacto.'
      },
      {
        equipment: 'PASSADEIRA',
        goal: 'Saúde e condição física',
        id: 'b5029417-d732-43c0-9810-de281eefb4ab',
        level: 2,
        pse: 5,
        speed: 5,
        duration: 600,
        cedisId: 596,
        name: 'CAMINHAR NA PASSADEIRA',
        instructions:
          'Caminhar de forma controlada. Por questões de segurança sugerimos que mantenha sempre o corpo à distância de um braço, o olhar em frente e se necessário, pode-se apoiar.'
      },
      {
        equipment: 'BICICLETA VERTICAL',
        goal: 'Saúde e condição física',
        id: 'd7653319-87ed-432a-8acc-81a8ec7b4dba',
        level: 2,
        pse: 5,
        speed: 50,
        duration: 600,
        cedisId: 594,
        name: 'PEDALAR NA BICICLETA VERTICAL',
        instructions:
          'Juste o banco, mantendo uma ligeira flexão do joelho no ponto máximo, realize o movimento similar ao pedalar.'
      },
      {
        equipment: 'BICICLETA HORIZONTAL',
        goal: 'Saúde e condição física',
        id: 'f55680a9-d47c-4db3-b93a-e6fac84cbba2',
        level: 2,
        pse: 5,
        speed: 50,
        duration: 600,
        cedisId: 593,
        name: 'PEDALAR NA BICICLETA HORIZONTAL',
        instructions:
          'Ajuste o banco, mantendo uma ligeira flexão do joelho no ponto máximo, as suas costas devem permanecer apoiadas no banco, mantendo o olhar em frente.'
      },
      {
        equipment: 'CAIXA/ STEP',
        goal: 'Saúde e condição física',
        id: '4c51cefe-63e6-43ed-a121-40619bf34d55',
        pse: 6,
        repetitions: 12,
        series: 1,
        duration: 120,
        cedisId: 716,
        name: 'SQUAT COM APOIO',
        instructions:
          'Realize o movimento de sentar e levantar de uma caixa/step. Empurre a anca para trás e para baixo, realizando a flexão de anca e joelhos mantendo sempre os calcanhares apoiados no chão e o olhar em frente.'
      },
      {
        equipment: 'TRX',
        goal: 'Saúde e condição física',
        id: 'd23396d9-8cfb-4545-a007-a77faef234f5',
        pse: 6,
        repetitions: 12,
        series: 1,
        duration: 120,
        cedisId: 730,
        name: 'REMADA FECHADA NO TRX',
        instructions:
          'Coloque o trx no ajuste médio. Mantenha o olhar no ponto de ancoragem e as fitas sempre em extensão. Realize a flexão e extensão de cotovelos, mantendo os cotovelos a passar sempre junto ao tronco (axilas fexhadas)'
      },
      {
        equipment: 'NA',
        goal: 'Saúde e condição física',
        id: '51ecf7d8-d7af-4e62-b946-2e72e31e10a9',
        pse: 6,
        repetitions: 12,
        series: 1,
        duration: 120,
        cedisId: 693,
        name: 'LUNGE',
        instructions:
          'Inicie em pé, com os pés à largura da anca. Dê um passo grande atrás, com uma perna e realize a flexão e extensão de joelhos, como se o corpo se movesse para baixo e para cima (não para frente e trás). Repita o exercício com a outra perna.'
      },
      {
        equipment: 'COLCHÃO',
        goal: 'Saúde e condição física',
        id: '30bb984b-6e33-42b4-9eea-346065421601',
        pse: 6,
        repetitions: 12,
        series: 1,
        duration: 120,
        cedisId: 664,
        name: 'FLEXÃO (PUSH UP)',
        instructions:
          'Deitado sobre um tapete, apoie as mãos mesmo por baixo dos ombros. Realize o movimento de extensão e flexão de cotovelos, assegurando que as suas costas permanecem sempre longas /direitas e o olhar sobre o chão. Pode optar por apoiar os joelhos ou a ponto dos dedos do pés no chão.'
      },
      {
        equipment: 'CAIXA/ STEP',
        goal: 'Saúde e condição física',
        id: '3d1180fd-6fd5-4f63-bc07-28c436d21ff3',
        pse: 6,
        repetitions: 12,
        series: 1,
        duration: 120,
        cedisId: 677,
        name: 'FUNDOS',
        instructions:
          'Sente-se sobre um step/caixa e mantenha as mãos por baixo das nadegas. Dê um passo em frente (cerca de 50cm), afastando a anca do step/caixa. Realize a flexão e extensão de cotovelos, apontando os cotovelos sempre para trás (e não para fora).'
      },
      {
        equipment: 'COLCHÃO',
        goal: 'Saúde e condição física',
        id: 'a66a4a51-8624-471c-99a6-562fc3f69a95',
        pse: 6,
        repetitions: 12,
        series: 1,
        duration: 120,
        cedisId: 668,
        name: 'AB CRUNCH',
        instructions:
          'Deitado sobre o tapete, mantenha as penas flectidas e os pés apoiados no chão. Cruze os braços sobre o peito ou apoie as mãos sobre as temporas. Realize a flexão de tronco, mantendo sempre a lombar apoiada no chão e olhar sobre o umbigo.'
      },
      {
        equipment: 'COLCHÃO',
        goal: 'Saúde e condição física',
        id: 'a43d8b8e-b841-49ee-b16a-12bd913db031',
        pse: 6,
        repetitions: 12,
        series: 1,
        duration: 120,
        cedisId: 751,
        name: 'SUPERMAN ALTERNADO',
        instructions:
          'Deitado sobre o tapete, mantendo o corpo em extensão, eleve de forma alternada, o braço direito e perna esquerda e o braço esquerdo e perna direita. Os gluteos devem permanecer contraídos para proteger a zona lombar e o olhar deve manter-se sobre o chão.'
      },
      {
        equipment: 'COLCHÃO',
        pse: 3,
        duration: 180,
        id: 'c0ebd72c-7f8f-4203-9f8d-c9f9f1b2d222',
        name: 'ALONGAMENTOS',
        instructions: 'Diriga-se à zona de alongamentos do seu clube'
      }
    ],
    MACHINES: [
      {
        equipment: 'BICICLETA HORIZONTAL',
        goal: 'Saúde e condição física',
        id: '4ebcbe0f-a292-4849-ae97-24b2afe52da6',
        level: 1,
        pse: 5,
        speed: 50,
        duration: 600,
        cedisId: 593,
        name: 'PEDALAR NA BICICLETA HORIZONTAL',
        instructions:
          'Ajuste o banco, mantendo uma ligeira flexão do joelho no ponto máximo, as suas costas devem permanecer apoiadas no banco, mantendo o olhar em frente.'
      },
      {
        equipment: 'BICICLETA VERTICAL',
        goal: 'Saúde e condição física',
        id: '483b16ed-2dce-481b-9cb1-c4edd5e634af',
        level: 1,
        pse: 5,
        speed: 50,
        duration: 600,
        cedisId: 594,
        name: 'PEDALAR NA BICICLETA VERTICAL',
        instructions:
          'Juste o banco, mantendo uma ligeira flexão do joelho no ponto máximo, realize o movimento similar ao pedalar.'
      },
      {
        equipment: 'PASSADEIRA',
        goal: 'Saúde e condição física',
        id: '93ff7360-befe-4e42-86d9-979d4eb1df31',
        level: 2,
        pse: 5,
        speed: 5,
        duration: 600,
        cedisId: 596,
        name: 'CAMINHAR NA PASSADEIRA',
        instructions:
          'Caminhar de forma controlada. Por questões de segurança sugerimos que mantenha sempre o corpo à distância de um braço, o olhar em frente e se necessário, pode-se apoiar.'
      },
      {
        equipment: 'TOP',
        goal: 'Saúde e condição física',
        id: 'c6871a00-422c-4958-9914-8ae488e08507',
        level: 1,
        pse: 5,
        speed: 30,
        duration: 600,
        cedisId: 602,
        name: 'TOP',
        instructions:
          'Ajuste o banco mantendo os joelhos confortáveis e os pés totalmente apoiados sobre o chão. Realize o movimento de rotação dos braços.'
      },
      {
        equipment: 'LEG PRESS',
        goal: 'Saúde e condição física',
        id: 'ac6070a5-6c62-4da7-9522-727de1747906',
        pse: 5,
        duration: 120,
        cedisId: 775,
        name: 'LEG PRESS',
        instructions:
          'Ajuste o banco até ao ponto máximo de flexão do joelho. As costas devem permancer totalmente apoiadas contra o banco e as mãos sobre os apoios. Realize a extensão quase completa dos joelhos'
      },
      {
        equipment: 'CHEST PRESS',
        goal: 'Saúde e condição física',
        id: 'f4569e34-a14d-4595-8b9f-ad35eed212e3',
        pse: 5,
        duration: 120,
        cedisId: 770,
        name: 'CHEST PRESS',
        instructions:
          'Ajuste o banco de forma a manter os puxadores em linha com o peito. As costas devem permanecer apoiadas contra o banco e o olhar em frente. Realize a extensão e flexão de cotovelos de forma c'
      },
      {
        equipment: 'LEG CURL',
        goal: 'Saúde e condição física',
        id: '546df11d-218b-49be-be54-78188907c903',
        pse: 5,
        duration: 120,
        cedisId: 764,
        name: 'LEG CURL',
        instructions:
          'Ajuste o banco mantendo os joelhos alinhados com o centro da roldana. Ajuste o rolo para manter a perna apoiada (acima da linha do tornozelo). Realize a extensão e flexão de joelhos de forma controlada.'
      },
      {
        equipment: 'LOW ROW',
        goal: 'Saúde e condição física',
        id: 'f4720114-325b-4c96-9a34-bd4573f6e51b',
        pse: 5,
        duration: 120,
        cedisId: 780,
        name: 'LOW ROW',
        instructions:
          'Ajuste o banco mantendo a almofada de apoio à altura do peito, permanecendo em contacto durante todo o movimento. Realize a extensão e flexão de cotovelo.'
      },
      {
        equipment: 'ABDOMINAL CRUNCH',
        goal: 'Saúde e condição física',
        id: '1b4ce764-7b9b-4b49-af03-e03d8a1fcdb3',
        pse: 5,
        duration: 120,
        cedisId: 760,
        name: 'ABDOMINAL CRUNCH',
        instructions:
          'Apoie os suportes sobre os ombros e segure com as mãos. Realize a flexão de tronco à frente, empurrando o umbigo contra as costas e enrolando o tronco como se quisesse aproximar o seu peito'
      },
      {
        equipment: 'LOWER BACK',
        goal: 'Saúde e condição física',
        id: '32641364-ebe7-4eb3-870e-9a27f49816e5',
        pse: 5,
        duration: 120,
        cedisId: 773,
        name: 'LOWER BACK',
        instructions:
          'Ajuste o rolo mantendo apoiado contra as omplatas. Inicie o movimento na posição máxima de flexão do tronco e realize a extensão, empurrando para cima e para trás.'
      },
      {
        equipment: 'COLCHÃO',
        pse: 3,
        duration: 180,
        id: 'c36ed210-2aa3-4d95-a778-cb2026673b40',
        name: 'ALONGAMENTOS',
        instructions: 'Diriga-se à zona de alongamentos do seu clube'
      }
    ]
  },
  FIT: {
    BODY_WEIGHT: [
      {
        equipment: 'PASSADEIRA',
        goal: 'Tonificação',
        id: '873a2d3f-434e-431e-b81a-b2e8b0782281',
        level: 2,
        pse: 5,
        speed: 6,
        duration: 600,
        cedisId: 598,
        name: 'CAMINHAR NA PASSADEIRA',
        instructions:
          'Caminhar de forma controlada. Por questões de segurança sugerimos que mantenha sempre o corpo à distância de um braço, o olhar em frente e se necessário, pode-se apoiar.'
      },
      {
        equipment: 'BICICLETA VERTICAL',
        goal: 'Tonificação',
        id: '7b449894-59e9-430d-9a37-f814e7012caf',
        level: 2,
        pse: 5,
        speed: 50,
        duration: 600,
        cedisId: 594,
        name: 'PEDALAR NA BICICLETA VERTICAL',
        instructions:
          'Juste o banco, mantendo uma ligeira flexão do joelho no ponto máximo, realize o movimento similar ao pedalar.'
      },
      {
        equipment: 'CROSSOVER',
        goal: 'Tonificação',
        id: '1ad866ed-8cf0-4f0d-b7c7-3d1015757567',
        level: 1,
        pse: 5,
        speed: 80,
        duration: 600,
        cedisId: 595,
        name: 'CROSSOVER',
        instructions:
          'Por questões de segurança, apoie os pés junto ao bordo anterior e interior do respectivo apoio para os pés e as mãos, sobre os respectivos apoios fixos ou móveis. Realize o movimento similar ao caminhar/marchar/ correr sem impacto.'
      },
      {
        equipment: 'BICICLETA HORIZONTAL',
        goal: 'Tonificação',
        id: 'bc5fe28a-ebf2-4dfe-80c2-226d34dbff40',
        level: 2,
        pse: 5,
        speed: 50,
        duration: 600,
        cedisId: 593,
        name: 'PEDALAR NA BICICLETA HORIZONTAL',
        instructions:
          'Ajuste o banco, mantendo uma ligeira flexão do joelho no ponto máximo, as suas costas devem permanecer apoiadas no banco, mantendo o olhar em frente.'
      },
      {
        equipment: 'CAIXA/ STEP',
        goal: 'Tonificação',
        id: 'f0744bf7-0f0f-4e9f-acee-ec3f1e4a5d8a',
        pse: 6,
        repetitions: 10,
        series: 2,
        duration: 120,
        cedisId: 716,
        name: 'SQUAT COM APOIO',
        instructions:
          'Realize o movimento de sentar e levantar de uma caixa/step. Empurre a anca para trás e para baixo, realizando a flexão de anca e joelhos mantendo sempre os calcanhares apoiados no chão e o olhar em frente.'
      },
      {
        equipment: 'NA',
        goal: 'Tonificação',
        id: '90b2988c-05cd-4cd7-ba2a-d16b2bafc053',
        pse: 6,
        repetitions: 10,
        series: 2,
        duration: 120,
        cedisId: 689,
        name: 'LUNGE',
        instructions:
          'Inicie em pé, com os pés à largura da anca. Dê um passo grande atrás, com uma perna e realize a flexão e extensão de joelhos, como se o corpo se movesse para baixo e para cima (não para frente e trás). Repita o exercício com a outra perna.'
      },
      {
        equipment: 'CAIXA/ STEP',
        goal: 'Tonificação',
        id: 'e80c8f52-b863-4b98-946d-32096ff5e9c3',
        pse: 6,
        repetitions: 10,
        series: 2,
        duration: 120,
        cedisId: 730,
        name: 'FUNDOS',
        instructions:
          'Sente-se sobre um step/caixa e mantenha as mãos por baixo das nadegas. Dê um passo em frente (cerca de 50cm), afastando a anca do step/caixa. Realize a flexão e extensão de cotovelos, apontando os cotovelos sempre para trás (e não para fora).'
      },
      {
        equipment: 'COLCHÃO',
        goal: 'Tonificação',
        id: 'fc7a79d6-d497-4acb-b94b-e2673bd77976',
        pse: 6,
        repetitions: 10,
        series: 2,
        duration: 120,
        cedisId: 664,
        name: 'FLEXÃO (PUSH UP)',
        instructions:
          'Deitado sobre um tapete, apoie as mãos mesmo por baixo dos ombros. Realize o movimento de extensão e flexão de cotovelos, assegurando que as suas costas permanecem sempre longas /direitas e o olhar sobre o chão. Pode optar por apoiar os joelhos ou a ponto dos dedos do pés no chão.'
      },
      {
        equipment: 'TRX',
        goal: 'Tonificação',
        id: '7c9a1aa0-f0cd-404f-b44e-8ac723a95c1f',
        pse: 6,
        repetitions: 10,
        series: 2,
        duration: 120,
        cedisId: 677,
        name: 'REMADA FECHADA NO TRX',
        instructions:
          'Coloque o trx no ajuste médio. Mantenha o olhar no ponto de ancoragem e as fitas sempre em extensão. Realize a flexão e extensão de cotovelos, mantendo os cotovelos a passar sempre junto ao tronco (axilas fexhadas)'
      },
      {
        equipment: 'COLCHÃO',
        goal: 'Tonificação',
        id: '759adaf1-ba4d-4f15-9383-37b7677669ec',
        pse: 6,
        repetitions: 10,
        series: 2,
        duration: 120,
        cedisId: 668,
        name: 'AB CRUNCH',
        instructions:
          'Deitado sobre o tapete, mantenha as penas flectidas e os pés apoiados no chão. Cruze os braços sobre o peito ou apoie as mãos sobre as temporas. Realize a flexão de tronco, mantendo sempre a lombar apoiada no chão e olhar sobre o umbigo.'
      },
      {
        equipment: 'COLCHÃO',
        goal: 'Tonificação',
        id: '727d57d0-f370-4e08-9e28-9470e01543d4',
        pse: 6,
        repetitions: 10,
        series: 2,
        duration: 120,
        cedisId: 751,
        name: 'SUPERMAN ALTERNADO',
        instructions:
          'Deitado sobre o tapete, mantendo o corpo em extensão, eleve de forma alternada, o braço direito e perna esquerda e o braço esquerdo e perna direita. Os gluteos devem permanecer contraídos para proteger a zona lombar e o olhar deve manter-se sobre o chão.'
      },
      {
        equipment: 'COLCHÃO',
        pse: 3,
        duration: 180,
        id: 'e255ddab-2705-4e9c-8a7b-eed5726cad4d',
        name: 'ALONGAMENTOS',
        instructions: 'Diriga-se à zona de alongamentos do seu clube'
      }
    ],
    MACHINES: [
      {
        equipment: 'CROSSOVER',
        id: '486ef576-7e30-4cbf-a415-1743f6519bcd',
        level: 1,
        pse: 5,
        speed: 80,
        duration: 600,
        cedisId: 595,
        name: 'CROSSOVER',
        instructions:
          'Por questões de segurança, apoie os pés junto ao bordo anterior e interior do respetivo apoio para os pés e mãos, sobre os respetivos apoios fixos ou móveis. Realize o movimento similar ao caminha/ marchar/ correr sem impacto.'
      },
      {
        equipment: 'PASSADEIRA',
        id: 'bb138bc1-43cb-4c03-8234-1a253677fbec',
        level: 2,
        pse: 5,
        speed: 5,
        duration: 600,
        cedisId: 596,
        name: 'CAMINHAR NA PASSADEIRA',
        instructions:
          'Caminhar de forma controlada. Por questões de segurança sugerimos que mantenha sempre o corpo à distância de um braço, o olhar em frente e, se necessário, pode-se apoiar.'
      },
      {
        equipment: 'BICICLETA VERTICAL',
        id: '23698bc3-f926-40c5-b2fa-c99c401291d5',
        level: 2,
        pse: 5,
        speed: 50,
        duration: 600,
        cedisId: 594,
        name: 'PEDALAR NA BICICLETA VERTICAL',
        instructions:
          'Ajuste o banco mantendo uma ligeira flexão do joelho no ponto máximo, realize o movimento similar ao pedalar.'
      },
      {
        equipment: 'BICICLETA HORIZONTAL',
        id: 'd5cef77b-5178-44a1-82a3-ec756aa1316b',
        level: 2,
        pse: 5,
        speed: 50,
        duration: 600,
        cedisId: 593,
        name: 'PEDALAR NA BICICLETA HORIZONTAL',
        instructions:
          'Ajuste o banco mantendo uma ligeira flexão do joelho no ponto máximo, as costas devem permanecer apoiadas no banco, mantendo o olhar em frente.'
      },
      {
        equipment: 'LEG PRESS',
        id: '8f3e6392-51e5-493f-96d0-e0a3241e590e',
        pse: 6,
        repetitions: 10,
        series: 2,
        duration: 120,
        cedisId: 775,
        name: 'LEG PRESS',
        instructions:
          'Ajuste o banco até ao ponto máximo de flexão do joelho. As costas devem permanecer, totalmente, apoiadas contra o banco e as mãos sobre os apoios, realize a extensão quase completa dos joelhos.'
      },
      {
        equipment: 'LEG CURL',
        id: '4ddc4665-74cb-450a-8460-957809c91ef2',
        pse: 5,
        repetitions: 10,
        series: 2,
        duration: 120,
        cedisId: 774,
        name: 'LEG CURL',
        instructions:
          'Ajuste o banco mantendo os joelhos alinhados com o centro da roldana, ajuste o rolo para manter a perna apoiada (acima da linha do tornozelo). Realize a extensão e flexão de joelhos de forma controlada.'
      },
      {
        equipment: 'LOW ROW',
        id: 'd79ffbe9-e8b2-4b2f-ab16-17902ab187db',
        pse: 6,
        repetitions: 10,
        series: 2,
        duration: 120,
        cedisId: 780,
        name: 'LOW ROW',
        instructions:
          'Ajuste o banco mantendo a almofada de apoio à altura do peito, permanecendo em contacto durante todo o movimento. Realize a extensão e flexão de cotovelo.'
      },
      {
        equipment: 'CHEST PRESS',
        id: '1675bd3b-f61d-450f-8fc5-b6773dc1dcd7',
        pse: 6,
        repetitions: 10,
        series: 2,
        duration: 120,
        cedisId: 770,
        name: 'CHEST PRESS',
        instructions:
          'Ajuste o banco de forma a manter os puxadores em linha com o peito. As costas devem permanecer apoiadas contra o banco e o olhar em frente. Realize a extensão e flexão de cotovelos de forma controlada.'
      },
      {
        equipment: 'ABDOMINAL CRUNCH',
        id: '2a7a27b2-5de9-4349-ab45-b1cd0c220b67',
        pse: 6,
        repetitions: 10,
        series: 2,
        duration: 120,
        cedisId: 760,
        name: 'ABDOMINAL CRUNCH',
        instructions:
          'Apoio os suportes sobre os ombros e segure com as mãos. Realize a flexão de tronco à frente, empurrando o umbigo contra as costas e enrolando o tronco como se quisesse aproximar o seu peito.'
      },
      {
        equipment: 'LOWER BACK',
        id: '4fb11c01-8618-493b-9378-2b21b1cb3ba0',
        pse: 6,
        repetitions: 10,
        series: 2,
        duration: 120,
        cedisId: 773,
        name: 'LOWER BACK',
        instructions:
          'Ajuste o rolo mantendo apoiado contra as omoplatas, inicie o movimento na posição máxima de flexão do tronco e realize a extensão, empurrando para cima e para trás.'
      },
      {
        equipment: 'ALONGAMENTOS',
        pse: 3,
        duration: 180,
        id: '61c269ff-5a84-4249-9d85-72cbc055c3a9',
        name: 'ALONGAMENTOS',
        instructions:
          'Diriga-se à zona de alongamentos do seu clube. Mantenha-se entre 20 a 30 segundos em cada posição, sem dor (apenas ligeira sensação de extensão) e respire de forma calma e natural.'
      }
    ]
  },
  BALANCE: {
    BODY_WEIGHT: [
      {
        equipment: 'BICICLETA HORIZONTAL',
        goal: 'Corpo e Mente',
        id: '5fcc1b08-9d52-4778-93f6-0aae01ac911b',
        level: 1,
        pse: 5,
        speed: 50,
        duration: 600,
        cedisId: 593,
        name: 'PEDALAR NA BICICLETA HORIZONTAL',
        instructions:
          'Ajuste o banco, mantendo uma ligeira flexão do joelho no ponto máximo, as suas costas devem permanecer apoiadas no banco, mantendo o olhar em frente.'
      },
      {
        equipment: 'BICICLETA VERTICAL',
        goal: 'Corpo e Mente',
        id: '223ff8b8-30be-4693-bfdc-ed3a011e082a',
        level: 1,
        pse: 5,
        speed: 50,
        duration: 600,
        cedisId: 594,
        name: 'PEDALAR NA BICICLETA VERTICAL',
        instructions:
          'Juste o banco, mantendo uma ligeira flexão do joelho no ponto máximo, realize o movimento similar ao pedalar.'
      },
      {
        equipment: 'CROSSOVER',
        goal: 'Corpo e Mente',
        id: 'fefbef47-095e-433b-b331-4633edb58fe1',
        level: 1,
        pse: 5,
        speed: 80,
        duration: 600,
        cedisId: 595,
        name: 'CROSSOVER',
        instructions:
          'Por questões de segurança, apoie os pés junto ao bordo anterior e interior do respectivo apoio para os pés e as mãos, sobre os respectivos apoios fixos ou móveis. Realize o movimento similar ao caminhar/marchar/ correr sem impacto.'
      },
      {
        equipment: 'PASSADEIRA',
        goal: 'Corpo e Mente',
        id: '34e5e8db-cbd7-41d1-8af9-fca9b0daab64',
        pse: 5,
        duration: 600,
        cedisId: 596,
        name: 'CAMINHAR NA PASSADEIRA',
        instructions:
          'Caminhar de forma controlada. Por questões de segurança sugerimos que mantenha sempre o corpo à distância de um braço, o olhar em frente e se necessário, pode-se apoiar.'
      },
      {
        equipment: 'LEG PRESS',
        goal: 'Corpo e Mente',
        id: '2443de1d-e6b8-481e-a924-a06cc912cb50',
        pse: 6,
        duration: 120,
        cedisId: 775,
        name: 'LEG PRESS',
        instructions:
          'Ajuste o banco até ao ponto máximo de flexão do joelho. As costas devem permancer totalmente apoiadas contra o banco e as mãos sobre os apoios. Realize a extensão quase completa dos joelhos'
      },
      {
        equipment: 'CHEST PRESS',
        goal: 'Corpo e Mente',
        id: 'a0b3756a-49a1-4a27-86b0-227f84080b5f',
        pse: 6,
        duration: 120,
        cedisId: 770,
        name: 'CHEST PRESS',
        instructions:
          'Ajuste o banco de forma a manter os puxadores em linha com o peito. As costas devem permanecer apoiadas contra o banco e o olhar em frente. Realize a extensão e flexão de cotovelos de forma c'
      },
      {
        equipment: 'LEG CURL',
        goal: 'Corpo e Mente',
        id: '76bfe6c8-fa7a-454b-9408-26c65ecf8645',
        pse: 6,
        duration: 120,
        cedisId: 774,
        name: 'LEG CURL',
        instructions:
          'Ajuste o banco mantendo os joelhos alinhados com o centro da roldana. Ajuste o rolo para manter a perna apoiada (acima da linha do tornozelo). Realize a extensão e flexão de joelhos de forma controlada.'
      },
      {
        equipment: 'LOW ROW',
        goal: 'Corpo e Mente',
        id: '96add71d-f67a-48fd-9ee5-b72d4c992b3f',
        pse: 6,
        duration: 120,
        cedisId: 780,
        name: 'LOW ROW',
        instructions:
          'Ajuste o banco mantendo a almofada de apoio à altura do peito, permanecendo em contacto durante todo o movimento. Realize a extensão e flexão de cotovelo.'
      },
      {
        equipment: 'ABDOMINAL CRUNCH',
        goal: 'Corpo e Mente',
        id: '708d6b9d-dbec-46a0-bed5-31e1fb971c90',
        pse: 6,
        duration: 120,
        cedisId: 760,
        name: 'ABDOMINAL CRUNCH',
        instructions:
          'Apoie os suportes sobre os ombros e segure com as mãos. Realize a flexão de tronco à frente, empurrando o umbigo contra as costas e enrolando o tronco como se quisesse aproximar o seu peito'
      },
      {
        equipment: 'LOWER BACK',
        goal: 'Corpo e Mente',
        id: '21f96f2e-c38f-46d2-8c78-bd2f0747dff4',
        pse: 6,
        duration: 120,
        cedisId: 773,
        name: 'LOWER BACK',
        instructions:
          'Ajuste o rolo mantendo apoiado contra as omplatas. Inicie o movimento na posição máxima de flexão do tronco e realize a extensão, empurrando para cima e para trás.'
      },
      {
        equipment: 'PASSADEIRA',
        goal: 'Corpo e Mente',
        id: '85e80f32-941c-430f-940f-7c7358e40e12',
        level: 2,
        pse: 5,
        speed: 5,
        duration: 600,
        cedisId: 596,
        name: 'CAMINHAR NA PASSADEIRA',
        instructions:
          'Caminhar de forma controlada. Por questões de segurança sugerimos que mantenha sempre o corpo à distância de um braço, o olhar em frente e se necessário, pode-se apoiar.'
      },
      {
        equipment: 'CROSSOVER',
        goal: 'Corpo e Mente',
        id: 'e066e67d-46d8-470a-8c8d-fb43f7deca05',
        level: 1,
        pse: 5,
        speed: 80,
        duration: 600,
        cedisId: 595,
        name: 'CROSSOVER',
        instructions: 'Movimento similar ao caminhar/marchar/ correr sem impacto.'
      },
      {
        equipment: 'BICICLETA VERTICAL',
        goal: 'Corpo e Mente',
        id: 'bfd0a3b8-8167-4702-92ec-7742c78e9a41',
        level: 2,
        pse: 5,
        speed: 50,
        duration: 600,
        cedisId: 594,
        name: 'PEDALAR NA BICICLETA VERTICAL',
        instructions:
          'Juste o banco, mantendo uma ligeira flexão do joelho no ponto máximo, realize o movimento similar ao pedalar.'
      },
      {
        equipment: 'BICICLETA HORIZONTAL',
        goal: 'Corpo e Mente',
        id: 'd6abeee7-b25c-4f48-b142-a4347c3c5b40',
        level: 2,
        pse: 5,
        speed: 50,
        duration: 600,
        cedisId: 593,
        name: 'PEDALAR NA BICICLETA HORIZONTAL',
        instructions:
          'Ajuste o banco, mantendo uma ligeira flexão do joelho no ponto máximo, as suas costas devem permanecer apoiadas no banco, mantendo o olhar em frente.'
      },
      {
        equipment: 'CAIXA/ STEP',
        goal: 'Corpo e Mente',
        id: 'ee7e1d0c-9d6e-4893-8399-539f37499150',
        pse: 6,
        repetitions: 10,
        series: 1,
        duration: 120,
        cedisId: 716,
        name: 'SQUAT COM APOIO',
        instructions:
          'Realize o movimento de sentar e levantar de uma caixa/step. Empurre a anca para trás e para baixo, realizando a flexão de anca e joelhos mantendo sempre os calcanhares apoiados no chão e o olhar em frente.'
      },
      {
        equipment: 'COLCHÃO',
        goal: 'Corpo e Mente',
        id: '156e746a-e3ad-4f7d-b159-796d6befd226',
        pse: 6,
        repetitions: 10,
        series: 1,
        duration: 120,
        cedisId: 664,
        name: 'FLEXÃO (PUSH UP)',
        instructions:
          'Deitado sobre um tapete, apoie as mãos mesmo por baixo dos ombros. Realize o movimento de extensão e flexão de cotovelos, assegurando que as suas costas permanecem sempre longas /direitas e o olhar sobre o chão. Pode optar por apoiar os joelhos ou a ponto dos dedos do pés no chão.'
      },
      {
        equipment: 'COLCHÃO',
        goal: 'Corpo e Mente',
        id: 'a82e1742-f479-4e2d-b781-2a03aa2a9cc3',
        pse: 6,
        repetitions: 10,
        series: 1,
        duration: 120,
        cedisId: 668,
        name: 'AB CRUNCH',
        instructions:
          'Deitado sobre o tapete, mantenha as penas flectidas e os pés apoiados no chão. Cruze os braços sobre o peito ou apoie as mãos sobre as temporas. Realize a flexão de tronco, mantendo sempre a lombar apoiada no chão e olhar sobre o umbigo.'
      },
      {
        equipment: 'NA',
        goal: 'Corpo e Mente',
        id: 'aa6e0da7-8d5f-498c-82a9-7810bd639db7',
        pse: 6,
        repetitions: 10,
        series: 1,
        duration: 120,
        cedisId: 693,
        name: 'LUNGE',
        instructions:
          'Inicie em pé, com os pés à largura da anca. Dê um passo grande atrás, com uma perna e realize a flexão e extensão de joelhos, como se o corpo se movesse para baixo e para cima (não para frente e trás). Repita o exercício com a outra perna.'
      },
      {
        equipment: 'CAIXA/ STEP',
        goal: 'Corpo e Mente',
        id: '97dd131a-9eac-4b2d-897d-39bf8783ac10',
        pse: 6,
        repetitions: 10,
        series: 1,
        duration: 120,
        cedisId: 730,
        name: 'FUNDOS',
        instructions:
          'Sente-se sobre um step/caixa e mantenha as mãos por baixo das nadegas. Dê um passo em frente (cerca de 50cm), afastando a anca do step/caixa. Realize a flexão e extensão de cotovelos, apontando os cotovelos sempre para trás (e não para fora).'
      },
      {
        equipment: 'TRX',
        goal: 'Corpo e Mente',
        id: 'd5f5e8bb-56af-4fea-b7ff-c0e376a38f76',
        pse: 6,
        repetitions: 10,
        series: 1,
        duration: 120,
        cedisId: 677,
        name: 'REMADA FECHADA NO TRX',
        instructions:
          'Coloque o trx no ajuste médio. Mantenha o olhar no ponto de ancoragem e as fitas sempre em extensão. Realize a flexão e extensão de cotovelos, mantendo os cotovelos a passar sempre junto ao tronco (axilas fexhadas)'
      },
      {
        equipment: 'COLCHÃO',
        goal: 'Corpo e Mente',
        id: '101bf0d4-1547-4f44-bef7-00768e0e2e65',
        pse: 6,
        repetitions: 10,
        series: 1,
        duration: 120,
        cedisId: 751,
        name: 'SUPERMAN ALTERNADO',
        instructions:
          'Deitado sobre o tapete, mantendo o corpo em extensão, eleve de forma alternada, o braço direito e perna esquerda e o braço esquerdo e perna direita. Os gluteos devem permanecer contraídos para proteger a zona lombar e o olhar deve manter-se sobre o chão.'
      },
      {
        equipment: 'COLCHÃO',
        pse: 3,
        duration: 180,
        id: 'daccc290-b195-4b77-b971-9db8080a9184',
        name: 'ALONGAMENTOS',
        instructions: 'Diriga-se à zona de alongamentos do seu Clube'
      }
    ]
  },
  WEIGHT: {
    BODY_WEIGHT: [
      {
        equipment: 'PASSADEIRA',
        goal: 'Perda de peso',
        id: 'e0e65356-f881-454a-83a2-c740e9a2ed43',
        level: 2,
        pse: 5,
        speed: 5,
        duration: 600,
        cedisId: 596,
        name: 'CAMINHAR NA PASSADEIRA',
        instructions:
          'Caminhar de forma controlada. Por questões de segurança sugerimos que mantenha sempre o corpo à distância de um braço, o olhar em frente e se necessário, pode-se apoiar.'
      },
      {
        equipment: 'CROSSOVER',
        goal: 'Perda de peso',
        id: '79f1fce0-d6be-4386-bb39-c98f505d4158',
        level: 1,
        pse: 5,
        speed: 80,
        duration: 600,
        cedisId: 595,
        name: 'CROSSOVER',
        instructions:
          'Por questões de segurança, apoie os pés junto ao bordo anterior e interior do respectivo apoio para os pés e as mãos, sobre os respectivos apoios fixos ou móveis. Realize o movimento similar ao caminhar/marchar/ correr sem impacto.'
      },
      {
        equipment: 'BICICLETA HORIZONTAL',
        goal: 'Perda de peso',
        id: '7096a9e8-2672-4dce-bc30-98f3b12782d8',
        level: 2,
        pse: 5,
        speed: 50,
        duration: 600,
        cedisId: 593,
        name: 'PEDALAR NA BICICLETA HORIZONTAL',
        instructions:
          'Ajuste o banco, mantendo uma ligeira flexão do joelho no ponto máximo, as suas costas devem permanecer apoiadas no banco, mantendo o olhar em frente.'
      },
      {
        equipment: 'BICICLETA VERTICAL',
        goal: 'Perda de peso',
        id: 'f3734b34-398c-42ac-a139-2ed9557c5ef6',
        level: 2,
        pse: 5,
        speed: 50,
        duration: 600,
        cedisId: 594,
        name: 'PEDALAR NA BICICLETA VERTICAL',
        instructions:
          'Juste o banco, mantendo uma ligeira flexão do joelho no ponto máximo, realize o movimento similar ao pedalar.'
      },
      {
        equipment: 'TRX',
        goal: 'Perda de peso',
        id: '313e8c6c-83bc-428e-b2c3-3677e7ab3f15',
        pse: 6,
        repetitions: 10,
        series: 1,
        duration: 120,
        cedisId: 730,
        name: 'REMADA FECHADA NO TRX',
        instructions:
          'Coloque o trx no ajuste médio. Mantenha o olhar no ponto de ancoragem e as fitas sempre em extensão. Realize a flexão e extensão de cotovelos, mantendo os cotovelos a passar sempre junto ao tronco (axilas fexhadas)'
      },
      {
        equipment: 'CAIXA/ STEP',
        goal: 'Perda de peso',
        id: '5803c377-732f-4e78-8ec1-8f44afaf87b0',
        pse: 6,
        repetitions: 10,
        series: 1,
        duration: 120,
        cedisId: 716,
        name: 'SQUAT COM APOIO',
        instructions:
          'Realize o movimento de sentar e levantar de uma caixa/step. Empurre a anca para trás e para baixo, realizando a flexão de joelhos mantendo sempre os calcanhares apoiados no chão.'
      },
      {
        goal: 'Perda de peso',
        id: '3fcbcce4-3e91-4811-b631-56574d5b0123',
        pse: 6,
        repetitions: 10,
        series: 1,
        duration: 120,
        cedisId: 664,
        name: 'FLEXÃO (PUSH UP)',
        instructions:
          'Deitado sobre um tapete, apoie as mãos mesmo por baixo dos ombros. Realize o movimento de extensão e flexão de cotovelos, assegurando que as suas costas permanecem sempre longas /direitas e o olhar sobre o chão. Pode optar por apoiar os joelhos ou a ponto dos dedos do pés no chão.'
      },
      {
        equipment: 'STEP',
        goal: 'Perda de peso',
        id: '3d29f400-8eb1-4afa-a761-0454999a288a',
        pse: 6,
        repetitions: 10,
        series: 1,
        duration: 120,
        cedisId: 693,
        name: 'LUNGE SOBRE STEP',
        instructions:
          'Apoie totalmente o pé da frente sobre o step. Dê um passo gigante com o pé de trás (mantendo os pés à largura da anca). Realize a flexão e extensão de joelhos, como se o corpo se movesse para baixo e para cima (não para frente e trás)'
      },
      {
        equipment: 'CAIXA/ STEP',
        goal: 'Perda de peso',
        id: 'd84bcca9-48f8-4ba6-8bd5-1941e6aa416f',
        pse: 6,
        repetitions: 10,
        series: 1,
        duration: 120,
        cedisId: 678,
        name: 'FUNDOS',
        instructions:
          'Sente-se sobre um step/caixa e mantenha as mãos por baixo das nadegas. Dê um passo em frente (cerca de 50cm), afastando a anca do step/caixa. Realize a flexão e extensão de cotovelos, apontando os cotovelos sempre para trás (e não para fora).'
      },
      {
        equipment: 'COLCHÃO',
        goal: 'Perda de peso',
        id: '92b71736-007d-48f9-b1bc-fdbdd4e9b365',
        pse: 6,
        repetitions: 10,
        series: 1,
        duration: 120,
        cedisId: 668,
        name: 'AB CRUNCH',
        instructions:
          'Deitado sobre o tapete, mantenha as penas flectidas e os pés apoiados no chão. Cruze os braços sobre o peito ou apoie as mãos sobre as temporas. Realize a flexão de tronco, mantendo sempre a lombar apoiada no chão e olhar sobre o umbigo.'
      },
      {
        equipment: 'COLCHÃO',
        goal: 'Perda de peso',
        id: 'c167765f-dff1-485c-a3f2-d7f47bdc36b8',
        pse: 6,
        repetitions: 10,
        series: 1,
        duration: 120,
        cedisId: 751,
        name: 'SUPERMAN ALTERNADO',
        instructions:
          'Deitado sobre o tapete, mantendo o corpo em extensão, eleve de forma alternada, o braço direito e perna esquerda e o braço esquerdo e perna direita. Os gluteos devem permanecer contraídos para proteger a zona lombar e o olhar deve manter-se sobre o chão.'
      },
      {
        equipment: 'COLCHÃO',
        pse: 3,
        duration: 180,
        id: 'ca1dbf80-8154-459b-9917-dc33cdf67dac',
        name: 'ALONGAMENTOS',
        instructions: 'Diriga-se à zona de alongamentos do seu clube'
      }
    ],
    MACHINES: [
      {
        equipment: 'PASSADEIRA',
        goal: 'Perda de peso',
        id: 'e8da1a55-9716-43bb-8355-671759161302',
        level: 2,
        pse: 5,
        speed: 5,
        duration: 600,
        cedisId: 596,
        name: 'CAMINHAR NA PASSADEIRA',
        instructions:
          'Caminhar de forma controlada. Por questões de segurança sugerimos que mantenha sempre o corpo à distância de um braço, o olhar em frente e se necessário, pode-se apoiar.'
      },
      {
        equipment: 'CROSSOVER',
        goal: 'Perda de peso',
        id: '75b8574d-53f3-4f6b-861f-6fbed807678d',
        level: 1,
        pse: 5,
        speed: 80,
        duration: 600,
        cedisId: 601,
        name: 'CROSSOVER',
        instructions:
          'Por questões de segurança, apoie os pés junto ao bordo anterior e interior do respectivo apoio para os pés e as mãos, sobre os respectivos apoios fixos ou móveis. Realize o movimento similar ao caminhar/marchar/ correr sem impacto.'
      },
      {
        equipment: 'BICICLETA HORIZONTAL',
        goal: 'Perda de peso',
        id: '55eddd03-3241-47da-b35d-c293a3466299',
        level: 1,
        pse: 5,
        speed: 50,
        duration: 600,
        cedisId: 593,
        name: 'PEDALAR NA BICICLETA HORIZONTAL',
        instructions:
          'Ajuste o banco, mantendo uma ligeira flexão do joelho no ponto máximo, realize o movimento similar ao pedalar. As suas costas devem permanecer apoiadas no banco, mantendo o olhar em frente.'
      },
      {
        equipment: 'BICICLETA VERTICAL',
        goal: 'Perda de peso',
        id: 'eb8dea38-f3df-4065-9695-f6079040d332',
        level: 2,
        pse: 5,
        speed: 50,
        duration: 600,
        cedisId: 594,
        name: 'PEDALAR NA BICICLETA VERTICAL',
        instructions:
          'Ajuste o banco, mantendo uma ligeira flexão do joelho no ponto máximo, realize o movimento similar ao pedalar.'
      },
      {
        equipment: 'LEG PRESS',
        goal: 'Perda de peso',
        id: '76ea695c-8660-4e13-aa11-8c7cfb60b7c1',
        pse: 6,
        duration: 120,
        cedisId: 775,
        name: 'LEG PRESS',
        instructions:
          'Ajuste o banco até ao ponto máximo de flexão do joelho. As costas devem permancer totalmente apoiadas contra o banco e as mãos sobre os apoios. Realize a extensão quase completa dos joelhos de forma controlada.'
      },
      {
        equipment: 'CHEST PRESS',
        goal: 'Perda de peso',
        id: '127ec593-c346-4f45-98fb-ed08612042ad',
        pse: 6,
        duration: 120,
        cedisId: 770,
        name: 'CHEST PRESS',
        instructions:
          'Ajuste o banco de forma a manter os puxadores em linha com o peito. As costas devem permanecer apoiadas contra o banco e o olhar em frente. Realize a extensão e flexão de cotovelos de forma controlada.'
      },
      {
        equipment: 'LEG CURL',
        goal: 'Perda de peso',
        id: 'c028299a-2225-481a-819d-3c63e2d1f83d',
        pse: 6,
        duration: 120,
        cedisId: 774,
        name: 'LEG CURL',
        instructions:
          'Ajuste o banco mantendo os joelhos alinhados com o centro da roldana. Ajuste o rolo para manter a perna apoiada (acima da linha do tornozelo). Realize a extensão e flexão de joelhos de forma controlada.'
      },
      {
        equipment: 'LOW ROW',
        goal: 'Perda de peso',
        id: 'b5cffacb-cc78-4894-adc5-caa2817e9622',
        pse: 6,
        duration: 120,
        cedisId: 780,
        name: 'LOW ROW',
        instructions:
          'Ajuste o banco mantendo a almofada de apoio à altura do peito, permanecendo em contacto durante todo o movimento. Realize a extensão e flexão de cotovelo.'
      },
      {
        equipment: 'ABDOMINAL CRUNCH',
        goal: 'Perda de peso',
        id: '3d6e099a-c91a-493b-9c3e-7d41cc13f307',
        pse: 6,
        duration: 120,
        cedisId: 760,
        name: 'ABDOMINAL CRUNCH',
        instructions:
          'Apoie os suportes sobre os ombros e segure com as mãos. Realize a flexão de tronco à frente, empurrando o umbigo contra as costas e enrolando o tronco como se quisesse aproximar o seu peito da sua anca.'
      },
      {
        equipment: 'LOWER BACK',
        goal: 'Perda de peso',
        id: '0c149b0d-72ce-4ded-8679-2a2ca479e6a1',
        pse: 5,
        repetitions: 10,
        series: 1,
        duration: 120,
        cedisId: 773,
        name: 'LOWER BACK',
        instructions:
          'Ajuste o rolo mantendo apoiado contra as omplatas. Inicie o movimento na posição máxima de flexão do tronco e realize a extensão, empurrando para cima e para trás.'
      },
      {
        equipment: 'COLCHÃO',
        id: '21c40121-eeaa-4bff-8e89-dabf89bec278',
        pse: 3,
        series: 1,
        duration: 180,
        name: 'ALONGAMENTOS',
        instructions: 'Diriga-se à zona de alongamentos do seu clube'
      }
    ]
  }
};
export const f = null;

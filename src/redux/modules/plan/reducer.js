/* eslint-disable no-bitwise */
import moment from 'moment';
import { LOGOUT } from '../auth/reducers/login';
import { USER_OBJECTIVES } from './constants';
import { getDayOfWeek } from '../../../utils/dates';

export const FETCH_PLAN_REQUEST = 'smartplan/plan/FETCH_PLAN_REQUEST';
export const FETCH_PLAN_SUCCESS = 'smartplan/plan/FETCH_PLAN_SUCCESS';
export const FETCH_PLAN_FAILURE = 'smartplan/plan/FETCH_PLAN_FAILURE';

export const FETCH_FULL_PLAN_REQUEST = 'smartplan/plan/FETCH_FULL_PLAN_REQUEST';
export const FETCH_FULL_PLAN_SUCCESS = 'smartplan/plan/FETCH_FULL_PLAN_SUCCESS';
export const FETCH_FULL_PLAN_FAILURE = 'smartplan/plan/FETCH_FULL_PLAN_FAILURE';

export const FIRST_VISIT_PLAN = 'smartplan/plan/FIRST_VISIT_PLAN';

export const SELECT_PLAN = 'smartplan/plan/SELECT_PLAN';
export const SELECT_PLANOLD = 'smartplan/plan/SELECT_PLANOLD';
export const START_PLAN = 'smartplan/plan/START_PLAN';
export const COMPLETE_PLAN = 'smartplan/plan/COMPLETE_PLAN';

export const START_EXERCISE = 'smartplan/plan/START_EXERCISE';
export const CANCEL_EXERCISE = 'smartplan/plan/CANCEL_EXERCISE';
export const COMPLETE_EXERCISE = 'smartplan/plan/COMPLETE_EXERCISE';

export const START_REST = 'smartplan/plan/START_REST';
export const CANCEL_REST = 'smartplan/plan/CANCEL_REST';
export const COMPLETE_REST = 'smartplan/plan/COMPLETE_REST';

export const SETUP_PLAN_QUESTIONNAIRE = 'smartplan/plan/SETUP_PLAN_QUESTIONNAIRE';
export const SETUP_PLAN_EXERCISE_TYPE = 'smartplan/plan/SETUP_PLAN_EXERCISE_TYPE';
export const SETUP_PLAN_EXERCISE_PLAN = 'smartplan/plan/SETUP_PLAN_EXERCISE_PLAN';
export const SETUP_PLAN_RECOMMENDED_MODALITIES = 'smartplan/plan/SETUP_PLAN_RECOMMENDED_MODALITIES';
export const SETUP_PLAN_MODALITIES_PLAN = 'smartplan/plan/SETUP_PLAN_MODALITIES_PLAN';
export const SETUP_PLAN_FINISH = 'smartplan/plan/SETUP_PLAN_FINISH';

const initialState = {
  hasFetched: false,
  isFetchingPlan: false,
  isFetching: false,
  isExecutingExercise: false,
  isResting: false,
  hasVisited: false,
  error: null,
  currentSession: null,
  currentPlan: {
    name: '',
    exercises: [],
    modalities: [],
    date: null
  },
  planError: false,
  plans: [],
  data: {
    objective: '',
    biometric: {
      date: null,
      data: {}
    },
    plans: []
  },
  setup: {
    userType: null,
    userFrequency: null,
    userObjective: null,
    exerciseType: null,
    exercisePlan: null,
    modalitiesPlan: null,
    isFinished: false
  }
};

export default function planReducer(state = initialState, action) {
  switch (action.type) {
    case LOGOUT:
      return { ...initialState, data: state.data, currentPlan: state.currentPlan };

    case FETCH_FULL_PLAN_REQUEST:
      return { ...state, isFetching: true };
    case FETCH_FULL_PLAN_SUCCESS: {
      const isNewPlanValid = action.data.plans && action.data.plans.length > 0;
      return {
        ...state,
        isFetching: false,
        data: isNewPlanValid ? { ...action.data } : { ...state.data },
        currentPlan: isNewPlanValid ? getNextPlan(action.data.plans) : { ...state.currentPlan },
        hasFetched: true,
        error: null
      };
    }
    case FETCH_FULL_PLAN_FAILURE:
      return { ...state, isFetching: false, hasFetched: true, error: action.error };

    case FETCH_PLAN_REQUEST:
      return { ...state, isFetchingPlan: true };
    case FETCH_PLAN_SUCCESS:
      return {
        ...state,
        isFetchingPlan: false,
        plans: action.data.plans,
        planError: action.data.error,
        error: null
      };
    case FETCH_PLAN_FAILURE:
      return { ...state, isFetchingPlan: false, error: action.error };

    case FIRST_VISIT_PLAN: {
      return {
        ...state,
        hasVisited: true
      };
    }

    case SELECT_PLAN: {
      return {
        ...state,
        currentPlan: { ...state.data.plans.find(e => e.name === action.name), date: action.date }
      };
    }

    case SELECT_PLANOLD: {
      return {
        ...state,
        currentPlan: state.data.plans[action.planIndex]
      };
    }

    case START_PLAN: {
      return {
        ...state,
        currentSession: {
          id: generateId(),
          exercises: generateExerciseData(state.currentPlan.exercises)
        }
      };
    }

    case COMPLETE_PLAN: {
      return {
        ...state,
        currentSession: null
      };
    }

    case START_REST: {
      return {
        ...state,
        isResting: true
      };
    }

    case CANCEL_REST: {
      return {
        ...state,
        isResting: false
      };
    }

    case COMPLETE_REST: {
      return {
        ...state,
        isResting: false
      };
    }

    case START_EXERCISE: {
      return {
        ...state,
        isExecutingExercise: true,
        currentSession: {
          ...state.currentSession,
          currentExercise: action.exercise
        }
      };
    }

    case CANCEL_EXERCISE: {
      return {
        ...state,
        isExecutingExercise: false,
        currentSession: {
          ...state.currentSession,
          currentExercise: null
        }
      };
    }

    case COMPLETE_EXERCISE: {
      const { duration } = action;
      const { currentExercise } = state.currentSession;
      return {
        ...state,
        isExecutingExercise: false,
        currentSession: {
          ...state.currentSession,
          currentExercise: null,
          exercises: complete(currentExercise, duration, state.currentSession.exercises)
        }
      };
    }

    case SETUP_PLAN_QUESTIONNAIRE:
      return { ...state, setup: { ...state.setup, ...action.answers } };

    case SETUP_PLAN_EXERCISE_TYPE:
      return { ...state, setup: { ...state.setup, exerciseType: action.exerciseType } };

    case SETUP_PLAN_EXERCISE_PLAN:
      return { ...state, setup: { ...state.setup, exercisePlan: action.exercisePlan } };

    case SETUP_PLAN_RECOMMENDED_MODALITIES:
      return { ...state, setup: { ...state.setup, recommendedModalities: action.recommendedModalities } };

    case SETUP_PLAN_MODALITIES_PLAN:
      return { ...state, setup: { ...state.setup, modalitiesPlan: action.modalities } };

    case SETUP_PLAN_FINISH: {
      const plans = [
        {
          name: 'Plano de treino inicial',
          // Recommend once a week for 20 weeks
          dates: fillDates(20),
          exercises: state.setup.exercisePlan,
          modalities: state.setup.modalitiesPlan
        }
      ];

      return {
        ...state,
        setup: { ...state.setup, isFinished: true },
        data: {
          objective: USER_OBJECTIVES[state.setup.userObjective].title,
          plans
        },
        currentPlan: getNextPlan(plans)
      };
    }

    default:
      return state;
  }
}

function getNextPlan(planList) {
  const startDate = moment()
    .startOf('day')
    .toDate();

  const endDate = moment()
    .startOf('day')
    .add(1, 'week')
    .toDate();

  const plans = [];

  planList.forEach(p => {
    p.dates.forEach(date => {
      if (date >= startDate && date < endDate) {
        plans.push({
          name: p.name,
          day: getDayOfWeek(date.getDay()).substr(0, 3),
          date,
          exercises: p.exercises,
          modalities: p.modalities
        });
      }
    });
  });

  return plans.sort((a, b) => a.date - b.date)[0];
}

function fillDates(weeks) {
  const dates = [];
  const now = moment();

  for (let i = 0; i < weeks; i += 1) {
    dates.push(
      now
        .clone()
        .add('week', i)
        .toDate()
    );
  }

  return dates;
}

function complete(exercise, duration, exercisesList) {
  const newList = [...exercisesList];
  const matches = newList.filter(e => e.id === exercise.id);

  if (!matches || matches.length === 0) {
    return exercisesList;
  }

  const newExercise = newList[newList.indexOf(matches[0])];
  newExercise.duration = duration;
  newExercise.endDate = new Date();

  return newList;
}

function generateExerciseData(exerciseList) {
  return exerciseList
    .filter(e => e.name.toLowerCase() !== 'alongamentos')
    .map(e => {
      return { id: e.id, cedisId: e.cedisId, name: e.name, endDate: null, duration: 0 };
    });
}

function generateId() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
    const r = (Math.random() * 16) | 0;
    const v = c === 'x' ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
}

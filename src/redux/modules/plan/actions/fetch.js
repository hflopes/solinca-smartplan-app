import axios from 'axios';
import _ from 'lodash';
import { FETCH_PLAN_REQUEST, FETCH_PLAN_SUCCESS, FETCH_PLAN_FAILURE } from '../reducer';
import fetchFullPlan from './fetchFull';
import endpoints from '../../../../config/api';
import mapError from '../../../utils/errorMapping';

function fetchPlanRequest() {
  return {
    type: FETCH_PLAN_REQUEST
  };
}

function fetchPlanSuccess(data) {
  return {
    type: FETCH_PLAN_SUCCESS,
    data
  };
}

function fetchPlanFailure(error) {
  return {
    type: FETCH_PLAN_FAILURE,
    error
  };
}

export default function fetchPlan() {
  return dispatch => {
    dispatch(fetchPlanRequest());

    return axios
      .get(endpoints().fetchPlan)
      .then(({ data }) => {
        const { ok, code, plans } = data;

        if (ok) {
          dispatch(fetchPlanSuccess(mapResponse(plans)));
          dispatch(fetchFullPlan());
        } else {
          dispatch(fetchPlanFailure(mapError(code)));
        }
      })
      .catch(err => dispatch(fetchPlanFailure(err)));
  };
}

function mapResponse(plans) {
  let planObjective = '';
  let planError = false;

  plans.forEach(({ objective }) => {
    if (objective) {
      planObjective = objective;
    }
  });

  // TODO: remove repetions patch
  const response = {
    objective: planObjective,
    plans: _.compact(
      plans.map(p => {
        if (p.exercises.length === 1 && p.exercises[0].name === 'Dica do Treinador') {
          planError = true;
          return null;
        }
        return {
          name: formatPlanName(p.type),
          planId: p.planId,
          exercises: p.exercises.map(e => ({ ...e, repetitions: e.repetitions || e.repetions })),
          modalities: p.classes
        };
      })
    )
  };

  response.error = planError;
  return response;
}

function formatPlanName(name) {
  // if (name.length === 1) {
  //   return `Plano ${name}`;
  // }

  return name;
}

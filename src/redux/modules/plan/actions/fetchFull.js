import axios from 'axios';
import moment from 'moment';
import { FETCH_FULL_PLAN_REQUEST, FETCH_FULL_PLAN_SUCCESS, FETCH_FULL_PLAN_FAILURE } from '../reducer';
import endpoints from '../../../../config/api';
import mapError from '../../../utils/errorMapping';

function fetchFullPlanRequest() {
  return {
    type: FETCH_FULL_PLAN_REQUEST
  };
}

function fetchFullPlanSuccess(data) {
  return {
    type: FETCH_FULL_PLAN_SUCCESS,
    data
  };
}

function fetchFullPlanFailure(error) {
  return {
    type: FETCH_FULL_PLAN_FAILURE,
    error
  };
}

export default function fetchFullPlan() {
  return (dispatch, getState) => {
    dispatch(fetchFullPlanRequest());

    return axios
      .get(endpoints().fetchFullPlan)
      .then(({ data }) => {
        const { code, schedule } = data;

        if (schedule) {
          const {
            plan: { plans }
          } = getState();
          dispatch(fetchFullPlanSuccess(mapResponse(schedule, plans)));
        } else if (code) {
          dispatch(fetchFullPlanFailure(mapError(code)));
        } else {
          dispatch(fetchFullPlanSuccess({ plans: [] }));
        }
      })
      .catch(err => dispatch(fetchFullPlanFailure(err)));
  };
}

function formatBiometric(array) {
  const obj = {};

  array.forEach(e => {
    if (e.value && e.value.length > 0 && e.enabled !== 'False') {
      obj[e.title] = e.value;
    }
  });

  return obj;
}

// Over complicated logic because CEDIS had an error and was returning duplicated exercises
function formatExercise(exercise, exercises) {
  const mapped = {
    cedisId: exercise.dtre_exeid,
    name: exercise.exe_nome,
    ...exercises.find(e => e.id === exercise.id),
    ...exercise
  };

  return mapped;
}

function formatTraining(training, correctPlans) {
  const obj = {};
  const classes = [];
  const plans = [];

  training.forEach(e => {
    if (e.namePlan) {
      if (obj[e.namePlan]) {
        obj[e.namePlan].dates.push(new Date(e.date));
      } else {
        const correctPlan = correctPlans.find(plan => plan.name === e.namePlan);
        if (correctPlan && correctPlan.exercises) {
          obj[e.namePlan] = {
            name: e.namePlan,
            dates: [new Date(e.date)],
            modalities: e.plan[0].classes,
            exercises: correctPlan.exercises.map(exercise => formatExercise(exercise, e.plan[0].exercises))
          };
          plans.push(e.namePlan);
        }
      }
    } else if (e.modalityId && moment(e.date).isAfter()) {
      classes.push(e.modalityId);
    }
  });

  return { plans: Object.values(obj), goals: { plans, classes } };
}

function mapResponse(data, correctPlans) {
  const { evaluation, training } = data;
  const { plans, goals } = formatTraining(training, correctPlans);

  const response = {
    objective: evaluation.objective ? evaluation.objective.dor_objectivo : '',
    biometric: { data: formatBiometric(evaluation.bodyComposition), date: evaluation.dt_insert },
    goals,
    plans
  };

  return response;
}

import { SELECT_PLAN, SELECT_PLANOLD } from '../reducer';


export function selectPlanOld(planIndex) {
  return dispatch => {
    dispatch({
      type: SELECT_PLANOLD,
      planIndex
    });

  };
}
export default function selectPlan(name, date) {
  return dispatch => {
    dispatch({
      type: SELECT_PLAN,
      name,
      date
    });
  };
}

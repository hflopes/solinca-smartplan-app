import { FIRST_VISIT_PLAN } from '../reducer';

export default function visitPlan() {
  return dispatch => {
    dispatch({
      type: FIRST_VISIT_PLAN
    });
  };
}

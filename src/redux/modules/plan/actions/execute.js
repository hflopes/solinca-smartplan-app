import axios from 'axios';
import {
  START_PLAN,
  COMPLETE_PLAN,
  START_EXERCISE,
  CANCEL_EXERCISE,
  COMPLETE_EXERCISE,
  START_REST,
  CANCEL_REST,
  COMPLETE_REST
} from '../reducer';
import endpoints from '../../../../config/api';

export function startPlan() {
  return {
    type: START_PLAN
  };
}

export function completePlan() {
  return {
    type: COMPLETE_PLAN
  };
}

export function forceCompletePlan() {
  return (dispatch, getState) => {
    sendCompletePlan(getState());
    dispatch(completePlan());
  };
}

export function startExercise({ exercise }) {
  return (dispatch, getState) => {
    if (!getState().plan.currentSession) {
      dispatch(startPlan());
    }

    dispatch({
      type: START_EXERCISE,
      exercise
    });
  };
}

export function cancelExercise() {
  return dispatch => {
    dispatch({
      type: CANCEL_EXERCISE
    });
  };
}

export function completeExercise({ exercise, duration }) {
  return (dispatch, getState) => {
    if (duration <= 0) {
      dispatch({
        type: CANCEL_EXERCISE
      });
    } else {
      dispatch({
        type: COMPLETE_EXERCISE,
        duration
      });

      sendCompleteExercise(getState(), exercise, duration);
    }
  };
}

export function startRest() {
  return dispatch => {
    dispatch({
      type: START_REST
    });
  };
}

export function cancelRest() {
  return dispatch => {
    dispatch({
      type: CANCEL_REST
    });
  };
}

export function completeRest() {
  return dispatch => {
    dispatch({
      type: COMPLETE_REST
    });
  };
}

function sendCompleteExercise(state, exercise, duration) {
  const sessionId = state.plan.currentSession.id;
  const currentPlanId = state.plan.currentPlan.planId;

  const body = {
    exerciseId: exercise.cedisId,
    duration,
    planId: currentPlanId,
    sessionId,
    scheduleId: 0,
    trainingId: 0,
    datePlan: new Date()
  };

  // Save the completed exercise and duration in the DataHub
  axios.post(endpoints().dataCompleteExercise, { ...body });
}

function sendCompletePlan(state) {
  const sessionId = state.plan.currentSession.id;
  const currentPlanId = state.plan.currentPlan.planId;

  const body = {
    exerciseId: 0,
    duration: 0,
    planId: currentPlanId,
    sessionId,
    scheduleId: 0,
    trainingId: 0,
    datePlan: new Date()
  };

  // Save the completed plan in the DataHub
  axios.post(endpoints().dataCompleteExercise, { ...body });
}

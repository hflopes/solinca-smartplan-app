import _ from 'lodash';
import {
  SETUP_PLAN_QUESTIONNAIRE,
  SETUP_PLAN_EXERCISE_TYPE,
  SETUP_PLAN_RECOMMENDED_MODALITIES,
  SETUP_PLAN_EXERCISE_PLAN,
  SETUP_PLAN_MODALITIES_PLAN,
  SETUP_PLAN_FINISH
} from '../reducer';
import { EXERCISES } from '../exercises';
import { USER_TYPES, USER_OBJECTIVES } from '../constants';

export function setupQuestionnaire(answers) {
  return {
    type: SETUP_PLAN_QUESTIONNAIRE,
    answers
  };
}

export function setupExerciseType(exerciseType) {
  return {
    type: SETUP_PLAN_EXERCISE_TYPE,
    exerciseType
  };
}

export function setupRecommendedModalities() {
  return (dispatch, getState) => {
    const { plan, modalities } = getState();
    const { data } = modalities;
    const { userType, userObjective } = plan.setup;

    let recommendedModalities;

    if (userType === USER_TYPES.EXPERIENCED_USER.key) {
      // filter modalities by userObjective (smart plans)

      const tag = USER_OBJECTIVES[userObjective].chooseYourGoalKey;
      recommendedModalities = _.filter(data, c => c.chooseYourGoal.toUpperCase() === tag);
    } else if (userType === USER_TYPES.NEW_USER_1.key) {
      // Include only modalities with intensity 1 or 2
      recommendedModalities = _.filter(data, c => c.intensity < 3);
    } else if (userType === USER_TYPES.NEW_USER_2.key) {
      // Include only modalities with intensity 1, 2 or 3
      recommendedModalities = _.filter(data, c => c.intensity < 4);
    }

    dispatch({
      type: SETUP_PLAN_RECOMMENDED_MODALITIES,
      recommendedModalities
    });
  };
}

export function setupExercisePlan() {
  return (dispatch, getState) => {
    const { plan } = getState();
    const { exerciseType, userObjective } = plan.setup;
    const recommendedPlan = EXERCISES[userObjective][exerciseType];

    dispatch({
      type: SETUP_PLAN_EXERCISE_PLAN,
      exercisePlan: recommendedPlan
    });
  };
}

export function setupModalitiesPlan(selectedModalities) {
  return {
    type: SETUP_PLAN_MODALITIES_PLAN,
    modalities: selectedModalities
  };
}

export function setupFinish() {
  return {
    type: SETUP_PLAN_FINISH
  };
}

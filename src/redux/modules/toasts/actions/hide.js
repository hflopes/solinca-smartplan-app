import { HIDE_TOAST } from '../reducer';

export default function hideToast() {
  return { type: HIDE_TOAST };
}

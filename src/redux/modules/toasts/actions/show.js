import { SHOW_TOAST } from '../reducer';

function displayToast({ title, message, type, duration }) {
  return {
    type: SHOW_TOAST,
    toast: {
      title,
      message,
      duration,
      type
    }
  };
}

export default function showToast({ title, message, type, duration }) {
  return async (dispatch, getState) => {
    const { visible } = getState().toasts;

    if (!visible) {
      dispatch(displayToast({ title, message, type, duration }));
    } else {
      setTimeout(() => {
        dispatch(showToast({ title, message, type, duration }));
      }, 1000);
    }
  };
}

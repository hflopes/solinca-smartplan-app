export const SHOW_TOAST = 'smartplan/toast/SHOW';
export const HIDE_TOAST = 'smartplan/toast/HIDE';

const emptyToast = {
  title: '',
  message: '',
  duration: 0,
  type: ''
};

const initialState = {
  visible: false,
  toast: {
    ...emptyToast
  }
};

export default function toastReducer(state = initialState, action) {
  switch (action.type) {
    case SHOW_TOAST:
      return { ...state, visible: true, toast: action.toast };
    case HIDE_TOAST:
      return { ...state, visible: false, ...emptyToast };

    default:
      return state;
  }
}

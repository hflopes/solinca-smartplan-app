export const KEYBOARD_UP = 'smartplan/device/KEYBOARD_UP';
export const KEYBOARD_DOWN = 'smartplan/device/KEYBOARD_DOWN';
export const KEYBOARD_IGNORE = 'smartplan/device/KEYBOARD_IGNORE';

export const NAVIGATE_BACK = 'smartplan/device/NAVIGATE_BACK';

export const keyboardSources = {
  DEVICE_EVENT: 'DEVICE_EVENT',
  FOCUS_EVENT: 'FOCUS_EVENT'
};

const initialState = {
  isKeyboardUp: false
};

export default function deviceReducer(state = initialState, action) {
  switch (action.type) {
    case KEYBOARD_UP:
      return { ...state, isKeyboardUp: true };
    case KEYBOARD_DOWN:
      return { ...state, isKeyboardUp: false };

    default:
      return state;
  }
}

import { KEYBOARD_UP, KEYBOARD_DOWN, KEYBOARD_IGNORE, keyboardSources } from '../reducer';
import isDevelopmentMode from '../../../../utils/development';

function showKeyboard() {
  return {
    type: KEYBOARD_UP
  };
}

function hideKeyboard() {
  return {
    type: KEYBOARD_DOWN
  };
}

function ignoreKeyboard() {
  return {
    type: KEYBOARD_IGNORE
  };
}

export default function toggleKeyboard({ isKeyboardUp, source = keyboardSources.FOCUS_EVENT }) {
  return dispatch => {
    /*
    Production builds already support native keyboard events,
    the focus event is only to be used in development mode.
    */
    if (!isDevelopmentMode() && source === keyboardSources.FOCUS_EVENT) {
      dispatch(ignoreKeyboard());
      return;
    }

    if (isKeyboardUp) {
      dispatch(showKeyboard());
    } else {
      dispatch(hideKeyboard());
    }
  };
}

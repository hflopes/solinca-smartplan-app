import { LOGOUT } from '../auth/reducers/login';

export const FETCH_SEARCH_RESULTS_REQUEST = 'smartplan/search/FETCH_RESULTS_REQUEST';
export const FETCH_SEARCH_RESULTS_SUCCESS = 'smartplan/search/FETCH_RESULTS_SUCCESS';
export const FETCH_SEARCH_RESULTS_FAILURE = 'smartplan/search/FETCH_RESULTS_FAILURE';

export const CLEAR_RESULTS = 'smartplan/search/CLEAR_RESULTS';
export const MAX_GYMS_FILTERS = 'smartplan/search/MAX_GYMS_FILTERS';
export const MAX_MODALITIES_FILTERS = 'smartplan/search/MAX_MODALITIES_FILTERS';
export const NO_RESULTS = 'smartplan/search/NO_RESULTS';

const initialState = {
  isFetching: false,
  results: {},
  currentQuery: [],
  recentQueries: [] // array of arrays
};

export default function searchReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_SEARCH_RESULTS_REQUEST:
      return {
        ...state,
        isFetching: true,
        currentQuery: action.query,
        recentQueries: [...state.recentQueries, action.query]
      };

    case FETCH_SEARCH_RESULTS_SUCCESS:
      return { ...state, isFetching: false, results: { ...action.results }, error: null };

    case FETCH_SEARCH_RESULTS_FAILURE:
      return { ...state, isFetching: false, error: action.error };

    case CLEAR_RESULTS:
      return { ...state, results: {} };

    case LOGOUT:
      return { ...state, recentQueries: [] };

    default:
      return state;
  }
}

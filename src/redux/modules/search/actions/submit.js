import qs from 'qs';
import _ from 'lodash';
import axios from 'axios';
import moment from 'moment';
import {
  FETCH_SEARCH_RESULTS_REQUEST,
  FETCH_SEARCH_RESULTS_SUCCESS,
  FETCH_SEARCH_RESULTS_FAILURE,
  NO_RESULTS,
  MAX_GYMS_FILTERS,
  MAX_MODALITIES_FILTERS
} from '../reducer';
import endpoints from '../../../../config/api';
import mapError from '../../../utils/errorMapping';
import { mapStudioName } from '../../../../utils/strings';
import { getBaseGym } from '../../../selectors/gyms';

function fetchResultsRequest(query) {
  return {
    type: FETCH_SEARCH_RESULTS_REQUEST,
    query
  };
}

function fetchResultsSuccess(results) {
  return {
    type: FETCH_SEARCH_RESULTS_SUCCESS,
    results
  };
}

function fetchResultsFailure(error) {
  return {
    type: FETCH_SEARCH_RESULTS_FAILURE,
    error
  };
}

export function noResults() {
  return {
    type: NO_RESULTS
  };
}

export function maxGymsFilters() {
  return {
    type: MAX_GYMS_FILTERS
  };
}

export function maxModalitiesFilters() {
  return {
    type: MAX_MODALITIES_FILTERS
  };
}

export default function submitQuery(query) {
  return (dispatch, getState) => {
    dispatch(fetchResultsRequest(query));

    let gymIds = query.filter(q => q.type === 'gym').map(q => q.id);
    const modalityIds = query.filter(q => q.type === 'modality').map(q => q.id);

    // If there is no gym filter, use base gym
    if (!gymIds || gymIds.length === 0) {
      gymIds = [getBaseGym(getState()).id];
    }

    axios
      .get(endpoints().fetchSearch, {
        params: { gyms: gymIds },
        paramsSerializer: params => {
          return qs.stringify(params);
        }
      })
      .then(({ data }) => {
        const { ok, code, results } = data;

        if (ok) {
          dispatch(fetchResultsSuccess(mapResponse(results, modalityIds)));
        } else {
          dispatch(fetchResultsFailure(mapError(code)));
        }
      })
      .catch(err => dispatch(fetchResultsFailure(err)));
  };
}

function mapResponse(results, modalityIds) {
  const mapClasses = classes => {
    return classes
      .map(c => {
        // Transform the classes objects to some usable format
        return {
          ...c,
          studio: mapStudioName(c.studio),
          // I know "!!" below looks strange, but the first one converts
          // the variable to a boolean value, the second one inverts it
          isBooked: !!c.cancelId,
          date: moment(c.dateStart).toDate()
        };
      })
      .sort((a, b) => {
        // Sort the classes by their start date
        return a.date.getTime() - b.date.getTime();
      });
  };

  const mappedClassesObj = {};

  _.forEach(results, (value, key) => {
    if (modalityIds && modalityIds.length > 0) {
      mappedClassesObj[key] = mapClasses(value).filter(c => modalityIds.includes(c.modalityId));
    } else {
      mappedClassesObj[key] = mapClasses(value);
    }
  });

  return mappedClassesObj;
}

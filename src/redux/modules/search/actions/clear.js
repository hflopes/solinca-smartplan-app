import { CLEAR_RESULTS } from '../reducer';

export default function clearResults() {
  return {
    type: CLEAR_RESULTS
  };
}

import { SET_APP_VERSION } from '../reducer';

export default function setVersion(version) {
  return {
    type: SET_APP_VERSION,
    version
  };
}

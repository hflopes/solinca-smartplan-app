import { Plugins } from '@capacitor/core';
import { APP_OFFLINE, APP_ONLINE } from '../reducer';

const { Network } = Plugins;

export function online() {
  return {
    type: APP_ONLINE
  };
}

export function offline() {
  return {
    type: APP_OFFLINE
  };
}

export default function toggleConnected() {
  return async dispatch => {
    const { connected } = await Network.getStatus();
    if (connected) {
      dispatch(online());
    } else {
      dispatch(offline());
    }
  };
}

import { CLEAR_COMPONENT_STATES } from '../reducer';

export default function clearState() {
  return {
    type: CLEAR_COMPONENT_STATES
  };
}

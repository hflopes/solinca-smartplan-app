import { CHANGE_APP_LANGUAGE } from '../reducer';

export default function changeLanguage({ language }) {
  return {
    type: CHANGE_APP_LANGUAGE,
    language
  };
}

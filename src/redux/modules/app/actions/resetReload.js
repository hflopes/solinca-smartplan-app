import { RESET_RELOAD } from '../reducer';

export default function resetReload() {
  return {
    type: RESET_RELOAD
  };
}

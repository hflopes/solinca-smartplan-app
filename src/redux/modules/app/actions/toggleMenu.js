import { OPEN_NAVIGATION_MENU, CLOSE_NAVIGATION_MENU } from '../reducer';

function openMenu() {
  return {
    type: OPEN_NAVIGATION_MENU
  };
}

function closeMenu() {
  return {
    type: CLOSE_NAVIGATION_MENU
  };
}

export default function toggleMenu(open) {
  return dispatch => {
    if (open) {
      dispatch(openMenu());
    } else {
      dispatch(closeMenu());
    }
  };
}

import { APP_LOADING_ADD, APP_LOADING_REMOVE, APP_LOADING_REMOVE_ALL } from '../reducer';

export function addLoading(modules) {
  return {
    type: APP_LOADING_ADD,
    modules
  };
}

export function removeLoading(modules) {
  return {
    type: APP_LOADING_REMOVE,
    modules
  };
}

export function removeAllLoading() {
  return {
    type: APP_LOADING_REMOVE_ALL
  };
}

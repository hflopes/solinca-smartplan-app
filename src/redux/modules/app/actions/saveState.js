import { SAVE_COMPONENT_STATE } from '../reducer';

export default function saveState(componentState) {
  return {
    type: SAVE_COMPONENT_STATE,
    componentState
  };
}

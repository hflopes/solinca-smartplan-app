import axios from 'axios';
import {
  FETCH_INITIAL_DATA_REQUEST,
  FETCH_INITIAL_DATA_SUCCESS,
  FETCH_INITIAL_DATA_FAILURE
} from '../reducer';
import endpoints from '../../../../config/api';
import mapError from '../../../utils/errorMapping';

function initialDataRequest() {
  return {
    type: FETCH_INITIAL_DATA_REQUEST
  };
}

function initialDataSuccess(data) {
  return {
    type: FETCH_INITIAL_DATA_SUCCESS,
    data
  };
}

export function initialDataFailure(error) {
  return {
    type: FETCH_INITIAL_DATA_FAILURE,
    error
  };
}

export default function fetchInitialData() {
  return async dispatch => {
    dispatch(initialDataRequest());

    return axios
      .get(endpoints().ping)
      .then(({ data }) => {
        const { ok, code } = data;
        if (ok) {
          dispatch(initialDataSuccess());
        } else {
          dispatch(initialDataFailure(mapError(code)));
        }
      })
      .catch(err => dispatch(initialDataFailure(err)));
  };
}

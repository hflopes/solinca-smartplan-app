import { SET_API_URL, RESET_API_URL, TOGGLE_API_URL_CHECK } from '../reducer';
import { setURL, URLS } from '../../../../config/api';

function setAPIURL(url) {
  setURL(url);
  return {
    type: SET_API_URL,
    url
  };
}

function resetAPIURL(url) {
  setURL(url);
  return {
    type: RESET_API_URL,
    url
  };
}

function toggleAPIURLCheck() {
  return {
    type: TOGGLE_API_URL_CHECK
  };
}

export default function toggleAPIURL() {
  return (dispatch, getState) => {
    const currentURL = getState().app.apiURL;

    if (!currentURL) {
      dispatch(setAPIURL(URLS[0]));
    } else {
      const currentIndex = URLS.findIndex(url => url === currentURL);
      const newUrl = URLS[currentIndex + 1];

      if (newUrl) {
        dispatch(setAPIURL(newUrl));
      } else {
        dispatch(resetAPIURL(URLS[0]));
        setTimeout(() => {
          dispatch(toggleAPIURLCheck());
        }, 2000);
      }
    }
  };
}

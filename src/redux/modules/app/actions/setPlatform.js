import { SET_PLATFORM } from '../reducer';

export default function setPlatform(platform) {
  return {
    type: SET_PLATFORM,
    platform
  };
}

import { APP_START } from '../reducer';

export default function start() {
  return {
    type: APP_START
  };
}

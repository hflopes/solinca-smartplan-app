import { SET_BACK_NAVIGATION } from '../reducer';

export default function setBackNavigation(callback) {
  return {
    type: SET_BACK_NAVIGATION,
    callback
  };
}

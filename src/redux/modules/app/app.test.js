import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchInitialDataAction from './actions/fetchInitialData';
import appReducer, { FETCH_INITIAL_DATA_REQUEST, FETCH_INITIAL_DATA_SUCCESS } from './reducer';

import { app as AppConfiguration } from '../../../config/tests';

const { initialState } = AppConfiguration;

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('App Tests', () => {
  describe('Reducer', () => {
    it('should return the initial state', () => {
      expect(appReducer(undefined, {})).toEqual(initialState);
    });
  });

  describe('Actions', () => {
    it('should fetch initial data', async () => {
      const expectedActions = [{ type: FETCH_INITIAL_DATA_REQUEST }, { type: FETCH_INITIAL_DATA_SUCCESS }];
      const store = mockStore({ app: initialState });

      await store.dispatch(fetchInitialDataAction());
      store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
    });
  });
});

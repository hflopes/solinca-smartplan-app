import { LOGOUT } from '../auth/reducers/login';

export const CHANGE_APP_LANGUAGE = 'smartplan/app/CHANGE_LANGUAGE';

export const SET_PLATFORM = 'smartplan/app/SET_PLATFORM';

export const RESET_RELOAD = 'smartplan/app/RESET_RELOAD';
export const SET_APP_VERSION = 'smartplan/app/SET_APP_VERSION';

export const SET_API_URL = 'smartplan/app/SET_API_URL';
export const RESET_API_URL = 'smartplan/app/RESET_API_URL';
export const TOGGLE_API_URL_CHECK = 'smartplan/app/TOGGLE_API_URL_CHECK';

export const SET_BACK_NAVIGATION = 'smartplan/app/SET_BACK_NAVIGATION';

export const CLEAR_COMPONENT_STATES = 'smartplan/app/CLEAR_COMPONENT_STATES';
export const SAVE_COMPONENT_STATE = 'smartplan/app/SAVE_COMPONENT_STATE';

export const FETCH_INITIAL_DATA_REQUEST = 'smartplan/app/FETCH_INITIAL_DATA_REQUEST';
export const FETCH_INITIAL_DATA_SUCCESS = 'smartplan/app/FETCH_INITIAL_DATA_SUCCESS';
export const FETCH_INITIAL_DATA_FAILURE = 'smartplan/app/FETCH_INITIAL_DATA_FAILURE';

export const OPEN_NAVIGATION_MENU = 'smartplan/app/OPEN_NAVIGATION_MENU';
export const CLOSE_NAVIGATION_MENU = 'smartplan/app/CLOSE_NAVIGATION_MENU';

export const APP_OFFLINE = 'smartplan/app/OFFLINE';
export const APP_ONLINE = 'smartplan/app/ONLINE';
export const APP_START = 'smartplan/app/START';

export const APP_LOADING_ADD = 'smartplan/app/LOADING_ADD';
export const APP_LOADING_REMOVE = 'smartplan/app/LOADING_REMOVE';
export const APP_LOADING_REMOVE_ALL = 'smartplan/app/LOADING_REMOVE_ALL';

const initialState = {
  checkedAllUrls: false,
  apiURL: '',
  version: '',
  language: 'pt-PT',
  componentStateStack: {},
  fetchedInitialData: false,
  isFetchingInitialData: false,
  errorFetchingInitialData: false,
  error: null,
  offline: false,
  isMenuOpen: false,
  loadingModules: [],
  backNavigationCallback: null,
  platform: ''
};

export default function appReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_INITIAL_DATA_REQUEST:
      return { ...state, isFetchingInitialData: true };
    case FETCH_INITIAL_DATA_SUCCESS:
      return { ...state, isFetchingInitialData: false, fetchedInitialData: true };
    case FETCH_INITIAL_DATA_FAILURE:
      return { ...state, isFetchingInitialData: false, error: action.error, errorFetchingInitialData: true };

    case OPEN_NAVIGATION_MENU:
      return { ...state, isMenuOpen: true };
    case CLOSE_NAVIGATION_MENU:
      return { ...state, isMenuOpen: false };

    case SAVE_COMPONENT_STATE:
      return {
        ...state,
        componentStateStack: {
          ...state.componentStateStack,
          [action.componentState.component]: action.componentState
        }
      };

    case CLEAR_COMPONENT_STATES:
      return { ...state, componentStateStack: {} };

    case CHANGE_APP_LANGUAGE:
      return { ...state, language: action.language };

    case SET_PLATFORM:
      return { ...state, platform: action.platform };

    case TOGGLE_API_URL_CHECK:
      return { ...state, checkedAllUrls: false };
    case SET_API_URL:
      return { ...state, apiURL: action.url, checkedAllUrls: false };
    case RESET_API_URL:
      return { ...state, apiURL: action.url, checkedAllUrls: true };

    case SET_BACK_NAVIGATION:
      return { ...state, backNavigationCallback: action.callback };

    case APP_OFFLINE:
      return { ...state, offline: true };
    case APP_ONLINE:
      return { ...state, offline: false };

    case APP_LOADING_ADD:
      return { ...state, loadingModules: [...state.loadingModules, ...action.modules] };
    case APP_LOADING_REMOVE:
      return { ...state, loadingModules: state.loadingModules.filter(m => !action.modules.includes(m)) };
    case APP_LOADING_REMOVE_ALL:
      return { ...state, loadingModules: [] };

    case SET_APP_VERSION:
      return { ...state, version: action.version };

    case LOGOUT:
      return { ...state, componentStateStack: {} };

    default:
      return state;
  }
}

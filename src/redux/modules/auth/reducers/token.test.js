import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import verifiyTokenAction from '../actions/verifyToken';
import tokenReducer, { VERIFY_TOKEN_FAILURE } from './token';

import { token as TokenConfiguration } from '../../../../config/tests';
import mapError from '../../../utils/errorMapping';

const { initialState, errors } = TokenConfiguration;

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Verification Token Tests', () => {
  jest.setTimeout(30000);

  describe('Reducer', () => {
    it('should return the initial state', () => {
      expect(tokenReducer(undefined, {})).toEqual(initialState);
    });
  });

  describe('Actions', () => {
    // TODO TESTING test for success token request, validade data received like on login
    it('should give error with missing token', async () => {
      const expectedActions = [{ type: VERIFY_TOKEN_FAILURE }];
      const store = mockStore({ token: initialState });
      await store.dispatch(verifiyTokenAction());
      store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
      const [error] = store.getActions();
      expect(error.error).toEqual(mapError(errors.invalidToken));
    });
  });
});

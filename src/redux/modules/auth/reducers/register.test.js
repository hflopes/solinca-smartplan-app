import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import registerAction from '../actions/register';
import registerReducer, { REGISTER_FAILURE } from './register';

import { register as RegisterConfiguration } from '../../../../config/tests';

const { initialState, variables } = RegisterConfiguration;

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Register Tests', () => {
  jest.setTimeout(30000);

  describe('Reducer', () => {
    it('should return the initial state', () => {
      expect(registerReducer(undefined, {})).toEqual(initialState);
    });
  });

  describe('Actions', () => {
    // TODO TESTING test for success register, validade data received like on login
    it('should give error from different password and password confirmation', async () => {
      const expectedActions = [{ type: REGISTER_FAILURE }];
      const { email, password, passwordConfirmation } = variables.failure;
      const store = mockStore({ register: initialState });

      await store.dispatch(registerAction({ email, password, passwordConfirmation }));
      store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
    });
  });
});

import { LOGOUT } from './login';
import { VERIFY_TOKEN_SUCCESS } from './token';

export const REGISTER_REQUEST = 'smartplan/auth/REGISTER_REQUEST';
export const REGISTER_SUCCESS = 'smartplan/auth/REGISTER_SUCCESS';
export const REGISTER_FAILURE = 'smartplan/auth/REGISTER_FAILURE';
export const REGISTER_FAILURE_ERROR = 'smartplan/auth/REGISTER_FAILURE_ERROR';

const initialState = {
  isRegistering: false,
  hasRequested: false,
  isActive: false,
  isRegistered: false,
  error: null,
  email: '',
  password: ''
};

export default function registerReducer(state = initialState, action) {
  switch (action.type) {
    case VERIFY_TOKEN_SUCCESS: {
      return { ...state, hasRequested: false };
    }
    case LOGOUT:
      return { ...initialState };

    case REGISTER_REQUEST:
      return { ...state, isRegistering: true, email: action.email, password: action.password };
    case REGISTER_SUCCESS:
      return {
        ...state,
        isRegistering: false,
        hasRequested: true,
        isRegistered: action.isRegistered,
        isActive: action.isActive
      };
    case REGISTER_FAILURE_ERROR:
      return {
        ...state,
        isRegistered: action.isRegistered,
        isActive: action.isActive,
        isRegistering: false,
        hasRequested: true,
        error: action.error
      };
    default:
      return state;
  }
}

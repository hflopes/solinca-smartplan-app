import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import loginAction from '../actions/login';
import syncAction from '../actions/sync';
import loginReducer, {
  LOGOUT,
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  LOGIN_WAIT,
  LOGIN_WAITING,
  LOGIN_SYNC_REQUEST,
  LOGIN_SYNC_SUCCESS,
  LOGIN_SYNC_FAILURE
} from './login';
import { updateToken } from '../../../../config/api';
import sleep from '../../../utils/sleep';
import { login as LoginConfiguration } from '../../../../config/tests';

const { initialState, properties, variables } = LoginConfiguration;

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Login Tests', () => {
  jest.setTimeout(30000);

  describe('Reducer', () => {
    it('should return the initial state', () => {
      expect(loginReducer(undefined, {})).toEqual(initialState);
    });

    it('should reset to initial state after LOGOUT', () => {
      expect(
        loginReducer([], {
          type: LOGOUT
        })
      ).toEqual(initialState);
    });
  });

  describe('Actions', () => {
    it('should login the user correctly and receive the correct data', async () => {
      const expectedActions = [{ type: LOGIN_REQUEST }, { type: LOGIN_SUCCESS }];
      const { email, password } = variables.success;
      const store = mockStore({ login: initialState });

      await store.dispatch(loginAction({ email, password }));
      const [, success] = store.getActions();
      store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
      properties.request.map(property =>
        expect(Object.prototype.hasOwnProperty.call(success, property)).toBe(true)
      );
      const { user } = success;
      properties.user.map(property =>
        expect(Object.prototype.hasOwnProperty.call(user, property)).toBe(true)
      );
    });

    it('should give error on login', async () => {
      const expectedActions = [{ type: LOGIN_REQUEST }, { type: LOGIN_FAILURE }];
      const { email, password } = variables.failure;
      const store = mockStore({ login: initialState });

      await store.dispatch(loginAction({ email, password }));
      store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
    });

    it('should block multiple login requests', () => {
      const expectedAction = { type: LOGIN_WAIT };
      const { email, password } = variables.failure;
      const store = mockStore({ login: { initialState, tries: 5 } });

      expect(store.dispatch(loginAction({ email, password }))).toEqual(expectedAction);
    });

    it('should wait for reset if blocked for multiple attempts', () => {
      const expectedAction = { type: LOGIN_WAITING };
      const { email, password } = variables.failure;
      const store = mockStore({ login: { initialState, tries: 5, waitingReset: true } });

      expect(store.dispatch(loginAction({ email, password }))).toEqual(expectedAction);
    });

    it('should fail sync user data', async () => {
      const expectedActions = [{ type: LOGIN_SYNC_REQUEST }, { type: LOGIN_SYNC_FAILURE }];
      const store = mockStore({ login: initialState });
      await store.dispatch(syncAction());
      store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
    });

    it('should success sync user data', async () => {
      const expectedActions = [
        { type: LOGIN_REQUEST },
        { type: LOGIN_SUCCESS },
        { type: LOGIN_SYNC_REQUEST },
        { type: LOGIN_SYNC_SUCCESS }
      ];
      const { email, password } = variables.success;
      const store = mockStore({ login: initialState });
      await store.dispatch(loginAction({ email, password }));
      const [, success] = store.getActions();
      const { leaseToken } = success;
      updateToken(leaseToken);
      sleep(500);
      await store.dispatch(syncAction());
      store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
    });
  });
});

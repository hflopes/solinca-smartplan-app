import { ACCEPT_TERMS_SUCCESS } from '../../terms/reducer';

export const LOGIN_REQUEST = 'smartplan/auth/LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'smartplan/auth/LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'smartplan/auth/LOGIN_FAILURE';
export const LOGIN_FAILURE_ERROR = 'smartplan/auth/LOGIN_FAILURE_ERROR';

export const LOGIN_WAIT = 'smartplan/auth/LOGIN_WAIT';
export const LOGIN_WAITING = 'smartplan/auth/LOGIN_WAITING';
export const LOGIN_WAIT_RESET = 'smartplan/auth/LOGIN_WAIT_RESET';

export const LOGIN_INVALID_EMAIL = 'smartplan/auth/LOGIN_INVALID_EMAIL';
export const LOGIN_INVALID_PASSWORD = 'smartplan/auth/LOGIN_INVALID_PASSWORD';

export const LOGIN_SYNC_REQUEST = 'smartplan/auth/LOGIN_SYNC_REQUEST';
export const LOGIN_SYNC_SUCCESS = 'smartplan/auth/LOGIN_SYNC_SUCCESS';
export const LOGIN_SYNC_FAILURE = 'smartplan/auth/LOGIN_SYNC_FAILURE';

export const LOGOUT = 'smartplan/auth/LOGOUT';

export const ONBOARDING_CLOSE = 'smartplan/auth/ONBOARDING_CLOSE';

const initialState = {
  isLoggingIn: false,
  isSyncing: false,
  isLoggedIn: false,
  isCanceledOrSuspended: false,
  isNewUser: false,
  maxMinutesAllowedToCancelClass: 5,
  blockedWarningMessage: '',
  shouldReadTos: false,
  subscriptionType: '',
  restrictionType: '',
  hasSynced: false,
  leaseToken: null,
  user: null,
  error: null,
  tries: 0,
  waitingReset: false,
  lastLogin: null,
  showOnboarding: true
};

export default function loginReducer(state = initialState, action) {
  switch (action.type) {
    case LOGIN_WAIT:
      return { ...state, waitingReset: true };
    case LOGIN_WAIT_RESET:
      return { ...state, tries: 0, waitingReset: false };

    case LOGIN_REQUEST:
      return { ...state, isLoggingIn: true, tries: state.tries + 1 };
    case LOGIN_SUCCESS:
      return {
        ...state,
        isLoggingIn: false,
        user: action.user,
        leaseToken: action.leaseToken,
        isLoggedIn: action.isLoggedIn,
        isCanceledOrSuspended: action.isCanceledOrSuspended,
        shouldReadTos: action.shouldReadTos,
        isNewUser: action.isNewUser,
        maxMinutesAllowedToCancelClass: action.maxMinutesAllowedToCancelClass,
        blockedWarningMessage: action.blockedWarningMessage,
        subscriptionType: action.subscriptionType,
        restrictionType: action.restrictionType,
        lastLogin: new Date()
      };
    case LOGIN_FAILURE_ERROR:
      return { ...state, isLoggingIn: false, error: action.error };

    case LOGIN_SYNC_REQUEST:
      return { ...state, isSyncing: true, hasSynced: false };
    case LOGIN_SYNC_SUCCESS:
      return {
        ...state,
        isSyncing: false,
        hasSynced: true,
        isCanceledOrSuspended: action.isCanceledOrSuspended,
        shouldReadTos: action.shouldReadTos,
        lastLogin: new Date(),
        maxMinutesAllowedToCancelClass: action.maxMinutesAllowedToCancelClass,
        blockedWarningMessage: action.blockedWarningMessage
      };
    case LOGIN_SYNC_FAILURE:
      return { ...state, isSyncing: false, hasSynced: false, error: action.error };

    case LOGOUT:
      return { ...initialState };

    case ACCEPT_TERMS_SUCCESS:
      return { ...state, shouldReadTos: false };

    case ONBOARDING_CLOSE:
      return { ...state, showOnboarding: false };

    default:
      return state;
  }
}

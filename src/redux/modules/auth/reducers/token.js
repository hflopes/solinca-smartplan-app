import { LOGOUT } from './login';

export const VERIFY_TOKEN_REQUEST = 'smartplan/auth/VERIFY_TOKEN_REQUEST';
export const VERIFY_TOKEN_SUCCESS = 'smartplan/auth/VERIFY_TOKEN_SUCCESS';
export const VERIFY_TOKEN_FAILURE = 'smartplan/auth/VERIFY_TOKEN_FAILURE';
export const VERIFY_TOKEN_RESET = 'smartplan/auth/VERIFY_TOKEN_RESET';

const initialState = {
  isVerifyingToken: false,
  hasRequested: false,
  isTokenVerified: false,
  error: null
};

export default function tokenReducer(state = initialState, action) {
  switch (action.type) {
    case VERIFY_TOKEN_REQUEST:
      return { ...state, isVerifyingToken: true };
    case VERIFY_TOKEN_SUCCESS:
      return {
        ...state,
        isVerifyingToken: false,
        hasRequested: true,
        isTokenVerified: true
      };
    case VERIFY_TOKEN_FAILURE:
      return { ...state, isVerifyingToken: false, error: action.error };
    case VERIFY_TOKEN_RESET:
    case LOGOUT:
      return { ...initialState };

    default:
      return state;
  }
}

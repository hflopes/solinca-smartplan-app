import { LOGOUT } from './login';
import { VERIFY_TOKEN_SUCCESS } from './token';

export const RESET_PASSWORD_REQUEST = 'smartplan/auth/RESET_PASSWORD_REQUEST';
export const RESET_PASSWORD_SUCCESS = 'smartplan/auth/RESET_PASSWORD_SUCCESS';
export const RESET_PASSWORD_FAILURE = 'smartplan/auth/RESET_PASSWORD_FAILURE';
export const RESET_PASSWORD_FAILURE_ERROR = 'smartplan/auth/RESET_PASSWORD_FAILURE_ERROR';

const initialState = {
  isResetting: false,
  hasRequestedReset: false,
  isActive: false,
  isRegistered: false,
  error: null,
  email: '',
  password: ''
};

export default function resetPasswordReducer(state = initialState, action) {
  switch (action.type) {
    case VERIFY_TOKEN_SUCCESS: {
      return { ...state, hasRequestedReset: false };
    }
    case LOGOUT:
      return { ...initialState };

    case RESET_PASSWORD_REQUEST:
      return { ...state, isResetting: true, email: action.email, password: action.password };
    case RESET_PASSWORD_SUCCESS:
      return {
        ...state,
        isResetting: false,
        hasRequestedReset: true,
        isRegistered: action.isRegistered,
        isActive: action.isActive
      };
    case RESET_PASSWORD_FAILURE_ERROR:
      return { ...state, isResetting: false, error: action.error };

    default:
      return state;
  }
}

import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import resetAction from '../actions/resetPassword';
import resetReducer, { RESET_PASSWORD_FAILURE } from './resetPassword';

import { resetPassword as ResetConfiguration } from '../../../../config/tests';

const { initialState, variables } = ResetConfiguration;

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Reset Password Tests', () => {
  jest.setTimeout(30000);

  describe('Reducer', () => {
    it('should return the initial state', () => {
      expect(resetReducer(undefined, {})).toEqual(initialState);
    });
  });

  describe('Actions', () => {
    // TODO TESTING test for success register, validade data received like on login
    it('should give error from different password and password confirmation', async () => {
      const expectedActions = [{ type: RESET_PASSWORD_FAILURE }];
      const { email, password, passwordConfirmation } = variables.failure;
      const store = mockStore({ register: initialState });

      await store.dispatch(resetAction({ email, password, passwordConfirmation }));
      store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
    });
  });
});

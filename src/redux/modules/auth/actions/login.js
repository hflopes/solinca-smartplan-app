import axios from 'axios';
import {
  LOGIN_FAILURE,
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_WAIT,
  LOGIN_WAIT_RESET,
  LOGIN_WAITING,
  LOGIN_INVALID_EMAIL,
  LOGIN_INVALID_PASSWORD,
  ONBOARDING_CLOSE,
  LOGIN_FAILURE_ERROR
} from '../reducers/login';
import endpoints from '../../../../config/api';
import mapError from '../../../utils/errorMapping';
import validations from '../../../../config/validations';

const MAX_LOGIN_REQUESTS = validations.password.retries;

function loginRequest() {
  return {
    type: LOGIN_REQUEST
  };
}

export function loginWait() {
  return {
    type: LOGIN_WAIT
  };
}

function loginWaiting() {
  return {
    type: LOGIN_WAITING
  };
}

export function loginWaitReset() {
  return {
    type: LOGIN_WAIT_RESET
  };
}

function loginSuccess({
  user,
  leaseToken,
  isLoggedIn,
  isCanceledOrSuspended,
  subscriptionType,
  restrictionType,
  isNewUser,
  tosInfo,
  maxMinutesAllowedToCancelClass
}) {
  const sessionTerms = tosInfo && tosInfo.filter(t => t.type === 'termsconditions');
  const shouldReadTos = sessionTerms && sessionTerms.length > 0 && !sessionTerms[0].accepted;

  return {
    type: LOGIN_SUCCESS,
    user,
    leaseToken,
    isLoggedIn,
    isCanceledOrSuspended,
    isNewUser,
    maxMinutesAllowedToCancelClass,
    shouldReadTos,
    subscriptionType,
    restrictionType
  };
}

function loginFailure(error, isAuto) {
  return {
    type: LOGIN_FAILURE,
    error,
    isAuto
  };
}

export function loginFailureError(error, isAuto) {
  return {
    type: LOGIN_FAILURE_ERROR,
    error,
    isAuto
  };
}

export function loginInvalidEmail() {
  return {
    type: LOGIN_INVALID_EMAIL
  };
}

export function loginInvalidPassword() {
  return {
    type: LOGIN_INVALID_PASSWORD
  };
}

export function closeOnboarding() {
  return {
    type: ONBOARDING_CLOSE
  };
}

export default function login({ email = '', password = '', isAuto = false } = {}) {
  return (dispatch, getState) => {
    const { tries, waitingReset } = getState().login;
    if (tries >= MAX_LOGIN_REQUESTS) {
      if (!waitingReset) {
        return dispatch(loginWait());
      }
      return dispatch(loginWaiting());
    }
    dispatch(loginRequest());

    const requestBody = {
      email,
      password
    };

    return axios
      .post(endpoints().login, requestBody)
      .then(({ data }) => {
        const { ok, code } = data;

        if (ok) {
          dispatch(loginSuccess(mapResponse({ data })));
        } else {
          dispatch(loginFailure(mapError(code), isAuto));
        }
      })
      .catch(err => dispatch(loginFailure(err.message, isAuto)));
  };
}

function mapResponse({ data }) {
  const {
    user,
    leaseToken,
    ok,
    isCanceledOrSuspended,
    isNewUser,
    tosInfo,
    subscriptionType,
    restrictionType,
    maxMinutesAllowedToCancelClass,
    blockedWarningMessage
  } = data;

  return {
    user,
    leaseToken,
    isLoggedIn: ok,
    isCanceledOrSuspended,
    isNewUser,
    tosInfo,
    subscriptionType,
    restrictionType,
    maxMinutesAllowedToCancelClass,
    blockedWarningMessage
  };
}

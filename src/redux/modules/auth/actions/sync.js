import axios from 'axios';
import { LOGIN_SYNC_REQUEST, LOGIN_SYNC_SUCCESS, LOGIN_SYNC_FAILURE } from '../reducers/login';
import endpoints from '../../../../config/api';
import mapError from '../../../utils/errorMapping';

function syncRequest() {
  return {
    type: LOGIN_SYNC_REQUEST
  };
}

function syncSuccess({
  isCanceledOrSuspended,
  tosInfo,
  maxMinutesAllowedToCancelClass,
  blockedWarningMessage
}) {
  const sessionTerms = tosInfo && tosInfo.filter(t => t.type === 'termsconditions');
  const shouldReadTos = sessionTerms && sessionTerms.length > 0 && !sessionTerms[0].accepted;

  return {
    type: LOGIN_SYNC_SUCCESS,
    isCanceledOrSuspended,
    blockedWarningMessage,
    maxMinutesAllowedToCancelClass,
    shouldReadTos
  };
}

function syncFailure(error) {
  return {
    type: LOGIN_SYNC_FAILURE,
    error
  };
}

export default function sync() {
  return async dispatch => {
    dispatch(syncRequest());

    return axios
      .get(endpoints().sync)
      .then(({ data }) => {
        const { ok, code } = data;

        if (ok) {
          dispatch(syncSuccess(data));
        } else {
          dispatch(syncFailure(mapError(code)));
        }
      })
      .catch(err => dispatch(syncFailure(err)));
  };
}

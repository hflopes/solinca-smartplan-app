// import axios from 'axios';
import { LOGOUT } from '../reducers/login';
// import endpoints from '../../../../config/api';

function logoutRequest() {
  return {
    type: LOGOUT
  };
}

export default function logout() {
  return dispatch => {
    // axios.post(endpoints().logout);
    dispatch(logoutRequest());
  };
}

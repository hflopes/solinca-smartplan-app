import axios from 'axios';
import {
  RESET_PASSWORD_REQUEST,
  RESET_PASSWORD_SUCCESS,
  RESET_PASSWORD_FAILURE,
  RESET_PASSWORD_FAILURE_ERROR
} from '../reducers/resetPassword';
import endpoints from '../../../../config/api';
import mapError from '../../../utils/errorMapping';

function resetPasswordRequest({ email, password }) {
  return {
    type: RESET_PASSWORD_REQUEST,
    email,
    password
  };
}

function resetPasswordSuccess({ isRegistered, isActive }) {
  return {
    type: RESET_PASSWORD_SUCCESS,
    isRegistered,
    isActive
  };
}

function resetPasswordFailure(error) {
  return {
    type: RESET_PASSWORD_FAILURE,
    error
  };
}

export function resetPasswordFailureError(error) {
  return {
    type: RESET_PASSWORD_FAILURE_ERROR,
    error
  };
}

export default function resetPassword({ email = '', password = '', passwordConfirmation = '' } = {}) {
  if (password !== passwordConfirmation) {
    return dispatch => {
      dispatch(resetPasswordFailure(mapError(6000)));
    };
  }

  return dispatch => {
    dispatch(resetPasswordRequest({ email, password }));

    const requestBody = {
      email,
      password
    };

    return axios
      .post(endpoints().resetPassword, requestBody)
      .then(({ data }) => {
        const { ok, code, isRegistered } = data;

        if (ok) {
          if (!isRegistered) {
            dispatch(resetPasswordFailure(mapError(6006)));
          } else {
            dispatch(resetPasswordSuccess(data));
          }
        } else {
          dispatch(resetPasswordFailure(mapError(code)));
        }
      })
      .catch(err => dispatch(resetPasswordFailure(err)));
  };
}

import axios from 'axios';
import {
  REGISTER_REQUEST,
  REGISTER_SUCCESS,
  REGISTER_FAILURE,
  REGISTER_FAILURE_ERROR
} from '../reducers/register';
import endpoints from '../../../../config/api';
import mapError from '../../../utils/errorMapping';

function registerRequest({ email, password }) {
  return {
    type: REGISTER_REQUEST,
    email,
    password
  };
}

export function registerSuccess({ isRegistered, isActive }) {
  return {
    type: REGISTER_SUCCESS,
    isRegistered,
    isActive
  };
}

function registerFailure(error, { isRegistered, isActive }) {
  return {
    type: REGISTER_FAILURE,
    error,
    isRegistered,
    isActive
  };
}

export function registerFailureError(error, { isRegistered, isActive }) {
  return {
    type: REGISTER_FAILURE_ERROR,
    error,
    isRegistered,
    isActive
  };
}

export default function register({ email = '', password = '', passwordConfirmation = '' } = {}) {
  if (password !== passwordConfirmation) {
    return dispatch => {
      dispatch(registerFailure(mapError(6000)));
    };
  }

  return dispatch => {
    dispatch(registerRequest({ email, password }));

    const requestBody = {
      email,
      password
    };

    return axios
      .post(endpoints().register, requestBody)
      .then(({ data }) => {
        const { code, isActive, isRegistered } = data;

        // !isActive && isRegistered -> Seguir fluxo normal
        // isActive && isRegistered -> Erro duplicado
        // !isActive && !isRegistered -> Erro inexistente

        if (!isActive && isRegistered) {
          dispatch(registerSuccess(data));
        } else if (isActive && isRegistered) {
          dispatch(registerFailure(mapError(8003), data));
        } else if (!isActive && !isRegistered) {
          dispatch(registerFailure(mapError(8001), data));
        } else {
          dispatch(registerFailure(mapError(code), data));
        }
      })
      .catch(err => dispatch(registerFailure(err)));
  };
}

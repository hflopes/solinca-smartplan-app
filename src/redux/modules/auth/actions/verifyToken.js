import axios from 'axios';
import {
  VERIFY_TOKEN_REQUEST,
  VERIFY_TOKEN_SUCCESS,
  VERIFY_TOKEN_FAILURE,
  VERIFY_TOKEN_RESET
} from '../reducers/token';
import endpoints from '../../../../config/api';
import mapError from '../../../utils/errorMapping';

function verifyTokenRequest() {
  return {
    type: VERIFY_TOKEN_REQUEST
  };
}

function verifyTokenSuccess() {
  return {
    type: VERIFY_TOKEN_SUCCESS
  };
}

function verifyTokenReset() {
  return {
    type: VERIFY_TOKEN_RESET
  };
}

function verifyTokenFailure(error) {
  return {
    type: VERIFY_TOKEN_FAILURE,
    error
  };
}

export default function verifyToken({ token = '' } = {}) {
  return (dispatch, getState) => {
    if (!token) {
      return dispatch(verifyTokenFailure(mapError(8002)));
    }

    dispatch(verifyTokenRequest());

    const { register, resetPassword } = getState();
    const requestBody = { registerToken: token };

    if (register.hasRequested) {
      const { email, password } = register;
      requestBody.email = email;
      requestBody.password = password;
    } else if (resetPassword.hasRequestedReset) {
      const { email, password } = resetPassword;
      requestBody.email = email;
      requestBody.password = password;
    }

    return axios
      .post(endpoints().verifyToken, requestBody)
      .then(({ data }) => {
        const { ok, code } = data;

        if (ok) {
          dispatch(verifyTokenSuccess());
          setTimeout(() => {
            dispatch(verifyTokenReset());
          }, 3000);
        } else {
          dispatch(verifyTokenFailure(mapError(code)));
        }
      })
      .catch(err => dispatch(verifyTokenFailure(err)));
  };
}

import axios from 'axios';
import { FETCH_TERMS_REQUEST, FETCH_TERMS_FAILURE, FETCH_TERMS_SUCCESS } from '../reducer';
import endpoints from '../../../../config/api';
import mapError from '../../../utils/errorMapping';

function fetchTermsRequest() {
  return {
    type: FETCH_TERMS_REQUEST
  };
}

function fetchTermsSuccess(data) {
  return {
    type: FETCH_TERMS_SUCCESS,
    data
  };
}

function fetchTermsFailure(error) {
  return {
    type: FETCH_TERMS_FAILURE,
    error
  };
}

export default function fetchTerms() {
  return dispatch => {
    dispatch(fetchTermsRequest());

    return axios
      .get(endpoints().terms)
      .then(({ data }) => {
        const { ok, code, termsOfService } = data;

        if (ok) {
          dispatch(fetchTermsSuccess(termsOfService));
        } else {
          dispatch(fetchTermsFailure(mapError(code)));
        }
      })
      .catch(err => dispatch(fetchTermsFailure(err)));
  };
}

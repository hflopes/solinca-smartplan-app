import axios from 'axios';
import { ACCEPT_TERMS_REQUEST, ACCEPT_TERMS_SUCCESS, ACCEPT_TERMS_FAILURE } from '../reducer';
import endpoints from '../../../../config/api';
import mapError from '../../../utils/errorMapping';

function acceptTermsRequest() {
  return {
    type: ACCEPT_TERMS_REQUEST
  };
}

function acceptTermsSuccess() {
  return {
    type: ACCEPT_TERMS_SUCCESS
  };
}

function acceptTermsFailure(error) {
  return {
    type: ACCEPT_TERMS_FAILURE,
    error
  };
}

export default function acceptTerms(termId) {
  return dispatch => {
    dispatch(acceptTermsRequest());

    return axios
      .put(`${endpoints().acceptTerms}?id=${termId}`)
      .then(({ data }) => {
        const { ok, code } = data;

        if (ok) {
          dispatch(acceptTermsSuccess());
        } else {
          dispatch(acceptTermsFailure(mapError(code)));
        }
      })
      .catch(err => dispatch(acceptTermsFailure(err)));
  };
}

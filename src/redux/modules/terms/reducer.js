export const FETCH_TERMS_REQUEST = 'smartplan/terms/FETCH_REQUEST';
export const FETCH_TERMS_SUCCESS = 'smartplan/terms/FETCH_SUCCESS';
export const FETCH_TERMS_FAILURE = 'smartplan/terms/FETCH_FAILURE';

export const ACCEPT_TERMS_REQUEST = 'smartplan/terms/ACCEPT_REQUEST';
export const ACCEPT_TERMS_SUCCESS = 'smartplan/terms/ACCEPT_SUCCESS';
export const ACCEPT_TERMS_FAILURE = 'smartplan/terms/ACCEPT_FAILURE';

const initialState = {
  hasFetched: false,
  isFetching: false,
  isAccepting: false,
  hasAccepted: true,
  error: null,
  data: {}
};

export default function termsReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_TERMS_REQUEST:
      return { ...state, isFetching: true, hasAccepted: false };
    case FETCH_TERMS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        data: { ...action.data },
        error: null,
        hasFetched: true
      };
    case FETCH_TERMS_FAILURE:
      return { ...state, isFetching: false, error: action.error, hasFetched: true };

    case ACCEPT_TERMS_REQUEST:
      return { ...state, isAccepting: true };
    case ACCEPT_TERMS_SUCCESS:
      return {
        ...state,
        isAccepting: false,
        hasAccepted: true,
        error: null
      };
    case ACCEPT_TERMS_FAILURE:
      return { ...state, isAccepting: false, error: action.error };

    default:
      return state;
  }
}

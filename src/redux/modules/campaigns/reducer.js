export const FETCH_CAMPAIGN_REQUEST = 'smartplan/campaigns/FETCH_CAMPAIGN_REQUEST';
export const FETCH_CAMPAIGN_SUCCESS = 'smartplan/campaigns/FETCH_CAMPAIGN_SUCCESS';
export const FETCH_CAMPAIGN_FAILURE = 'smartplan/campaigns/FETCH_CAMPAIGN_FAILURE';

const initialState = {
  isFetching: false,
  data: {
    title: '',
    description: '',
    highlightText: '',
    fileUrl: ''
  }
};

export default function campaignsReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_CAMPAIGN_REQUEST:
      return { ...state, isFetching: true };

    case FETCH_CAMPAIGN_SUCCESS:
      return { ...state, isFetching: false, data: action.data };

    case FETCH_CAMPAIGN_FAILURE:
      return { ...state, isFetching: false, error: action.error };

    default:
      return state;
  }
}

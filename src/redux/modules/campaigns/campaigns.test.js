import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchAction from './actions/fetch';
import campaignReducer, { FETCH_CAMPAIGN_REQUEST, FETCH_CAMPAIGN_SUCCESS } from './reducer';

import { campaigns as CampaignsConfiguration } from '../../../config/tests';

const { initialState, properties } = CampaignsConfiguration;

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Campaigns Tests', () => {
  describe('Reducer', () => {
    it('should return the initial state', () => {
      expect(campaignReducer(undefined, {})).toEqual(initialState);
    });
  });

  describe('Actions', () => {
    it('should fetch campaigns data', async () => {
      const expectedActions = [{ type: FETCH_CAMPAIGN_REQUEST }, { type: FETCH_CAMPAIGN_SUCCESS }];
      const store = mockStore({ app: initialState });

      await store.dispatch(fetchAction());
      store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
      const [, success] = store.getActions();
      const { data } = success;
      properties.request.map(property =>
        expect(Object.prototype.hasOwnProperty.call(data, property)).toBe(true)
      );
    });
  });
});

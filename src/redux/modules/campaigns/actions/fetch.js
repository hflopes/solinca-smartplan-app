import axios from 'axios';
import { FETCH_CAMPAIGN_REQUEST, FETCH_CAMPAIGN_SUCCESS, FETCH_CAMPAIGN_FAILURE } from '../reducer';
import endpoints from '../../../../config/api';
import mapError from '../../../utils/errorMapping';

function fetchCampaignsRequest() {
  return {
    type: FETCH_CAMPAIGN_REQUEST
  };
}

function fetchCampaignsSuccess(data) {
  return {
    type: FETCH_CAMPAIGN_SUCCESS,
    data
  };
}

function fetchCampaignsFailure(error) {
  return {
    type: FETCH_CAMPAIGN_FAILURE,
    error
  };
}

export default function fetchCampaigns() {
  return dispatch => {
    dispatch(fetchCampaignsRequest());

    return axios
      .get(endpoints().fetchCampaign)
      .then(({ data }) => {
        const { ok, code, title, description, highlightText, fileUrl } = data;

        if (ok) {
          dispatch(fetchCampaignsSuccess({ title, description, highlightText, fileUrl }));
        } else {
          dispatch(fetchCampaignsFailure(mapError(code)));
        }
      })
      .catch(err => dispatch(fetchCampaignsFailure(err)));
  };
}

export const FETCH_CHALLENGES_REQUEST = 'smartplan/gamification/challenges/FETCH_REQUEST';
export const FETCH_CHALLENGES_SUCCESS = 'smartplan/gamification/challenges/FETCH_SUCCESS';
export const FETCH_CHALLENGES_FAILURE = 'smartplan/gamification/challenges/FETCH_FAILURE';

export const DETAILS_CHALLENGE_REQUEST = 'smartplan/gamification/challenges/DETAILS_REQUEST';
export const DETAILS_CHALLENGE_SUCCESS = 'smartplan/gamification/challenges/DETAILS_SUCCESS';
export const DETAILS_CHALLENGE_FAILURE = 'smartplan/gamification/challenges/DETAILS_FAILURE';

export const JOIN_CHALLENGE_REQUEST = 'smartplan/gamification/challenges/JOIN_REQUEST';
export const JOIN_CHALLENGE_SUCCESS = 'smartplan/gamification/challenges/JOIN_SUCCESS';
export const JOIN_CHALLENGE_FAILURE = 'smartplan/gamification/challenges/JOIN_FAILURE';

export const CANCEL_CHALLENGE_REQUEST = 'smartplan/gamification/challenges/CANCEL_REQUEST';
export const CANCEL_CHALLENGE_SUCCESS = 'smartplan/gamification/challenges/CANCEL_SUCCESS';
export const CANCEL_CHALLENGE_FAILURE = 'smartplan/gamification/challenges/CANCEL_FAILURE';

export const CHECK_COMPLETED_CHALLENGE = 'smartplan/gamification/challenges/CHECK_COMPLETED_CHALLENGE';
export const SHARE_CHALLENGE = 'smartplan/gamification/challenges/SHARE_CHALLENGE';

const initialState = {
  isFetching: false,
  isFetchingDetails: false,
  isCancelling: false,
  isJoining: false,
  error: null,
  data: {}
};

export default function challengesReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_CHALLENGES_REQUEST:
      return { ...state, isFetching: true };
    case FETCH_CHALLENGES_SUCCESS:
      return {
        ...state,
        isFetching: false,
        data: { ...state.data, ...action.data },
        error: null
      };
    case FETCH_CHALLENGES_FAILURE:
      return { ...state, isFetching: false, error: action.error };

    case DETAILS_CHALLENGE_REQUEST:
      return { ...state, isFetchingDetails: true };
    case DETAILS_CHALLENGE_SUCCESS:
      return {
        ...state,
        isFetchingDetails: false,
        data: { ...state.data, [action.details.id]: { ...action.details } },
        error: null
      };
    case DETAILS_CHALLENGE_FAILURE:
      return { ...state, isFetchingDetails: false, error: action.error };

    case JOIN_CHALLENGE_REQUEST:
      return { ...state, isJoining: true };
    case JOIN_CHALLENGE_SUCCESS: {
      return {
        ...state,
        isJoining: false,
        data: { ...state.data, [action.challengeId]: { ...state.data[action.challengeId], enrolled: true } }
      };
    }
    case JOIN_CHALLENGE_FAILURE:
      return { ...state, isJoining: false, error: action.error };

    case CANCEL_CHALLENGE_REQUEST:
      return { ...state, isCanceling: true };
    case CANCEL_CHALLENGE_SUCCESS: {
      return {
        ...state,
        isCanceling: false,
        data: { ...state.data, [action.challengeId]: { ...state.data[action.challengeId], enrolled: false } }
      };
    }
    case CANCEL_CHALLENGE_FAILURE:
      return { ...state, isCanceling: false, error: action.error };

    case CHECK_COMPLETED_CHALLENGE:
      return {
        ...state,
        data: { ...state.data, [action.id]: { ...state.data[action.id], completedSinceLastSession: false } }
      };

    default:
      return state;
  }
}

import qs from 'qs';
import axios from 'axios';
import { DETAILS_CHALLENGE_REQUEST, DETAILS_CHALLENGE_SUCCESS, DETAILS_CHALLENGE_FAILURE } from '../reducer';
import endpoints from '../../../../../config/api';
import mapError from '../../../../utils/errorMapping';

function fetchDetailsRequest() {
  return {
    type: DETAILS_CHALLENGE_REQUEST
  };
}

function fetchDetailsSuccess(challenge) {
  return {
    type: DETAILS_CHALLENGE_SUCCESS,
    details: challenge
  };
}

function fetchDetailsFailure(error) {
  return {
    type: DETAILS_CHALLENGE_FAILURE,
    error
  };
}

export default function fetchDetails(id) {
  return dispatch => {
    dispatch(fetchDetailsRequest());

    const request = {
      params: { Id: id },
      paramsSerializer: params => {
        return qs.stringify(params);
      }
    };

    return axios
      .get(endpoints().challenge, request)
      .then(({ data }) => {
        console.log(data);
        const { ok, code, challenge } = data;

        if (ok) {
          dispatch(fetchDetailsSuccess(challenge));
        } else {
          dispatch(fetchDetailsFailure(mapError(code)));
        }
      })
      .catch(err => dispatch(fetchDetailsFailure(err)));
  };
}

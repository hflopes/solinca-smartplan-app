import axios from 'axios';
import { CANCEL_CHALLENGE_REQUEST, CANCEL_CHALLENGE_SUCCESS, CANCEL_CHALLENGE_FAILURE } from '../reducer';
import endpoints from '../../../../../config/api';
import mapError from '../../../../utils/errorMapping';
import fetchBadges from '../../badges/actions/list';

function cancelChallengeRequest() {
  return {
    type: CANCEL_CHALLENGE_REQUEST
  };
}

function cancelChallengeSuccess(challengeId) {
  return {
    type: CANCEL_CHALLENGE_SUCCESS,
    challengeId
  };
}

function cancelChallengeFailure(error) {
  return {
    type: CANCEL_CHALLENGE_FAILURE,
    error
  };
}

export default function cancelChallenge(info) {
  return (dispatch, getState) => {
    dispatch(cancelChallengeRequest());

    const requestBody = {
      ...info,
      gymId: getState().gyms.selectedGymId
    };

    return axios
      .post(endpoints().leaveChallenge, requestBody)
      .then(({ data }) => {
        const { ok, code } = data;

        if (ok) {
          dispatch(cancelChallengeSuccess(info.ChallengeId));
          dispatch(fetchBadges());
        } else {
          dispatch(cancelChallengeFailure(mapError(code)));
        }
      })
      .catch(err => dispatch(cancelChallengeFailure(err)));
  };
}

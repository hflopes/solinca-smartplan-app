import axios from 'axios';
import { JOIN_CHALLENGE_REQUEST, JOIN_CHALLENGE_SUCCESS, JOIN_CHALLENGE_FAILURE } from '../reducer';
import endpoints from '../../../../../config/api';
import mapError from '../../../../utils/errorMapping';
import fetchBadges from '../../badges/actions/list';

function joinChallengeRequest() {
  return {
    type: JOIN_CHALLENGE_REQUEST
  };
}

function joinChallengeSuccess(challengeId) {
  return {
    type: JOIN_CHALLENGE_SUCCESS,
    challengeId
  };
}

function joinChallengeFailure(error) {
  return {
    type: JOIN_CHALLENGE_FAILURE,
    error
  };
}

export default function joinChallenge(info) {
  return (dispatch, getState) => {
    dispatch(joinChallengeRequest());

    const requestBody = {
      ...info,
      gymId: getState().gyms.selectedGymId
    };

    return axios
      .post(endpoints().joinChallenge, requestBody)
      .then(({ data }) => {
        const { ok, code } = data;

        if (ok) {
          dispatch(joinChallengeSuccess(info.ChallengeId));
          dispatch(fetchBadges());
        } else {
          dispatch(joinChallengeFailure(mapError(code)));
        }
      })
      .catch(err => dispatch(joinChallengeFailure(err)));
  };
}

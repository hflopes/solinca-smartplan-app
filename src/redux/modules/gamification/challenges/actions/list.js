// import _ from 'lodash';
import axios from 'axios';
import { FETCH_CHALLENGES_REQUEST, FETCH_CHALLENGES_SUCCESS, FETCH_CHALLENGES_FAILURE } from '../reducer';
import endpoints from '../../../../../config/api';
import mapError from '../../../../utils/errorMapping';
import fetchBadges from '../../badges/actions/list';
// import fetchDetails from './details';

function fetchChallengesRequest() {
  return {
    type: FETCH_CHALLENGES_REQUEST
  };
}

function fetchChallengesSuccess(challenges) {
  return {
    type: FETCH_CHALLENGES_SUCCESS,
    data: challenges
  };
}

function fetchChallengesFailure(error) {
  return {
    type: FETCH_CHALLENGES_FAILURE,
    error
  };
}

export default function fetchChallenges() {
  return (dispatch, getState) => {
    dispatch(fetchChallengesRequest());

    const query = {
      params: { gymId: getState().gyms.selectedGymId }
    };

    return axios
      .get(endpoints().challenges, query)
      .then(({ data }) => {
        const { ok, code, challenges } = data;

        if (ok) {
          dispatch(fetchChallengesSuccess(mapResponse(challenges)));
          // challenges.map(challenge => {
          //   dispatch(fetchDetails(challenge.id || challenge.dh__oid));
          // });
          dispatch(fetchBadges());
        } else {
          dispatch(fetchChallengesFailure(mapError(code)));
        }
      })
      .catch(err => dispatch(fetchChallengesFailure(err)));
  };
}

function mapResponse(challenges) {
  return challenges.reduce((res, challenge) => {
    const id = challenge.id || challenge.dh__oid;
    res[id] = {
      ...challenge,
      group: challenge.groupchallenge,
      groupchallenge: undefined,
      id
    };

    return res;
  }, {});
}

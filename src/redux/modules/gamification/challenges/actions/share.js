import { SHARE_CHALLENGE } from '../reducer';

export default function share(id, name, group) {
  return {
    type: SHARE_CHALLENGE,
    id,
    name,
    group
  };
}

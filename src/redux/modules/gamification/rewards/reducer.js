export const FETCH_REWARDS_REQUEST = 'smartplan/gamification/rewards/FETCH_REQUEST';
export const FETCH_REWARDS_SUCCESS = 'smartplan/gamification/rewards/FETCH_SUCCESS';
export const FETCH_REWARDS_FAILURE = 'smartplan/gamification/rewards/FETCH_FAILURE';

export const FETCH_PURCHASES_REQUEST = 'smartplan/gamification/rewards/FETCH_PURCHASES_REQUEST';
export const FETCH_PURCHASES_SUCCESS = 'smartplan/gamification/rewards/FETCH_PURCHASES_SUCCESS';
export const FETCH_PURCHASES_FAILURE = 'smartplan/gamification/rewards/FETCH_PURCHASES_FAILURE';

export const REDEEM_REWARD_REQUEST = 'smartplan/gamification/rewards/REDEEM_REWARD_REQUEST';
export const REDEEM_REWARD_SUCCESS = 'smartplan/gamification/rewards/REDEEM_REWARD_SUCCESS';
export const REDEEM_REWARD_FAILURE = 'smartplan/gamification/rewards/REDEEM_REWARD_FAILURE';

export const APPLY_ALL_FILTERS = 'smartplan/gamification/rewards/APPLY_ALL_FILTERS';

export const ADD_TO_CART = 'smartplan/gamification/rewards/ADD_TO_CART';
export const REMOVE_FROM_CART = 'smartplan/gamification/rewards/REMOVE_FROM_CART';

const initialState = {
  isFetching: false,
  isFetchingPurchases: false,
  error: null,
  data: {},
  purchases: [],
  filters: [],
  cart: []
};

export default function rewardsReducer(state = initialState, action) {
  switch (action.type) {
    case APPLY_ALL_FILTERS:
      return { ...state, filters: action.filters };

    case ADD_TO_CART:
      return { ...state, cart: [...state.cart, action.id] };
    case REMOVE_FROM_CART:
      return { ...state, cart: state.cart.filter(c => c != action.id) }; // eslint-disable-line

    case FETCH_PURCHASES_REQUEST:
      return { ...state, isFetchingPurchases: true };
    case FETCH_PURCHASES_SUCCESS:
      return {
        ...state,
        isFetchingPurchases: false,
        purchases: action.purchases,
        error: null
      };
    case FETCH_PURCHASES_FAILURE:
      return { ...state, isFetchingPurchases: false, error: action.error };

    case FETCH_REWARDS_REQUEST:
      return { ...state, isFetching: true };
    case FETCH_REWARDS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        data: action.data,
        error: null
      };
    case FETCH_REWARDS_FAILURE:
      return { ...state, isFetching: false, error: action.error };
    default:
      return state;
  }
}

import axios from 'axios';
import { REDEEM_REWARD_REQUEST, REDEEM_REWARD_SUCCESS, REDEEM_REWARD_FAILURE } from '../reducer';
import endpoints from '../../../../../config/api';
import mapError from '../../../../utils/errorMapping';

function redeemRequest() {
  return { type: REDEEM_REWARD_REQUEST };
}

function redeemSuccess(challengeId) {
  return { type: REDEEM_REWARD_SUCCESS, challengeId };
}

function redeemFailure(error) {
  return { type: REDEEM_REWARD_FAILURE, error };
}

export default function redeem(rewardId) {
  return (dispatch, getState) => {
    dispatch(redeemRequest());

    const requestBody = {
      rewardId
    };

    return axios
      .post(endpoints().redeemReward, requestBody)
      .then(({ data }) => {
        const { ok, code } = data;

        if (ok) {
          dispatch(redeemSuccess());
        } else {
          dispatch(redeemFailure(mapError(code)));
        }
      })
      .catch(err => dispatch(redeemFailure(err)));
  };
}

import axios from 'axios';
import { FETCH_REWARDS_REQUEST, FETCH_REWARDS_SUCCESS, FETCH_REWARDS_FAILURE } from '../reducer';
import endpoints from '../../../../../config/api';

function fetchRewardsRequest() {
  return {
    type: FETCH_REWARDS_REQUEST
  };
}

function fetchRewardsSuccess(rewards) {
  return {
    type: FETCH_REWARDS_SUCCESS,
    data: rewards
  };
}

function fetchRewardsFailure(error) {
  return {
    type: FETCH_REWARDS_FAILURE,
    error
  };
}

export default function fetchRewards() {
  return dispatch => {
    dispatch(fetchRewardsRequest());

    return axios
      .get(endpoints().rewards)
      .then(({ data }) => {
        dispatch(fetchRewardsSuccess(mapResponse(data)));
      })
      .catch(err => dispatch(fetchRewardsFailure(err)));
  };
}

function mapResponse(rewards) {
  return rewards.reduce((res, reward) => {
    const id = reward.id || reward.dh__oid;
    res[id] = {
      ...reward,
      id
    };

    return res;
  }, {});
}

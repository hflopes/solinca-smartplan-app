import axios from 'axios';
import { FETCH_PURCHASES_REQUEST, FETCH_PURCHASES_SUCCESS, FETCH_PURCHASES_FAILURE } from '../reducer';
import endpoints from '../../../../../config/api';

function fetchPurchasesRequest() {
  return { type: FETCH_PURCHASES_REQUEST };
}

function fetchPurchasesSuccess(rewards) {
  return { type: FETCH_PURCHASES_SUCCESS, purchases: rewards };
}

function fetchPurchasesFailure(error) {
  return { type: FETCH_PURCHASES_FAILURE, error };
}

export default function fetchPurchases() {
  return dispatch => {
    dispatch(fetchPurchasesRequest());

    return axios
      .get(endpoints().rewardPurchases)
      .then(({ data }) => {
        dispatch(fetchPurchasesSuccess(data));
      })
      .catch(err => dispatch(fetchPurchasesFailure(err)));
  };
}

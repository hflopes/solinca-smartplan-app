import { APPLY_ALL_FILTERS } from '../reducer';

export function applyFilters(filters) {
  return { type: APPLY_ALL_FILTERS, filters };
}

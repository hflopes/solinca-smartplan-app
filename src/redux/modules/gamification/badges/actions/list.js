import axios from 'axios';
import { FETCH_BADGES_REQUEST, FETCH_BADGES_SUCCESS, FETCH_BADGES_FAILURE } from '../reducer';
import endpoints from '../../../../../config/api';
import mapError from '../../../../utils/errorMapping';

function fetchBadgesRequest() {
  return {
    type: FETCH_BADGES_REQUEST
  };
}

function fetchBadgesSuccess(badges) {
  return {
    type: FETCH_BADGES_SUCCESS,
    data: badges
  };
}

function fetchBadgesFailure(error) {
  return {
    type: FETCH_BADGES_FAILURE,
    error
  };
}

export default function fetchBadges() {
  return dispatch => {
    dispatch(fetchBadgesRequest());

    return axios
      .get(endpoints().badges)
      .then(({ data }) => {
        const { ok, code, badges } = data;

        if (ok) {
          dispatch(fetchBadgesSuccess(mapResponse(badges)));
        } else {
          dispatch(fetchBadgesFailure(mapError(code)));
        }
      })
      .catch(err => dispatch(fetchBadgesFailure(err)));
  };
}

function mapResponse(badges) {
  return badges.reduce((res, badge) => {
    const challengeId = badge.challengeDh__oid;
    res[challengeId] = {
      ...badge,
      challengeId
    };

    return res;
  }, {});
}

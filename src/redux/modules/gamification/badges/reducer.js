export const FETCH_BADGES_REQUEST = 'smartplan/gamification/badges/FETCH_REQUEST';
export const FETCH_BADGES_SUCCESS = 'smartplan/gamification/badges/FETCH_SUCCESS';
export const FETCH_BADGES_FAILURE = 'smartplan/gamification/badges/FETCH_FAILURE';

const initialState = {
  isFetching: false,
  error: null,
  data: {}
};

export default function badgesReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_BADGES_REQUEST:
      return { ...state, isFetching: true };
    case FETCH_BADGES_SUCCESS:
      return {
        ...state,
        isFetching: false,
        data: { ...state.data, ...action.data },
        error: null
      };
    case FETCH_BADGES_FAILURE:
      return { ...state, isFetching: false, error: action.error };
    default:
      return state;
  }
}

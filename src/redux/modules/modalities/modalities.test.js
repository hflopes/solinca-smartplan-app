import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import _ from 'lodash';
import loginAction from '../auth/actions/login';
import fetchModalitiesAction from './actions/fetch';
import modalitiesReducer, { FETCH_MODALITIES_REQUEST, FETCH_MODALITIES_SUCCESS } from './reducer';
import { updateToken } from '../../../config/api';

import { modalities as ModalitiesConfiguration, login as LoginConfiguration } from '../../../config/tests';

const { initialState, properties } = ModalitiesConfiguration;
const { variables, initialState: loginInitialState } = LoginConfiguration;

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
const store = mockStore({ login: loginInitialState, classes: initialState, gyms: { selectedGymId: '89' } });

describe('Modalities Tests', () => {
  describe('Reducer', () => {
    it('should return initial state', () => {
      expect(modalitiesReducer(undefined, {})).toEqual(initialState);
    });
  });

  describe('Actions', () => {
    jest.setTimeout(30000);

    beforeAll(async () => {
      const { email, password } = variables.success;
      await store.dispatch(loginAction({ email, password }));
      const [, success] = store.getActions();
      const { leaseToken } = success;
      updateToken(leaseToken);
      return store.clearActions();
    });

    it('should fetch modalities data', async () => {
      const expectedActions = [{ type: FETCH_MODALITIES_REQUEST }, { type: FETCH_MODALITIES_SUCCESS }];

      await store.dispatch(fetchModalitiesAction());
      store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
      const [, success] = store.getActions();
      const { data } = success;
      _.map(data, modalityData => {
        return properties.map(property =>
          expect(Object.prototype.hasOwnProperty.call(modalityData, property)).toBe(true)
        );
      });
    });
  });
});

import axios from 'axios';
import { FETCH_MODALITIES_REQUEST, FETCH_MODALITIES_SUCCESS, FETCH_MODALITIES_FAILURE } from '../reducer';
import endpoints from '../../../../config/api';
import mapError from '../../../utils/errorMapping';

function fetchModalitiesRequest() {
  return {
    type: FETCH_MODALITIES_REQUEST
  };
}

function fetchModalitiesSuccess(modalities) {
  return {
    type: FETCH_MODALITIES_SUCCESS,
    data: modalities
  };
}

function fetchModalitiesFailure(error) {
  return {
    type: FETCH_MODALITIES_FAILURE,
    error
  };
}

export default function fetchModalities() {
  return dispatch => {
    dispatch(fetchModalitiesRequest());

    return axios
      .get(endpoints().fetchModalities)
      .then(({ data }) => {
        const { ok, code, modalities } = data;

        if (!modalities.length) {
          dispatch(fetchModalities());
        } else if (ok) {
          dispatch(fetchModalitiesSuccess(mapResponse({ modalities })));
        } else {
          dispatch(fetchModalitiesFailure(mapError(code)));
        }
      })
      .catch(err => dispatch(fetchModalitiesFailure(err)));
  };
}

function mapResponse({ modalities }) {
  const mappedModalities = {};

  modalities
    .map(c => {
      // Transform the modalities objects to some usable format
      return {
        ...c,
        // The maximum intensity is 5, if new levels get added
        // in the future, further development is required
        intensity: c.intensity ? Math.max(1, Math.min(5, c.intensity)) : 1,
        chooseYourGoal: defaultTo(c.chooseYourGoal, 'N/A'),
        category: defaultTo(c.category, 'N/A')
      };
    })
    .sort((a, b) => {
      // Sort the classes by their name (alphabetically)
      return a.name.localeCompare(b.name);
    })
    .forEach(m => {
      // Push the classes into an indexed object
      mappedModalities[m.id] = m;
    });

  return mappedModalities;
}

function defaultTo(original, defaultString) {
  if (!original || original === '') {
    return defaultString;
  }

  return original;
}

export const FETCH_MODALITIES_REQUEST = 'smartplan/modalities/FETCH_REQUEST';
export const FETCH_MODALITIES_SUCCESS = 'smartplan/modalities/FETCH_SUCCESS';
export const FETCH_MODALITIES_FAILURE = 'smartplan/modalities/FETCH_FAILURE';

const initialState = {
  isFetching: false,
  error: null,
  data: {}
};

export default function modalitiesReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_MODALITIES_REQUEST:
      return { ...state, isFetching: true };
    case FETCH_MODALITIES_SUCCESS:
      return {
        ...state,
        isFetching: false,
        data: { ...state.data, ...action.data },
        error: null
      };
    case FETCH_MODALITIES_FAILURE:
      return { ...state, isFetching: false, error: action.error };

    default:
      return state;
  }
}

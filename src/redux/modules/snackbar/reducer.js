export const SHOW_SNACKBAR = 'smartplan/snackbar/SHOW';
export const HIDE_SNACKBAR = 'smartplan/snackbar/HIDE';

const emptySnackbar = {
  message: '',
  type: ''
};

const initialState = {
  visible: false,
  snackbar: {
    ...emptySnackbar
  }
};

export default function snackbarReducer(state = initialState, action) {
  switch (action.type) {
    case SHOW_SNACKBAR:
      return { ...state, visible: true, snackbar: action.snackbar };
    case HIDE_SNACKBAR:
      return { ...state, visible: false, ...emptySnackbar };

    default:
      return state;
  }
}

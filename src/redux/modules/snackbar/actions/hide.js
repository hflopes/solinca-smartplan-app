import { HIDE_SNACKBAR } from '../reducer';

export default function hideSnackbar() {
  return { type: HIDE_SNACKBAR };
}

import { SHOW_SNACKBAR } from '../reducer';

export default function showSnackbar({ title, message, type }) {
  return {
    type: SHOW_SNACKBAR,
    snackbar: {
      message,
      type
    }
  };
}

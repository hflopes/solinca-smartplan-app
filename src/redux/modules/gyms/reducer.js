import { LOGOUT } from '../auth/reducers/login';
import { FETCH_USER_INFO_SUCCESS } from '../user/reducer';

export const FETCH_GYMS_REQUEST = 'smartplan/gyms/FETCH_REQUEST';
export const FETCH_GYMS_SUCCESS = 'smartplan/gyms/FETCH_SUCCESS';
export const FETCH_GYMS_FAILURE = 'smartplan/gyms/FETCH_FAILURE';

export const CHANGE_SELECTED_GYM = 'smartplan/gyms/CHANGE_SELECTED_GYM';

const initialState = {
  isFetching: false,
  error: null,
  selectedGymId: null,
  data: {}
};

export default function gymsReducer(state = initialState, action) {
  switch (action.type) {
    case LOGOUT:
      return { ...state, selectedGymId: null };

    case FETCH_USER_INFO_SUCCESS:
      return { ...state, selectedGymId: action.user.defaultGym };

    case FETCH_GYMS_REQUEST:
      return { ...state, isFetching: true };
    case FETCH_GYMS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        data: { ...state.data, ...action.data },
        error: null
      };
    case FETCH_GYMS_FAILURE:
      return { ...state, isFetching: false, error: action.error };

    case CHANGE_SELECTED_GYM:
      return { ...state, selectedGymId: action.selectedGymId };

    default:
      return state;
  }
}

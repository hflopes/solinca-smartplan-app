import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import _ from 'lodash';
import loginAction from '../auth/actions/login';
import fetchGymsAction from './actions/fetch';
import gymsReducer, { FETCH_GYMS_REQUEST, FETCH_GYMS_SUCCESS } from './reducer';
import { updateToken } from '../../../config/api';

import { gyms as GymsConfiguration, login as LoginConfiguration } from '../../../config/tests';

const { initialState, properties } = GymsConfiguration;
const { variables, initialState: loginInitialState } = LoginConfiguration;

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
const store = mockStore({ login: loginInitialState, classes: initialState, gyms: { selectedGymId: '89' } });

describe('Gyms Tests', () => {
  describe('Reducer', () => {
    it('should return initial state', () => {
      expect(gymsReducer(undefined, {})).toEqual(initialState);
    });
  });

  describe('Actions', () => {
    jest.setTimeout(30000);

    beforeAll(async () => {
      const { email, password } = variables.success;
      await store.dispatch(loginAction({ email, password }));
      const [, success] = store.getActions();
      const { leaseToken } = success;
      updateToken(leaseToken);
      return store.clearActions();
    });

    it('should fetch gyms data', async () => {
      const expectedActions = [{ type: FETCH_GYMS_REQUEST }, { type: FETCH_GYMS_SUCCESS }];

      await store.dispatch(fetchGymsAction());
      store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
      const [, success] = store.getActions();
      const { data } = success;
      _.map(data, gymData => {
        properties.request.map(property =>
          expect(Object.prototype.hasOwnProperty.call(gymData, property)).toBe(true)
        );

        return properties.coordinates.map(property =>
          expect(Object.prototype.hasOwnProperty.call(gymData.coordinates, property)).toBe(true)
        );
      });
    });
  });
});

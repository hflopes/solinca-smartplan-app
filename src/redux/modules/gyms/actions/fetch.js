import _ from 'lodash';
import axios from 'axios';
import { FETCH_GYMS_REQUEST, FETCH_GYMS_SUCCESS, FETCH_GYMS_FAILURE } from '../reducer';
import endpoints from '../../../../config/api';
import mapError from '../../../utils/errorMapping';

function fetchGymsRequest() {
  return {
    type: FETCH_GYMS_REQUEST
  };
}

function fetchGymsSuccess(gyms) {
  return {
    type: FETCH_GYMS_SUCCESS,
    data: gyms
  };
}

function fetchGymsFailure(error) {
  return {
    type: FETCH_GYMS_FAILURE,
    error
  };
}

export default function fetchGyms() {
  return dispatch => {
    dispatch(fetchGymsRequest());

    return axios
      .get(endpoints().fetchGyms)
      .then(({ data }) => {
        const { ok, code, gyms } = data;

        if (ok) {
          dispatch(fetchGymsSuccess(mapResponse({ gyms })));
        } else {
          dispatch(fetchGymsFailure(mapError(code)));
        }
      })
      .catch(err => dispatch(fetchGymsFailure(err)));
  };
}

function parseValues(obj) {
  return _.mapValues(obj, val => {
    return parseFloat(val);
  });
}

function mapResponse({ gyms }) {
  const mappedGyms = {};

  gyms
    .map(c => {
      // Transform the gyms objects to some usable format
      return { ...c, coordinates: parseValues(c.coordinates) || { latitude: 0, longitude: 0 } };
    })
    .sort((a, b) => {
      // Sort the classes by their name (alphabetically)
      return a.name.localeCompare(b.name);
    })
    .forEach(g => {
      const name = g.name
        .split(' ')
        .map(word => {
          if (word.length <= 2) return word.toLowerCase();
          return word[0].toUpperCase() + word.substring(1).toLowerCase();
        })
        .join(' ');
      // Push the classes into an indexed object
      mappedGyms[g.id] = {
        ...g,
        name
      };
    });

  return mappedGyms;
}

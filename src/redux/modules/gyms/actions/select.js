import { CHANGE_SELECTED_GYM } from '../reducer';

export default function changeSelectedGym({ gym }) {
  return {
    type: CHANGE_SELECTED_GYM,
    selectedGymId: Number(gym.id)
  };
}

import axios from 'axios';
import moment from 'moment';
import {
  FETCH_UPCOMING_CLASSES_REQUEST,
  FETCH_UPCOMING_CLASSES_SUCCESS,
  FETCH_UPCOMING_CLASSES_FAILURE
} from '../reducer';
import endpoints from '../../../../config/api';
import mapError from '../../../utils/errorMapping';
import { mapStudioName } from '../../../../utils/strings';

function fetchClassesRequest() {
  return {
    type: FETCH_UPCOMING_CLASSES_REQUEST
  };
}

function fetchClassesSuccess(classes) {
  return {
    type: FETCH_UPCOMING_CLASSES_SUCCESS,
    upcoming: classes
  };
}

function fetchClassesFailure(error) {
  return {
    type: FETCH_UPCOMING_CLASSES_FAILURE,
    error
  };
}

export function fetchClassesSilent() {
  return dispatch => {
    dispatch(fetchClasses(true));
  };
}

export default function fetchClasses(silent = false) {
  return (dispatch, getState) => {
    if (!silent) {
      dispatch(fetchClassesRequest());
    }

    const query = {
      params: { gymId: getState().gyms.selectedGymId }
    };

    return axios
      .get(endpoints().fetchUpcomingClasses, query)
      .then(({ data }) => {
        const { ok, code, classes } = data;

        if (ok) {
          const { maxMinutesAllowedToCancelClass } = getState().login;
          dispatch(fetchClassesSuccess(mapResponse({ classes, maxMinutesAllowedToCancelClass })));
        } else {
          dispatch(fetchClassesFailure(mapError(code)));
        }
      })
      .catch(err => dispatch(fetchClassesFailure(err)));
  };
}

function mapResponse({ classes, maxMinutesAllowedToCancelClass = 5 }) {
  const mappedClassesObj = {};

  classes
    .map(c => {
      // Transform the classes objects to some usable format
      return {
        ...c,
        studio: mapStudioName(c.studio),
        // I know "!!" below looks strange, but the first one converts
        // the variable to a boolean value, the second one inverts it
        isBooked: !!c.cancelId,
        date: moment(c.dateStart).toDate(),
        maxMinutesAllowedToCancelClass
      };
    })
    .sort((a, b) => {
      // Sort the classes by their start date
      return a.date.getTime() - b.date.getTime();
    })
    .forEach(c => {
      // Push the classes into an indexed object
      mappedClassesObj[c.id] = c;
    });

  return mappedClassesObj;
}

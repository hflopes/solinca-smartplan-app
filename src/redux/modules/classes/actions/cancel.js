import axios from 'axios';
import { CANCEL_CLASS_REQUEST, CANCEL_CLASS_SUCCESS, CANCEL_CLASS_FAILURE } from '../reducer';
import endpoints from '../../../../config/api';
import mapError from '../../../utils/errorMapping';

function cancelClassRequest() {
  return {
    type: CANCEL_CLASS_REQUEST
  };
}

function cancelClassSuccess(classId) {
  return {
    type: CANCEL_CLASS_SUCCESS,
    classId
  };
}

function cancelClassFailure(error) {
  return {
    type: CANCEL_CLASS_FAILURE,
    error
  };
}

export default function cancelClass(classData) {
  return dispatch => {
    dispatch(cancelClassRequest());

    const { id, cancelId, gymId } = classData;

    if (!cancelId) {
      return dispatch(cancelClassFailure(mapError(6002)));
    }
    const requestBody = {
      cancelId,
      gymId
    };

    return axios
      .post(endpoints().cancelClass, requestBody)
      .then(({ data }) => {
        const { ok, code } = data;

        if (ok) {
          dispatch(cancelClassSuccess(id));
        } else {
          dispatch(cancelClassFailure(mapError(code)));
        }
      })
      .catch(err => dispatch(cancelClassFailure(err)));
  };
}

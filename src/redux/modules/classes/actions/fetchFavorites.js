import axios from 'axios';
import moment from 'moment';
import {
  FETCH_CLASSES_FAVORITE_REQUEST,
  FETCH_CLASSES_FAVORITE_SUCCESS,
  FETCH_CLASSES_FAVORITE_FAILURE
} from '../reducer';
import endpoints from '../../../../config/api';
import mapError from '../../../utils/errorMapping';
import { mapStudioName } from '../../../../utils/strings';

function fetchFavoriteClassesRequest() {
  return {
    type: FETCH_CLASSES_FAVORITE_REQUEST
  };
}

function fetchFavoriteClassesSuccess(classes) {
  return {
    type: FETCH_CLASSES_FAVORITE_SUCCESS,
    favorites: classes
  };
}

function fetchFavoriteClassesFailure(error) {
  return {
    type: FETCH_CLASSES_FAVORITE_FAILURE,
    error
  };
}

export default function fetchFavoriteClasses() {
  return async dispatch => {
    dispatch(fetchFavoriteClassesRequest());

    return axios
      .get(endpoints().fetchFavoriteClasses)
      .then(({ data }) => {
        const { ok, code, classes } = data;

        if (ok) {
          dispatch(fetchFavoriteClassesSuccess(mapResponse({ classes })));
        } else {
          dispatch(fetchFavoriteClassesFailure(mapError(code)));
        }
      })
      .catch(err => dispatch(fetchFavoriteClassesFailure(err)));
  };
}

function mapResponse({ classes }) {
  const mappedClassesObj = {};

  classes
    .map(c => {
      // Transform the classes objects to some usable format
      return {
        ...c,
        studio: mapStudioName(c.studio),
        // I know "!!" below looks strange, but the first one converts
        // the variable to a boolean value, the second one inverts it
        isBooked: !!c.cancelId,
        date: moment(c.dateStart).toDate()
      };
    })
    .sort((a, b) => {
      // Sort the classes by their start date
      return a.date.getTime() - b.date.getTime();
    })
    .forEach(c => {
      // Push the classes into an indexed object
      mappedClassesObj[c.id] = c;
    });

  return mappedClassesObj;
}

import axios from 'axios';
import {
  FETCH_CLASSES_REVIEW_REQUEST,
  FETCH_CLASSES_REVIEW_SUCCESS,
  FETCH_CLASSES_REVIEW_FAILURE
} from '../reducer';
import endpoints from '../../../../config/api';
import mapError from '../../../utils/errorMapping';
import { mapStudioName } from '../../../../utils/strings';

function fetchReviewClassesRequest() {
  return {
    type: FETCH_CLASSES_REVIEW_REQUEST
  };
}

function fetchReviewClassesSuccess(classes) {
  return {
    type: FETCH_CLASSES_REVIEW_SUCCESS,
    classes
  };
}

function fetchReviewClassesFailure(error) {
  return {
    type: FETCH_CLASSES_REVIEW_FAILURE,
    error
  };
}

export default function fetchReviewClasses() {
  return async dispatch => {
    dispatch(fetchReviewClassesRequest());

    return axios
      .get(endpoints().fetchReviewClasses)
      .then(({ data }) => {
        const { ok, code, classes } = data;

        if (ok) {
          dispatch(fetchReviewClassesSuccess(mapResponse({ classes })));
        } else {
          dispatch(fetchReviewClassesFailure(mapError(code)));
        }
      })
      .catch(err => dispatch(fetchReviewClassesFailure(err)));
  };
}

function mapResponse({ classes }) {
  const mappedClassesObj = {};

  classes
    .map(c => {
      // Transform the classes objects to some usable format
      return {
        ...c,
        studio: mapStudioName(c.studio),
        // I know "!!" below looks strange, but the first one converts
        // the variable to a boolean value, the second one inverts it
        isBooked: !!c.cancelId,
        date: new Date(c.dateStart)
      };
    })
    .sort((a, b) => {
      // Sort the classes by their start date
      return a.date.getTime() - b.date.getTime();
    })
    .forEach(c => {
      // Push the classes into an indexed object
      mappedClassesObj[c.id] = c;
    });

  return mappedClassesObj;
}

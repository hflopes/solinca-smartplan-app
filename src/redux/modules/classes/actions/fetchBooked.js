import axios from 'axios';
import moment from 'moment';
import {
  FETCH_CLASSES_BOOKED_REQUEST,
  FETCH_CLASSES_BOOKED_FAILURE,
  FETCH_CLASSES_BOOKED_SUCCESS
} from '../reducer';
import endpoints from '../../../../config/api';
import mapError from '../../../utils/errorMapping';
import { mapStudioName } from '../../../../utils/strings';

function fetchBookedClassesRequest() {
  return {
    type: FETCH_CLASSES_BOOKED_REQUEST
  };
}

function fetchBookedClassesSuccess(classes) {
  return {
    type: FETCH_CLASSES_BOOKED_SUCCESS,
    booked: classes
  };
}

function fetchBookedClassesFailure(error) {
  return {
    type: FETCH_CLASSES_BOOKED_FAILURE,
    error
  };
}

export default function fetchBookedClasses() {
  return async dispatch => {
    dispatch(fetchBookedClassesRequest());
    return axios
      .get(endpoints().fetchBookedClasses)
      .then(({ data }) => {
        const { ok, code, bookedClasses } = data;

        if (ok) {
          dispatch(fetchBookedClassesSuccess(mapResponse({ bookedClasses })));
        } else {
          dispatch(fetchBookedClassesFailure(mapError(code)));
        }
      })
      .catch(err => dispatch(fetchBookedClassesFailure(err)));
  };
}

function mapResponse({ bookedClasses }) {
  const mappedClassesObj = {};

  bookedClasses
    .map(c => {
      // Transform the classes objects to some usable format
      return {
        ...c,
        studio: mapStudioName(c.studio),
        // I know "!!" below looks strange, but the first one converts
        // the variable to a boolean value, the second one inverts it
        isBooked: !!c.cancelId,
        date: moment(c.dateStart).toDate()
      };
    })
    .sort((a, b) => {
      // Sort the classes by their start date
      return a.date.getTime() - b.date.getTime();
    })
    .forEach(c => {
      // Push the classes into an indexed object
      mappedClassesObj[c.id] = c;
    });

  return mappedClassesObj;
}

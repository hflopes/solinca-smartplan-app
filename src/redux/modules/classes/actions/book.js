import axios from 'axios';
import moment from 'moment';
import { BOOK_CLASS_REQUEST, BOOK_CLASS_SUCCESS, BOOK_CLASS_FAILURE } from '../reducer';
import endpoints from '../../../../config/api';
import { REGULAR_BOOKING_GAP, PREMIUM_BOOKING_GAP } from '../../../../config/classes';
import mapError from '../../../utils/errorMapping';
import { isPremium } from '../../../selectors/user';

function bookClassRequest() {
  return {
    type: BOOK_CLASS_REQUEST
  };
}

export function bookClassSuccess(classData, minutes = 5) {
  return {
    type: BOOK_CLASS_SUCCESS,
    classData,
    minutes
  };
}

function bookClassFailure(error) {
  return {
    type: BOOK_CLASS_FAILURE,
    error
  };
}

export default function bookClass(classData) {
  return (dispatch, getState) => {
    const { bookingId, gymId, isBooked, isAvailable, date } = classData;

    if (isBooked) {
      return dispatch(bookClassFailure(mapError(6001)));
    }

    const timeLeft = moment(date).diff(moment(), 'minutes');
    if (
      !isPremium(getState()) &&
      timeLeft < PREMIUM_BOOKING_GAP * 60 &&
      timeLeft > REGULAR_BOOKING_GAP * 60
    ) {
      return dispatch(bookClassFailure(mapError(6003)));
    }

    if (!isAvailable) {
      return dispatch(bookClassFailure(mapError(6007)));
    }

    dispatch(bookClassRequest());

    const requestBody = {
      bookingId,
      gymId,
      classDate: `${date.toISOString().split('.')[0]}`
    };

    return axios
      .post(endpoints().bookClass, requestBody)
      .then(({ data }) => {
        const { ok, code, cancelId } = data;

        if (ok) {
          const newClass = { ...classData, cancelId, isBooked: true };
          const { maxMinutesAllowedToCancelClass } = getState().login;
          dispatch(bookClassSuccess(newClass, maxMinutesAllowedToCancelClass));
        } else {
          dispatch(bookClassFailure(mapError(code)));
        }
      })
      .catch(err => dispatch(bookClassFailure(err)));
  };
}

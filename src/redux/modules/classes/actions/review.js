import axios from 'axios';
import {
  REVIEW_CLASS_REQUEST,
  REVIEW_CLASS_SUCCESS,
  REVIEW_CLASS_FAILURE,
  REVIEW_CLASS_CLOSE,
  REVIEW_CLASS_VIEW
} from '../reducer';
import endpoints from '../../../../config/api';
import mapError from '../../../utils/errorMapping';

function reviewClassRequest() {
  return {
    type: REVIEW_CLASS_REQUEST
  };
}

function reviewClassSuccess(classId) {
  return {
    type: REVIEW_CLASS_SUCCESS,
    classId
  };
}

function reviewClassFailure(error) {
  return {
    type: REVIEW_CLASS_FAILURE,
    error
  };
}

export function closeReviewClass(classId) {
  return {
    type: REVIEW_CLASS_CLOSE,
    classId
  };
}

export function viewReviewClass(classId) {
  return {
    type: REVIEW_CLASS_VIEW,
    classId
  };
}

export default function reviewClass(classData, rating) {
  return async (dispatch, getState) => {
    dispatch(reviewClassRequest());

    const { id, fullName, gymId, date } = classData;
    const memberId = getState().user.data.memberNumber;
    const requestBody = {
      memberId,
      gymId,
      className: fullName,
      classDate: date,
      rating
    };

    return axios
      .post(endpoints().reviewClass, requestBody)
      .then(({ data }) => {
        const { ok, code } = data;

        if (ok) {
          dispatch(reviewClassSuccess(id));
        } else {
          dispatch(reviewClassFailure(mapError(code)));
        }
      })
      .catch(err => dispatch(reviewClassFailure(err)));
  };
}

import axios from 'axios';
import endpoints from '../../../../config/api';
import mapError from '../../../utils/errorMapping';
import {
  TOGGLE_CLASS_FAVORITE_REQUEST,
  TOGGLE_CLASS_FAVORITE_SUCCESS,
  TOGGLE_CLASS_FAVORITE_FAILURE
} from '../reducer';

function toggleFavoriteRequest() {
  return {
    type: TOGGLE_CLASS_FAVORITE_REQUEST
  };
}

function toggleFavoriteSuccess(classData) {
  return {
    type: TOGGLE_CLASS_FAVORITE_SUCCESS,
    classData
  };
}

function toggleFavoriteFailure(error) {
  return {
    type: TOGGLE_CLASS_FAVORITE_FAILURE,
    error
  };
}

export default function toggleFavorite(classData) {
  return dispatch => {
    dispatch(toggleFavoriteRequest());

    const { fullName, gymId } = classData;

    const requestBody = {
      classFullName: fullName,
      gymId
    };

    axios
      .post(endpoints().toggleClassFavorite, requestBody)
      .then(({ data }) => {
        const { ok, code } = data;

        if (ok) {
          dispatch(toggleFavoriteSuccess(classData));
        } else {
          dispatch(toggleFavoriteFailure(mapError(code)));
        }
      })
      .catch(err => dispatch(toggleFavoriteFailure(err)));
  };
}

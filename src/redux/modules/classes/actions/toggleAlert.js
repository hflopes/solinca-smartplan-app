import moment from 'moment';

import {
  TOGGLE_CLASS_ALERT_REQUEST,
  TOGGLE_CLASS_ALERT_SUCCESS,
  TOGGLE_CLASS_ALERT_FAILURE,
  ADD_CLASS_ALERT,
  REMOVE_CLASS_ALERT
} from '../reducer';
import { scheduleLocalNotification, cancelLocalNotification } from '../../../../utils/push/local';

function toggleAlertRequest() {
  return {
    type: TOGGLE_CLASS_ALERT_REQUEST
  };
}

function toggleAlertSuccess(classId) {
  return {
    type: TOGGLE_CLASS_ALERT_SUCCESS,
    classId
  };
}

function addAlert(classId, minutes) {
  return {
    type: ADD_CLASS_ALERT,
    classId,
    minutes
  };
}

function removeAlert(classId) {
  return {
    type: REMOVE_CLASS_ALERT,
    classId
  };
}

function toggleAlertFailure(error) {
  return {
    type: TOGGLE_CLASS_ALERT_FAILURE,
    error
  };
}

const minutesForNotification = date => {
  let minutesToNotify = 10;
  const minutesToClass = moment.duration(moment(date).diff(moment())).asMinutes();
  if (minutesToClass <= 10) {
    if (minutesToClass <= 5) {
      minutesToNotify = 1;
    } else {
      minutesToNotify = 5;
    }
  }

  return minutesToNotify;
};

export default function toggleAlert({ id, date, studio, modality, gym, fullName }) {
  return (dispatch, getState) => {
    dispatch(toggleAlertRequest());
    try {
      const { alert } = getState().classes;
      if (alert.indexOf(id) === -1) {
        const minutesToSubtract = minutesForNotification(date);
        const sendDate = moment(date)
          .subtract(minutesToSubtract, 'minutes')
          .toDate();
        scheduleLocalNotification({
          id,
          title: `${modality ? modality.name : fullName.split(' ')[0]} | ${moment(date).format('HH:mm')}`,
          body: `${gym.name}: ${studio}`,
          date: sendDate
        });
        dispatch(addAlert(id, minutesToSubtract));
      } else {
        cancelLocalNotification(id);
        dispatch(removeAlert(id));
      }
      return dispatch(toggleAlertSuccess(id));
    } catch (error) {
      return dispatch(toggleAlertFailure(error));
    }
  };
}

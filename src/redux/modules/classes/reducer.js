import _ from 'lodash';
import { editList } from '../../utils/lists';

import { LOGOUT } from '../auth/reducers/login';

export const FETCH_UPCOMING_CLASSES_REQUEST = 'smartplan/classes/FETCH_REQUEST';
export const FETCH_UPCOMING_CLASSES_SUCCESS = 'smartplan/classes/FETCH_SUCCESS';
export const FETCH_UPCOMING_CLASSES_FAILURE = 'smartplan/classes/FETCH_FAILURE';

export const FETCH_CLASSES_FAVORITE_REQUEST = 'smartplan/classes/FETCH_FAVORITES_REQUEST';
export const FETCH_CLASSES_FAVORITE_SUCCESS = 'smartplan/classes/FETCH_FAVORITES_SUCCESS';
export const FETCH_CLASSES_FAVORITE_FAILURE = 'smartplan/classes/FETCH_FAVORITES_FAILURE';

export const FETCH_CLASSES_BOOKED_REQUEST = 'smartplan/classes/FETCH_BOOKED_REQUEST';
export const FETCH_CLASSES_BOOKED_SUCCESS = 'smartplan/classes/FETCH_BOOKED_SUCCESS';
export const FETCH_CLASSES_BOOKED_FAILURE = 'smartplan/classes/FETCH_BOOKED_FAILURE';

export const FETCH_CLASSES_REVIEW_REQUEST = 'smartplan/classes/FETCH_REVIEW_REQUEST';
export const FETCH_CLASSES_REVIEW_SUCCESS = 'smartplan/classes/FETCH_REVIEW_SUCCESS';
export const FETCH_CLASSES_REVIEW_FAILURE = 'smartplan/classes/FETCH_REVIEW_FAILURE';

export const REVIEW_CLASS_REQUEST = 'smartplan/classes/REVIEW_REQUEST';
export const REVIEW_CLASS_SUCCESS = 'smartplan/classes/REVIEW_SUCCESS';
export const REVIEW_CLASS_FAILURE = 'smartplan/classes/REVIEW_FAILURE';
export const REVIEW_CLASS_CLOSE = 'smartplan/classes/REVIEW_CLOSE';
export const REVIEW_CLASS_VIEW = 'smartplan/classes/REVIEW_VIEW';

export const BOOK_CLASS_REQUEST = 'smartplan/classes/BOOK_REQUEST';
export const BOOK_CLASS_SUCCESS = 'smartplan/classes/BOOK_SUCCESS';
export const BOOK_CLASS_FAILURE = 'smartplan/classes/BOOK_FAILURE';

export const CANCEL_CLASS_REQUEST = 'smartplan/classes/CANCEL_REQUEST';
export const CANCEL_CLASS_SUCCESS = 'smartplan/classes/CANCEL_SUCCESS';
export const CANCEL_CLASS_FAILURE = 'smartplan/classes/CANCEL_FAILURE';

export const TOGGLE_CLASS_FAVORITE_REQUEST = 'smartplan/classes/TOGGLE_FAVORITE_REQUEST';
export const TOGGLE_CLASS_FAVORITE_SUCCESS = 'smartplan/classes/TOGGLE_FAVORITE_SUCCESS';
export const TOGGLE_CLASS_FAVORITE_FAILURE = 'smartplan/classes/TOGGLE_FAVORITE_FAILURE';

export const TOGGLE_CLASS_ALERT_REQUEST = 'smartplan/classes/TOGGLE_ALERT_REQUEST';
export const TOGGLE_CLASS_ALERT_SUCCESS = 'smartplan/classes/TOGGLE_ALERT_SUCCESS';
export const TOGGLE_CLASS_ALERT_FAILURE = 'smartplan/classes/TOGGLE_ALERT_FAILURE';
export const ADD_CLASS_ALERT = 'smartplan/classes/ADD_CLASS_ALERT';
export const REMOVE_CLASS_ALERT = 'smartplan/classes/REMOVE_CLASS_ALERT';

const initialState = {
  isFetchingUpcoming: false,
  isFetchingFavorites: false,
  isFetchingBooked: false,
  isFetchingReview: false,
  isReviewing: false,
  isBooking: false,
  isCanceling: false,
  isTogglingFavorite: false,
  isTogglingAlert: false,
  error: null,
  upcoming: {},
  favorites: {},
  booked: {},
  alert: [],
  favoriteClasses: [],
  reviewClasses: []
};

export default function classesReducer(state = initialState, action) {
  switch (action.type) {
    case LOGOUT:
      return { ...initialState };

    case FETCH_UPCOMING_CLASSES_REQUEST:
      return { ...state, isFetchingUpcoming: true };
    case FETCH_UPCOMING_CLASSES_SUCCESS:
      return {
        ...state,
        isFetchingUpcoming: false,
        upcoming: { ...action.upcoming },
        error: null
      };
    case FETCH_UPCOMING_CLASSES_FAILURE:
      return { ...state, isFetchingUpcoming: false, error: action.error, upcoming: {} };

    case FETCH_CLASSES_FAVORITE_REQUEST:
      return { ...state, isFetchingFavorites: true };
    case FETCH_CLASSES_FAVORITE_SUCCESS:
      return {
        ...state,
        isFetchingFavorites: false,
        favorites: { ...action.favorites },
        error: null
      };
    case FETCH_CLASSES_FAVORITE_FAILURE:
      return { ...state, isFetchingFavorites: false, error: action.error };

    case FETCH_CLASSES_REVIEW_REQUEST:
      return { ...state, isFetchingReview: true };
    case FETCH_CLASSES_REVIEW_SUCCESS:
      return {
        ...state,
        isFetchingReview: false,
        reviewClasses: action.classes,
        error: null
      };
    case FETCH_CLASSES_REVIEW_FAILURE:
      return { ...state, isFetchingReview: false, error: action.error };

    case REVIEW_CLASS_REQUEST:
      return { ...state, isReviewing: true };
    case REVIEW_CLASS_SUCCESS: {
      return {
        ...state,
        isReviewing: false,
        reviewClasses: _.omit(state.reviewClasses, [action.classId])
      };
    }
    case REVIEW_CLASS_CLOSE:
      return { ...state, reviewClasses: _.omit(state.reviewClasses, [action.classId]) };
    case REVIEW_CLASS_FAILURE:
      return { ...state, isReviewing: false, error: action.error };

    case FETCH_CLASSES_BOOKED_REQUEST:
      return { ...state, isFetchingBooked: true };
    case FETCH_CLASSES_BOOKED_SUCCESS:
      return {
        ...state,
        isFetchingBooked: false,
        booked: { ...action.booked },
        error: null
      };
    case FETCH_CLASSES_BOOKED_FAILURE:
      return { ...state, isFetchingBooked: false, error: action.error };

    case BOOK_CLASS_REQUEST:
      return { ...state, isBooking: true };
    case BOOK_CLASS_SUCCESS: {
      return {
        ...state,
        isBooking: false,
        booked: { ...state.booked, [action.classData.id]: action.classData },
        upcoming: editList(state.upcoming, action.classData.id, {
          isBooked: true,
          cancelId: action.classData.cancelId
        }),
        favorites: editList(state.favorites, action.classData.id, {
          isBooked: true,
          cancelId: action.classData.cancelId
        })
      };
    }
    case BOOK_CLASS_FAILURE:
      return { ...state, isBooking: false, error: action.error };

    case CANCEL_CLASS_REQUEST:
      return { ...state, isCanceling: true };
    case CANCEL_CLASS_SUCCESS:
      return {
        ...state,
        isCanceling: false,
        booked: _.omit(state.booked, [action.classId]),
        upcoming: editList(state.upcoming, action.classId, { isBooked: false, cancelId: null }),
        favorites: editList(state.favorites, action.classId, { isBooked: false, cancelId: null })
      };
    case CANCEL_CLASS_FAILURE:
      return { ...state, isCanceling: false, error: action.error };

    case TOGGLE_CLASS_FAVORITE_REQUEST:
      return { ...state, isTogglingFavorite: true };
    case TOGGLE_CLASS_FAVORITE_SUCCESS:
      return {
        ...state,
        isTogglingFavorite: false,
        favorites: action.classData.isFavorite
          ? _.omit(state.favorites, [action.classData.id])
          : { ...state.favorites, [action.classData.id]: { ...action.classData, isFavorite: true } },
        upcoming: editList(state.upcoming, action.classData.id, { isFavorite: !action.classData.isFavorite }),
        booked: editList(state.booked, action.classData.id, { isFavorite: !action.classData.isFavorite }),
        favoriteClasses:
          state.favoriteClasses.indexOf(action.classData.id) === -1
            ? [...state.favoriteClasses, action.classData.id]
            : state.favoriteClasses.filter(favorite => favorite !== action.classData.id)
      };
    case TOGGLE_CLASS_FAVORITE_FAILURE:
      return { ...state, isTogglingFavorite: false, error: action.error };

    case TOGGLE_CLASS_ALERT_REQUEST:
      return { ...state, isTogglingAlert: true };
    case TOGGLE_CLASS_ALERT_SUCCESS:
      return {
        ...state,
        isTogglingAlert: false,
        upcoming: editList(state.upcoming, action.classId, 'isAlert'),
        booked: editList(state.booked, action.classId, 'isAlert'),
        favorites: editList(state.favorites, action.classId, 'isAlert')
      };
    case ADD_CLASS_ALERT:
      return {
        ...state,
        alert: [...state.alert, action.classId]
      };
    case REMOVE_CLASS_ALERT:
      return {
        ...state,
        alert: state.alert.filter(alert => alert !== action.classId)
      };
    case TOGGLE_CLASS_ALERT_FAILURE:
      return { ...state, isTogglingAlert: false, error: action.error };

    default:
      return state;
  }
}

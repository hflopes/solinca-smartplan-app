import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import _ from 'lodash';
import loginAction from '../auth/actions/login';
// import bookAction from './actions/book';
// import cancelAction from './actions/cancel';
import fetchUpcomingAction from './actions/fetchUpcoming';
import fetchGymsAction from '../gyms/actions/fetch';
import fetchModalitiesAction from '../modalities/actions/fetch';
import fetchFavoritesAction from './actions/fetchFavorites';
import fetchBookedAction from './actions/fetchBooked';
import classesReducer, {
  FETCH_CLASSES_BOOKED_REQUEST,
  FETCH_CLASSES_BOOKED_SUCCESS,
  FETCH_UPCOMING_CLASSES_REQUEST,
  FETCH_UPCOMING_CLASSES_SUCCESS,
  FETCH_CLASSES_FAVORITE_REQUEST,
  FETCH_CLASSES_FAVORITE_SUCCESS
} from './reducer';
import { FETCH_GYMS_REQUEST, FETCH_GYMS_SUCCESS } from '../gyms/reducer';
import { FETCH_MODALITIES_REQUEST, FETCH_MODALITIES_SUCCESS } from '../modalities/reducer';
import { updateToken } from '../../../config/api';

import {
  classes as ClassesConfiguration,
  gyms as GymsConfiguration,
  modalities as ModalitiesConfiguration,
  login as LoginConfiguration
} from '../../../config/tests';

const { initialState, properties: classesProperties } = ClassesConfiguration;
const { initialState: gymsInitialState, properties: gymsProperties } = GymsConfiguration;
const { properties: modalitiesProperties } = ModalitiesConfiguration;
const { variables, initialState: loginInitialState } = LoginConfiguration;

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
const store = mockStore({
  login: loginInitialState,
  classes: initialState,
  gyms: { ...gymsInitialState, selectedGymId: 16 }
});

describe('Classes Tests', () => {
  describe('Reducer', () => {
    it('should return initial state', () => {
      expect(classesReducer(undefined, {})).toEqual(initialState);
    });
  });

  describe('Actions', () => {
    // TODO TESTING missing book and cancel action

    jest.setTimeout(30000);

    beforeAll(async () => {
      const { email, password } = variables.success;
      await store.dispatch(loginAction({ email, password }));
      const [, success] = store.getActions();
      const { leaseToken } = success;
      updateToken(leaseToken);
      return store.clearActions();
    });

    beforeEach(() => {
      store.clearActions();
    });

    it('should fetch gyms, modalities and upcoming classes', async () => {
      const expectedActions = [
        { type: FETCH_GYMS_REQUEST },
        { type: FETCH_GYMS_SUCCESS },
        { type: FETCH_MODALITIES_REQUEST },
        { type: FETCH_MODALITIES_SUCCESS },
        { type: FETCH_UPCOMING_CLASSES_REQUEST },
        { type: FETCH_UPCOMING_CLASSES_SUCCESS }
      ];

      await store.dispatch(fetchGymsAction());
      await store.dispatch(fetchModalitiesAction());
      await store.dispatch(fetchUpcomingAction());
      store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
      const [, gymsSuccess, , modalitiesSuccess, , classesSuccess] = store.getActions();
      const { data: gymsData } = gymsSuccess;
      _.map(gymsData, gymData => {
        gymsProperties.request.map(property =>
          expect(Object.prototype.hasOwnProperty.call(gymData, property)).toBe(true)
        );

        return gymsProperties.coordinates.map(property =>
          expect(Object.prototype.hasOwnProperty.call(gymData.coordinates, property)).toBe(true)
        );
      });
      const { data: modalitiesData } = modalitiesSuccess;
      _.map(modalitiesData, modalityData => {
        return modalitiesProperties.map(property =>
          expect(Object.prototype.hasOwnProperty.call(modalityData, property)).toBe(true)
        );
      });
      const { upcoming: classesData } = classesSuccess;
      _.map(classesData, classData => {
        return classesProperties.map(property =>
          expect(Object.prototype.hasOwnProperty.call(classData, property)).toBe(true)
        );
      });
    });

    it('should fetch booked classes', async () => {
      const expectedActions = [
        { type: FETCH_CLASSES_BOOKED_REQUEST },
        { type: FETCH_CLASSES_BOOKED_SUCCESS }
      ];
      await store.dispatch(fetchBookedAction());
      store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
      const [, success] = store.getActions();
      expect(Object.prototype.hasOwnProperty.call(success, 'booked')).toBe(true);
    });

    it('should fetch favorite classes', async () => {
      const expectedActions = [
        { type: FETCH_CLASSES_FAVORITE_REQUEST },
        { type: FETCH_CLASSES_FAVORITE_SUCCESS }
      ];
      await store.dispatch(fetchFavoritesAction());
      store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
      const [, success] = store.getActions();
      expect(Object.prototype.hasOwnProperty.call(success, 'favorites')).toBe(true);
    });
  });
});

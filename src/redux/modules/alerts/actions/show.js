import { SHOW_ALERT } from '../reducer';

export default function showAlert({ title, persist, message, icon, cta, redirectUrl, onClick, colorScheme }) {
  return {
    type: SHOW_ALERT,
    alert: {
      title,
      persist,
      message,
      icon,
      cta,
      redirectUrl,
      onClick,
      colorScheme
    }
  };
}

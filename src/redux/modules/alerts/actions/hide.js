import { HIDE_ALERT } from '../reducer';

export default function hideAlert() {
  return { type: HIDE_ALERT };
}

export const SHOW_ALERT = 'smartplan/alert/SHOW';
export const HIDE_ALERT = 'smartplan/alert/HIDE';

const emptyAlert = {
  title: '',
  persist: false,
  message: '',
  icon: '',
  cta: '',
  redirectUrl: null,
  colorScheme: null,
  onClick: null
};

const initialState = {
  visible: false,
  alert: {
    ...emptyAlert
  }
};

export default function alertReducer(state = initialState, action) {
  switch (action.type) {
    case SHOW_ALERT:
      return { ...state, visible: true, alert: action.alert };
    case HIDE_ALERT:
      return { ...state, visible: false, ...emptyAlert };

    default:
      return state;
  }
}

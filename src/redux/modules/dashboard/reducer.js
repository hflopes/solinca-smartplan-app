import { LOGOUT } from '../auth/reducers/login';

export const FETCH_CLASSES_HISTORY_REQUEST = 'smartplan/dashboard/FETCH_CLASSES_HISTORY_REQUEST';
export const FETCH_CLASSES_HISTORY_SUCCESS = 'smartplan/dashboard/FETCH_CLASSES_HISTORY_SUCCESS';
export const FETCH_CLASSES_HISTORY_FAILURE = 'smartplan/dashboard/FETCH_CLASSES_HISTORY_FAILURE';

export const FETCH_PLANS_HISTORY_REQUEST = 'smartplan/dashboard/FETCH_PLANS_HISTORY_REQUEST';
export const FETCH_PLANS_HISTORY_SUCCESS = 'smartplan/dashboard/FETCH_PLANS_HISTORY_SUCCESS';
export const FETCH_PLANS_HISTORY_FAILURE = 'smartplan/dashboard/FETCH_PLANS_HISTORY_FAILURE';

export const FETCH_EVALUATIONS_HISTORY_REQUEST = 'smartplan/dashboard/FETCH_EVALUATIONS_HISTORY_REQUEST';
export const FETCH_EVALUATIONS_HISTORY_SUCCESS = 'smartplan/dashboard/FETCH_EVALUATIONS_HISTORY_SUCCESS';
export const FETCH_EVALUATIONS_HISTORY_FAILURE = 'smartplan/dashboard/FETCH_EVALUATIONS_HISTORY_FAILURE';

export const SEEN_GRAPH_FAT = 'smartplan/dashboard/SEEN_GRAPH_FAT';
export const SEEN_GRAPH_IMC = 'smartplan/dashboard/SEEN_GRAPH_IMC';
export const SEEN_GRAPH_WEIGHT = 'smartplan/dashboard/SEEN_GRAPH_WEIGHT';

const initialState = {
  isFetchingClassesHistory: false,
  isFetchingPlansHistory: false,
  isFetchingEvaluationsHistory: false,
  error: null,
  classesHistory: [],
  plansHistory: [],
  evaluationsHistory: [],
  seenFatGraph: false,
  seenIMCGraph: false,
  seenWeightGraph: false
};

export default function classesReducer(state = initialState, action) {
  switch (action.type) {
    case LOGOUT:
      return { ...initialState };

    case FETCH_CLASSES_HISTORY_REQUEST:
      return { ...state, isFetchingClassesHistory: true };
    case FETCH_CLASSES_HISTORY_SUCCESS:
      return {
        ...state,
        isFetchingClassesHistory: false,
        classesHistory: action.history,
        error: null
      };
    case FETCH_CLASSES_HISTORY_FAILURE:
      return { ...state, isFetchingClassesHistory: false, error: action.error };

    case FETCH_PLANS_HISTORY_REQUEST:
      return { ...state, isFetchingPlansHistory: true };
    case FETCH_PLANS_HISTORY_SUCCESS:
      return {
        ...state,
        isFetchingPlansHistory: false,
        plansHistory: action.history,
        error: null
      };
    case FETCH_PLANS_HISTORY_FAILURE:
      return { ...state, isFetchingPlansHistory: false, error: action.error };

    case FETCH_EVALUATIONS_HISTORY_REQUEST:
      return { ...state, isFetchingEvaluationsHistory: true };
    case FETCH_EVALUATIONS_HISTORY_SUCCESS:
      return {
        ...state,
        isFetchingEvaluationsHistory: false,
        evaluationsHistory: action.history,
        error: null
      };
    case FETCH_EVALUATIONS_HISTORY_FAILURE:
      return { ...state, isFetchingEvaluationsHistory: false, error: action.error };

    case SEEN_GRAPH_FAT:
      return { ...state, seenFatGraph: true };
    case SEEN_GRAPH_IMC:
      return { ...state, seenIMCGraph: true };
    case SEEN_GRAPH_WEIGHT:
      return { ...state, seenWeightGraph: true };

    default:
      return state;
  }
}

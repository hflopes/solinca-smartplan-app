import axios from 'axios';
import {
  FETCH_PLANS_HISTORY_REQUEST,
  FETCH_PLANS_HISTORY_SUCCESS,
  FETCH_PLANS_HISTORY_FAILURE
} from '../reducer';
import endpoints from '../../../../config/api';
import mapError from '../../../utils/errorMapping';
import moment from 'moment';

function fetchHistoryRequest() {
  return {
    type: FETCH_PLANS_HISTORY_REQUEST
  };
}

function fetchHistorySuccess(history) {
  return {
    type: FETCH_PLANS_HISTORY_SUCCESS,
    history
  };
}

function fetchHistoryFailure(error) {
  return {
    type: FETCH_PLANS_HISTORY_FAILURE,
    error
  };
}

export default function fetchHistory() {
  return dispatch => {
    dispatch(fetchHistoryRequest());

    const now = moment();
    const fourMonthsAgo = moment().subtract(4, 'month');

    const query = {
      params: {
        dateStart: fourMonthsAgo.format('YYYY-MM-DD'),
        dateEnd: now.format('YYYY-MM-DD')
      }
    };

    return axios
      .get(endpoints().fetchPlansHistory, query)
      .then(({ data }) => {
        const { ok, code, training } = data;

        if (ok) {
          dispatch(fetchHistorySuccess(mapResponse(training)));
        } else {
          dispatch(fetchHistoryFailure(mapError(code)));
        }
      })
      .catch(err => dispatch(fetchHistoryFailure(err)));
  };
}

function mapResponse(response) {
  const completedPlans = response.filter(
    ({ dateComplete }) => dateComplete && !dateComplete.startsWith('0001')
  );

  return completedPlans.map(p => {
    const { plan, dateComplete } = p;
    return { name: plan[0].tre_plano, date: new Date(dateComplete) };
  });
}

import { SEEN_GRAPH_FAT, SEEN_GRAPH_IMC, SEEN_GRAPH_WEIGHT } from '../reducer';

export function seenFatGraph() {
  return {
    type: SEEN_GRAPH_FAT
  };
}

export function seenIMCGraph() {
  return {
    type: SEEN_GRAPH_IMC
  };
}

export function seenWeightGraph() {
  return {
    type: SEEN_GRAPH_WEIGHT
  };
}

import axios from 'axios';
import moment from 'moment';
import {
  FETCH_CLASSES_HISTORY_REQUEST,
  FETCH_CLASSES_HISTORY_SUCCESS,
  FETCH_CLASSES_HISTORY_FAILURE
} from '../reducer';
import endpoints from '../../../../config/api';
import mapError from '../../../utils/errorMapping';

function fetchHistoryRequest() {
  return {
    type: FETCH_CLASSES_HISTORY_REQUEST
  };
}

function fetchHistorySuccess(history) {
  return {
    type: FETCH_CLASSES_HISTORY_SUCCESS,
    history
  };
}

function fetchHistoryFailure(error) {
  return {
    type: FETCH_CLASSES_HISTORY_FAILURE,
    error
  };
}

export default function fetchHistory() {
  return dispatch => {
    dispatch(fetchHistoryRequest());

    const now = moment();
    const fourMonthsAgo = moment().subtract(4, 'month');

    const query = {
      params: {
        dateStart: fourMonthsAgo.format('YYYY-MM-DD'),
        dateEnd: now.format('YYYY-MM-DD'),
        checkin: 1
      }
    };

    return axios
      .get(endpoints().fetchClassesHistory, query)
      .then(({ data }) => {
        const { ok, code, historyClasses } = data;

        if (ok) {
          dispatch(fetchHistorySuccess(historyClasses));
        } else {
          dispatch(fetchHistoryFailure(mapError(code)));
        }
      })
      .catch(err => dispatch(fetchHistoryFailure(err)));
  };
}

import axios from 'axios';
import {
  FETCH_EVALUATIONS_HISTORY_REQUEST,
  FETCH_EVALUATIONS_HISTORY_SUCCESS,
  FETCH_EVALUATIONS_HISTORY_FAILURE
} from '../reducer';
import endpoints from '../../../../config/api';
import mapError from '../../../utils/errorMapping';
import moment from 'moment';

function fetchHistoryRequest() {
  return {
    type: FETCH_EVALUATIONS_HISTORY_REQUEST
  };
}

function fetchHistorySuccess(history) {
  return {
    type: FETCH_EVALUATIONS_HISTORY_SUCCESS,
    history
  };
}

function fetchHistoryFailure(error) {
  return {
    type: FETCH_EVALUATIONS_HISTORY_FAILURE,
    error
  };
}

export default function fetchHistory() {
  return dispatch => {
    dispatch(fetchHistoryRequest());

    const now = moment();
    const sixMonthsAgo = moment().subtract(6, 'month');

    const query = {
      params: {
        dateStart: sixMonthsAgo.format('YYYY-MM-DD'),
        dateEnd: now.format('YYYY-MM-DD')
      }
    };

    return axios
      .get(endpoints().fetchEvaluationsHistory, query)
      .then(({ data }) => {
        const { ok, code, evaluation } = data;

        if (ok) {
          dispatch(fetchHistorySuccess(mapResponse(evaluation)));
        } else {
          dispatch(fetchHistoryFailure(mapError(code)));
        }
      })
      .catch(err => dispatch(fetchHistoryFailure(err)));
  };
}

function mapResponse(evaluation) {
  return evaluation.map(({ dt_insert, bodyComposition }) => ({ date: dt_insert, bodyComposition }));
}

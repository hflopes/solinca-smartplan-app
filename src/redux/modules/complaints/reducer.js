import { LOGOUT } from '../auth/reducers/login';

export const SEND_COMPLAINT_REQUEST = 'smartplan/complaints/SEND_COMPLAINT_REQUEST';
export const SEND_COMPLAINT_SUCCESS = 'smartplan/complaints/SEND_COMPLAINT_SUCCESS';
export const SEND_COMPLAINT_FAILURE = 'smartplan/complaints/SEND_COMPLAINT_FAILURE';

export const FETCH_COMPLAINTS_REQUEST = 'smartplan/complaints/FETCH_COMPLAINTS_REQUEST';
export const FETCH_COMPLAINTS_SUCCESS = 'smartplan/complaints/FETCH_COMPLAINTS_SUCCESS';
export const FETCH_COMPLAINTS_FAILURE = 'smartplan/complaints/FETCH_COMPLAINTS_FAILURE';

export const READ_COMPLAINT_REQUEST = 'smartplan/complaints/READ_COMPLAINT_REQUEST';
export const READ_COMPLAINT_SUCCESS = 'smartplan/complaints/READ_COMPLAINT_SUCCESS';
export const READ_COMPLAINT_FAILURE = 'smartplan/complaints/READ_COMPLAINT_FAILURE';

const initialState = {
  isReading: false,
  isFetching: false,
  isSending: false,
  hasSent: false,
  data: {},
  error: null
};

export default function complaintsReducer(state = initialState, action) {
  switch (action.type) {
    case LOGOUT:
      return { ...initialState };

    case SEND_COMPLAINT_REQUEST:
      return { ...state, isSending: true };
    case SEND_COMPLAINT_SUCCESS:
      return {
        ...state,
        isSending: false,
        hasSent: true,
        form: action.formInfo
      };
    case SEND_COMPLAINT_FAILURE:
      return { ...state, isSending: false, error: action.error };

    case FETCH_COMPLAINTS_REQUEST:
      return { ...state, isFetching: true };
    case FETCH_COMPLAINTS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        data: action.data.reduce((acc, complaint) => {
          const { id, replies } = complaint;
          const hasComplaint = state.data[id];
          const isUnread = Boolean(
            (state.data.length && !hasComplaint) ||
              (hasComplaint && hasComplaint.replies.length < replies.length)
          );
          const isReplied = Boolean(replies.length);
          return {
            ...acc,
            [id]: { ...complaint, id, isUnread, isReplied }
          };
        }, state.data)
      };
    case FETCH_COMPLAINTS_FAILURE:
      return { ...state, isFetching: false, error: action.error };

    case READ_COMPLAINT_REQUEST:
      return { ...state, isReading: true };
    case READ_COMPLAINT_SUCCESS:
      return {
        ...state,
        isReading: false,
        data: { ...state.data, [action.complaintId]: { ...state.data[action.complaintId], isUnread: false } }
      };
    case READ_COMPLAINT_FAILURE:
      return { ...state, isReading: false, error: action.error };

    default:
      return state;
  }
}

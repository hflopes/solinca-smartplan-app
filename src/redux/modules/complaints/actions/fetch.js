import axios from 'axios';
import { FETCH_COMPLAINTS_REQUEST, FETCH_COMPLAINTS_FAILURE, FETCH_COMPLAINTS_SUCCESS } from '../reducer';
import endpoints from '../../../../config/api';
import mapError from '../../../utils/errorMapping';

function fetchComplaintsRequest() {
  return {
    type: FETCH_COMPLAINTS_REQUEST
  };
}

function fetchComplaintsSuccess(data) {
  return {
    type: FETCH_COMPLAINTS_SUCCESS,
    data
  };
}

function fetchComplaintsFailure(error) {
  return {
    type: FETCH_COMPLAINTS_FAILURE,
    error
  };
}

export default function fetchComplaints() {
  return dispatch => {
    dispatch(fetchComplaintsRequest());

    return axios
      .get(endpoints().complaints)
      .then(({ data }) => {
        const { ok, code, complaints } = data;

        if (ok) {
          dispatch(fetchComplaintsSuccess(mapResponse(complaints)));
        } else {
          dispatch(fetchComplaintsFailure(mapError(code)));
        }
      })
      .catch(err => dispatch(fetchComplaintsFailure(err)));
  };
}

function mapResponse(complaints) {
  // return complaints.reduce((acc, complaint) => {
  //   const id = `complaint_${complaint.id}`;
  //   return {
  //     ...acc,
  //     [id]: { ...complaint, id }
  //   };
  // }, {});
  return complaints.map(complaint => {
    const id = `complaint_${complaint.id}`;
    const replies = complaint.replies.map(reply => ({
      ...reply,
      message: decodeURIComponent(reply.message)
    }));
    const message = decodeURIComponent(complaint.message);
    return { ...complaint, message, replies, id };
  });
}

// import axios from 'axios';
// import endpoints from '../../../../config/api';
// import mapError from '../../../utils/errorMapping';
import {
  READ_COMPLAINT_REQUEST,
  READ_COMPLAINT_SUCCESS
  // READ_COMPLAINT_FAILURE,
} from '../reducer';

function readComplaintRequest() {
  return {
    type: READ_COMPLAINT_REQUEST
  };
}

function readComplaintSuccess(complaintId) {
  return {
    type: READ_COMPLAINT_SUCCESS,
    complaintId
  };
}

// function readComplaintFailure(error) {
//   return {
//     type: READ_COMPLAINT_FAILURE,
//     error
//   };
// }

export default function readComplaint(complaintId) {
  return dispatch => {
    dispatch(readComplaintRequest());
    dispatch(readComplaintSuccess(complaintId));

    // const requestBody = { complaintId };

    // axios
    //   .post(endpoints().complaintsRead, requestBody)
    //   .then(({ data }) => {
    //     const { ok, code } = data;

    //     if (ok) {
    //       dispatch(readComplaintSuccess(complaintId));
    //     } else {
    //       dispatch(readComplaintFailure(mapError(code)));
    //     }
    //   })
    //   .catch(err => dispatch(readComplaintFailure(err)));
  };
}

import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import sendLeadAction from './actions/sendLead';
import sendTicketAction from './actions/sendTicket';
import supportReducer, {
  SEND_TICKET_REQUEST,
  SEND_TICKET_SUCCESS,
  SEND_TICKET_FAILURE,
  SUBMIT_LEAD_FAILURE,
  SUBMIT_LEAD_REQUEST,
  SUBMIT_LEAD_SUCCESS
} from './reducer';

import { support as SupportConfiguration } from '../../../config/tests';

const { initialState, variables } = SupportConfiguration;

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
const store = mockStore({ app: initialState });

describe('Support Tests', () => {
  jest.setTimeout(30000);

  describe('Reducer', () => {
    it('should return the initial state', () => {
      expect(supportReducer(undefined, {})).toEqual(initialState);
    });
  });

  describe('Actions', () => {
    beforeEach(() => {
      store.clearActions();
    });

    it('should fail to send new lead because of empty name', async () => {
      const expectedActions = [{ type: SUBMIT_LEAD_FAILURE }];
      const { email, phone, gym } = variables;
      await store.dispatch(sendLeadAction({ email, phone, gym }));
      store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
    });
    it('should fail to send new lead because of empty email', async () => {
      const expectedActions = [{ type: SUBMIT_LEAD_FAILURE }];
      const { name, phone, gym } = variables;
      await store.dispatch(sendLeadAction({ name, phone, gym }));
      store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
    });
    it('should fail to send new lead because of empty phone', async () => {
      const expectedActions = [{ type: SUBMIT_LEAD_FAILURE }];
      const { name, email, gym } = variables;
      await store.dispatch(sendLeadAction({ name, email, gym }));
      store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
    });
    it('should fail to send new lead because of empty gym', async () => {
      const expectedActions = [{ type: SUBMIT_LEAD_FAILURE }];
      const { name, phone, email } = variables;
      await store.dispatch(sendLeadAction({ name, phone, email }));
      store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
    });
    it('should send new lead', async () => {
      const expectedActions = [{ type: SUBMIT_LEAD_REQUEST }, { type: SUBMIT_LEAD_SUCCESS }];
      const { name, phone, email, gymId } = variables;
      await store.dispatch(sendLeadAction({ name, phone, email, gymId }));
      store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
    });

    it('should fail to send new ticket support because of empty name', async () => {
      const expectedActions = [{ type: SEND_TICKET_FAILURE }];
      const { email, phone, gym, motive, message } = variables;
      await store.dispatch(sendTicketAction({ email, phone, gym, motive, message }));
      store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
    });
    it('should fail to send new ticket support because of empty email', async () => {
      const expectedActions = [{ type: SEND_TICKET_FAILURE }];
      const { name, phone, gym, motive, message } = variables;
      await store.dispatch(sendTicketAction({ name, phone, gym, motive, message }));
      store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
    });
    it('should fail to send new ticket support because of empty phone', async () => {
      const expectedActions = [{ type: SEND_TICKET_FAILURE }];
      const { email, name, gym, motive, message } = variables;
      await store.dispatch(sendTicketAction({ email, name, gym, motive, message }));
      store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
    });
    it('should fail to send new ticket support because of empty message', async () => {
      const expectedActions = [{ type: SEND_TICKET_FAILURE }];
      const { email, phone, gym, motive, name } = variables;
      await store.dispatch(sendTicketAction({ email, phone, gym, motive, name }));
      store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
    });
    it('should fail to send new ticket support because of empty gym', async () => {
      const expectedActions = [{ type: SEND_TICKET_FAILURE }];
      const { email, phone, name, motive, message } = variables;
      await store.dispatch(sendTicketAction({ email, phone, name, motive, message }));
      store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
    });

    it('should send new support ticket', async () => {
      const expectedActions = [{ type: SEND_TICKET_REQUEST }, { type: SEND_TICKET_SUCCESS }];
      const { email, phone, gym, name, message, motive } = variables;
      await store.dispatch(sendTicketAction({ email, phone, gym, name, message, motive }));
      store.getActions().map((action, index) => expect(action.type).toEqual(expectedActions[index].type));
    });
  });
});

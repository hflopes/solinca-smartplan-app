import { LOGOUT } from '../auth/reducers/login';

export const SEND_TICKET_REQUEST = 'smartplan/support/SEND_TICKET_REQUEST';
export const SEND_TICKET_SUCCESS = 'smartplan/support/SEND_TICKET_SUCCESS';
export const SEND_TICKET_SUCCESS_WITH_ALERT = 'smartplan/support/SEND_TICKET_SUCCESS_WITH_ALERT';
export const SEND_TICKET_FAILURE = 'smartplan/support/SEND_TICKET_FAILURE';

export const SUBMIT_LEAD_REQUEST = 'smartplan/auth/SUBMIT_LEAD_REQUEST';
export const SUBMIT_LEAD_SUCCESS = 'smartplan/auth/SUBMIT_LEAD_SUCCESS';
export const SUBMIT_LEAD_FAILURE = 'smartplan/auth/SUBMIT_LEAD_FAILURE';

export const RENEW_PLAN_REQUEST = 'smartplan/auth/RENEW_PLAN_REQUEST';
export const RENEW_PLAN_SUCCESS = 'smartplan/auth/RENEW_PLAN_SUCCESS';
export const RENEW_PLAN_FAILURE = 'smartplan/auth/RENEW_PLAN_FAILURE';

export const SAVE_FORM_STATUS = 'smartplan/auth/SAVE_FORM_STATUS';
export const RESET_SENT_VALUE = 'smartplan/auth/RESET_SENT_VALUE';

const initialState = {
  isSendingTicket: false,
  isSendingLead: false,
  hasSentLead: false,
  hasSentTicket: false,
  form: {},
  error: null
};

export default function supportReducer(state = initialState, action) {
  switch (action.type) {
    case LOGOUT:
      return { ...initialState };

    case SEND_TICKET_REQUEST:
      return { ...state, isSendingTicket: true };
    case SEND_TICKET_SUCCESS:
    case SEND_TICKET_SUCCESS_WITH_ALERT:
      return {
        ...state,
        isSendingTicket: false,
        hasSentTicket: true,
        form: action.formInfo
      };
    case SEND_TICKET_FAILURE:
      return { ...state, isSendingTicket: false, error: action.error };

    case SUBMIT_LEAD_REQUEST:
      return { ...state, isSendingLead: true };
    case SUBMIT_LEAD_SUCCESS:
      return {
        ...state,
        isSendingLead: false,
        hasSentLead: true,
        form: {}
      };
    case SUBMIT_LEAD_FAILURE:
      return { ...state, isSendingLead: false, error: action.error };

    case SAVE_FORM_STATUS:
      return { ...state, form: action.form };

    case RESET_SENT_VALUE:
      return { ...state, hasSentLead: false, hasSentTicket: false };

    default:
      return state;
  }
}

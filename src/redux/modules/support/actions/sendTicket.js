import axios from 'axios';
import {
  SEND_TICKET_REQUEST,
  SEND_TICKET_FAILURE,
  SEND_TICKET_SUCCESS,
  SEND_TICKET_SUCCESS_WITH_ALERT
} from '../reducer';
import endpoints from '../../../../config/api';
import mapError from '../../../utils/errorMapping';

function sendTicketRequest() {
  return {
    type: SEND_TICKET_REQUEST
  };
}

function sendTicketSuccess() {
  return { type: SEND_TICKET_SUCCESS };
}

function sendTicketSuccessWithAlert(formInfo) {
  return {
    type: SEND_TICKET_SUCCESS_WITH_ALERT,
    formInfo
  };
}

function sendTicketFailure(error) {
  return {
    type: SEND_TICKET_FAILURE,
    error
  };
}

export default function sendTicket({
  name = '',
  email = '',
  phone = '',
  message = '',
  gym = null,
  motive = null,
  gymId = null,
  withAlert = false
} = {}) {
  return async (dispatch, getState) => {
    if (name === '' || email === '' || phone === '' || message === '' || (!gym && !gymId)) {
      return dispatch(sendTicketFailure(mapError(6005)));
    }

    dispatch(sendTicketRequest());

    const { user } = getState();
    let membercode = '';
    if (user && user.data && user.data.memberNumber) {
      membercode = user.data.memberNumber;
    }

    const requestBody = {
      membercode,
      name,
      email,
      phone,
      message,
      motive,
      gym: gymId || gym.id
    };

    let endpoint = endpoints().sendTicket;
    switch (motive) {
      case 'Nutrição':
        endpoint = endpoints().sendNutrition;
        break;
      case 'Reclamações':
        endpoint = endpoints().complaints;
        break;
      default:
        endpoint = endpoints().sendTicket;
    }

    return axios
      .post(endpoint, requestBody)
      .then(({ data }) => {
        const { ok, code } = data;

        if (ok) {
          withAlert ? dispatch(sendTicketSuccessWithAlert({ email, phone })) : dispatch(sendTicketSuccess());
        } else {
          dispatch(sendTicketFailure(mapError(code)));
        }
      })
      .catch(err => dispatch(sendTicketFailure(err)));
  };
}

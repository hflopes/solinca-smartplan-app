import { RESET_SENT_VALUE } from '../reducer';

export default function resetSent() {
  return {
    type: RESET_SENT_VALUE
  };
}

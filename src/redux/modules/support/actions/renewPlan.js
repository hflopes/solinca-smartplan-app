import axios from 'axios';
import { RENEW_PLAN_REQUEST, RENEW_PLAN_FAILURE, RENEW_PLAN_SUCCESS } from '../reducer';
import endpoints from '../../../../config/api';
import mapError from '../../../utils/errorMapping';

function renewPlanRequest() {
  return { type: RENEW_PLAN_REQUEST };
}

function renewPlanSuccess(gymId) {
  return { type: RENEW_PLAN_SUCCESS, gym: gymId };
}

function renewPlanFailure(error) {
  return { type: RENEW_PLAN_FAILURE, error };
}

export default function renewPlan(payload) {
  return async (dispatch, getState) => {
    dispatch(renewPlanRequest());

    const { user, gyms } = getState();
    let membercode = '';
    if (user && user.data && user.data.memberNumber) {
      membercode = user.data.memberNumber;
    }
    let gymId = '';
    if (user && user.data && user.data.defaultGym) {
      gymId = user.data.defaultGym;
    }

    const requestBody = {
      membercode,
      gymId,
      ...payload
    };

    return axios
      .post(endpoints().renewPlan, requestBody)
      .then(({ data }) => {
        const { ok, code } = data;

        if (ok) {
          const gymsData = gyms.data;
          const gymName = gymsData[gymId].name;
          dispatch(renewPlanSuccess(gymName));
        } else {
          dispatch(renewPlanFailure(mapError(code)));
        }
      })
      .catch(err => dispatch(renewPlanFailure(err)));
  };
}

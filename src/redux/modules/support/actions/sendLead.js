import axios from 'axios';
import { SUBMIT_LEAD_REQUEST, SUBMIT_LEAD_SUCCESS, SUBMIT_LEAD_FAILURE } from '../reducer';
import endpoints from '../../../../config/api';
import mapError from '../../../utils/errorMapping';

function sendLeadRequest() {
  return {
    type: SUBMIT_LEAD_REQUEST
  };
}

function sendLeadSuccess() {
  return {
    type: SUBMIT_LEAD_SUCCESS
  };
}

function sendLeadFailure(error) {
  return {
    type: SUBMIT_LEAD_FAILURE,
    error
  };
}

export default function lead({ name = '', email = '', phone = '', gymId = -1 } = {}) {
  return dispatch => {
    if (name === '' || email === '' || phone === '' || !gymId || gymId === -1) {
      return dispatch(sendLeadFailure(mapError(6004)));
    }

    dispatch(sendLeadRequest());

    const requestBody = {
      name,
      email,
      phone,
      gym: gymId
    };

    return axios
      .post(endpoints().sendLead, requestBody)
      .then(({ data }) => {
        const { ok, code } = data;

        if (ok) {
          dispatch(sendLeadSuccess());
        } else {
          dispatch(sendLeadFailure(mapError(code)));
        }
      })
      .catch(err => dispatch(sendLeadFailure(err)));
  };
}

import { SAVE_FORM_STATUS } from '../reducer';

export default function saveForm(form) {
  return {
    type: SAVE_FORM_STATUS,
    form
  };
}
